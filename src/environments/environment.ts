// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  ClientIdentitySettings: {
    authority: 'https://spaspacedev.azurewebsites.net/sts',
    client_id: 'spaapp',
    redirect_uri: 'https://localhost:44359/spa/authentication/login-callback',
    response_type: 'code',
    scope: 'openid SpaSpaceApi profile email sparecordsscope',
    post_logout_redirect_uri: 'https://localhost:44359/spa/authentication/logout-callback',
    post_login_route: '/postLogin',
    accessTokenExpiringNotificationTime: 10,
    automaticSilentRenew: true,
    silent_redirect_uri: 'https://localhost:44359/spa/assets/silent-refresh.html'
  },
  ClientAppSettings: {
    mobileUrl: 'https://localhost:4200/mobile',
    authority: 'https://spaspacedev.azurewebsites.net/sts/api',
    apiServer: 'https://localhost:44385/api',
    apiServerUrl: 'https://localhost:44385',
    googlemapsapikey: 'AIzaSyAarPSOwrlU4oV84gohgE1WBWKvuYlYuIU',
    stripeClientId: 'ca_H2kNIqFIOlD5Qd8C6kc66h9JKzixRLfb',
    stripePublishableKey: 'pk_test_buNspgd7O4xZkT6rr3JR0WSU'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
