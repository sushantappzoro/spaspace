export const environment = {
  production: true,
  environmentName: 'Prod',
  ClientIdentitySettings: {
    authority: 'https://app.spaspace.com/sts',
    client_id: 'spaapp',
    redirect_uri: 'https://app.spaspace.com/spa/authentication/login-callback',
    response_type: 'code',
    scope: 'openid SpaSpaceApi profile email sparecordsscope',
    post_logout_redirect_uri: 'https://app.spaspace.com/spa/authentication/logout-callback',
    post_login_route: '/postLogin',
    accessTokenExpiringNotificationTime: 10,
    automaticSilentRenew: true,
    silent_redirect_uri: 'https://app.spaspace.com/spa/assets/silent-refresh.html'
  },
  ClientAppSettings: {
    authority: 'https://app.spaspace.com/sts/api',
    apiServer: 'https://app.spaspace.com/api/api',
    apiServerUrl: 'https://app.spaspace.com/spa',
    mobileUrl: 'https://app.spaspace.com/mobile',
    googlemapsapikey: 'AIzaSyAarPSOwrlU4oV84gohgE1WBWKvuYlYuIU',
    stripeClientId: 'ca_H2kNMgPWRsu0ZkmIiYIzNG1JolgywZGd',
    stripePublishableKey: 'pk_live_8Y0Zfwg9ZCHxixvD1QxiI6rd'
  }
};
