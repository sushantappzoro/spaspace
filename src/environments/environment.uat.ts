export const environment = {
    production: true,
    environmentName: 'UAT',
    ClientIdentitySettings: {
      authority: 'https://spaspaceuat.azurewebsites.net/uat',
      client_id: 'spaapp',
      redirect_uri: 'https://spaspaceuat.azurewebsites.net/spa/authentication/login-callback',
      response_type: 'code',
      scope: 'openid SpaSpaceApi profile email sparecordsscope',
      post_logout_redirect_uri: 'https://spaspaceuat.azurewebsites.net/spa/authentication/logout-callback',
      post_login_route: '/postLogin',
      accessTokenExpiringNotificationTime: 10,
      automaticSilentRenew: true,
      silent_redirect_uri: 'https://spaspaceuat.azurewebsites.net/spa/assets/silent-refresh.html'
    },
    ClientAppSettings: {
      authority: 'https://spaspaceuat.azurewebsites.net/sts/api',
      apiServer: 'https://spaspaceuat.azurewebsites.net/api/api',
      apiServerUrl: 'https://spaspaceuat.azurewebsites.net/api',
      mobileUrl: 'https://spaspaceuat.azurewebsites.net/mobile',
      googlemapsapikey: 'AIzaSyAarPSOwrlU4oV84gohgE1WBWKvuYlYuIU',
      stripeClientId: 'ca_H2kNIqFIOlD5Qd8C6kc66h9JKzixRLfb',
      stripePublishableKey: 'pk_test_buNspgd7O4xZkT6rr3JR0WSU'
    }
  };
