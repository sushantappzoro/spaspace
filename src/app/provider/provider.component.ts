import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { DxDrawerComponent } from 'devextreme-angular';
import themes from 'devextreme/ui/themes';
import { Router, ActivatedRoute } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { ProviderNavService } from './core/services/provider-nav.service';
import { MenuListItemModel } from '../shared/models/common/menu-list-item.model';
import { TherapistProfileService } from '../core/services/therapist/therapist-profile.service';
import { TherapistProfileForEditModel } from '../shared/models/therapist/therapist-profile-for-edit.model';

@Component({
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})

export class ProviderComponent implements OnInit, OnDestroy {
  // @ViewChild(DxDrawerComponent) drawer: DxDrawerComponent;

  private subs = new SubSink();

  therapist: TherapistProfileForEditModel;

  homePage = '/therapist';

  navigation: MenuListItemModel[] = [
    { id: 1, text: 'Dashboard', icon: 'fas fa-home', link: './dashboard',
      status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    { id: 2, text: 'Availability', icon: 'fas fa-calendar', link: './availability',
      status: ['Approved'], disabled: false },
    { id: 3, text: 'Schedule', icon: 'fas fa-clock', link: './appointments',
      status: ['Approved'], disabled: false },
    // { id: 4, text: 'Spas', icon: 'fas fa-clock', link: './facilities',
    // status: ['Approved'], disabled: false },
    { id: 5, text: 'Reviews', icon: 'fas fa-star-half-alt', link: './reviews',
      status: ['Approved'], disabled: false },
    { id: 6, text: 'Transactions', icon: 'fas fa-money-check', link: './account',
      status: ['Approved'], disabled: false },
    {
      id: 7, text: 'Resources',
      icon: 'fas fa-photo-video', 
      status: ['Approved'], disabled: false,
      hasSubItems: true,
      subItems: [
        { id: 7.1, text: "Health and Safety", disabled: false, link: "./resources/health" },
        { id: 7.2, text: "Service Standards", disabled: false, link: "./resources/standards" },
        { id: 7.3, text: "Process", disabled: false, link: "./resources/process" },
        { id: 7.4, text: "FAQ", disabled: false, link: "./resources/faq" },
        { id: 7.5, text: "Resources", disabled: false, link: "./resources/other" }
      ]
    },
    { id: 8, text: 'Profile', icon: ' fas fa-user', link: './profile',
      status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    // { id: 5, text: 'Account', icon: 'fas fa-dollar-sign', link: './account',
    // status: ['Approved'], disabled: false},
    // { id: 6, text: 'Standards', icon: 'fas fa-certificate', link: './standards',
    // status: ['Approved'], disabled: false },
    // { id: 2, text: 'My Profile', icon: ' fas fa-user', link: './profile',
    // status: ['New', 'Pending Approval', 'Approved'], disabled: false },
  ];

  // showSubmenuModes: string[] = ['slide', 'expand'];
  // positionModes: string[] = ['left', 'right'];
  // showModes: string[] = ['push', 'shrink', 'overlap'];
  // text: string;
  // selectedOpenMode: string = 'shrink';
  // selectedPosition: string = 'left';
  // selectedRevealMode: string = 'slide';
  // elementAttr: any;

  // toolbarContent = [{
  //  widget: 'dxButton',
  //  location: 'before',
  //  options: {
  //    icon: 'menu',
  //    onClick: () => this.drawer.instance.toggle()
  //  }
  // }];

  // selectedItems: any[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private providerService: TherapistProfileService,
    private navbarService: ProviderNavService
  ) {
    // console.log('change theme');
    themes.current('material.admin.light');
  }

  ngOnInit() {
    this.subs.add(this.route.data
      .subscribe((data: { therapist: TherapistProfileForEditModel }) => {
        if (data.therapist) {
          this.therapist = data.therapist;
          this.navigation.forEach(st => {
            st.disabled = !st.status.includes(data.therapist.Status);
          });
        }
      },
        error => {
          console.log(error);
        }));

    // this.subs.add(this.providerService.getTherapistStatus()
    //  .subscribe(status => {
    //    this.navigation.forEach(st => {
    //      st.disabled = !st.status.includes(status);
    //    })
    //  }));

    // this.subs.add(this.navbarService.toggle()
    //  .subscribe(a => {
    //    this.drawer.instance.toggle();
    //    }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }


  onItemRendered(e) {
    // console.log('onItemRendered', e);
  }

  // drawerToggle() {
  //  this.drawer.instance.toggle();
  // }

  setupMenu(status: string) {

  }

  menu_selectionChanged(e) {
    const link = e.addedItems[0].link;

    if (link) {

      this.router.navigate([link], { relativeTo: this.route });
    }
    // switch (selection) {
    //  case 'My Profile':
    //    this.router.navigate(['./profile'], { relativeTo: this.route });
    //    break;
    //  case 'Schedule':
    //    this.router.navigate(['./schedule'], { relativeTo: this.route });
    //    break;
    //  case 'Account':
    //    this.router.navigate(['./account'], { relativeTo: this.route });
    //    break;
    //  case 'Availability':
    //    this.router.navigate(['./availability'], { relativeTo: this.route });
    //    break;
    //  case 'Standards':
    //    this.router.navigate(['./standards'], { relativeTo: this.route });
    //    break;
    //  default:
    // }
  }
}
