import { Component, OnInit, ViewChild } from '@angular/core';
import { TherapistProfileForEditModel, TherapistProfileStatus } from '../../shared/models/therapist/therapist-profile-for-edit.model';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import { TherapistDashboardService } from '../../core/services/therapist/therapist-dashboard.service';
import { MessageForListModel } from '../../shared/models/common/message-for-list.model';
import { TherapistAppointmentForDashboardModel } from '../../shared/models/therapist/therapist-appointment-for-dashboard.model';
import { TherapistTransactionsForDashboardModel } from '../../shared/models/therapist/therapist-transactions-for-dashboard.model';
import { ModalDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-provider-dashboard',
  templateUrl: './provider-dashboard.component.html',
  styleUrls: ['./provider-dashboard.component.scss']
})
export class ProviderDashboardComponent implements OnInit {
  @ViewChild('appointmentModal') appointmentModal: ModalDirective;

  private subs = new SubSink();

  therapist: TherapistProfileForEditModel;

  transactions: TherapistTransactionsForDashboardModel;

  messages: MessageForListModel[];

  appointmentUID: string;
  upcomingAppointments: TherapistAppointmentForDashboardModel[];
  pastAppointments: TherapistAppointmentForDashboardModel[];
  appointmentPopupVisible: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dashboardService: TherapistDashboardService
  ) {

    //this.appointmentSelected = this.appointmentSelected.bind(this);
  }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { therapist: TherapistProfileForEditModel }) => {
        if (data.therapist.Status === TherapistProfileStatus.new) {
          
          this.router.navigate(['/therapist/stepper']);
        }
        this.therapist = data.therapist;
      },
        error => {
          console.log(error);
        }));    

    this.loadMessages();
    this.loadUpcomingAppointments();
    this.loadPastAppointments();
    this.loadTransactions();

  }

  loadMessages() {
    this.subs.add(this.dashboardService.getMessages(this.therapist.UID)
      .subscribe(resp => {
        this.messages = resp;
      },
        error => {
          //notify('Unable to load treatment areas.', "error", 3000);
        }));
  }

  loadUpcomingAppointments() {
    this.subs.add(this.dashboardService.getUpcomingAppointments(this.therapist.UID)
      .subscribe(resp => {
        this.upcomingAppointments = resp;
      },
        error => {
          //notify('Unable to load upcoming appointments.', "error", 3000);
        }));
  }

  loadPastAppointments() {
    this.subs.add(this.dashboardService.getPastAppointments(this.therapist.UID)
      .subscribe(resp => {
        this.pastAppointments = resp;
      },
        error => {
          //notify('Unable to load past appointments.', "error", 3000);
        }));
  }

  messageSelected(uid: string) {

  }

  appointmentSelected(uid: string) {

    this.router.navigate(['/therapist/appointments', uid]);

  //  //get appointment detail
  //  this.appointmentUID = uid;
  //  //popup view
  //  this.appointmentPopupVisible = true;
  //  this.appointmentModal.show();
  }

  width() {
    return window.innerWidth / 1.5;
  }

  loadTransactions() {
    this.subs.add(this.dashboardService.getTransactions(this.therapist.UID)
      .subscribe(resp => {
        //console.log('trans', resp);
        this.transactions = resp;
      },
        error => {
          //notify('Unable to load transactions.', "error", 3000);
        }));
  }

  transactionSelected(uid: string) {

  }

}
