import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProviderAppointmentSummaryComponent } from './components/appointment/provider-appointment-summary.component';
import { ProviderAppointmentDetailComponent } from './components/appointment/provider-appointment-detail.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DevexModuleModule } from './devex-module.module';
import { Ng5SliderModule } from 'ng5-slider';
import { MaterialModuleModule } from './material-module.module';
import { ProviderFooterComponent } from './layout/provider-footer/provider-footer.component';
import { ProviderAppointmentCompactComponent } from './components/appointment/provider-appointment-compact/provider-appointment-compact.component';
import { ProviderComponentsModule } from './components/provider-components.module';


@NgModule({
  declarations: [
    ProviderAppointmentSummaryComponent,
    ProviderAppointmentDetailComponent,
    ProviderFooterComponent,
    ProviderAppointmentCompactComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    DevexModuleModule,
    MaterialModuleModule,
    Ng5SliderModule,
    ProviderComponentsModule
  ],
  exports: [    
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    DevexModuleModule,
    MaterialModuleModule,
    Ng5SliderModule,
    ProviderComponentsModule,
    ProviderAppointmentSummaryComponent,
    ProviderAppointmentDetailComponent,
    ProviderFooterComponent,
    ProviderAppointmentCompactComponent
  ]
})
export class ProviderSharedModule { }
