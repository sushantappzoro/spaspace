import { NgModule } from '@angular/core';
import {
  DxButtonModule,
  DxDataGridModule,
  DxDraggableModule,
  DxDrawerModule,
  DxFileUploaderModule,
  DxListModule,
  DxNumberBoxModule,
  DxPopupModule,
  DxRadioGroupModule,
  DxSchedulerModule,
  DxScrollViewModule,
  DxTabPanelModule,
  DxTabsModule,
  DxTextAreaModule,
  DxTextBoxModule,
  DxToolbarModule,
  DxValidatorModule,
} from 'devextreme-angular';



@NgModule({
  declarations: [],
  exports: [
    DxButtonModule,
    DxDataGridModule,
    DxDraggableModule,
    DxDrawerModule,
    DxFileUploaderModule,
    DxListModule,
    DxNumberBoxModule,
    DxPopupModule,
    DxRadioGroupModule,
    DxSchedulerModule,
    DxScrollViewModule,
    DxTabPanelModule,
    DxTabsModule,
    DxTextAreaModule,
    DxTextBoxModule,
    DxToolbarModule,
    DxValidatorModule,
  ]
})
export class DevexModuleModule { }
