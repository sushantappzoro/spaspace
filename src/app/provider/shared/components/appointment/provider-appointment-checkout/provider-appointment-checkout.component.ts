import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import notify from 'devextreme/ui/notify';
import { SubSink } from 'subsink';
import { SpaAppointmentCostSummaryForCheckoutModel } from '../../../../../../../projects/spaspace-lib/src/shared/models';
import { TherapistAppointmentsService } from '../../../../../core/services/therapist/therapist-appointments.service';
import { AppointmentForTherapistDetailModel } from '../../../../../shared/models/therapist/appointment-for-therapist-detail.model';
import { IUser, AuthorizeService } from '../../../../../../api-authorization/authorize.service';
import { CompleteCheckoutRequestModel } from '../../../../../shared/models/therapist/complete-checkout-request.model';

@Component({
  selector: 'spa-provider-appointment-checkout',
  templateUrl: './provider-appointment-checkout.component.html',
  styleUrls: ['./provider-appointment-checkout.component.scss']
})
export class ProviderAppointmentCheckoutComponent implements OnInit, OnDestroy, OnChanges {
  @Input() appointment: AppointmentForTherapistDetailModel;

  @Output() checkoutCompleted: EventEmitter<boolean> = new EventEmitter<boolean>();

  private subs = new SubSink();

  checkoutForm: FormGroup;

  calculatedTotal: number;

  tipAmount1: boolean;
  tipAmount2: boolean;
  tipAmount3: boolean;

  user: IUser;

  suspendEvents: boolean;

  constructor(
    private appointmentService: TherapistAppointmentsService,
    private authService: AuthorizeService
  ) { }

  ngOnInit() {

    this.checkoutForm = new FormGroup({
      Gratuity: new FormControl(),
    });

    this.checkoutForm.get('Gratuity').valueChanges
      .subscribe(change => {
        if (!this.suspendEvents) {
          this.calculatedTotal = this.appointment.CostSummary.Total + change;
        }
      });

    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.user = user;
      })
    )
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.appointment) {
      this.suspendEvents = true;
      if (this.checkoutForm) {
        this.checkoutForm.patchValue({ Gratuity: changes.appointment.currentValue.Gratuity });
      }
      this.calculatedTotal = changes.appointment.currentValue.CostSummary.Total;
      this.suspendEvents = false;
    }

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  gratuityChanged(e) {
    console.log('gratuityChanged', e);
  }

  onCompleteCheckout() {

    let gratuity: number = this.checkoutForm.get("Gratuity").value

    let request: CompleteCheckoutRequestModel = { Total: this.calculatedTotal, Gratuity: gratuity };

    console.log('request', request)

    this.subs.add(this.appointmentService.completeCheckOut(this.user.sub, this.appointment.UID, request)
      .subscribe(resp => {
        if (resp.Successful) {
          this.checkoutCompleted.emit(true);
        }
      })
    )
  }

}
