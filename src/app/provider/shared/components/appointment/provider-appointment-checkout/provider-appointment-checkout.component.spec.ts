import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAppointmentCheckoutComponent } from './provider-appointment-checkout.component';

describe('ProviderAppointmentCheckoutComponent', () => {
  let component: ProviderAppointmentCheckoutComponent;
  let fixture: ComponentFixture<ProviderAppointmentCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAppointmentCheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAppointmentCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
