import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAppointmentNotesComponent } from './provider-appointment-notes.component';

describe('ProviderAppointmentNotesComponent', () => {
  let component: ProviderAppointmentNotesComponent;
  let fixture: ComponentFixture<ProviderAppointmentNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAppointmentNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAppointmentNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
