import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAppointmentDetailComponent } from './provider-appointment-detail.component';

describe('ProviderAppointmentDetailComponent', () => {
  let component: ProviderAppointmentDetailComponent;
  let fixture: ComponentFixture<ProviderAppointmentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAppointmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAppointmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
