import { Component, OnInit, Input } from '@angular/core';
import { TherapistAppointmentSummaryModel } from '../../../../shared/models/therapist/therapist-appointment-summary.model';

@Component({
  selector: 'spa-provider-appointment-summary',
  templateUrl: './provider-appointment-summary.component.html',
  styleUrls: ['./provider-appointment-summary.component.scss']
})
export class ProviderAppointmentSummaryComponent implements OnInit {
  @Input() appointment: TherapistAppointmentSummaryModel

  constructor() { }

  ngOnInit() {
  }

}
