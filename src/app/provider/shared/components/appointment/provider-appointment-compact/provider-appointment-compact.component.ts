import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { TherapistAppointmentsService } from '../../../../../core/services/therapist/therapist-appointments.service';
import { TherapistAppointmentSummaryForViewModel } from '../../../../../shared/models/therapist/therapist-appointment-summary-for-view.model';

@Component({
  selector: 'spa-provider-appointment-compact',
  templateUrl: './provider-appointment-compact.component.html',
  styleUrls: ['./provider-appointment-compact.component.scss']
})
export class ProviderAppointmentCompactComponent implements OnInit {
  @Input() therapistUID: string;

  @Output() appointmentSelected: EventEmitter<string> = new EventEmitter<string>();

  private subs = new SubSink();

  appointments: TherapistAppointmentSummaryForViewModel[];

  constructor(private therapistAppointmentService: TherapistAppointmentsService) { }

  ngOnInit() {

    this.loadAppointments();

  }

  loadAppointments() {
    console.log('ProviderAppointmentCompactComponent-loadAppointments');
    this.subs.add(this.therapistAppointmentService.getUpcomingAppointments(this.therapistUID)
      .subscribe(resp => {
        this.appointments = resp;
      },
        error => {
          notify('Unable to load treatment areas.', "error", 3000);
        }));
  }

  itemClicked(e) {
    this.appointmentSelected.emit(e);
  }

}
