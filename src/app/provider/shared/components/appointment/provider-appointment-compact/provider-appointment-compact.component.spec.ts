import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAppointmentCompactComponent } from './provider-appointment-compact.component';

describe('ProviderAppointmentCompactComponent', () => {
  let component: ProviderAppointmentCompactComponent;
  let fixture: ComponentFixture<ProviderAppointmentCompactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAppointmentCompactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAppointmentCompactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
