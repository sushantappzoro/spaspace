import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAppointmentServiceAddonsComponent } from './provider-appointment-service-addons.component';

describe('ProviderAppointmentServiceAddonsComponent', () => {
  let component: ProviderAppointmentServiceAddonsComponent;
  let fixture: ComponentFixture<ProviderAppointmentServiceAddonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAppointmentServiceAddonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAppointmentServiceAddonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
