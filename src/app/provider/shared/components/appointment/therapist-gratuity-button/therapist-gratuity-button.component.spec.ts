import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistGratuityButtonComponent } from './therapist-gratuity-button.component';

describe('TherapistGratuityButtonComponent', () => {
  let component: TherapistGratuityButtonComponent;
  let fixture: ComponentFixture<TherapistGratuityButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistGratuityButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistGratuityButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
