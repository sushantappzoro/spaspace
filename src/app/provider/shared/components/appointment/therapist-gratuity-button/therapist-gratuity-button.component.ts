import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'spa-therapist-gratuity-button',
  templateUrl: './therapist-gratuity-button.component.html',
  styleUrls: ['./therapist-gratuity-button.component.scss']
})
export class TherapistGratuityButtonComponent implements OnInit {
  @Input() selected: boolean;
  @Output() selectedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  isActive: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onClick1() {
    this.selected = !this.selected;
    this.selectedChange.emit(this.selected);
  }


}
