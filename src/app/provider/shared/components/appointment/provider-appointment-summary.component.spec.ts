import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAppointmentSummaryComponent } from './provider-appointment-summary.component';

describe('ProviderAppointmentSummaryComponent', () => {
  let component: ProviderAppointmentSummaryComponent;
  let fixture: ComponentFixture<ProviderAppointmentSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAppointmentSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAppointmentSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
