import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'spa-appointment-action',
  templateUrl: './appointment-action.component.html',
  styleUrls: ['./appointment-action.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppointmentActionComponent implements OnInit, OnChanges {
  @Input() step: string;

  @Output() checkin: EventEmitter<any> = new EventEmitter<any>();
  @Output() noshow: EventEmitter<any> = new EventEmitter<any>();
  @Output() checkout: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() finalize: EventEmitter<any> = new EventEmitter<any>();

  title: string;
  description: string;
  showCheckin: boolean;
  showFinalize: boolean;
  showCheckout: boolean;
  showCancel: boolean;
  showReview: boolean;

  constructor() { }

  ngOnInit() {
    if (this.step) {
      this.showAction(this.step);
    }    
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.step) {      
      this.showAction(this.step);
    }
  }

  showAction(step) {

    this.showCheckin = false;
    this.showCheckout = false;
    this.showFinalize = false;
    this.showReview = false;
    //console.log('step', step);
    switch (step) {
      case 'Upcoming':
        this.title = "It's time to start the massage.";
        this.description = "Check-in to your massage or notify us if the client didn't show";
        this.showCheckin = true;
        break;
      case 'In Process':
        this.title = "Your appointment is active.";
        this.description = "Check out when the session is complete.";
        this.showCheckout = true;
        break;
      case 'Checking Out':
        this.title = "Time to check out.";
        this.description = "Complete the transaction below.";
        this.showFinalize = true;
        break;
      case 'Cancelled':
        this.title = "This session was cancelled.";
        this.description = "This session was cancelled by your or your client.";
        this.showFinalize = true;
        break;
      case 'Completed':
        //this.title = "Your session is complete. Rate Client";
        //this.description = "Please rate the client.";
        this.title = "Your session is complete.";
        this.description = "";
        this.showReview = true;
        break;
      case 'No Show':
        //this.title = "Your session is complete. Rate Client";
        //this.description = "Please rate the client.";
        this.title = "Your client was a no-show. Appointment cancelled.";
        this.description = "";
        this.showReview = true;
        break;        
      case 'cancel':
        this.title = "Cancel the session.";
        this.description = "Cancel the Appointment.";
        this.showFinalize = true;
        break;
      case 'finalize':
        this.title = "Finalize the session.";
        this.description = "Finalize the appointment.";
        this.showFinalize = true;
        break;
      default:
        this.title = "It's time to start the massage.";
        this.description = "Check-in to your massage or notify us if the client didn't show";
        this.showCheckin = true;

    }
  }

  onCheckin() {
    this.checkin.emit(null);
  }

  onMissed() {
    this.noshow.emit(null);
  }

  onCheckout() {
    this.checkout.emit(null);
  }

  onCancel() {
    this.cancel.emit(null);
  }

  //onFinailize() {
  //  this.finalize.emit(null);
  //}
}
