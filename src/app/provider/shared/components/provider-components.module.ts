import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentActionComponent } from './appointment/appointment-action/appointment-action.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProviderAppointmentServiceAddonsComponent } from './appointment/provider-appointment-service-addons/provider-appointment-service-addons.component';
import { MaterialModuleModule } from '../material-module.module';
import { ProviderAppointmentNotesComponent } from './appointment/provider-appointment-notes/provider-appointment-notes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProviderAppointmentCheckoutComponent } from './appointment/provider-appointment-checkout/provider-appointment-checkout.component';
import { SpaSpaceSharedModule } from '../../../../../projects/spaspace-lib/src/public-api';
import { TherapistGratuityButtonComponent } from './appointment/therapist-gratuity-button/therapist-gratuity-button.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  declarations: [
    AppointmentActionComponent,
    ProviderAppointmentServiceAddonsComponent,
    ProviderAppointmentNotesComponent,
    ProviderAppointmentCheckoutComponent,
    TherapistGratuityButtonComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModulesPro,
    FlexLayoutModule,
    MaterialModuleModule,
    SpaSpaceSharedModule,
    NgxCurrencyModule
  ],
  exports: [
    AppointmentActionComponent,
    ProviderAppointmentServiceAddonsComponent,
    ProviderAppointmentNotesComponent,
    ProviderAppointmentCheckoutComponent,
  ]
})
export class ProviderComponentsModule { }
