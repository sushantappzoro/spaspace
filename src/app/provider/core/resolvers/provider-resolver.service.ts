import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';

import { TherapistProfileForUIModel } from '../../../shared/models/therapist/therapist-profile-for-ui.model';
import { TherapistProfileService } from '../../../core/services/therapist/therapist-profile.service';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderResolverService implements Resolve<TherapistProfileForUIModel> {

  constructor(
    private authService: AuthorizeService,
    private providerService: TherapistProfileService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistProfileForUIModel> | Observable<never> {
    console.log('ProviderResolverService');
    return this.authService.getUser()
      .pipe(
        switchMap(user => {
          console.log('ProviderResolverService - got user', user);
          return this.providerService.getTherapistProfile(user.sub)
            .pipe(
              mergeMap(response => {
                console.log('ProviderResolverService - got therapist profile', response);
                if (response) {
                  return of(response)
                } else {
                  this.router.navigate(['/therapist/error']);
                  return EMPTY;
                }
              })
            )
        })
      )
  }

}
