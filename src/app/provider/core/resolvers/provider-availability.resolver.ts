import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';
import { TherapistAvailabilityForViewModel } from '../../../shared/models/therapist/therapist-availability-for-view.model';
import { TherapistAvailabilityService } from '../../../core/services/therapist/therapist-availability.service';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderAvailabilityResolver implements Resolve<TherapistAvailabilityForViewModel[]> {

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistAvailabilityService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistAvailabilityForViewModel[]> | Observable<never> {
    console.log('ProviderAvailabilityResolver');
    return this.authService.getUser()
      .pipe(
        switchMap(user => {
          return this.therapistService.getAvailability(user.sub)
            .pipe(
              mergeMap(response => {
                if (response) {
                  return of(response)
                } else {
                  this.router.navigate(['/therapist/error']);
                  return EMPTY;
                }
              })
            )
        })
      )
  }
}
