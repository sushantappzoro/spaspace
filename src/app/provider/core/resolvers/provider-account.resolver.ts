import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';
import { StripeInfoModel } from 'src/app/shared/models/therapist/stripe-info.model';
import { AuthorizeService } from 'src/api-authorization/authorize.service';
import { TherapistAccountService } from 'src/app/core/services/therapist/therapist-account.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderAccountResolver implements Resolve<StripeInfoModel> {

  constructor(
    private authService: AuthorizeService,
    private accountService: TherapistAccountService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<StripeInfoModel> | Observable<never> {
    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          return this.accountService.getStripeInfo(user.sub)
            .pipe(
              mergeMap(response => {
                if (response) {
                  return of(response);
                } else {
                  return EMPTY;
                }
              })
            );
        })
      );
  }
}
