import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

import { ProviderScheduleService } from '../services/provider-schedule.service';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';



@Injectable({
  providedIn: 'root'
})
export class ProviderScheduleResolver implements Resolve<TherapistScheduleAndAppointmentsModel> {

  constructor(
    private providerService: ProviderScheduleService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistScheduleAndAppointmentsModel> | Observable<never> {
    return this.providerService.getAppointments().pipe(
      take(1),
      mergeMap(response => {
        if (response) {
          return of(response);
        } else { // not found
          this.router.navigate(['/therapist/error']);
          return EMPTY;
        }
      })
    );
  }

}
