import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProviderNavService {
  private _toggle = new Subject();
  private toggle$ = this._toggle.asObservable();

  toggle() {
    return this.toggle$;
  }

  toggleNext() {
    this._toggle.next();
  }
}
