import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProviderStepperService {

  //private validationStepNumber: number;
  private validateSubject: BehaviorSubject<number | null> = new BehaviorSubject(null);

  //private completeStepNumber: number;
  private completeSubject: BehaviorSubject<number | null> = new BehaviorSubject(null);

  //private validationError: string;
  private validationSubject: BehaviorSubject<string | null> = new BehaviorSubject(null);

  constructor() { }

  public validateStep(stepNumber: number) {    
    //this.validationStepNumber = stepNumber;
    this.validateSubject.next(stepNumber);
  }

  public getValidationStep(): Observable<number | null> {
    return this.validateSubject.asObservable();
  }

  public completeStep(stepNumber: number) {
    //this.completeStepNumber = stepNumber;
    this.completeSubject.next(stepNumber);
  }

  public getCompleteStep(): Observable<number | null> {
    return this.completeSubject.asObservable();
  }

  public reportError(error: string) {
    //this.validationError = error;
    this.validationSubject.next(error);
  }

  public getError(): Observable<string | null> {
    return this.validationSubject.asObservable();
  }

}
