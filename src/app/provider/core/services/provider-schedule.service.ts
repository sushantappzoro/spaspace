import { Injectable } from '@angular/core';
import { ProviderScheduleApiService } from '../api/provider-schedule-api.service';
import { Observable } from 'rxjs';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';

@Injectable({
  providedIn: 'root'
})
export class ProviderScheduleService {

  constructor(private scheduleApi: ProviderScheduleApiService) { }

  public getAppointments(): Observable<TherapistScheduleAndAppointmentsModel> {
    return this.scheduleApi.getAppointments();
  }

}
