import { TherapistCertificationModel } from "../../../shared/models/therapist/therapist-certification.model";

export interface ProviderCertificationsState {
  providerCertifications: TherapistCertificationModel[];
}
