import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';


@Injectable({
  providedIn: 'root'
})
export class ProviderScheduleApiService {

  constructor(private http: HttpClient) { }

  public getAppointments(): Observable<TherapistScheduleAndAppointmentsModel> {
    return this.http.get<TherapistScheduleAndAppointmentsModel>(configSettings.WebAPI_URL + '/Therapist/Schedule');
  }

}
