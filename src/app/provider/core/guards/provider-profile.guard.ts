import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { take, mergeMap, switchMap, map } from 'rxjs/operators';
import { TherapistProfileService } from '../../../core/services/therapist/therapist-profile.service';
import { TherapistProfileStatus } from '../../../shared/models/therapist/therapist-profile-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class ProviderProfileGuard implements CanActivate, CanActivateChild {

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistProfileService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const allowedStatus: any[] = next.data.allowedStatus;
    const url = state.url;

    console.log('allowedStatus', allowedStatus);
    console.log('hold', url);

    //return true;

    let parentUrl = state.url.slice(0, state.url.indexOf(next.url[next.url.length - 1].path));

    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          return this.therapistService.getTherapistProfile(user.sub)
            .pipe(take(1),
              switchMap(resp => {
                if (allowedStatus.includes(resp.Status)) {
                  return of(true);
                }

                if (url.indexOf("profile") > 0 && resp.Status === TherapistProfileStatus.new) {                  
                  this.router.navigate([parentUrl, 'stepper']);
                  return of(null);
                }

                if (url.indexOf("stepper") > 0) {
                  this.router.navigate([parentUrl, 'profile']);
                  return of(null);
                }

                this.router.navigate([parentUrl, 'error']);
                return of(null);
              })
            )
        }));

  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  
}
