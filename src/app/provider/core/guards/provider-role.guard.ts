import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProviderRoleGuard implements CanActivate {

  constructor() {}  

  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const allowedStatus = _next.data.expectedRole;

    //this.providerService.getProviderStatus()
    //  .subscribe(status => {
    //    this.navigation.forEach(st => {
    //      st.disabled = !st.status.includes(status);
    //    })
    //  });

    return true;
  }
  
}
