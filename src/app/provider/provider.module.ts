import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderRoutingModule } from './provider-routing.module';
import { ProviderCoreModule } from './core/provider-core.module';
import { ProviderSharedModule } from './shared/provider-shared.module';
import { ProviderDashboardComponent } from './dashboard/provider-dashboard.component';
import { ProviderProfileComponent } from './profile/provider-profile.component';
//import { ProviderSchedulingComponent } from './scheduling/provider-scheduling.component';
import { ProviderComponent } from './provider.component';
import { SharedModule } from '../shared/shared.module';
import { ProviderAvailabilityComponent } from './availability/provider-availability.component';
import { ProviderStandardsComponent } from './standards/provider-standards.component';
import { ProviderAccountComponent } from './account/provider-account.component';
import { ProviderErrorComponent } from './error/provider-error.component';
import { ProviderUnauthorizedComponent } from './unauthorized/provider-unauthorized.component';
import { ProviderNavService } from './core/services/provider-nav.service';
import { ProviderAppointmentsComponent } from './appointments/provider-appointments.component';
import { ProviderProfileStepperComponent } from './profile/provider-profile-stepper.component';
import { ProviderResolverService } from './core/resolvers/provider-resolver.service';
import { ProviderRoleGuard } from './core/guards/provider-role.guard';
import { ProviderRequirementsComponent } from './requirements/provider-requirements.component';
import { ApiAuthorizationModule } from '../../api-authorization/api-authorization.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProviderThanksForRegisteringComponent } from './thanks/provider-thanks-for-registering.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CanDeactivateGuardService } from '../core/guards/can-deactivate-guard.service';
import { DialogService } from '../core/services/dialog.service';
import { ProviderAppointmentComponent } from './appointments/provider-appointment/provider-appointment.component';
import { ProviderResourcesComponent } from './resources/provider-resources.component';
import { ProviderFacilitiesComponent } from './facilities/provider-facilities.component';
import { ProviderReviewsComponent } from './reviews/provider-reviews.component';
import { SpaSpaceSharedModule } from '../../../projects/spaspace-lib/src/public-api';
import { ProviderFaqComponent } from './resources/faq/provider-faq.component';
import { ProviderOtherResourcesComponent } from './resources/other-resources/provider-other-resources.component';
import { ProviderHealthSafetyComponent } from './resources/health-safety/provider-health-safety.component';
import { ProviderProcessComponent } from './resources/process/provider-process.component';
import { ProviderServiceStandardsComponent } from './resources/service-standards/provider-service-standards.component';


@NgModule({
  declarations: [
    ProviderDashboardComponent,
    ProviderProfileComponent,
    //ProviderSchedulingComponent,
    ProviderComponent,
    ProviderAvailabilityComponent,
    ProviderStandardsComponent,
    ProviderAccountComponent,
    ProviderErrorComponent,
    ProviderUnauthorizedComponent,
    ProviderAppointmentsComponent,
    ProviderProfileStepperComponent,
    ProviderRequirementsComponent,
    ProviderThanksForRegisteringComponent,
    ProviderAppointmentComponent,
    ProviderResourcesComponent,
    ProviderFacilitiesComponent,
    ProviderReviewsComponent,
    ProviderFaqComponent,
    ProviderOtherResourcesComponent,
    ProviderHealthSafetyComponent,
    ProviderProcessComponent,
    ProviderServiceStandardsComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro.forRoot(),
    NgbModule,
    ApiAuthorizationModule,
    ProviderRoutingModule,
    ProviderCoreModule,
    ProviderSharedModule,
    SharedModule,
    SpaSpaceSharedModule

  ],
  providers: [
    // ProviderRoleGuard,
    // ProviderResolverService,
    // ProviderNavService
  ]
  //bootstrap: [ProviderComponent]
})
export class ProviderModule { }
