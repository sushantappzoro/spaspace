import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderResourcesComponent } from './provider-resources.component';

describe('ProviderResourcesComponent', () => {
  let component: ProviderResourcesComponent;
  let fixture: ComponentFixture<ProviderResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
