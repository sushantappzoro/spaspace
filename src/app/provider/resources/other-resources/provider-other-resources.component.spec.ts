import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderOtherResourcesComponent } from './provider-other-resources.component';

describe('ProviderOtherResourcesComponent', () => {
  let component: ProviderOtherResourcesComponent;
  let fixture: ComponentFixture<ProviderOtherResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderOtherResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderOtherResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
