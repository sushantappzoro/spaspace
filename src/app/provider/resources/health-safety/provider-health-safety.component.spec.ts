import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderHealthSafetyComponent } from './provider-health-safety.component';

describe('ProviderHealthSafetyComponent', () => {
  let component: ProviderHealthSafetyComponent;
  let fixture: ComponentFixture<ProviderHealthSafetyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderHealthSafetyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderHealthSafetyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
