import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderServiceStandardsComponent } from './provider-service-standards.component';

describe('ProviderServiceStandardsComponent', () => {
  let component: ProviderServiceStandardsComponent;
  let fixture: ComponentFixture<ProviderServiceStandardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderServiceStandardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderServiceStandardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
