import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderProcessComponent } from './provider-process.component';

describe('ProviderProcessComponent', () => {
  let component: ProviderProcessComponent;
  let fixture: ComponentFixture<ProviderProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
