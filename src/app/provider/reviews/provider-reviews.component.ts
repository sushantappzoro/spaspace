import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

import { SpaReviewsModel } from '../../../../projects/spaspace-lib/src/shared/models/common/spa-reviews.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-provider-reviews',
  templateUrl: './provider-reviews.component.html',
  styleUrls: ['./provider-reviews.component.scss']
})
export class ProviderReviewsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  reviews: SpaReviewsModel;

  max: number = 5;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { reviews: SpaReviewsModel }) => {
        this.reviews = data.reviews;
      },
        error => {
          console.log(error);
        }));
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  viewAppointment(appointmentUID: string) {
    this.router.navigate(['../appointments/' + appointmentUID], { relativeTo: this.route });
  }
}
