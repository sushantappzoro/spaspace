import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderProfileStepperComponent } from './provider-profile-stepper.component';

describe('ProviderProfileStepperComponent', () => {
  let component: ProviderProfileStepperComponent;
  let fixture: ComponentFixture<ProviderProfileStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderProfileStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderProfileStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
