import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { map } from 'rxjs/operators';
import { Route } from '@angular/compiler/src/core';
import { CanActivate, ActivatedRoute, Router } from '@angular/router';

import { TherapistProfileForEditModel, TherapistProfileStatus } from '../../shared/models/therapist/therapist-profile-for-edit.model';
import { TherapistProfileService } from '../../core/services/therapist/therapist-profile.service';
import { MatStepper } from '@angular/material/stepper';
import { ProviderStepperService } from '../core/services/provider-stepper.service';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { ApiErrorModel } from '../../shared/models/common/api-error.model';
import { copyArrayItem } from '@angular/cdk/drag-drop';
import { DxPopupComponent } from 'devextreme-angular';

@Component({
  selector: 'spa-provider-profile-stepper',
  templateUrl: './provider-profile-stepper.component.html',
  styleUrls: ['./provider-profile-stepper.component.scss']
})
export class ProviderProfileStepperComponent implements OnInit, OnDestroy {
  @ViewChild("stepper") stepper: MatStepper;
  @ViewChild("thanksPopup") thanksPopup : DxPopupComponent

  private subs = new SubSink();

  canEdit: boolean;
  canSubmit: boolean;

  therapist: TherapistProfileForEditModel;

  lastSub: string = '';

  stepIsComplete: boolean[] = [false, false, false, false, false, false];

  nextClicked: boolean;

  errorMessage: string;
  submissionErrors: string[] = [];
  errorsVisible: boolean;

  thanksPopupVisible: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private therapistService: TherapistProfileService,
    private stepperService: ProviderStepperService,
    private authservice: AuthorizeService
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { therapist: TherapistProfileForEditModel }) => {
        this.therapist = data.therapist;
      },
        error => {
          console.log(error);
        }));

    this.updateStatus(this.therapist.Status);

    this.subs.add(this.therapistService.stateChanged
      .subscribe(state => {
        if (state) {
          this.therapist = state.therapist;
          this.updateStatus(this.therapist.Status);
        }
      }));

    this.subs.add(this.stepperService.getError()
      .subscribe(error => {
        this.errorMessage = error;
      }));


    this.subs.add(this.stepperService.getCompleteStep()
      .subscribe(step => {        
        this.processCompleteStep(step);
      }));

  }

  updateStatus(status: string) {

    switch (status) {
      case TherapistProfileStatus.approved:
        this.canEdit = true;
        this.canSubmit = false;
        break;
      case TherapistProfileStatus.new:
        this.canEdit = true;
        this.canSubmit = true;
        break;
      default:
        this.canEdit = false;
        this.canSubmit = false;
    }
  }

  ngAfterViewInit() {
    //fix this 
    //this.tabMenu.selectedIndex = 0;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }


  selectionChange(e) {
    this.stepIsComplete[e] = false;
    this.errorMessage = '';
  }

  submitApplication() {
    
    this.therapistService.submitTherapist(this.therapist)
      .subscribe(resp => {
        //console.log('submitApplication');
        this.thanksPopupVisible = true;        
      },
      error => {
        //console.log('submitApplication-error', error);
        this.processErrors(error);
      })
  }

  onHidden() {
    this.router.navigate(['/authentication/logout']);
  }

  processCompleteStep(step: number) {

    this.stepIsComplete[step] = true;
    if (this.stepper && this.stepper.selected) {
      this.stepper.selected.completed = true;
    }
    
    //console.log('processCompleteStep-stepIsComplete=' + this.stepIsComplete);

    if (this.stepper != null) {
      this.stepper.next();
    }
  }

  next(step: number) {
    this.stepperService.validateStep(this.stepper.selectedIndex);    
  }

  back() {
    this.stepper.previous();
  }

  closePopup() {
    //this.thanksPopupVisible = false;
    this.thanksPopup.instance.hide();
  }

  processErrors(error) {
    this.submissionErrors = [];
    if (error && error.error && error.error.errors) {
      Object.entries(error.error.errors).forEach((key, value) => {
        this.submissionErrors.push(key.toString());
      });

      this.errorsVisible = true;
    } else {
      notify('An error occurred, unable to submit.', "error", 3000);
    }
    //console.log('error.error', error.error);
    //console.log('error.error.errors', error.error.errors);
    //Object.entries(error.error.errors).forEach((key, value) => {
    //  console.log('key=' + key + ', value=' + value);
    //});
  }

}
