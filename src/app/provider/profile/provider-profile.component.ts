import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import { CanActivate, ActivatedRoute, Router } from '@angular/router';
import { DxTabsComponent } from 'devextreme-angular';
import { ProviderNavService } from '../core/services/provider-nav.service';
import { TherapistProfileForEditModel, TherapistProfileStatus } from '../../shared/models/therapist/therapist-profile-for-edit.model';
import { TherapistProfileService } from '../../core/services/therapist/therapist-profile.service';

export class Longtab {
  text: string;
}

@Component({
  selector: 'app-provider-profile',
  templateUrl: './provider-profile.component.html',
  styleUrls: ['./provider-profile.component.scss']
})
export class ProviderProfileComponent implements OnInit, OnDestroy, CanActivate, AfterViewInit {  
  @ViewChild("tabMenu") tabMenu: DxTabsComponent;

  private subs = new SubSink();

  showSaveButton: boolean;
  canEdit: boolean;
  canSubmit: boolean;

  therapist: TherapistProfileForEditModel;

  lastSub: string = '';

  constructor(
    private route: ActivatedRoute,
    //private router: Router,
    //private authService: AuthorizeService,
    private navService: ProviderNavService,
    private therapistService: TherapistProfileService
  ) { }

  ngOnInit() {
    console.log('ProviderProfileComponent - on init');

    this.subs.add(this.route.data
      .subscribe((data: { therapist: TherapistProfileForEditModel }) => {
        this.therapist = data.therapist;
        this.updateStatus(this.therapist.Status);
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.therapistService.stateChanged
      .subscribe(state => {
        if (state) {
          this.therapist = state.therapist;
          this.updateStatus(this.therapist.Status);
        }
      }));

  }


  updateStatus(status: string) {
    
    switch (status) {      
      case TherapistProfileStatus.approved:
        this.showSaveButton = true;
        this.canEdit = true;
        this.canSubmit = false;
        break;
      case TherapistProfileStatus.new:
        this.showSaveButton = true;
        this.canEdit = true;
        this.canSubmit = true;
        break;
      default:
        this.showSaveButton = false;
        this.canEdit = false;
        this.canSubmit = false;
    }

  }

  ngAfterViewInit() {
        //fix this 

    if (this.tabMenu) {
      this.tabMenu.selectedIndex = 0;
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  menuToggle() {
    this.navService.toggleNext();
  }

  canActivate() {
    return true;
  }

}
