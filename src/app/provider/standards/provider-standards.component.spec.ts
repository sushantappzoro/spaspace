import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderStandardsComponent } from './provider-standards.component';

describe('ProviderStandardsComponent', () => {
  let component: ProviderStandardsComponent;
  let fixture: ComponentFixture<ProviderStandardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderStandardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderStandardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
