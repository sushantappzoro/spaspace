import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { AppointmentForTherapistDetailModel } from '../../../shared/models/therapist/appointment-for-therapist-detail.model';
import { SpaFacilityForCardModel, SpaAppointmentCostSummaryForCheckoutModel } from '../../../../../projects/spaspace-lib/src/shared/models';
import { TherapistAppointmentsService } from '../../../core/services/therapist/therapist-appointments.service';
import { AuthorizeService, IUser } from '../../../../api-authorization/authorize.service';
import { StartCheckoutResponseModel } from 'src/app/shared/models/therapist/start-checkout-response.model';
import { CompleteCheckoutRequestModel } from 'src/app/shared/models/therapist/complete-checkout-request.model';
import { CompleteCheckoutResponseModel } from 'src/app/shared/models/therapist/complete-checkout-response.model';

@Component({
  selector: 'app-provider-appointment',
  templateUrl: './provider-appointment.component.html',
  styleUrls: ['./provider-appointment.component.scss']
})
export class ProviderAppointmentComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  appointment: AppointmentForTherapistDetailModel;

  user: IUser;

  facilityForCard: SpaFacilityForCardModel;
  startCheckOutResponse: StartCheckoutResponseModel;
  completeCheckoutResponse: CompleteCheckoutResponseModel;

  errorMessage: string;
  submissionErrors: string[] = [];
  errorsVisible: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthorizeService,
    private appointmentsService: TherapistAppointmentsService
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { appointment: AppointmentForTherapistDetailModel }) => {
        console.log('got data', data.appointment)
        this.appointment = data.appointment;
        this.facilityForCard = this.appointment.Facility;
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.user = user;
      })
    );

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addonsUpdated() {
    this.reloadAppointment();
  }

  checkin() {
    this.subs.add(this.appointmentsService.checkIn(this.user.sub, this.appointment.UID)
      .subscribe(appt => {
        this.appointment = appt;
      },
        error => {
          this.processErrors(error);
        }));
  }

  noshow() {
    this.subs.add(this.appointmentsService.noShow(this.user.sub, this.appointment.UID)
      .subscribe(appt => {
        //this.appointment = appt;
        this.reloadAppointment();
      },
        error => {
          this.processErrors(error);
        }));
  }

  startCheckout() {
    // puts the appointment in "Checking Out" status and returns the payment method and cost summary
    this.subs.add(this.appointmentsService.startCheckOut(this.user.sub, this.appointment.UID)
      .subscribe(response => {
        this.startCheckOutResponse = response;
        this.reloadAppointment();
      },
        error => {
          this.processErrors(error);
        }
    ));
  }

  completeCheckout() {

    const model: CompleteCheckoutRequestModel = { Total: 0, Gratuity: 0 };

    this.subs.add(this.appointmentsService.completeCheckOut(this.user.sub, this.appointment.UID, model)
      .subscribe(response => {
        this.completeCheckoutResponse = response;
      },
        error => {
          this.processErrors(error);
        }));
  }

  checkoutCompleted() {
    //reload the appointment to show change in status
    this.reloadAppointment();
  }

  cancel() {
    this.subs.add(this.appointmentsService.cancel(this.user.sub, this.appointment.UID)
      .subscribe(appt => {
        this.appointment = appt;
      },
        error => {
          this.processErrors(error);
        }));
  }

  checkout() {

  }

  reloadAppointment() {
    this.subs.add(this.appointmentsService.getAppointmentDetail(this.user.sub, this.appointment.UID)
      .subscribe(resp => {
        this.appointment = resp;
      })
    )
  }

  processErrors(error) {
    this.submissionErrors = [];
    if (error && error.error && error.error.errors) {
      Object.entries(error.error.errors).forEach((key, value) => {
        //console.log('key=' + key);
        //console.log('value=' + value);

        key.forEach(error => {
          this.submissionErrors.push(error.toString());
        })
        
      });
      this.errorMessage = error.error.title;
      this.errorsVisible = true;
    } else {
      notify('An error occurred.', "error", 3000);
    }
  }

}
