import { Component, OnInit, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { formatDate } from '@angular/common';
import { TherapistScheduleAndAppointmentsModel } from '../../shared/models/therapist/therapist-schedule-and-appointments.model';
import { TherapistAppointmentSummaryModel } from '../../shared/models/therapist/therapist-appointment-summary.model';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { DxSchedulerComponent } from 'devextreme-angular';
import { ModalDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-provider-appointments',
  templateUrl: './provider-appointments.component.html',
  styleUrls: ['./provider-appointments.component.scss']
})
export class ProviderAppointmentsComponent implements OnInit {
  @ViewChild("scheduler") scheduler: DxSchedulerComponent;
  @ViewChild('appointmentModal') appointmentModal: ModalDirective;

  private subs = new SubSink();

  currentDate: Date = new Date();

  maxDate: Date;
  minDate: Date;

  schedule: TherapistScheduleAndAppointmentsModel;

  appointments: TherapistAppointmentSummaryModel[] = [];

  recCount: number = 0;

  selectedAppointment: TherapistAppointmentSummaryModel;

  therapistUID: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthorizeService
  ) { }

  ngOnInit() {

    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.therapistUID = user.sub;
      }));

    this.subs.add(this.route.data
      .subscribe((data: { schedule: TherapistScheduleAndAppointmentsModel }) => {
        this.schedule = data.schedule;
        this.schedule.Schedule.forEach(appt => {
          this.appointments.push(
            {
              TreatmentArea: appt.TreatmentArea,
              TreatmentAreaUID: appt.TreatmentAreaUID,
              Customer: appt.Customer,
              CustomerUID: appt.CustomerUID,
              EndDateTime: new Date(appt.EndDateTime),
              Facilty: appt.Facilty,
              FaciltyUID: appt.FaciltyUID,
              Service: appt.Service,
              StartDateTime: new Date(appt.StartDateTime),
              UID: appt.UID
            })
        });
      },
        error => {
          console.log(error);
        }));

    this.maxDate = new Date();
    this.maxDate.setMonth(this.maxDate.getMonth() + 1);

    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDay() - 7);

  }



  markWeekEnd(cellData) {

    var classObject = {};
    classObject['employee-weekend-1'] = this.hasAvailability(cellData.startDate, cellData.endDate)
    return classObject;

  }

  hasAvailability(startDate, endDate) {
    try {


      if (this.schedule && this.schedule.Availability) {
        var item = this.schedule.Availability.filter(appointment => { return new Date(startDate) >= new Date(appointment.StartTime) && new Date(endDate) <= new Date(appointment.EndTime) })
        if (item && item[0]) {
          return true;
        } else {
          return false;
        }
      }
    }
    catch {

    }
    return false;
  }

  onAppointmentClick(e) {

    e.cancel = true;

    this.selectedAppointment = {...e.appointmentData};

    this.appointmentModal.show();


  }

  onAppointmentFormOpening(e) {
    e.cancel = true;
    this.scheduler.instance.hideAppointmentTooltip();
  }

  width() {
    return window.innerWidth / 1.5;
  }

  appointmentSelected(e) {
    this.router.navigate([e], { relativeTo: this.route });
  }

}
