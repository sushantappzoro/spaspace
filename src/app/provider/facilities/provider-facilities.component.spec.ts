import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderFacilitiesComponent } from './provider-facilities.component';

describe('ProviderFacilitiesComponent', () => {
  let component: ProviderFacilitiesComponent;
  let fixture: ComponentFixture<ProviderFacilitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderFacilitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderFacilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
