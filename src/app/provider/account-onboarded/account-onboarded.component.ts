import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TherapistAccountService } from 'src/app/core/services/therapist/therapist-account.service';
import { StripeInfoModel } from 'src/app/shared/models/therapist/stripe-info.model';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-account-onboarded',
  templateUrl: './account-onboarded.component.html',
  styleUrls: ['./account-onboarded.component.css']
})
export class AccountOnboardedComponent implements OnInit {

  stripeInfo: StripeInfoModel;
  private subs = new SubSink();

  constructor(
    private route: ActivatedRoute,
    private accountService: TherapistAccountService,
    private router: Router
  ) { }

  ngOnInit() {
    const authCode = this.route.snapshot.queryParamMap.get('code');
    if (authCode && authCode.length > 0 ) {
      this.stripeInfo = new StripeInfoModel();
      //this.stripeInfo.AuthorizationCode = authCode;
      this.subs.add(this.accountService.addStripeInfo(this.route.snapshot.queryParamMap.get('state'), this.stripeInfo)
      .subscribe(resp => {
        this.stripeInfo = resp;
        this.router.navigate(['/therapist/account']);
      }, error => {
        console.log(error);
      })
      );
    }
  }
}
