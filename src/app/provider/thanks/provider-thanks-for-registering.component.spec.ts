import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderThanksForRegisteringComponent } from './provider-thanks-for-registering.component';

describe('ProviderThanksForRegisteringComponent', () => {
  let component: ProviderThanksForRegisteringComponent;
  let fixture: ComponentFixture<ProviderThanksForRegisteringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderThanksForRegisteringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderThanksForRegisteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
