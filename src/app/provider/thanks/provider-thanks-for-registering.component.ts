import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-provider-thanks-for-registering',
  templateUrl: './provider-thanks-for-registering.component.html',
  styleUrls: ['./provider-thanks-for-registering.component.scss']
})
export class ProviderThanksForRegisteringComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
