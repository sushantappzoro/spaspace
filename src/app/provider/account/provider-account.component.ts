import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AuthorizeService, IUser } from 'src/api-authorization/authorize.service';
import { SubSink } from 'subsink';
import { StripeInfoModel } from 'src/app/shared/models/therapist/stripe-info.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TherapistAccountService } from 'src/app/core/services/therapist/therapist-account.service';


@Component({
  selector: 'app-provider-account',
  templateUrl: './provider-account.component.html',
  styleUrls: ['./provider-account.component.scss']
})
export class ProviderAccountComponent implements OnInit, OnDestroy {
  user: IUser;
  needsToOnboardWithStripe: boolean;
  stripeInfo: StripeInfoModel;
  private subs = new SubSink();

  constructor(
    private authService: AuthorizeService,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: TherapistAccountService
  ) { }

  ngOnInit() {
    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.user = user;
      })
    );
    this.subs.add(this.route.data
      .subscribe((data: { stripeInfo: StripeInfoModel }) => {
        this.stripeInfo = data.stripeInfo;
      }));
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  goToStripe() {
    window.location.assign(this.stripeInfo.Url);
  }

  onboardAtStripe() {

    const stripeClientId = environment.ClientAppSettings.stripeClientId;
    const therapistUid = this.user.sub;
    const stripeUri = 'https://connect.stripe.com/express/oauth/authorize?' +
                      'client_id=' + stripeClientId +
                      '&state=' + therapistUid +
                      '&stripe_user[business_type]=individual' +
                      '&stripe_user[email]=' + this.user.email +
                      '&stripe_user[first_name]=' + this.user.firstName +
                      '&stripe_user[last_name]=' + this.user.lastName +
                      '&redirect_uri=https://localhost:44359/therapist/stripeaccountcreated';


    window.location.assign(stripeUri);
  }

}
