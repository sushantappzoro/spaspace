import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderAccountComponent } from './provider-account.component';

describe('ProviderAccountComponent', () => {
  let component: ProviderAccountComponent;
  let fixture: ComponentFixture<ProviderAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
