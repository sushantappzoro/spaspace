import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderComponent } from './provider.component';
import { ProviderDashboardComponent } from './dashboard/provider-dashboard.component';
import { ProviderProfileComponent } from './profile/provider-profile.component';
import { ProviderAvailabilityComponent } from './availability/provider-availability.component';
import { ProviderStandardsComponent } from './standards/provider-standards.component';
import { ProviderAccountComponent } from './account/provider-account.component';
import { ProviderErrorComponent } from './error/provider-error.component';
import { AuthorizeGuard } from '../../api-authorization/authorize.guard';
import { RoleGuard } from '../../api-authorization/role.guard';
import { ProviderScheduleResolver } from './core/resolvers/provider-shedule.resolver';
import { ProviderAppointmentsComponent } from './appointments/provider-appointments.component';
import { ProviderProfileStepperComponent } from './profile/provider-profile-stepper.component';
import { ProviderResolverService } from './core/resolvers/provider-resolver.service';
import { TherapistAssociatedFacilitiesResolver } from '../core/resolvers/therapist/therapist-assoc-facilities.resolver';
import { TherapistResolverService } from '../core/resolvers/therapist/therapist-resolver.service';
import { TherapistAvailabilityResolver } from '../core/resolvers/therapist/therapist-availability.resolver';
import { ProviderThanksForRegisteringComponent } from './thanks/provider-thanks-for-registering.component';
import { CanDeactivateGuardService } from '../core/guards/can-deactivate-guard.service';
import { ProviderProfileGuard } from './core/guards/provider-profile.guard';
import { ProviderAppointmentComponent } from './appointments/provider-appointment/provider-appointment.component';
import { ProviderResourcesComponent } from './resources/provider-resources.component';
import { ProviderFacilitiesComponent } from './facilities/provider-facilities.component';
import { ProviderReviewsComponent } from './reviews/provider-reviews.component';
import { TherapistScheduleResolver } from '../core/resolvers/therapist/therapist-schedule.resolver';
import { TherapistAppointmentsResolver } from '../core/resolvers/therapist/therapist-appointments.resolver';
import { TherapistAppointmentResolver } from '../core/resolvers/therapist/therapist-appointment.resolver';
import { UserProfileComponent } from '../shared/components/user/user-profile/user-profile.component';
import { UserProfileResolver } from '../core/resolvers/user/user-profile.resolver';
import { SpaTherapistReviewResolver } from '../../../projects/spaspace-lib/src/core/resolvers/therapist/spa-therapist-review.resolver';
import { ProviderAccountResolver } from './core/resolvers/provider-account.resolver';
import { AccountOnboardedComponent } from './account-onboarded/account-onboarded.component';
import { ProviderHealthSafetyComponent } from './resources/health-safety/provider-health-safety.component';
import { ProviderServiceStandardsComponent } from './resources/service-standards/provider-service-standards.component';
import { ProviderProcessComponent } from './resources/process/provider-process.component';
import { ProviderFaqComponent } from './resources/faq/provider-faq.component';
import { ProviderOtherResourcesComponent } from './resources/other-resources/provider-other-resources.component';


const routes: Routes = [
  {
    path: '',
    component: ProviderComponent,
    data: {
      //breadcrumb: 'Therapist'
    },
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
      },
      {
        path: 'dashboard',
        component: ProviderDashboardComponent,
        resolve: {
          therapist: TherapistResolverService
        },
        data: {
          breadcrumb: 'Dashboard'
        }
      },
      {
        path: 'profile',
        component: ProviderProfileComponent,
        resolve: {
          therapist: TherapistResolverService
        },
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Profile'
        },
        canActivate: [ProviderProfileGuard]
        //canActivate: [
        //  AuthorizeGuard,
        //  RoleGuard
        //],
        //data: {
        //  expectedRole: 'therapist'
        //}
      },
      {
        path: 'userprofile',
        component: UserProfileComponent,
        resolve: {
          userProfile: UserProfileResolver
        },
        data: {
          breadcrumb: 'User Profile'
        }
      },
      {
        path: 'stepper',
        component: ProviderProfileStepperComponent,
        resolve: {
          therapist: TherapistResolverService
        },
        data: {
          allowedStatus: ['New'],
          breadcrumb: 'Profile'
        },
        canActivate: [ProviderProfileGuard]
        //data: {
        //  allowedStatus: ['New', 'Pending Approval', 'Approved']
        //}
      },
      {
        path: 'appointments',
        component: ProviderAppointmentsComponent,
        resolve: {
          schedule: TherapistAppointmentsResolver
        },
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Schedule'
        }
      },
      {
        path: 'appointments/:uid',
        component: ProviderAppointmentComponent,
        resolve: {
          appointment: TherapistAppointmentResolver
        },
        data: {
          breadcrumb: 'Appointment'
        }
      },
      {
        path: 'availability',
        component: ProviderAvailabilityComponent,
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Availability'
        },
        resolve: {
          facilities: TherapistAssociatedFacilitiesResolver,
          availability: TherapistAvailabilityResolver
        },
      },
      {
        path: 'facilities',
        component: ProviderFacilitiesComponent,
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Spas'
        }
      },
      {
        path: 'reviews',
        component: ProviderReviewsComponent,
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Reviews'
        },
        resolve: {
          reviews: SpaTherapistReviewResolver
        },
      },
      {
        path: 'resources',
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Resources'
        },
        children: [
          {
            path: 'health',
            component: ProviderHealthSafetyComponent
          },
          {
            path: 'standards',
            component: ProviderServiceStandardsComponent
          },
          {
            path: 'process',
            component: ProviderProcessComponent
          },
          {
            path: 'faq',
            component: ProviderFaqComponent
          },
          {
            path: 'other',
            component: ProviderOtherResourcesComponent
          }
        ]
      },
      {
        path: 'account',
        component: ProviderAccountComponent,
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Transaction'
        },
        resolve: {
          stripeInfo: ProviderAccountResolver
        },
      },
      {
        path: 'accountonboarded',
        component: AccountOnboardedComponent,
        data: {
          allowedStatus: ['Approved'],
          breadcrumb: 'Transaction'
        }
      },
      {
        path: 'thanks',
        component: ProviderThanksForRegisteringComponent
      },
      {
        path: 'error',
        component: ProviderErrorComponent
      },
      {
        path: '**',
        redirectTo: 'dashboard',
        //canActivate: [AuthorizeGuard]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProviderRoutingModule { }
