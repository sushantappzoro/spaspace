import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-provider-unauthorized',
  templateUrl: './provider-unauthorized.component.html',
  styleUrls: ['./provider-unauthorized.component.scss']
})
export class ProviderUnauthorizedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
