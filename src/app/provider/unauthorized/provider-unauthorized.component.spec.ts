import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderUnauthorizedComponent } from './provider-unauthorized.component';

describe('ProviderUnauthorizedComponent', () => {
  let component: ProviderUnauthorizedComponent;
  let fixture: ComponentFixture<ProviderUnauthorizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderUnauthorizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderUnauthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
