import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderRequirementsComponent } from './provider-requirements.component';

describe('ProviderRequirementsComponent', () => {
  let component: ProviderRequirementsComponent;
  let fixture: ComponentFixture<ProviderRequirementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderRequirementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderRequirementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
