import { Component, OnInit, OnDestroy } from '@angular/core';
import Query from 'devextreme/data/query';
import { SubSink } from 'subsink';
import * as cloneDeep from 'lodash/cloneDeep';

import { TherapistAssociatedFacilityModel } from '../../shared/models/therapist/therapist-associated-facility.model';
import { TherapistAvailabilityForViewModel } from '../../shared/models/therapist/therapist-availability-for-view.model';
import { TherapistAvailabilityForAddModel } from '../../shared/models/therapist/therapist-availability-for-add.model';
import { TherapistAvailabilityForEditModel } from '../../shared/models/therapist/therapist-availability-for-edit.model';
import { TherapistFacilitiesApiService } from '../../core/api/therapist/therapist-facilities-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { TherapistAvailabilityService } from '../../core/services/therapist/therapist-availability.service';
import { map } from 'rxjs/operators';
import notify from 'devextreme/ui/notify';
import { SpinnerOverlayService } from 'src/app/core/services/common/spinner-overlay.service';

@Component({
  selector: 'app-provider-availability',
  templateUrl: './provider-availability.component.html',
  styleUrls: ['./provider-availability.component.scss']
})
export class ProviderAvailabilityComponent implements OnInit, OnDestroy {

  colors: string[] = [
    "#26A69A", "#00BCD4", "#03A9F4", "#2196F3", "#3F51B5"
  ]

  private subs = new SubSink();

  currentDate: Date = new Date();

  facilityData: TherapistAssociatedFacilityModel[] = [];
  availability: TherapistAvailabilityForViewModel[] = [];

  facswcolor: FacilityForAvailability[];

  draggingGroupName: string = "appointmentsGroup";

  userUID: string;

  //facilityData: TherapistFacilityForListModel[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthorizeService,
    private availabilityService: TherapistAvailabilityService,
    private facilityService: TherapistFacilitiesApiService,
    private spinnerService: SpinnerOverlayService,
  ) {
    this.onAppointmentRemove = this.onAppointmentRemove.bind(this);
    this.onAppointmentAdd = this.onAppointmentAdd.bind(this);
  }

  ngOnInit() {
    //console.log('ProviderAvailabilityComponent');
    var x: number = 0;

    this.subs.add(this.authService.getUser()
      .pipe(map(u => u && u.sub))
      .subscribe(sub => {
        this.userUID = sub;
      }));

    this.subs.add(this.route.data
      .subscribe((data: { facilities: TherapistAssociatedFacilityModel[] }) => {
        this.facilityData = data.facilities;

        this.facswcolor = [];

        this.facilityData.forEach(item => {
          var newItem: FacilityForAvailability = new FacilityForAvailability();
          newItem.FacilityName = item.Name;
          newItem.FacilityUID = item.FacilityUID;
          newItem.IsDefault = item.IsDefault;
          newItem.UID = item.UID;

          var color = this.colors[x];
          newItem.Color = color;
          x += 1;
          this.facswcolor.push(newItem);

        });

      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.route.data
      .subscribe((data: { availability: TherapistAvailabilityForViewModel[] }) => {
        this.availability = data.availability;
      },
        error => {
          console.log(error);
          this.availability = [];
        }));


  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.spinnerService.hide();
  }

  onAppointmentFormOpening(data) {
    var that = this;
    var form = data.form;
    var facilityInfo = that.getFacilityByUID(data.appointmentData.FacilityUID) || {}

    var elements: Array<any> = form.option('items');

    //console.log('onAppointmentFormOpening', elements);

    if (elements.length == 11) {
      elements.splice(0, 1);
      elements.splice(4, 3);
      elements.splice(6, 1);

      var newElement = {
        label: {
          text: "Facility"
        },
        colSpan: 2,
        editorType: "dxSelectBox",
        dataField: "FacilityUID",
        editorOptions: {
          items: that.facswcolor,
          displayExpr: "FacilityName",
          valueExpr: "FacilityUID",
          onValueChanged: function (args) {
            facilityInfo = that.getFacilityByUID(args.value);
          }.bind(this)
        }
      };

      elements.unshift(newElement);

      var hold = elements[1]['colSpan'] = 2;
      var hold = elements[3]['colSpan'] = 2;
      //var hold = elements[6]['colSpan'] = 1;


      //console.log('onAppointmentFormOpening', elements);
    }
  }



  //
  // drag and drop
  //


  getColor(uid: string) {
    //console.log()
    return this.facswcolor.find(item => item.FacilityUID == uid).Color;
  }

  onAppointmentRemove(e) {
    //const index = this.appointments.indexOf(e.itemData);

    //if (index >= 0) {
    //  this.appointments.splice(index, 1);
    //  this.tasks.push(e.itemData);
    //}
  }

  onAppointmentAdd(e) {

    this.spinnerService.show();

    //console.log('onAppointmentAdd', e);

    var item: TherapistAvailabilityForAddModel = { ...e.itemData }

    //console.log('onAppointmentAdd', item);

    let tempdate = new Date(item.StartTime);
    //console.log('tempdate1', tempdate);
    //tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    //item.EndTime = tempdate;

    //tempdate = new Date(item.StartTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    //console.log('tempdate2', tempdate);
    item.StartTime = new Date(tempdate);

    tempdate = new Date(item.StartTime);
    //console.log('tempdate3', tempdate);
    tempdate.setHours(item.StartTime.getHours() + 1);
    //console.log('tempdate4', tempdate);
    item.EndTime = new Date(tempdate);


    //console.log('onAppointmentAdd', e);
    //console.log('onAppointmentAdd', this.availability);
    //var startTime: Date = new Date(e.itemData.StartTime);
    //var endTime: Date = new Date(startTime);
    //endTime.setHours(startTime.getHours() + 1);

    //var item: TherapistAvailabilityForAddModel =
    //{ EndTime: endTime, StartTime: startTime, FacilityUID: e.itemData.FacilityUID, RecurrenceRule: e.itemData.RecurrenceRule };

    //item.FacilityUID = e.appointmentData.FacilityName;  // this.getFacilityByName(e.appointmentData.FacilityName).FacilityUID;

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.add(this.userUID, item)
        .subscribe(resp => {

          this.availability.push(resp);
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);
            this.spinnerService.hide();
            resolve(true);
          }));
    });

  }

  onListDragStart(e) {
    //console.log('onListDragStart', e);
    e.cancel = true;
  }

  onItemDragStart(e) {
    //console.log('onItemDragStart', e);
    e.itemData = e.fromData;
  }

  onItemDragEnd(e) {
    //console.log('onItemDragEnd', e);
    if (e.toData) {
      e.cancel = true;
    }
  }

  //
  // end drag and drop
  //

  getFacilityByName(name: string): TherapistAssociatedFacilityModel {
    return Query(this.facswcolor).filter(["Name", "=", name]).toArray()[0];
    //return this.facilityData.find(x => { x.Name = name });
  }

  getFacilityByUID(uid: string): TherapistAssociatedFacilityModel {
    return Query(this.facswcolor).filter(["FacilityUID", "=", uid]).toArray()[0];
    //return this.facilityData.find(x => { x.Name = name });
  }

  onAppointmentAdding(e) {
    //console.log('onAppointmentAdding', e);
    //console.log('useruid=' + this.userUID);
    //console.log('recurrence rule=' + e.appointmentData.recurrence);
    this.spinnerService.show();

    const item: TherapistAvailabilityForAddModel = { ...e.appointmentData};

    let tempdate = new Date(item.StartTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    item.StartTime = new Date(tempdate);

    tempdate = new Date(item.EndTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    item.EndTime = new Date(tempdate);

    //item.FacilityUID = e.appointmentData.FacilityName;  // this.getFacilityByName(e.appointmentData.FacilityName).FacilityUID;

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.add(this.userUID, item)
        .subscribe(resp => {
          e.appointmentData.UID = resp.UID;
          e.appointmentData.FacilityName = resp.FacilityName;
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);
            this.spinnerService.hide();
            resolve(true);
          }));
    });
  }

  onAppointmentUpdating(e) {

    this.spinnerService.show();
    //console.log('onAppointmentUpdating', e);

    //var item: ProviderAvailabilityForViewModel = { ...e.oldData, ...e.newData }

    //var editItem: ProviderAvailabilityForEditModel = new ProviderAvailabilityForEditModel();

    //editItem.EndTime = item.EndTime;
    //editItem.FacilityUID = this.getFacilityByName(item.FacilityName).UID;
    //editItem.StartTime = item.StartTime;
    //editItem.UID = item.UID;

    var item: TherapistAvailabilityForViewModel = { ...e.oldData, ...e.newData }
    var editItem: TherapistAvailabilityForEditModel = { ...e.oldData, ...e.newData }

    //editItem.FacilityUID = this.getFacilityByName(item.FacilityName).FacilityUID;

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.edit(this.userUID, editItem)
        .subscribe(resp => {
          e.newData.FacilityName = resp.FacilityName;
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);
            resolve(true);
            this.spinnerService.hide();
          }));
    });
  }

  onAppointmentDeleting(e) {
    this.spinnerService.show();
    //console.log('onAppointmentDeleting', e);
    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.remove(this.userUID, e.appointmentData.UID)
        .subscribe(resp => {
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);
            this.spinnerService.hide();
            resolve(true);
          }));
    });
  }
}


export class FacilityForAvailability {
  public UID: string;
  public FacilityUID: string;
  public IsDefault: boolean;
  public FacilityName: string;
  public Color: string;
}
