import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { MainComponent } from './main/main.component';
import { CoreModule } from './core/core.module';
import { DevExtremeModule } from "devextreme-angular";
import { AgmCoreModule } from '@agm/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MainModule } from './main/main.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material-module.module';
import { LandingComponent } from './main/landing/landing.component';
import { HomeComponent } from './main/home/home.component';
import { FacilityPitchComponent } from './main/facility-pitch/facility-pitch.component';
import { ProviderPitchComponent } from './main/provider-pitch/provider-pitch.component';
import { UnauthorizedComponent } from './main/unauthorized/unauthorized.component';
import { RoleGuard } from '../api-authorization/role.guard';
import { NavBarService } from './core/services/nav-bar.service';
import { SharedModule } from './shared/shared.module';
import { TherapistResolverService } from './core/resolvers/therapist/therapist-resolver.service';
import { MainTherapistsComponent } from './main/therapist/main-therapists/main-therapists.component';
import { TherapistRequirementsComponent } from './main/therapist/therapist-requirements/therapist-requirements.component';
import { TherapistJoinusComponent } from './main/therapist/therapist-joinus/therapist-joinus.component';
import { TherapistThanksComponent } from './main/therapist/therapist-thanks/therapist-thanks.component';
import { AboutUsComponent } from './main/about-us/about-us.component';
import { NewsComponent } from './main/news/news.component';
import { TermsComponent } from './main/terms/terms.component';
import { FaqComponent } from './main/faq/faq.component';
import { PrivacyComponent } from './main/privacy/privacy.component';
import { SafetyComponent } from './main/safety/safety.component';
import { AppRoleGuardService } from './core/guards/app-role-guard.service';
import { FacilityManagerResolver } from './facility/core/resolvers/facility-manager.resolver';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { SpaPortalGuard } from '../../projects/spaspace-lib/src/core/guards/spa-portal.guard';
import { MainContentComponent } from './main/content/main-content.component';
import { MainFaqComponent } from './main/content/main-faq/main-faq.component';
import { MainHealthSafetyComponent } from './main/content/main-health-safety/main-health-safety.component';
import { MainProcessComponent } from './main/content/main-process/main-process.component';
import { MainAboutComponent } from './main/content/main-about/main-about.component';
import { MainSanitizationComponent } from './main/content/main-sanitization/main-sanitization.component';
import { MainStoryComponent } from './main/content/main-story/main-story.component';


//import { AppRoutingModule } from './app.routing.module';

@NgModule({
  declarations: [
      AppComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyAarPSOwrlU4oV84gohgE1WBWKvuYlYuIU',
        libraries: ['places']
    }),
    DevExtremeModule,
    CoreModule,
    SharedModule,
    ApiAuthorizationModule,
    //AppRoutingModule,
    RouterModule.forRoot([
      {
        path: '',
        component: MainComponent,
        pathMatch: 'full',
        children: [
          {
            path: '',
            component: HomeComponent
          }
        ]
      },
      {
        path: 'prototype',
        loadChildren: () => import('./proto/prototype.module').then(m => m.PrototypeModule),
      },
      {
        path: 'main',
        component: MainComponent,
        children: [
          {
            path: '',
            component: HomeComponent
          },
          {
            path: 'content',
            component: MainContentComponent,
            children: [
              {
                path: 'faq',
                component: MainFaqComponent
              },
              {
                path: 'safety',
                component: MainSanitizationComponent
              },
              {
                path: 'join',
                component: MainProcessComponent
              },
              {
                path: 'about',
                component: MainAboutComponent
              },
              {
                path: 'faq',
                component: MainFaqComponent
              },
              {
                path: 'story',
                component: MainStoryComponent
              }
            ]
          },
          {
            path: 'facility',
            component: FacilityPitchComponent
          },
          {
            path: 'about',
            component: AboutUsComponent
          },
          {
            path: 'news',
            component: NewsComponent
          },
          {
            path: 'terms',
            component: TermsComponent
          },
          {
            path: 'faq',
            component: FaqComponent
          },
          {
            path: 'privacy',
            component: PrivacyComponent
          },
          {
            path: 'safety',
            component: SafetyComponent
          },
          {
            path: 'therapist',
            component: MainTherapistsComponent,
            children: [
              {
              path: 'requirements',
              component: TherapistRequirementsComponent
              },
              {
                path: 'join',
                component: TherapistJoinusComponent
              },
              {
                path: 'thanks',
                component: TherapistThanksComponent
              }
            ]
          },
          {
            path: 'provider',
            component: ProviderPitchComponent
          },
          {
            path: 'unauthorized',
            component: UnauthorizedComponent
          },
          {
            path: 'loading',
            component: LandingComponent
          }
        ]
      },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
        canActivate: [AppRoleGuardService],
        data: {
          expectedRole: 'admin',
          breadcrumb: 'Admin'
        }
      },
      {
        path: 'portal',
        loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule),
        canActivate: [SpaPortalGuard]
      },
      {
        path: 'facility',
        //loadChildren: () => import('./facility/facility.module').then(m => m.FacilityModule),
        loadChildren: () => import('./facility/facility-manager.module').then(m => m.FacilityManagerModule),
        canActivate: [AppRoleGuardService],
        data: {
          expectedRole: 'facilitymanager',
          breadcrumb: 'Facility Manager'
        },
        resolve: {
          facilityManager: FacilityManagerResolver
        },
        //canLoad: [RoleGuard],
      },
      {
        path: 'therapist',
        loadChildren: () => import('./provider/provider.module').then(m => m.ProviderModule),
        canActivate: [AppRoleGuardService],
        resolve: {
          therapist: TherapistResolverService
        },
        data: {
          expectedRole: 'therapist',
          breadcrumb: 'Therapist'
        },
        //canLoad: [RoleGuard],
        //canLoad: [
        //  AuthorizeGuard
        //],
        //canActivate: [
        //  AuthorizeGuard
        //],
      },
      {
        path: '**',
        redirectTo: 'main'
      }
    ]),
    MainModule,
    MaterialModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true },
    NavBarService,
    //SpaConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
