import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { AuthorizeService, IUser } from '../../../../api-authorization/authorize.service';
import { SpaCustomerPreferencesModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-preferences.model';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { CustomerService } from '../../core/services/customer.service';
import { TherapistPressureValues } from '../../../shared/globals';

@Component({
  selector: 'app-customer-preferences',
  templateUrl: './customer-preferences.component.html',
  styleUrls: ['./customer-preferences.component.scss']
})
export class CustomerPreferencesComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  user: IUser;

  preferencesForm: FormGroup;

  preferences: SpaCustomerPreferencesModel;

  pressures = TherapistPressureValues;

  //needs: MassageNeedForBookingModel[];
  //focusAreas: MassageFocusAreaForBookingModel[];
  //amenities: SpaFacilityAmenityForBookingModel[];

  constructor(
    private route: ActivatedRoute,
    private authService: AuthorizeService,
    private customerService: CustomerService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.preferencesForm = this.formBuilder.group({
      FocusAreas: this.formBuilder.array([]),
      Needs: this.formBuilder.array([]),
      PreferredAmenities: this.formBuilder.array([]),
      TherapistGender: new FormControl(''),
      PressureLevel: new FormControl(''),
    });

    this.subs.add(this.route.data
      .subscribe((data: { preferences: any }) => {
        console.log('got data', data.preferences)
        this.preferences = data.preferences;
        this.populateForm();
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.user = user;
      })
    )

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  populateForm() {

    if (this.preferences) {

      if (this.preferences.FocusAreas) {
        this.preferences.FocusAreas.forEach(area => {        
          this.areaArray.push(this.addFormControl(area));
        })
      }

      if (this.preferences.Needs) {
        this.preferences.Needs.forEach(need => {
          this.needsArray.push(this.addFormControl(need));
        })
      }

      if (this.preferences.PreferredAmenities) {
        this.preferences.PreferredAmenities.forEach(amenity => {
          this.amenitiesArray.push(this.addFormControl(amenity));
        })
      }

      this.preferencesForm.get('PressureLevel').setValue(this.preferences.PressureLevel);
      this.preferencesForm.get('TherapistGender').setValue(this.preferences.TherapistGender);

    }

  }

  get areaArray() {
    return this.preferencesForm.get('FocusAreas') as FormArray;
  }

  get needsArray() {
    return this.preferencesForm.get('Needs') as FormArray;
  }

  get amenitiesArray() {
    return this.preferencesForm.get('PreferredAmenities') as FormArray;
  }

  addFormControl(item: any) {
    return this.formBuilder.group({
      ID: item.ID,
      Name: item.Name,
      IsChecked: item.IsChecked
    })
  }


  needChanged() {

  }

  focusAreaChanged() {

  }

  amenityChanged() {

  }

  onSave() {
    var bob: SpaCustomerPreferencesModel = Object.assign({}, this.preferencesForm.value);
    this.subs.add(this.customerService.editPreferences(this.user.sub, bob)
      .subscribe(resp => {
        notify('Preferences updated successfully.', "success", 3000);
      },
        error => {
          notify('Unable to save preferences.', "error", 3000);
        }
      ));
  }

}
