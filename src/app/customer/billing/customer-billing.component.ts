import { Component, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import { CustomerBillingInfoModel } from '../shared/models/customer-billing-info.model';

@Component({
  selector: 'app-customer-billing',
  templateUrl: './customer-billing.component.html',
  styleUrls: ['./customer-billing.component.scss']
})
export class CustomerBillingComponent implements OnInit {

  private subs = new SubSink();

  billingInfo: CustomerBillingInfoModel[]

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.subs.add(this.route.data
      .subscribe((data: { billinginfo: CustomerBillingInfoModel[] }) => {
        this.billingInfo = data.billinginfo;
      },
        error => {
          console.log(error);
        }));
  }

}
