import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { CustomerSessionService, CustomerSearchType } from '../core/services/customer-session.service';
import themes from "devextreme/ui/themes";


@Component({
  selector: 'cust',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  //calendar
  now: Date = new Date();
  currentValue: Date = new Date();
  firstDay: number = 0;
  minDateValue: Date = new Date();
  maxDateValue: Date = undefined;
  disabledDates: Function = null;
  zoomLevels: string[] = [
    "month", "year", "decade", "century"
  ];
  cellTemplate = "cell";

  public isAuthenticated: Observable<boolean>;
  public userName: Observable<string>;
  public role: Observable<string>;

  constructor(
    private authorizeService: AuthorizeService,
    private customerSessionService: CustomerSessionService,
    private router: Router
  ) {
    //themes.current("material.teal.light.compact");
  }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.name));
    this.role = this.authorizeService.getUser().pipe(map(u => u && u.role));
  }

  ngOnDestroy() {
    //themes.current("material.teal.light");
  }

  byTherapist_click() {
    this.customerSessionService.setSearchType(CustomerSearchType.ByTherapist);
    this.router.navigate(['/portal/search/therapist']);
  }

  byLocation_click() {
    this.customerSessionService.setSearchType(CustomerSearchType.ByLocation);
    this.router.navigate(['/portal/search/location']);
  }

  valueChanged() {
    this.customerSessionService.setSearchType(CustomerSearchType.ByDate);
    console.log('valueChanged', this.currentValue);
    this.router.navigate(['/portal/search']);
  }

}
