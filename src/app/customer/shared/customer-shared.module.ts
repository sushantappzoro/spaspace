import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetsModule } from './widgets/widgets.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng5SliderModule } from 'ng5-slider';
import { CustomerExpandingAppointmentComponent } from './components/customer-expanding-appointment/customer-expanding-appointment.component';
import { MaterialModule } from '../../material-module.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CustomerFocusAreasComponent } from './components/customer-focus-areas/customer-focus-areas.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations:
    [
    CustomerExpandingAppointmentComponent,
      CustomerFocusAreasComponent
    ],
  imports: [
    CommonModule,
    WidgetsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgbModule,
    MDBBootstrapModulesPro,
    MaterialModule,
    Ng5SliderModule,
    SharedModule
  ],
  exports: [
    WidgetsModule,
    FormsModule,    
    ReactiveFormsModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro,
    NgbModule,
    CustomerExpandingAppointmentComponent,
    CustomerFocusAreasComponent,
    Ng5SliderModule,

  ]
})
export class CustomerSharedModule { }
