import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNeedsButtonComponent } from './cust-needs-button.component';

describe('CustNeedsButtonComponent', () => {
  let component: CustNeedsButtonComponent;
  let fixture: ComponentFixture<CustNeedsButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNeedsButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNeedsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
