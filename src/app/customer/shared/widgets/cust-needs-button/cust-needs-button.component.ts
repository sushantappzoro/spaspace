import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'cust-needs-button',
  templateUrl: './cust-needs-button.component.html',
  styleUrls: ['./cust-needs-button.component.scss']
})
export class CustNeedsButtonComponent implements OnInit {

  @Input() selected: boolean;

  @Output() selectedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  checkMark = faCheck;

  isActive: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onClick1() {
    this.selected = !this.selected;
    this.selectedChange.emit(this.selected);
  }

}
