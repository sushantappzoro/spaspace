import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cust-login-dialog',
  templateUrl: './cust-login-dialog.component.html',
  styleUrls: ['./cust-login-dialog.component.scss']
})
export class CustLoginDialogComponent implements OnInit {
  @Input() popupVisible: boolean;

  @Output() continueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() loginClick: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  continue() {
    this.continueClick.emit();
  }

  login() {
    this.loginClick.emit();
  }

}
