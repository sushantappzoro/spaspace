import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustLoginDialogComponent } from './cust-login-dialog.component';

describe('CustLoginDialogComponent', () => {
  let component: CustLoginDialogComponent;
  let fixture: ComponentFixture<CustLoginDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustLoginDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustLoginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
