import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNeedsSummaryComponent } from './cust-needs-summary.component';

describe('CustNeedsSummaryComponent', () => {
  let component: CustNeedsSummaryComponent;
  let fixture: ComponentFixture<CustNeedsSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNeedsSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNeedsSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
