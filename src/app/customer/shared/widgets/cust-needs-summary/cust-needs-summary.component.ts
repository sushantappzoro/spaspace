import { Component, OnInit, Input } from '@angular/core';
import { CustomerNeedsModel } from '../../../../shared/models/client/customer-needs.model';

@Component({
  selector: 'cust-needs-summary',
  templateUrl: './cust-needs-summary.component.html',
  styleUrls: ['./cust-needs-summary.component.scss']
})
export class CustNeedsSummaryComponent implements OnInit {
  @Input() needs: CustomerNeedsModel;

  constructor() { }

  ngOnInit() {
  }

}
