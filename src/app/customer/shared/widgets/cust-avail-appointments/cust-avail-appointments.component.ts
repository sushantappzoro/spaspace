import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { faSwimmingPool, faHotTub, faPizzaSlice, faBeer, faParking, faVolumeMute } from '@fortawesome/free-solid-svg-icons';
import { SubSink } from 'subsink';
import { ApptSearchResultModel } from '../../models/appt-search-response.model';
import { WindowService } from '../../../../core/services/window.service';
import { FacilityService } from '../../../../core/services/facility/facility.service';
import { ProviderService } from '../../../../core/services/provider/provider.service';

@Component({
  selector: 'cust-avail-appointments',
  templateUrl: './cust-avail-appointments.component.html',
  styleUrls: ['./cust-avail-appointments.component.scss']
})
export class CustAvailAppointmentsComponent implements OnInit, OnDestroy {  
  @ViewChild("locationImage") locationImage: ElementRef;
  @ViewChild("therapistImage") therapistImage: ElementRef;

  @Input() availableAppointment: ApptSearchResultModel;
  @Input() facilityImage: Blob;
  @Input() providerImage: Blob;
  @Input() index: number;

  @Output() selected: EventEmitter<any> = new EventEmitter<string>();

  private subs = new SubSink();

  private _window: Window;

  poolIcon = faSwimmingPool;
  hotTubIcon = faHotTub;
  whatev = faPizzaSlice;
  whatev2 = faBeer;

  currentRate = 4;
  max = 5;

  constructor(
    private windowService: WindowService,
    private facilityService: FacilityService,
    private providerService: ProviderService) { }

  ngOnInit() {
    console.log('ClientAvailableAppointmentsComponent- oninit');
    this._window = this.windowService.nativeWindow;

    //if (this.availableAppointment && this.availableAppointment.Facility.UID) {
    //  this.facilityService.getPhoto(this.availableAppointment.Facility.UID)
    //    .subscribe((img) => {
    //      //let objectURL = 'data:image/jpeg;base64,' + this.facilityImage;
    //      this.locationImage.nativeElement.src = this._window.window.URL.createObjectURL(img);
    //    });

    //}

    //if (this.availableAppointment && this.availableAppointment.Therapist.UID) {
    //  this.providerService.getPhoto(this.availableAppointment.Therapist.UID)
    //    .subscribe((img2) => {
    //      //let objectURL = 'data:image/jpeg;base64,' + this.facilityImage;
    //      this.therapistImage.nativeElement.src = this._window.window.URL.createObjectURL(img2);
    //    });

    //}

  }

  onClick() {
    console.log('click');
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
