import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustAvailAppointmentsComponent } from './cust-avail-appointments.component';

describe('CustAvailAppointmentsComponent', () => {
  let component: CustAvailAppointmentsComponent;
  let fixture: ComponentFixture<CustAvailAppointmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustAvailAppointmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustAvailAppointmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
