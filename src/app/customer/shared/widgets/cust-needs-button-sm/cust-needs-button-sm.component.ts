import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'cust-needs-button-sm',
  templateUrl: './cust-needs-button-sm.component.html',
  styleUrls: ['./cust-needs-button-sm.component.scss']
})
export class CustNeedsButtonSmComponent implements OnInit {

  @Input() selected: boolean;

  @Output() selectedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  isActive: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onClick1() {
    this.selected = !this.selected;
    this.selectedChange.emit(this.selected);
  }

}
