import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNeedsButtonSmComponent } from './cust-needs-button-sm.component';

describe('CustNeedsButtonSmComponent', () => {
  let component: CustNeedsButtonSmComponent;
  let fixture: ComponentFixture<CustNeedsButtonSmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNeedsButtonSmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNeedsButtonSmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
