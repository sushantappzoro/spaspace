import { Component, OnInit, Input } from '@angular/core';
import { CustomerFocusAreaModel } from '../../../../shared/models/client/customer-focus-area.model';

@Component({
  selector: 'cust-focus-area-summary',
  templateUrl: './cust-focus-area-summary.component.html',
  styleUrls: ['./cust-focus-area-summary.component.scss']
})
export class CustFocusAreaSummaryComponent implements OnInit {
  @Input() focusArea: CustomerFocusAreaModel;

  value: any;

  constructor() { }

  ngOnInit() {
  }

}
