import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustFocusAreaSummaryComponent } from './cust-focus-area-summary.component';

describe('CustFocusAreaSummaryComponent', () => {
  let component: CustFocusAreaSummaryComponent;
  let fixture: ComponentFixture<CustFocusAreaSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustFocusAreaSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustFocusAreaSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
