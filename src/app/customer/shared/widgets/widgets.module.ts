import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CustAvailAppointmentsComponent } from './cust-avail-appointments/cust-avail-appointments.component';
import { CustLoginDialogComponent } from './cust-login-dialog/cust-login-dialog.component';
import { CustNeedsButtonComponent } from './cust-needs-button/cust-needs-button.component';
import { CustNeedsButtonSmComponent } from './cust-needs-button-sm/cust-needs-button-sm.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Ng5SliderModule } from 'ng5-slider';
import { MaterialModuleModule } from '../../material-module.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DevexModuleModule } from '../../devex-module.module';
import { CustResultsBarComponent } from './cust-results-bar/cust-results-bar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CustFocusAreaSummaryComponent } from './cust-focus-area-summary/cust-focus-area-summary.component';
import { CustNeedsSummaryComponent } from './cust-needs-summary/cust-needs-summary.component';
import { CustPreferencesSummaryComponent } from './cust-preferences-summary/cust-preferences-summary.component';


@NgModule({
  declarations: [
    CustAvailAppointmentsComponent,
    CustLoginDialogComponent,
    CustNeedsButtonComponent,
    CustNeedsButtonSmComponent,
    CustResultsBarComponent,
    CustFocusAreaSummaryComponent,
    CustNeedsSummaryComponent,
    CustPreferencesSummaryComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    FontAwesomeModule,
    Ng5SliderModule,
    NgbModule,
    MaterialModuleModule,
    DevexModuleModule
  ],
  exports: [
    CustAvailAppointmentsComponent,
    CustLoginDialogComponent,
    CustNeedsButtonComponent,
    CustNeedsButtonSmComponent,
    CustResultsBarComponent,
    CustFocusAreaSummaryComponent,
    CustNeedsSummaryComponent,
    CustPreferencesSummaryComponent
  ]
})
export class WidgetsModule { }
