import { Component, OnInit, Input } from '@angular/core';
import { CustomerFacilityPreferencesModel } from '../../../../shared/models/client/customer-facility-preferences.model';
import { CustomerTherapistPreferencesModel } from '../../../../shared/models/client/customer-therapist-preferences.model';

@Component({
  selector: 'cust-preferences-summary',
  templateUrl: './cust-preferences-summary.component.html',
  styleUrls: ['./cust-preferences-summary.component.scss']
})
export class CustPreferencesSummaryComponent implements OnInit {
  @Input() facilityPreferences: CustomerFacilityPreferencesModel;
  @Input() therapistPreferences: CustomerTherapistPreferencesModel;

  constructor() { }

  ngOnInit() {
  }

}
