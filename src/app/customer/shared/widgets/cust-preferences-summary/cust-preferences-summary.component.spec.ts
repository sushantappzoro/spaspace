import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustPreferencesSummaryComponent } from './cust-preferences-summary.component';

describe('CustPreferencesSummaryComponent', () => {
  let component: CustPreferencesSummaryComponent;
  let fixture: ComponentFixture<CustPreferencesSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustPreferencesSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustPreferencesSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
