import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustResultsBarComponent } from './cust-results-bar.component';

describe('CustResultsBarComponent', () => {
  let component: CustResultsBarComponent;
  let fixture: ComponentFixture<CustResultsBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustResultsBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustResultsBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
