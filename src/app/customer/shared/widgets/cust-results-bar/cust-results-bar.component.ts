import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from '../../../../core/services/service.service';
import { CustomerSessionService } from '../../../core/services/customer-session.service';
import { CustomerService } from '../../../core/services/customer.service';
import { AuthorizeService } from '../../../../../api-authorization/authorize.service';
import { ServiceCategoryModel } from '../../../../shared/models/offerings/service-category.model';
import { CustomerModel } from '../../../../shared/models/client/customer.model';
import { of } from 'rxjs';
import { DxTreeViewComponent } from 'devextreme-angular';
import { faCalendar, faClock, faMapMarkedAlt, faDiagnoses, faTasks } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'cust-results-bar',
  templateUrl: './cust-results-bar.component.html',
  styleUrls: ['./cust-results-bar.component.scss']
})
export class CustResultsBarComponent implements OnInit, OnDestroy {
  @ViewChild(DxTreeViewComponent) treeView;

  private subs = new SubSink();

  faClock = faClock;
  faCalendar = faCalendar;
  faMapMarkedAlt = faMapMarkedAlt;
  faDiagnoses = faDiagnoses;
  faTasks = faTasks;

  serviceBoxOptions: any;
  dateBoxOptions: any;
  timeBoxOptions: any;

  tempDate: Date = new Date();
  tempTime: Date;

  treeDataSource: any;

  //categories: ServiceCategoryModel[] = [];
  categories: any[] = [
    {
      "ID": 1,
      "Name": "Massages",      
    },
    {
      "ID": "1-1",
      "ParentID": "1",
      "Name": "100 Minute Personalized Massage",
      "Description": "Including stress relieving aromatherapy, this premium experience is perfect for supporting overall wellbeing. This extended massage allows for deeper relaxation, unique customization options, and targeted therapy to address issues such as muscle pain, athletic performance, stress, and promotes overall relaxation and rejuvenation. ",
      "Duration": 100
    },
    {
      "ID": "1-2",
      "ParentID": "1",
      "Name": "80 Minute Personalized Massage",
      "Description": "Perfect for supporting overall wellbeing. This extended massage allows for deeper relaxation, unique customization options, and targeted therapy to address issues such as muscle pain, athletic performance, stress, and promotes overall relaxation and rejuvenation. ",
      "Duration": 80
    },
    {
      "ID": "1-3",
      "ParentID": "1",
      "Name": "50 Minute Personalized Massage",
      "Description": "This introductory service is a completely customized massage that addresses your specific needs based on your preferences. Your therapist may blend multiple types of massage to create a targeted or full body experience leaving you completely refreshed.",
      "Duration": 50
    },
    {
      "ID": 2,
      "Name": "Skin Care",
      "Services": [
      ]
    }
  ];

  treeBoxValue;

  currentCustomer: CustomerModel;

  isAuthenticated: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private serviceService: ServiceService,
    private customerSessionService: CustomerSessionService,
    private clientService: CustomerService,
    private authService: AuthorizeService
  ) { }


  ngOnInit() {

    this.treeDataSource = new DataSource({
      store: {
        type: "array",
        key: "id",
        data: this.categories
      }
    });

    this.loadCustomer();
    this.loadServiceCategories();

    this.authService.isAuthenticated()
      .subscribe((resp) => {
        this.isAuthenticated = resp;
      });

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  click(e) {

  }

  loadServiceCategories() {
    this.subs.add(this.serviceService.getServices()
      .subscribe(resp => {
        this.categories = resp;
      },
        error => {
          notify('Unable to process payment.', "error", 3000);
        }));
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }

  selectService(id) {
    this.customerSessionService.setSelectedService(id);

    //if (this.isAuthenticated) {
    //  console.log('is logged in');
    //  this.router.navigate(['../needs'], { relativeTo: this.route });
    //} else {
    //  console.log('is logged not in');
    //  this.popupVisible = true;
    //}
  }

  treeView_itemSelectionChanged(e) {
    console.log('treeView_itemSelectionChanged', e);
    const nodes = e.component.getNodes();
    this.treeBoxValue = this.getSelectedItemsKeys(nodes).join("");
    console.log('this.treeBoxValue', this.treeBoxValue);
  }

  syncTreeViewSelection(e) {
    if (!this.treeView) return;

    if (!this.treeBoxValue) {
      this.treeView.instance.unselectAll();
    } else {
      this.treeView.instance.selectItem(this.treeBoxValue);
    }
  }

  getSelectedItemsKeys(items) {
    var result = [],
      that = this;

    items.forEach(function (item) {
      if (item.selected) {
        result.push(item.key);
      }
      if (item.items.length) {
        result = result.concat(that.getSelectedItemsKeys(item.items));
      }
    });
    return result;
  }

  makeAsyncDataSource(jsonFile) {
    return new CustomStore({
      loadMode: "raw",
      key: "ID",
      load: function () {
        return of(jsonFile);
      }
    });
  };

}
