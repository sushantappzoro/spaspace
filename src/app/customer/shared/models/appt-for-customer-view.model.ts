export interface IApptForCustomerView {
  UID: string;
  DateTime: string;
  FacilityAndLocation: string;
  FacilityUID: string;
  FacilityName: string;
  FacilityPhotoUrl: string;
  ServiceName: string;
  TherapistName: string;
  TherapistUID: string;
  TherapistReviewDate: Date;
  TherapistReviewUID: string;
  FacilityReviewDate: Date;
  FacilityReviewUID: string;
  CheckoutSessionID: string;
  FacilityAddress: string;
}

export class ApptForCustomerViewModel {
  public UID: string;
  public DateTime: string;
  public FacilityAndLocation: string;
  public FacilityUID: string;
  public FacilityName: string;
  public FacilityPhotoUrl: string;
  public ServiceName: string;
  public TherapistName: string;
  public TherapistUID: string;
  public TherapistReviewDate: Date;
  public TherapistReviewUID: string;
  public FacilityReviewDate: Date;
  public FacilityReviewUID: string;
  public CheckoutSessionID: string;
  public FacilityAddress: string;
}
