import { ApptSearchRequestPrefModel } from "./appt-search-request-pref.model";

export class ApptSearchRequestModel {
    public SearchMethod: number;
    public AppointmentDate: Date;
    public ServiceUID: string;
    public CustomerUID: string;
    public Preferences: ApptSearchRequestPrefModel;
}
