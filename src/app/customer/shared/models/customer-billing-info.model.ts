export class CustomerBillingInfoModel {
  public StartDateTime: Date;
  public Service: string;
  public Length: number;
  public TherapistName: string;
  public FacilityName: string;
  public RatingOfProvider: number;
  public RatingOfFacility: number;
  public ServicePrice: number;
  public ServiceCharge: number;
  public AddOns: number;
  public AdditionalGratuity: number;
  public Total: number;
  public StripeCheckoutSessionId: string;
  public AppointmentUid: string;
  public CustomerUid: string;
}
