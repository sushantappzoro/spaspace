import { CustomerNeedsModel } from "../../../shared/models/client/customer-needs.model";
import { CustomerFocusAreaModel } from "../../../shared/models/client/customer-focus-area.model";
import { CustomerTherapistPreferencesModel } from "../../../shared/models/client/customer-therapist-preferences.model";
import { CustomerFacilityPreferencesModel } from "../../../shared/models/client/customer-facility-preferences.model";


export class ApptSearchRequestPrefModel {
    public CurrentNeeds: CustomerNeedsModel;
    public CurrentFocusAreas: CustomerFocusAreaModel;
    public TherapistPreferences: CustomerTherapistPreferencesModel;
    public FacilityPreferences: CustomerFacilityPreferencesModel;
}
