export class ServiceForPublicViewModel {
  public UID: string;
  public Name: string;
  public Description: string;
  public Duration: number;
  public Category: string;
  public DisplayPrice: number;
}
