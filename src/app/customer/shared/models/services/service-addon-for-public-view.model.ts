export class ServiceAddonForPublicView {
  public UID: string;
  public Name: string;
  public Price: number;
  public Category: string;
}
