import { ServiceForPublicViewModel } from "./service-for-public-view.model";

export class ServiceCategoryForPublicViewModel {
  public UID: string;
  public ID: number;
  public Name: string;
  public Services: ServiceForPublicViewModel[];
}
