import { FacilityAmenitiesModel } from "../../../shared/models/facility/facility-amenities.model";


export class ApptSearchResponseModel {
    public ResultCount: number;
    public Results: ApptSearchResultModel[];
}

export class ApptSearchResultModel {
    public Facility: ApptFacilityModel;
    public Therapist: ApptTherapistModel;
    public AppointmentDateTime: Date;
}

export class ApptFacilityModel {
    public UID: string;
    public Name: string;
    public Address: string;
    public CityStateZIP: string;
    public Amenities: FacilityAmenitiesModel;
    public AverageRating: number;
}

export class ApptTherapistModel {
    public UID: string;
    public Name: string;
    public TherapistUID: string;
    public TherapistName: string;
    public AverageRating: number;
}
