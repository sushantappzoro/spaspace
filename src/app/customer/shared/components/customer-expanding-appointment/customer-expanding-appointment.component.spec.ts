import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerExpandingAppointmentComponent } from './customer-expanding-appointment.component';

describe('CustomerExpandingAppointmentComponent', () => {
  let component: CustomerExpandingAppointmentComponent;
  let fixture: ComponentFixture<CustomerExpandingAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerExpandingAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerExpandingAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
