import { Component, OnInit, Input } from '@angular/core';
import { ApptForCustomerViewModel } from '../../models/appt-for-customer-view.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'spa-customer-expanding-appointment',
  templateUrl: './customer-expanding-appointment.component.html',
  styleUrls: ['./customer-expanding-appointment.component.scss']
})
export class CustomerExpandingAppointmentComponent implements OnInit {
  @Input() appointment: ApptForCustomerViewModel;
  @Input() upcoming: boolean;

  type: string;
  showModal: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  getFacilityImage() {
    return this.appointment.FacilityPhotoUrl;
  }

  rateTherapist() {
    this.type = 'Therapist';
    this.showModal = true;
  }

  rateFacility() {
    this.type = 'Facility';
    this.showModal = true;
  }

  getDirections() {
    const loc = 'https://www.google.com/maps/dir/?api=1&destination=' + this.appointment.FacilityAddress;
    window.open(loc, "_blank");
  }

  viewTherapistProfile() {
    this.router.navigate(['./therapist', this.appointment.TherapistUID], { relativeTo: this.route });
  }

}
