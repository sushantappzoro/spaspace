import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFocusAreasComponent } from './customer-focus-areas.component';

describe('CustomerFocusAreasComponent', () => {
  let component: CustomerFocusAreasComponent;
  let fixture: ComponentFixture<CustomerFocusAreasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFocusAreasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFocusAreasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
