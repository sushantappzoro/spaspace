export interface IExistingAppointment {
    Uid: string;
    Date: Date;
    FacilityName: string;
    ProviderName: string;
    Service: string;
}
