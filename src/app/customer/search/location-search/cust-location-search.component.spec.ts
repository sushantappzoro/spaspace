import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustLocationSearchComponent } from './cust-location-search.component';

describe('CustLocationSearchComponent', () => {
  let component: CustLocationSearchComponent;
  let fixture: ComponentFixture<CustLocationSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustLocationSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustLocationSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
