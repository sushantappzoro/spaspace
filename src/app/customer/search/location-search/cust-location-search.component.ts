import { Component, OnInit, OnDestroy, ViewChild, NgZone, ElementRef } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Subject } from 'rxjs';
import { SubSink } from 'subsink';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { FacilitySummaryModel } from '../../../shared/models/facility/facility-summary.model';
import { CustomerService } from '../../core/services/customer.service';
import { FacilitySearchService } from '../../../core/services/facility/facility-search.service';

@Component({
  selector: 'app-cust-location-search',
  templateUrl: './cust-location-search.component.html',
  styleUrls: ['./cust-location-search.component.scss']
})
export class CustLocationSearchComponent implements OnInit, OnDestroy  {

  @ViewChild('search')
  public searchElementRef: ElementRef;

  private subs = new SubSink();

  currentCustomer: CustomerModel;

  latitude: number; // = 33.3832192;
  longitude: number; // = -84.742144;

  title: string = 'AGM project';
  zoom: number;
  address: string;
  private geoCoder;

  facilities: FacilitySummaryModel[];

  refreshMap: Subject<any> = new Subject<any>();

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private clientService: CustomerService,
    private facilitySearchService: FacilitySearchService
  ) { }

  ngOnInit() {

    this.loadCustomer();

    this.mapsAPILoader.load().then(() => {
      this.getLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 18;
        });
      });
    });

    this.refreshMap.subscribe((b) => {
      this.loadFacilities();
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }

  getLocation() {

    if (navigator.geolocation) {
      //navigator.geolocation.getCurrentPosition(this.showPosition);
      //navigator.geolocation.getCurrentPosition(this.showPosition.bind(this));

      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 10;
        this.refreshMap.next('refresh');
        //this.loadFacilities();
      });

    } else {
      console.log("this browser does not support gelocation");
    }
  }

  loadFacilities() {

    //this.latitude = position.coords.latitude;
    //this.longitude = position.coords.longitude;

    //console.log("Latitude: " + latitude +
    //    " Longitude: " + longitude + " Accuracy: " + position.coords.accuracy);

    this.subs.add(this.facilitySearchService.getFacilitiesForLocation(this.latitude, this.longitude)
      .subscribe(res => {
        console.log('gotresponse', res);
        this.facilities = res.Result;
      }));

  }

  selectMarker(e) {
    console.log('selectMarker', e);
  }

  facilityClick(e) {
    console.log('facilityClick', e);
  }

}
