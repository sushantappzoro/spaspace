import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { ServiceCategoryModel } from '../../../shared/models/offerings/service-category.model';
import { ServiceService } from '../../../core/services/service.service';
import { CustomerSessionService } from '../../core/services/customer-session.service';
import { CustomerService } from '../../core/services/customer.service';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';

@Component({
  selector: 'app-search-services',
  templateUrl: './search-services.component.html',
  styleUrls: ['./search-services.component.scss']
})
export class SearchServicesComponent implements OnInit, OnDestroy {
  private subs = new SubSink();

  currentCustomer: CustomerModel;

  categories: ServiceCategoryModel[] = [];

  isAuthenticated: boolean = false;

  popupVisible: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private serviceService: ServiceService,
    private customerSessionService: CustomerSessionService,
    private clientService: CustomerService,
    private authService: AuthorizeService
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { services: ServiceCategoryModel[] }) => {
        this.categories = data.services;
      },
        error => {
          console.log(error);
        }));

    this.loadCustomer();
    //this.loadServiceCategories();

    this.authService.isAuthenticated()
      .subscribe((resp) => {
        this.isAuthenticated = resp;
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  //loadServiceCategories() {
  //  this.subs.add(this.serviceService.getServices()
  //    .subscribe(resp => {
  //      if (resp.StatusCode == 200) {
  //        //var srvs: ServiceModel[] = <ServiceModel[]> resp.Result;
  //        this.categories = resp.Result;
  //        //console.log("availabilityData", this.availabilityData);
  //      } else {
  //        notify('Unable to load services.', "error", 3000);
  //      }
  //    },
  //      error => {
  //        notify('Unable to load services.', "error", 3000);
  //      }));
  //}

  selectService(id) {
    this.customerSessionService.setSelectedService(id);

    if (this.isAuthenticated) {
      console.log('is logged in');
      this.router.navigate(['../needs'], { relativeTo: this.route });
    } else {
      console.log('is logged not in');
      this.popupVisible = true;
    }
  }

  continueClick() {
    this.router.navigate(['../needs'], { relativeTo: this.route });
  }

  loginClick() {
    this.router.navigate(['/authentication/login'], { relativeTo: this.route });
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }

}
