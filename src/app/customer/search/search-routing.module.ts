import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search.component';
import { SearchServicesComponent } from './services/search-services.component';
import { SearchPreferencesComponent } from './preferences/search-preferences.component';
import { SearchNeedsComponent } from './needs/search-needs.component';
import { SearchFocusAreasComponent } from './focus-areas/search-focus-areas.component';
import { CustProviderSearchComponent } from './provider-search/cust-provider-search.component';
import { CustLocationSearchComponent } from './location-search/cust-location-search.component';
import { CustSearchResultsComponent } from './results/cust-search-results/cust-search-results.component';
import { ServicesResolver } from '../core/resolvers/services.resolver';


const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      {
        path: 'services',
        component: SearchServicesComponent,
        resolve: {
          services: ServicesResolver
        },
      },
      {
        path: 'preferences',
        component: SearchPreferencesComponent
      },
      {
        path: 'needs',
        component: SearchNeedsComponent
      },
      {
        path: 'focus',
        component: SearchFocusAreasComponent
      },
      //{
      //    path: 'results',
      //    component: ClientResultsComponent
      //},
      {
        path: 'therapist',
        component: CustProviderSearchComponent
      },
      {
        path: 'location',
        component: CustLocationSearchComponent
      },
      {
        path: 'results',
        component: CustSearchResultsComponent
      },
      {
        path: '', redirectTo: 'services', pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
