import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchNeedsComponent } from './search-needs.component';

describe('SearchNeedsComponent', () => {
  let component: SearchNeedsComponent;
  let fixture: ComponentFixture<SearchNeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchNeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchNeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
