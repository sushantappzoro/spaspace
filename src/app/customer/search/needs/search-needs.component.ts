import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { CustomerService } from '../../core/services/customer.service';

@Component({
  selector: 'app-search-needs',
  templateUrl: './search-needs.component.html',
  styleUrls: ['./search-needs.component.scss']
})
export class SearchNeedsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  currentCustomer: CustomerModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private clientService: CustomerService) { }


  ngOnInit() {
    this.loadCustomer();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  continue() {
    this.clientService.updateCustomer(this.currentCustomer);
    this.router.navigate(['../focus'], { relativeTo: this.route });
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }

}
