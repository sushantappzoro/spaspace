import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustProviderSearchComponent } from './cust-provider-search.component';

describe('CustProviderSearchComponent', () => {
  let component: CustProviderSearchComponent;
  let fixture: ComponentFixture<CustProviderSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustProviderSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustProviderSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
