import { Component, OnInit, OnDestroy, Injectable, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { MapsAPILoader } from '@agm/core';
import { SubSink } from 'subsink';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { CustomerService } from '../../core/services/customer.service';
import { ProviderSearchService } from '../../../core/services/provider/provider-search.service';
import { FacilitySearchService } from '../../../core/services/facility/facility-search.service';
import { FacilitySummaryModel } from '../../../shared/models/facility/facility-summary.model';
import { TherapistModel } from '../../../shared/models/therapist/therapist.model';

@Component({
  selector: 'app-cust-provider-search',
  templateUrl: './cust-provider-search.component.html',
  styleUrls: ['./cust-provider-search.component.scss']
})
export class CustProviderSearchComponent implements OnInit, OnDestroy {
  @ViewChild('search')
  public searchElementRef: ElementRef;

  private subs = new SubSink();

  currentCustomer: CustomerModel;

  latitude: number; // = 33.3832192;
  longitude: number; // = -84.742144;

  title: string = 'AGM project';
  zoom: number;
  address: string;
  private geoCoder;

  facilities: FacilitySummaryModel[];

  refreshMap: Subject<any> = new Subject<any>();

  therapists: TherapistModel[];

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private clientService: CustomerService,
    private providerSearchService: ProviderSearchService,
    private facilitySearchService: FacilitySearchService
  ) { }

  ngOnInit() {
    this.loadCustomer();

    this.mapsAPILoader.load().then(() => {
      this.getLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 18;
        });
      });
    });

    this.refreshMap.subscribe((b) => {
      this.loadTherapists();
    });

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }

  getLocation() {

    if (navigator.geolocation) {
      //navigator.geolocation.getCurrentPosition(this.showPosition);
      //navigator.geolocation.getCurrentPosition(this.showPosition.bind(this));

      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 10;
        this.refreshMap.next('refresh');
        //this.loadFacilities();
      });

    } else {
      console.log("this browser does not support gelocation");
    }
  }


  loadTherapists() {

    //this.latitude = position.coords.latitude;
    //this.longitude = position.coords.longitude;

    //console.log("Latitude: " + latitude +
    //    " Longitude: " + longitude + " Accuracy: " + position.coords.accuracy);

    this.subs.add(this.providerSearchService.getTherapistsForLocation(this.latitude, this.longitude)
      .subscribe(res => {
        this.therapists = res.Result;
      }));

  }

  loadFacilities() {

    //this.latitude = position.coords.latitude;
    //this.longitude = position.coords.longitude;

    //console.log("Latitude: " + latitude +
    //    " Longitude: " + longitude + " Accuracy: " + position.coords.accuracy);

    this.subs.add(this.facilitySearchService.getFacilitiesForLocation(this.latitude, this.longitude)
      .subscribe(res => {
        console.log('gotresponse', res);
        this.facilities = res.Result;
      }));

  }

  //showPosition(position) {

  //    var latitude = position.coords.latitude;
  //    var longitude = position.coords.longitude;

  //    console.log("Latitude: " + latitude +
  //        " Longitude: " + longitude + " Accuracy: " + position.coords.accuracy);

  //    this.subs.add(this.providerSearchService.getTherapistsForLocation(latitude, longitude)
  //        .subscribe(res => {
  //            this.therapists = res.Result;
  //        }));

  //}


  selectMarker(e) {
    console.log('selectMarker', e);
  }

  therapistClick(e) {
    console.log('facilityClick', e);
  }
}
