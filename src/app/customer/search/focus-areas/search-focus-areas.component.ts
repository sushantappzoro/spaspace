import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { Options } from 'ng5-slider';

import { CustomerModel } from '../../../shared/models/client/customer.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../../core/services/customer.service';

@Component({
  selector: 'app-search-focus-areas',
  templateUrl: './search-focus-areas.component.html',
  styleUrls: ['./search-focus-areas.component.scss']
})
export class SearchFocusAreasComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  currentCustomer: CustomerModel;

  popupVisible: boolean = false;

  chosenPressure: number;

  pressures: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 0, legend: 'Let the Therapist Decide' },
      { value: 1, legend: 'Light' },
      { value: 2, legend: 'Light Medium' },
      { value: 3, legend: 'Medium' },
      { value: 4, legend: 'Medium Deep' },
      { value: 5, legend: 'Deep' }
    ]
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private clientService: CustomerService
  ) { }

  ngOnInit() {
    this.loadCustomer();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  continue() {

    this.clientService.updateCustomer(this.currentCustomer);

    //this.popupVisible = true;

    this.router.navigate(['../preferences'], { relativeTo: this.route });
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }
}
