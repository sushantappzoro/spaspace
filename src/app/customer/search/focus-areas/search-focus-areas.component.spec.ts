import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFocusAreasComponent } from './search-focus-areas.component';

describe('SearchFocusAreasComponent', () => {
  let component: SearchFocusAreasComponent;
  let fixture: ComponentFixture<SearchFocusAreasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFocusAreasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFocusAreasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
