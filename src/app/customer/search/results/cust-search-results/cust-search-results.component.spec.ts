import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustSearchResultsComponent } from './cust-search-results.component';

describe('CustSearchResultsComponent', () => {
  let component: CustSearchResultsComponent;
  let fixture: ComponentFixture<CustSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
