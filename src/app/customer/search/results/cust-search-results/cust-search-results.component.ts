import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { AppointmentSearchService } from '../../../core/services/appointment-search.service';
import { FacilitySearchService } from '../../../../core/services/facility/facility-search.service';
import { FacilitySummaryModel } from '../../../../shared/models/facility/facility-summary.model';
import { ProviderSearchService } from '../../../../core/services/provider/provider-search.service';
import { TherapistModel } from '../../../../shared/models/therapist/therapist.model';

@Component({
  selector: 'app-cust-search-results',
  templateUrl: './cust-search-results.component.html',
  styleUrls: ['./cust-search-results.component.scss']
})
export class CustSearchResultsComponent implements OnInit, OnDestroy {

  facilities: FacilitySummaryModel[];
  providers: TherapistModel[];

  private subs = new SubSink();

  latitude: number = 33.3832192;
  longitude: number = -84.742144;

  constructor(
    private appointmentService: AppointmentSearchService,
    private providerSearchService: ProviderSearchService,
    private facilitySearchService: FacilitySearchService
  ) { }

  ngOnInit() {

    this.loadFacilities();
    this.loadTherapists();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  //submitSearch() {
  //  console.log('submitSearch');
  //  this.subs.add(this.appointmentService.tempSearch() //(this.buildRequest(this.currentCustomer))
  //    .subscribe(res => {
  //      console.log('submitSearch - response', res.Result);
  //      this.results = res.Result;
  //    }))
  //}

  loadFacilities() {

    this.subs.add(this.facilitySearchService.getFacilitiesForLocation(this.latitude, this.longitude)
      .subscribe(res => {
        console.log('gotresponse', res);
        this.facilities = res.Result;
      }));

  }

  loadTherapists() {

    this.subs.add(this.providerSearchService.getTherapistsForLocation(this.latitude, this.longitude)
      .subscribe(res => {
        this.providers = res.Result;
      }));

  }

}
