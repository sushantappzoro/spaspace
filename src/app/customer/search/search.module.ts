import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SearchFocusAreasComponent } from './focus-areas/search-focus-areas.component';
import { CustLocationSearchComponent } from './location-search/cust-location-search.component';
import { SearchNeedsComponent } from './needs/search-needs.component';
import { SearchPreferencesComponent } from './preferences/search-preferences.component';
import { SearchServicesComponent } from './services/search-services.component';
import { CustProviderSearchComponent } from './provider-search/cust-provider-search.component';
import { CustSearchResultsComponent } from './results/cust-search-results/cust-search-results.component';
import { CustomerSharedModule } from '../shared/customer-shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../../material-module.module';
import { DevexModuleModule } from '../devex-module.module';
import { AgmCoreModule } from '@agm/core';
import { Ng5SliderModule } from 'ng5-slider';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    SearchComponent,
    SearchFocusAreasComponent,
    CustLocationSearchComponent,
    SearchNeedsComponent,
    SearchPreferencesComponent,
    SearchServicesComponent,
    CustProviderSearchComponent,
    CustSearchResultsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    MaterialModule,
    DevexModuleModule,
    AgmCoreModule,
    Ng5SliderModule,
    SearchRoutingModule,
    CustomerSharedModule,
    SharedModule

  ]
})
export class SearchModule { }
