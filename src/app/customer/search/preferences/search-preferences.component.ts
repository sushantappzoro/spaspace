import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Options } from 'ng5-slider';
import { SubSink } from 'subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { ClientService } from '../../../core/services/client/client.service';
import { CustomerSessionService } from '../../core/services/customer-session.service';

@Component({
  selector: 'app-search-preferences',
  templateUrl: './search-preferences.component.html',
  styleUrls: ['./search-preferences.component.scss']
})
export class SearchPreferencesComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  currentCustomer: CustomerModel;

  preferences: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 1, legend: 'No Preference' },
      { value: 2, legend: 'Nice to Have' },
      { value: 3, legend: 'Must Have' },
    ]
  };

  twoPreferences: Options = {
    showTicksValues: true,
    stepsArray: [
      { value: 2, legend: 'Nice to Have' },
      { value: 3, legend: 'Must Have' },
    ]
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private clientService: ClientService,
    private sessionService: CustomerSessionService
  ) { }

  ngOnInit() {
    this.loadCustomer();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  continue() {
    this.clientService.updateCustomer(this.currentCustomer);
    //this.router.navigate(['../results/' + this.sessionService.getSearchType()], { relativeTo: this.route });
    this.router.navigate(['../results'], { relativeTo: this.route });
  }

  loadCustomer() {
    this.subs.add(this.clientService.getCurrentCustomer()
      .subscribe(cust => {
        this.currentCustomer = cust
      }));
  }
}
