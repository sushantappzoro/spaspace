import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPreferencesComponent } from './search-preferences.component';

describe('SearchPreferencesComponent', () => {
  let component: SearchPreferencesComponent;
  let fixture: ComponentFixture<SearchPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPreferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
