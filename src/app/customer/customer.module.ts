import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { BookModule } from './book/book.module';
import { SearchModule } from './search/search.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { HomeComponent } from './home/home.component';
import { CustomerSharedModule } from './shared/customer-shared.module';
import { MaterialModuleModule } from './material-module.module';
import { DevexModuleModule } from './devex-module.module';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../shared/shared.module';
import { ServicesResolver } from './core/resolvers/services.resolver';
import { CustomerAppointmentsComponent } from './appointments/customer-appointments.component';
import { CustomerPreferencesComponent } from './profile/customer-preferences/customer-preferences.component';
import { CustomerTherapistProfileComponent } from './appointments/customer-therapist-profile/customer-therapist-profile.component';
import { CustomerBillingComponent } from './billing/customer-billing.component';


@NgModule({
  declarations: [
    CustomerComponent,
    HomeComponent,
    CustomerAppointmentsComponent,
    CustomerPreferencesComponent,
    CustomerTherapistProfileComponent,
    CustomerBillingComponent,

  ],
  imports: [
    CommonModule,
    AgmCoreModule,
    CustomerRoutingModule,
    BookModule,
    SearchModule,
    DashboardModule,
    CustomerSharedModule,
    MaterialModuleModule,
    DevexModuleModule,
    SharedModule,
  ],
  providers: [
    //ServicesResolver
  ]
})
export class CustomerModule { }
