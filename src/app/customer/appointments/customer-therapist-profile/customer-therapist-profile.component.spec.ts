import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerTherapistProfileComponent } from './customer-therapist-profile.component';

describe('CustomerTherapistProfileComponent', () => {
  let component: CustomerTherapistProfileComponent;
  let fixture: ComponentFixture<CustomerTherapistProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerTherapistProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTherapistProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
