import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpaTherapistProfileForPublicModel } from '../../../../../projects/spaspace-lib/src/shared/models/therapist/spa-therapist-profile-for-public.model';

@Component({
  selector: 'app-customer-therapist-profile',
  templateUrl: './customer-therapist-profile.component.html',
  styleUrls: ['./customer-therapist-profile.component.scss']
})
export class CustomerTherapistProfileComponent implements OnInit {

  therapistProfile: SpaTherapistProfileForPublicModel;

  constructor(private route: ActivatedRoute,) { }

  ngOnInit() {
    this.therapistProfile = this.route.snapshot.data['therapistProfile'];
    console.log('SpaTherapistProfileForPublicModel', this.therapistProfile);
  }

}
