import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

import { ApptForCustomerViewModel } from '../shared/models/appt-for-customer-view.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customer-appointments',
  templateUrl: './customer-appointments.component.html',
  styleUrls: ['./customer-appointments.component.scss']
})
export class CustomerAppointmentsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  upcomingAppointments: ApptForCustomerViewModel[];
  pastAppointments: ApptForCustomerViewModel[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    //private authService: AuthorizeService,
  ) {
    console.log('CustomerAppointmentsComponent constructor');
  }

  ngOnInit() {

    console.log('CustomerAppointmentsComponent ngOnInit');

    this.upcomingAppointments = this.route.snapshot.data['upcoming'];
    this.pastAppointments = this.route.snapshot.data['past'];

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
