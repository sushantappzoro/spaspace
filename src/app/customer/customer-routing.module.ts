import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { CustomerAppointmentsComponent } from './appointments/customer-appointments.component';
import { UpcomingAppointmentResolver } from './core/resolvers/upcoming-appointment.resolver';
import { PastAppointmentResolver } from './core/resolvers/past-appointment.resolver';
import { UserProfileComponent } from '../shared/components/user/user-profile/user-profile.component';
import { UserProfileResolver } from '../core/resolvers/user/user-profile.resolver';
import { CustomerPreferencesComponent } from './profile/customer-preferences/customer-preferences.component';
import { CustomerPreferencesResolver } from './core/resolvers/customer-preferences.resolver';
import { SpaTherapistProfileDetailComponent } from '../../../projects/spaspace-lib/src/shared/components/therapist/spa-therapist-profile-detail/spa-therapist-profile-detail.component';
import { SpaPublicTherapistProfileResolver } from '../../../projects/spaspace-lib/src/core/resolvers/public/spa-public-therapist-profile.resolver';
import { CustomerTherapistProfileComponent } from './appointments/customer-therapist-profile/customer-therapist-profile.component';
import { CustomerBillingComponent } from './billing/customer-billing.component';
import { CustomerBillingInfoResolver } from './core/resolvers/customer-billing-info.resolver';


const routes: Routes = [
  {
    path: '',
    component: CustomerComponent,
    children: [
      {
        path: 'home',
        component: DashboardComponent
      },
      //{
      //  path: 'dashboard',
      //  component: DashboardComponent
      //},
      {
        path: 'profile',
        component: UserProfileComponent,
        resolve: {
          userProfile: UserProfileResolver
        },
        data: {
          breadcrumb: 'User Profile'
        }
      },
      {
        path: ':uid/profile',
        component: UserProfileComponent,
        resolve: {
          userProfile: UserProfileResolver
        },
        data: {
          breadcrumb: 'User Profile'
        }
      },
      {
        path: 'appointments',
        component: CustomerAppointmentsComponent,
        resolve: {
          upcoming: UpcomingAppointmentResolver,
          past: PastAppointmentResolver
        }
      },
      {
        path: 'appointments/therapist/:therapistUID',
        component: SpaTherapistProfileDetailComponent,
        resolve: {
          therapistProfile: SpaPublicTherapistProfileResolver
        }
      },
      {
        path: ':uid/appointments',
        component: CustomerAppointmentsComponent,
        resolve: {
          upcoming: UpcomingAppointmentResolver,
          past: PastAppointmentResolver
        }
      },
      {
        path: 'preferences',
        component: CustomerPreferencesComponent,
        resolve: {
          preferences: CustomerPreferencesResolver
        }
      },
      {
        path: ':uid/preferences',
        component: CustomerPreferencesComponent,
        resolve: {
          preferences: CustomerPreferencesResolver
        }
      },
      {
        path: 'billing',
        component: CustomerBillingComponent,
        resolve: {
          billinginfo: CustomerBillingInfoResolver
        }
      },
      //{
      //  path: 'search',
      //  loadChildren: () => import('./search/search.module').then(m => m.SearchModule)
      //},
      {
        path: '',
        redirectTo: 'appointments',
      },
      {
        path: '**',
        redirectTo: 'appointments',
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CustomerRoutingModule { }
