import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { Router, RoutesRecognized, ActivatedRoute } from '@angular/router';
import { CustomerService } from './core/services/customer.service';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { pairwise, filter, map, take } from 'rxjs/operators';
import { ProviderNavService } from '../provider/core/services/provider-nav.service';
import { MenuListItemModel } from '../shared/models/common/menu-list-item.model';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  private userSub: string = '';


  homePage = '/therapist';

  navigation: MenuListItemModel[] = [
    //{ id: 1, text: "Dashboard", icon: "fas fa-home", link: "./dashboard", status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    { id: 2, text: "Appointments", icon: "fas fa-calendar", link: "./appointments",  disabled: false },
    { id: 3, text: "Billing Info", icon: "fas fa-wallet", link: "./billing",  disabled: false },
    //{ id: 3, text: "Schedule", icon: "fas fa-clock", link: "./appointments", status: ['Approved'], disabled: false },
    //{ id: 4, text: "Spas", icon: "fas fa-clock", link: "./facilities", status: ['Approved'], disabled: false },
    //{ id: 5, text: "Reviews", icon: "fas fa-clock", link: "./reviews", status: ['Approved'], disabled: false },
    //{ id: 6, text: "Transactions", icon: "fas fa-clock", link: "./account", status: ['Approved'], disabled: false },
    //{ id: 7, text: "Resources", icon: "fas fa-clock", link: "./resources", status: ['Approved'], disabled: false },
    { id: 8, text: "Profile", icon: " fas fa-user", link: "./profile", status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    { id: 9, text: "Preferences", icon: " fas fa-check", link: "./preferences", status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    //{ id: 5, text: "Account", icon: "fas fa-dollar-sign", link: "./account", status: ['Approved'], disabled: false},
    //{ id: 6, text: "Standards", icon: "fas fa-certificate", link: "./standards", status: ['Approved'], disabled: false },
    //{ id: 2, text: "My Profile", icon: " fas fa-user", link: "./profile", status: ['New', 'Pending Approval', 'Approved'], disabled: false },
  ];

  constructor(
    private router: Router,
    private clientService: CustomerService,
    private authService: AuthorizeService,
    private route: ActivatedRoute,
    private navbarService: ProviderNavService
  ) { }

  ngOnInit() {

    // saving return url for logging in so the customer will return to the right page.
    // doing it here because customer is the only user type that can enter their
    // module without authenticating

    this.checkLogin();

    this.subs.add(this.router.events
      .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
      .subscribe((events: RoutesRecognized[]) => {
        var prevUrl = events[0].urlAfterRedirects;
        //console.log('ClientComponent - setting last url-' + prevUrl);
        sessionStorage.setItem('lastUrl', prevUrl);
      }));

    this.subs.add(this.authService.getUser()
      .pipe(
        take(1),
        map(u => u && u.sub))
      .subscribe(sub => {
        console.log('ClientComponent.getuser this.userSub=' + this.userSub + ' sub=' + sub);
        if (sub && this.userSub != sub) {
          this.userSub = sub;
          //this.clientService.getCustomer(sub);
        }
      }));

    //this.subs.add(this.clientService.getCurrentCustomer()
    //    .subscribe(cust => {
    //        console.log('ClientSearchComponent - get cust', cust);
    //    }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private async checkLogin() {
    const si = this.authService.checkSignedIn();
    const signin = this.authService.signInSilent(null);
    return await this.authService.checkSignedIn();
  }

}
