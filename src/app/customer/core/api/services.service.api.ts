import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ServiceForPublicViewModel } from '../../shared/models/services/service-for-public-view.model';
import { ServiceCategoryForPublicViewModel } from '../../shared/models/services/service-category-for-public-view.model';
import { configSettings } from '../../../app.config';
import { ServiceAddonForPublicView } from '../../shared/models/services/service-addon-for-public-view.model';

@Injectable({
  providedIn: 'root'
})
export class ServicesServiceApi {

  constructor(private http: HttpClient) { }

  public getCategories(): Observable<ServiceCategoryForPublicViewModel[]> {
    return this.http.get<ServiceCategoryForPublicViewModel[]>(configSettings.WebAPI_URL + '/public/ServiceCategories');
  }

  public getServices(categoryID: string): Observable<ServiceForPublicViewModel[]> {
    return this.http.get<ServiceForPublicViewModel[]>(configSettings.WebAPI_URL + '/public/ServiceCategories/' + categoryID + '/services');
  }

  public getServiceAddons(categoryID: string): Observable<ServiceAddonForPublicView[]> {
    return this.http.get<ServiceAddonForPublicView[]>(configSettings.WebAPI_URL + '/public/ServiceCategories/' + categoryID + ' / serviceaddons');
  }

}
