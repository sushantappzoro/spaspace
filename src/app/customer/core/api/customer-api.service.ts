import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { ApptForCustomerViewModel } from '../../shared/models/appt-for-customer-view.model';
import { SpaCustomerPreferencesModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-preferences.model';
import { SpaCustomerReviewForViewModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-review-for-view.model';
import { SpaCustomerReviewForAddModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-review-for-add.model';
import { CustomerBillingInfoModel } from '../../shared/models/customer-billing-info.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerApiService {

  constructor(private http: HttpClient) { }

  public getUpcomingAppointments(customerUID: string): Observable<ApptForCustomerViewModel[]> {
    return this.http.get<ApptForCustomerViewModel[]>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/upcomingappointments');
  }

  public getPastAppointments(customerUID: string): Observable<ApptForCustomerViewModel[]> {
    return this.http.get<ApptForCustomerViewModel[]>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/pastappointments');
  }

  public getPreferences(customerUID: string): Observable<SpaCustomerPreferencesModel> {
    return this.http.get<SpaCustomerPreferencesModel>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/Preferences');
  }

  public editPreferences(customerUID: string, preferences: SpaCustomerPreferencesModel): Observable<ApptForCustomerViewModel> {
    return this.http.put<ApptForCustomerViewModel>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/Preferences', preferences);
  }

  public reviewFacility(customerUID: string, review: SpaCustomerReviewForAddModel): Observable<SpaCustomerReviewForViewModel> {
    return this.http.post<SpaCustomerReviewForViewModel>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/facilityreviews', review);
  }

  public reviewTherapist(customerUID: string, review: SpaCustomerReviewForAddModel): Observable<SpaCustomerReviewForViewModel> {
    return this.http.post<SpaCustomerReviewForViewModel>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/therapistreviews', review);
  }

  public getBillingInfo(customerUID: string): Observable<CustomerBillingInfoModel[]> {
    return this.http.get<any>(configSettings.WebAPI_URL + '/auth/Customers/' + customerUID + '/billinginfo');
  }

}
