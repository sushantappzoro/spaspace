import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of, EMPTY } from "rxjs";
import { catchError, switchMap, mergeMap, take, map } from "rxjs/operators";
import { ApptForCustomerViewModel } from '../../shared/models/appt-for-customer-view.model';
import { CustomerService } from '../services/customer.service';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class PastAppointmentResolver implements Resolve<ApptForCustomerViewModel[]> {

  constructor(
    private authService: AuthorizeService,
    private apptService: CustomerService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ApptForCustomerViewModel[]> | Observable<any> {
    
    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          var uid = '';
          if (user && user.sub) {
            uid = user.sub;
          } else {
            uid = route.params['uid'];
          }
          return this.apptService.getPastAppointments(uid).pipe(
            map(resp => resp),
            catchError(error => {
              console.log(error);
              this.router.navigate(['/home']);
              return of(null);
            })
          )
        })
    );

  }

}
