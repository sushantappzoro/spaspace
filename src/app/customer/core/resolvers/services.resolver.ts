import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { ServiceService } from "../../../core/services/service.service";
import { ServiceCategoryModel } from "../../../shared/models/offerings/service-category.model";

@Injectable({
  providedIn: 'root'
})
export class ServicesResolver implements Resolve<ServiceCategoryModel[]> {

  constructor(
    private servicesService: ServiceService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ServiceCategoryModel[]> {
    return this.servicesService.getServices().pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
