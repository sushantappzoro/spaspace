import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Router, Resolve } from '@angular/router';
import { AuthorizeService } from 'src/api-authorization/authorize.service';
import { CustomerService } from '../services/customer.service';
import { take, switchMap, map, catchError } from 'rxjs/operators';
import { CustomerBillingInfoModel } from '../../shared/models/customer-billing-info.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerBillingInfoResolver implements Resolve<any> {

  constructor(
    private authService: AuthorizeService,
    private customerService: CustomerService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CustomerBillingInfoModel[]> | Observable<any> {

    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          var uid = '';
          uid = user.sub;
          return this.customerService.getBillingInfo(uid).pipe(
            map(resp => resp),
            catchError(error => {
              return of(null);
            })
          )
        })
      );
    }
}
