import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of, EMPTY } from "rxjs";
import { catchError, switchMap, mergeMap, take, map } from "rxjs/operators";

import { CustomerService } from '../services/customer.service';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { SpaCustomerPreferencesModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-preferences.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerPreferencesResolver implements Resolve<SpaCustomerPreferencesModel> {

  constructor(
    private authService: AuthorizeService,
    private customerService: CustomerService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpaCustomerPreferencesModel> | Observable<any> {

    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          var uid = '';
          //if (user && user.sub) {
          uid = user.sub;
          //} else {
          //  uid = route.params['uid'];
          //}
          return this.customerService.getPreferences(uid).pipe(
            map(resp => resp),
            catchError(error => {              
              return of(null);
            })
          )
        })
      );
  }

}
