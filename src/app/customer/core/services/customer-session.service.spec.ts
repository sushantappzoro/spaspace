import { TestBed } from '@angular/core/testing';

import { CustomerSessionService } from './customer-session.service';

describe('CustomerSessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerSessionService = TestBed.get(CustomerSessionService);
    expect(service).toBeTruthy();
  });
});
