import { Injectable } from '@angular/core';
import { ServicesServiceApi } from '../api/services.service.api';
import { ServiceCategoryForPublicViewModel } from '../../shared/models/services/service-category-for-public-view.model';
import { Observable } from 'rxjs';
import { ServiceForPublicViewModel } from '../../shared/models/services/service-for-public-view.model';
import { ServiceAddonForPublicView } from '../../shared/models/services/service-addon-for-public-view.model';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private api: ServicesServiceApi) { }

  public getCategories(): Observable<ServiceCategoryForPublicViewModel[]> {
    return this.api.getCategories();
  }

  public getServices(categoryID: string): Observable<ServiceForPublicViewModel[]> {
    return this.api.getServices(categoryID);
  }

  public getServiceAddons(categoryID: string): Observable<ServiceAddonForPublicView[]> {
    return this.api.getServiceAddons(categoryID);
  }

}
