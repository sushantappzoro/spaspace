import { TestBed } from '@angular/core/testing';

import { CustSearchService } from './cust-search.service';

describe('CustSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustSearchService = TestBed.get(CustSearchService);
    expect(service).toBeTruthy();
  });
});
