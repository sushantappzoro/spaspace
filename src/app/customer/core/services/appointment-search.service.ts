import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { ApptSearchRequestModel } from '../../shared/models/appt-search-request.model';
import { ApptSearchRequestPrefModel } from '../../shared/models/appt-search-request-pref.model';
import { CustomerFocusAreaModel } from '../../../shared/models/client/customer-focus-area.model';
import { CustomerNeedsModel } from '../../../shared/models/client/customer-needs.model';
import { CustomerFacilityPreferencesModel } from '../../../shared/models/client/customer-facility-preferences.model';
import { CustomerTherapistPreferencesModel } from '../../../shared/models/client/customer-therapist-preferences.model';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';

export interface StoreState {
  currentSearchRequest: ApptSearchRequestModel;
}

export enum CustomersStoreActions {
  GetCurrentSearchRequest = 'get-current-search-request',
  SetCurrentSearchRequest = 'set-current-search-request'
}

@Injectable({
  providedIn: 'root'
})
export class AppointmentSearchService extends ObservableStore<StoreState> {

  constructor(private http: HttpClient) {
    super({ trackStateHistory: false, logStateChanges: false });
    //console.log('ClientService - constructor');
  }

  getCurrentSearchRequest() {
    console.log('ClientService.getCustomer');
    const state = this.getState();

    if (state && state.currentSearchRequest) {
      return of(state.currentSearchRequest);
    } else {

      var currentSearchRequest = this.initGetCurrentSearchRequest()

      console.log('ClientService - set state on dummy customer', currentSearchRequest);
      this.setState({ currentSearchRequest }, CustomersStoreActions.SetCurrentSearchRequest);
      return of(currentSearchRequest);
    }
  }

  private initGetCurrentSearchRequest(): ApptSearchRequestModel {
    let currentSearchRequest = new ApptSearchRequestModel();
    currentSearchRequest.Preferences = new ApptSearchRequestPrefModel();
    currentSearchRequest.Preferences.CurrentFocusAreas = new CustomerFocusAreaModel();
    currentSearchRequest.Preferences.CurrentNeeds = new CustomerNeedsModel();
    currentSearchRequest.Preferences.FacilityPreferences = new CustomerFacilityPreferencesModel();
    currentSearchRequest.Preferences.TherapistPreferences = new CustomerTherapistPreferencesModel();
    return currentSearchRequest;
  }

  setCurrentSearchRequest(currentSearchRequest: ApptSearchRequestModel) {
    const state = this.getState();
    this.setState({ currentSearchRequest }, CustomersStoreActions.SetCurrentSearchRequest);
  }

  public submitRequest(request: ApptSearchRequestModel): Observable<ApiResponseModel> {
    return this.http.post<ApiResponseModel>(configSettings.WebAPI_URL + '/AppointmentSearch', null); // JSON.stringify(request));
  };

  public tempSearch(): Observable<ApiResponseModel> {
    return this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/AppointmentSearch/GetTemp');
  };
}
