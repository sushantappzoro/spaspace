import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';
import { ObservableStore } from '@codewithdan/observable-store';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { IUser } from '../../../../api-authorization/authorize.service';
import { CustomerProfileModel } from '../../../shared/models/client/customer-profile.model';
import { CustomerFocusAreaModel } from '../../../shared/models/client/customer-focus-area.model';
import { CustomerTherapistPreferencesModel } from '../../../shared/models/client/customer-therapist-preferences.model';
import { CustomerFacilityPreferencesModel } from '../../../shared/models/client/customer-facility-preferences.model';
import { CustomerNeedsModel } from '../../../shared/models/client/customer-needs.model';
import { CustomerApiService } from '../api/customer-api.service';
import { ApptForCustomerViewModel } from '../../shared/models/appt-for-customer-view.model';
import { SpaCustomerPreferencesModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-preferences.model';
import { SpaCustomerReviewForAddModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-review-for-add.model';
import { SpaCustomerReviewForViewModel } from '../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-review-for-view.model';
import { CustomerBillingInfoModel } from '../../shared/models/customer-billing-info.model';


export interface StoreState {
  currentCustomer: CustomerModel;
}

export enum CustomersStoreActions {
  GetCustomers = 'get_customers',
  GetCustomer = 'get_customer',
  SetCustomer = 'set_customer'
}

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends ObservableStore<StoreState> {

  user: IUser = null;

  constructor(
    private http: HttpClient,
    private api: CustomerApiService
  ) {
    super({ trackStateHistory: false, logStateChanges: false });
    console.log('ClientService - constructor');
  }

  public getUpcomingAppointments(customerUID: string): Observable<ApptForCustomerViewModel[]> {
    return this.api.getUpcomingAppointments(customerUID);
  }

  public getPastAppointments(customerUID: string): Observable<ApptForCustomerViewModel[]> {
    return this.api.getPastAppointments(customerUID);
  }

  public getPreferences(customerUID: string): Observable<SpaCustomerPreferencesModel> {
    return this.api.getPreferences(customerUID);
  }

  public editPreferences(customerUID: string, preferences: SpaCustomerPreferencesModel): Observable<ApptForCustomerViewModel> {
    return this.api.editPreferences(customerUID, preferences);
  }

  public reviewFacility(customerUID: string, review: SpaCustomerReviewForAddModel): Observable<SpaCustomerReviewForViewModel> {
    return this.api.reviewFacility(customerUID, review);
  }

  public reviewTherapist(customerUID: string, review: SpaCustomerReviewForAddModel): Observable<SpaCustomerReviewForViewModel> {
    return this.api.reviewTherapist(customerUID, review);
  }

  public getBillingInfo(customerUID: string): Observable<CustomerBillingInfoModel[]> {
    return this.api.getBillingInfo(customerUID);
  }

  public getCustomer(id: string) {
    console.log('ClientService.getCustomer');
    const state = this.getState();

    if (state && state.currentCustomer) {
      console.log('ClientService - customer from state', state.currentCustomer);
    } else {
      console.log('ClientService - no current state');
    }

    if (state && state.currentCustomer && state.currentCustomer.UserUID === id) {
      return of(state.currentCustomer);
    } else {
      if (!id || id.length == 0) {
        var currentCustomer = this.initCustomer()
        console.log('ClientService - set state on dummy customer', currentCustomer);
        this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
        return of(currentCustomer);
      } else {
        this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Customer/GetByUserID/' + id)
          .pipe(map(u => u && u.Result))
          .subscribe(currentCustomer => {
            console.log('ClientService - set state on actual customer', currentCustomer);
            this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
            return of(currentCustomer);
          });
      }
    }
  }

  updateCustomer(currentCustomer: CustomerModel) {
    //const state = this.getState();
    this.setState({ currentCustomer }, CustomersStoreActions.SetCustomer);
  }

  getCurrentCustomer() {
    console.log('ClientService.getCustomer');
    const state = this.getState();

    if (state && state.currentCustomer) {
      console.log('ClientService - customer from state', state.currentCustomer);
    } else {
      console.log('ClientService - no current state');
    }

    if (state && state.currentCustomer) {
      return of(state.currentCustomer);
    } else {

      var currentCustomer = this.initCustomer()

      console.log('ClientService - set state on dummy customer', currentCustomer);
      this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
      return of(currentCustomer);
    }
  }

  private initCustomer(): CustomerModel {
    let currentCustomer = new CustomerModel();
    currentCustomer.Profile = new CustomerProfileModel();
    currentCustomer.Profile.CurrentFocusArea = new CustomerFocusAreaModel();
    currentCustomer.Profile.CurrentNeeds = new CustomerNeedsModel();
    currentCustomer.Profile.FacilityPreferences = new CustomerFacilityPreferencesModel();
    currentCustomer.Profile.TherapistPreferences = new CustomerTherapistPreferencesModel();
    return currentCustomer;
  }
}
