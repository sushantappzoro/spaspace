import { TestBed } from '@angular/core/testing';

import { CustLocationSearchService } from './cust-location-search.service';

describe('CustLocationSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustLocationSearchService = TestBed.get(CustLocationSearchService);
    expect(service).toBeTruthy();
  });
});
