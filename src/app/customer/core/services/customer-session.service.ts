import { Injectable } from '@angular/core';
import { ApptSearchRequestModel } from '../../shared/models/appt-search-request.model';

export interface StoreState {
    currentSearchRequest: ApptSearchRequestModel;
}

@Injectable({
  providedIn: 'root'
})
export class CustomerSessionService {

    constructor() { }

    getSearchType() {
        return sessionStorage.getItem('searchType');        
    }

    setSearchType(value: string) {
        console.log('setSearchType', value);
        return sessionStorage.setItem('searchType', value);
    }

    getSelectedService() {
        return sessionStorage.getItem('selectedService');
    }

    setSelectedService(value: string) {
        return sessionStorage.setItem('selectedService', value);
    }
}

export enum CustomerSearchType {
    ByDate = "bydate",
    ByTherapist = "bytherapist",
    ByLocation = "bylocation"
}
