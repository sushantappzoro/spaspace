import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard.component';
import { AdminClientsComponent } from './clients/admin-clients.component';
import { AdminFacilitiesComponent } from './facilities/admin-facilities.component';
import { AdminTherapistsComponent } from './therapists/admin-therapists.component';
import { AdminTherapistApprovalDetailComponent } from './therapists/therapist-approvals/admin-therapist-approval-detail/admin-therapist-approval-detail.component';
import { AdminTherapistDetailResolver } from './core/resolvers/admin-therapist-detail.resolver';
import { AdminTherapistDetailComponent } from './therapists/therapist-detail/admin-therapist-detail.component';
import { AdminTherapistApprovalDetailResolver } from './core/resolvers/admin-therapist-approval-detail.resolver';
import { AdminServicesComponent } from './services/admin-services.component';
import { AdminServiceCategoryResolver } from './core/resolvers/admin-service-category.resolver';
import { FacilityListResolver } from '../core/resolvers/facility/facility-list.resolver';
import { AdminFacilityDetailComponent } from './facilities/facility-detail/admin-facility-detail.component';
import { AdminFacilityDetailResolver } from './core/resolvers/admin-facility-detail.resolver';
import { AdminFacilityApprovalDetailResolver } from './core/resolvers/admin-facility-approval-detail.resolver';
import { AdminErrorComponent } from './error/admin-error.component';
import { AdminFacilityApprovalDetailComponent } from './facilities/facility-approvals/facility-approvals-detail/admin-facility-approval-detail.component';
import { AdminFacilityManagementComponent } from './facilities/facility-management/admin-facility-management.component';
import { AdminFacilityListResolver } from './core/resolvers/admin-facility-list.resolver';
import { AdminFacilityInfoResolver } from './core/resolvers/admin-facility-info.resolver';
import { AdminFacilityManagersResolver } from './core/resolvers/admin-facility-managers.resolver';
import { FacilityManagersComponent } from '../shared/components/facility/facility-managers/facility-managers.component';
import { FacilityTreatmentAreasComponent } from '../shared/components/facility/facility-treatment-areas/facility-treatment-areas.component';
import { FacilityTreatmentAreasResolver } from '../core/resolvers/facility/facility-treatment-areas.resolver';
import { AdminFacilityCompensationService } from './core/services/admin-facility-compensation.service';
import { AdminServiceCategoryEditComponent } from './services/admin-service-category-edit/admin-service-category-edit.component';
import { AdminServiceEditComponent } from './services/admin-service-edit/admin-service-edit.component';
import { UserProfileComponent } from '../shared/components/user/user-profile/user-profile.component';
import { UserProfileResolver } from '../core/resolvers/user/user-profile.resolver';
import { AdminTherapistManagementComponent } from './therapists/therapist-management/admin-therapist-management.component';
import { AdminTherapistCompensationResolver } from './core/resolvers/admin-therapist-compensation.resolver';
import { FacilityTreatmentAreaAvailabilityResolver } from '../core/resolvers/facility/facility-treatment-area-availability.resolver';
import { FacilityTreatmentAreaResolver } from '../core/resolvers/facility/facility-treatment-area.resolver';
import { FacilityAvailabilityComponent } from '../shared/components/facility/facility-availability/facility-availability.component';
import { FacilityAppointmentsViewComponent } from '../shared/components/facility/facility-appointments-view/facility-appointments-view.component';
import { FacilityAppointmentsListComponent } from '../shared/components/facility/facility-appointments-list/facility-appointments-list.component';
import { FacilityAppointmentsResolver } from '../facility/core/resolvers/facility-appointments.resolver';
import { FacilityAppointmentDetailComponent } from '../shared/components/facility/facility-appointment-detail/facility-appointment-detail.component';
import { FacilityAppointmentResolver } from '../core/resolvers/facility/facility-appointment.resolver';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        children: [
          {
            path: 'dashboard',
            component: AdminDashboardComponent,
            data: {
              breadcrumb: 'Dashboard'
            }
          },
          {
            path: 'userprofile',
            component: UserProfileComponent,
            resolve: {
              userProfile: UserProfileResolver
            },
            data: {
              breadcrumb: 'User Profile'
            }
          },
          {
            path: 'customers', component: AdminClientsComponent,
            data: {
              breadcrumb: 'Customers'
            }
          },
          {
            path: 'services',
            component: AdminServicesComponent,            
            resolve: {
              categories: AdminServiceCategoryResolver
            },
            data: { breadcrumb: 'Services' },
            //children: [
            //  {
            //    path: 'category/:uid',
            //    component: AdminServiceCategoryEditComponent,
            //    data: { breadcrumb: 'Category' },
            //  },
            //  {
            //    path: 'service/:uid',
            //    component: AdminServiceEditComponent,
            //    data: { breadcrumb: 'Service' },
            //  }
            //]
          },
          {
            path: 'facilities',
            children: [
              {
                path: '',
                component: AdminFacilitiesComponent,
                resolve: {
                  facilities: AdminFacilityListResolver
                },
                data: {
                  breadcrumb: 'Facilities'
                }
              },
              {
                path: 'list',
                component: AdminFacilitiesComponent,
                resolve: {
                  facilities: AdminFacilityListResolver
                },
                data: {
                  breadcrumb: 'Facilities'
                }
              },
              {
                path: 'detail/:uid',
                component: AdminFacilityDetailComponent,
                resolve: {
                  facility: AdminFacilityDetailResolver
                },
                data: {
                  breadcrumb: 'Facility'
                }
              },
              {
                path: 'approvals/:uid',
                component: AdminFacilityApprovalDetailComponent,
                resolve: {
                  facility: AdminFacilityApprovalDetailResolver
                },
                data: {
                  breadcrumb: 'Approval'
                }
              },
              {
                path: 'manage/:uid',
                component: AdminFacilityManagementComponent,
                resolve: {
                  facility: AdminFacilityInfoResolver
                },
                data: {
                  breadcrumb: 'Manage'
                }
              },
              {
                path: 'managers/:uid',
                component: FacilityManagersComponent,
                resolve: {
                  facilityManagers: AdminFacilityManagersResolver
                },
                data: {
                  breadcrumb: 'Managers'
                }
              },
              {
                path: ':facilityUID/appointments',
                component: FacilityAppointmentsViewComponent,
              },
              {
                path: ':facilityUID/appointments/list',
                component: FacilityAppointmentsListComponent,
                resolve: {
                  appointments: FacilityAppointmentsResolver
                },
              },
              {
                path: ':facilityUID/appointments/:appointmentUID/detail',
                component: FacilityAppointmentDetailComponent,
                resolve: {
                  appointment: FacilityAppointmentResolver
                },
              },
              {
                path: ':facilityUID/treatmentareas',
                component: FacilityTreatmentAreasComponent,
                resolve: {
                  treatmentAreas: FacilityTreatmentAreasResolver
                },
                data: {
                  breadcrumb: 'Treatment Area'
                }
              },
              {
                path: ':facilityUID/treatmentareas/:uid/availability',
                component: FacilityAvailabilityComponent,
                resolve: {
                  availability: FacilityTreatmentAreaAvailabilityResolver,
                  treatmentArea: FacilityTreatmentAreaResolver
                },
              }
            ]
          },
          {
            path: 'therapists',
            children: [
              {
                path: '',
                component: AdminTherapistsComponent,
                data: {
                  breadcrumb: 'Therapists'
                }
              },
              {
                path: ':uid/approvals',
                component: AdminTherapistApprovalDetailComponent,
                resolve: {
                  therapist: AdminTherapistApprovalDetailResolver
                },
                data: {
                  breadcrumb: 'Approvals'
                }
              },
              {
                path: ':uid/detail',
                component: AdminTherapistDetailComponent,
                resolve: {
                  therapist: AdminTherapistDetailResolver
                },
                data: {
                  breadcrumb: 'Therapist'
                }
              },
              {
                path: ':uid/manage',
                component: AdminTherapistManagementComponent,
                resolve: {
                  therapist: AdminTherapistDetailResolver,
                  compensations: AdminTherapistCompensationResolver
                },
                data: {
                  breadcrumb: 'Manage'
                }
              },
            ]
          },
          //{
          //  path: 'therapists',
          //  component: AdminTherapistsComponent,
          //  data: {
          //    breadcrumb: 'Therapists'
          //  }
          //},
          //{
          //  path: 'therapistdetail/:uid',
          //  component: AdminTherapistDetailComponent,
          //  resolve: {
          //    therapist: AdminTherapistDetailResolver
          //  },
          //  data: {
          //    breadcrumb: 'Therapist'
          //  }
          //},
          //{
          //  path: 'therapists/approvals/:uid',
          //  component: AdminTherapistApprovalDetailComponent,
          //  resolve: {
          //    therapist: AdminTherapistApprovalDetailResolver
          //  },
          //  data: {
          //    breadcrumb: 'Approvals'
          //  }
          //},
          {
            path: 'error/:code',
            component: AdminErrorComponent,
            data: {
              breadcrumb: 'Error'
            }
          },
          {
            path: '',
            redirectTo: 'dashboard'
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
