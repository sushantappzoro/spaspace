import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceTypeEditComponent } from './admin-service-type-edit.component';

describe('AdminServiceTypeEditComponent', () => {
  let component: AdminServiceTypeEditComponent;
  let fixture: ComponentFixture<AdminServiceTypeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminServiceTypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
