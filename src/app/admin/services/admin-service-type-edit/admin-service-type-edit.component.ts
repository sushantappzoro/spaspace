import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { ServiceTypeForViewEditModel } from '../../shared/models/services/service-type-for-view-edit.model';

@Component({
  selector: 'spa-admin-service-type-edit',
  templateUrl: './admin-service-type-edit.component.html',
  styleUrls: ['./admin-service-type-edit.component.scss']
})
export class AdminServiceTypeEditComponent implements OnInit, OnChanges {
  @Input() type: ServiceTypeForViewEditModel;
  @Input() categoryName: string;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveType: EventEmitter<ServiceTypeForViewEditModel> = new EventEmitter<ServiceTypeForViewEditModel>();

  @ViewChild('typeModal') typeModal: ModalDirective;

  title: string = 'Add Type';

  typeForm: FormGroup;

  constructor() { }

  ngOnInit() {

    this.typeForm = new FormGroup({
      ID: new FormControl(),
      Active: new FormControl(true),
      Name: new FormControl('', Validators.required),
      Description: new FormControl('', Validators.required),
      IconUrl: new FormControl('')
    });

    if (this.type) {
      this.typeForm.patchValue(this.type);
      this.title = (this.type.ID) ? 'Edit Type' : 'Add Type';
      this.title += ' for ' + this.categoryName;
    }

  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.typeModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.typeModal.show();
      } else {
        this.typeModal.hide();
      }
    }

    if (changes.type && this.typeForm) {

      if (changes.type.currentValue && changes.type.currentValue.ID) {
        this.type = { ...changes.type.currentValue };
        this.title = (this.type.ID) ? 'Edit Type' : 'Add Type';
        this.title += ' for ' + this.categoryName;
        this.typeForm.patchValue(this.type);
      }

    }
  }

  onSave() {

    if (this.typeForm.invalid) {
      this.typeForm.markAllAsTouched();
      return;
    }

    var comp = new ServiceTypeForViewEditModel();
    comp = Object.assign({}, this.type, this.typeForm.value);

    this.saveType.emit(comp);

  }

  onClose() {
    this.type = null;
    this.typeForm.reset();
    this.showModalChange.emit(false);
  }

}
