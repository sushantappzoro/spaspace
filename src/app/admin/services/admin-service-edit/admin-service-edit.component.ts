import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import { ServiceForEditModel } from '../../shared/models/services/service-for-edit.model';
import { ServiceCategoryForAdminViewModel } from '../../shared/models/services/service-category-for-admin-view.model';
import { ServiceCategoryModel } from '../../shared/models/services/service-category.model';


@Component({
  selector: 'spa-admin-service-edit',
  templateUrl: './admin-service-edit.component.html',
  styleUrls: ['./admin-service-edit.component.scss']
})
export class AdminServiceEditComponent implements OnInit, OnChanges {
  @Input() service: ServiceForEditModel;
  @Input() categories: ServiceCategoryModel[];
  @Input() categoryName: string;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveService: EventEmitter<ServiceForEditModel> = new EventEmitter<ServiceForEditModel>();

  private subs = new SubSink();

  @ViewChild('serviceModal') serviceModal: ModalDirective;

  title: string = 'Add Service';

  serviceForm: FormGroup;

  constructor(

  ) { }

  ngOnInit() {
    this.buildForm();
    //this.serviceForm.setValidators(this.therapistFlatRatePercentValidator);
    if (this.service) {
      if (!this.service.Active) {
        this.service.Active = false;
      }

      this.serviceForm.patchValue(this.service);
      this.title = (this.service.UID && this.service.UID.length > 0) ? 'Edit Service' : 'Add Service';
      this.title += ' for ' + this.categoryName;
    }

  }

  buildForm() {
    this.serviceForm = new FormGroup({
      UID: new FormControl(),
      Active: new FormControl(false),
      Name: new FormControl('', Validators.required),
      Description: new FormControl(''),
      Duration: new FormControl('', Validators.required),
      ProviderDuration: new FormControl('', Validators.required),
      BasePrice: new FormControl('', Validators.required),
      ServiceCharge: new FormControl(0, Validators.required),
      TherapistPercentOfBase: new FormControl(0),
      TherapistFlatRate: new FormControl(0),
      TherapistPercentOfServiceCharge: new FormControl(0, Validators.required),
      FacilityPercentOfBase: new FormControl('')
    });
  }

  therapistFlatRatePercentValidator(formControl: AbstractControl) {
    const percentOfBaseControl = this.serviceForm.get('TherapistPercentOfBase');
    const flatRateControl = this.serviceForm.get('TherapistFlatRate');

    const percentOfBase = (formControl.parent.get('TherapistPercentOfBase') &&
      formControl.parent.get('TherapistPercentOfBase').value) ?
      formControl.parent.get('TherapistPercentOfBase').value : null;

    const flatRate = (formControl.parent.get('TherapistFlatRate') &&
      formControl.parent.get('TherapistFlatRate').value) ?
      formControl.parent.get('TherapistFlatRate').value : null;

      return null;
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.serviceModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.serviceModal.show();
      } else {
        this.serviceModal.hide();
      }
    }

    if (changes.service && this.serviceForm) {
      this.serviceForm.patchValue(this.service);
      if (changes.service.currentValue) {
        this.service = { ...changes.service.currentValue }
        this.title = (this.service.UID && this.service.UID.length > 0) ? 'Edit Service' : 'Add Service';
        this.title += ' for ' + this.categoryName;
        this.serviceForm.patchValue(this.service);
      }
    }
  }

  onSave() {

    if (this.serviceForm.invalid) {
      this.serviceForm.markAllAsTouched();
      return;
    }

    let comp = new ServiceForEditModel();
    comp = Object.assign({}, this.service, this.serviceForm.value);

    this.saveService.emit(comp);

  }

  onClose() {
    this.service = null;
    this.serviceForm.reset();
    this.showModalChange.emit(false);
  }
}
