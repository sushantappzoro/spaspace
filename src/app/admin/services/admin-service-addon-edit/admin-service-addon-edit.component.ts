import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';

import { ServiceAddOnForEditModel } from '../../shared/models/services/service-addon-for-edit.model';

@Component({
  selector: 'spa-admin-service-addon-edit',
  templateUrl: './admin-service-addon-edit.component.html',
  styleUrls: ['./admin-service-addon-edit.component.scss']
})
export class AdminServiceAddonEditComponent implements OnInit, OnChanges {
  @Input() addon: ServiceAddOnForEditModel;
  @Input() categoryName: string;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveAddOn: EventEmitter<ServiceAddOnForEditModel> = new EventEmitter<ServiceAddOnForEditModel>();

  @ViewChild('addonModal') addonModal: ModalDirective;

  title: string = 'Add AddOn';

  addonForm: FormGroup;

  constructor(

  ) { }

  ngOnInit() {

    this.addonForm = new FormGroup({
      ID: new FormControl(),
      Active: new FormControl(true),
      Name: new FormControl('', Validators.required),
      Description: new FormControl(''),
      Price: new FormControl(0, Validators.required),
      TherapistPercent: new FormControl(0, Validators.required)
    });

    if (this.addon) {
      this.addonForm.patchValue(this.addon);
      this.title = (this.addon.ID) ? 'Edit AddOn' : 'Add AddOn';
      this.title += ' for ' + this.categoryName;
    }

  }


  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges', changes);
    if (changes.showModal && this.addonModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.addonModal.show();
      } else {
        this.addonModal.hide();
      }
    }

    if (changes.addon && this.addonForm) {

      if (changes.addon.currentValue && changes.addon.currentValue.ID) {
        console.log('editAddOn', changes.addon.currentValue);
        //this.addon = { ...changes.addon.currentValue };
        this.title = (this.addon.ID) ? 'Edit AddOn' : 'Add AddOn';
        this.title += ' for ' + this.categoryName;
        this.addonForm.patchValue(this.addon);
      }

    }
  }

  onSave() {

    if (this.addonForm.invalid) {
      this.addonForm.markAllAsTouched();
      return;
    }

    var comp = new ServiceAddOnForEditModel();
    comp = Object.assign({}, this.addon, this.addonForm.value);

    this.saveAddOn.emit(comp);

  }

  onClose() {
    this.addon = null;
    this.addonForm.reset();
    this.showModalChange.emit(false);
  }
}
