import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceAddonEditComponent } from './admin-service-addon-edit.component';

describe('AdminServiceAddonEditComponent', () => {
  let component: AdminServiceAddonEditComponent;
  let fixture: ComponentFixture<AdminServiceAddonEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminServiceAddonEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceAddonEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
