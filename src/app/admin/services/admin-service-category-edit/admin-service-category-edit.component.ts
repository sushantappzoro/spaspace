import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';

import { AdminServiceCategoryService } from '../../core/services/admin-service-category.service';
import { ServiceCategoryForEditModel } from '../../shared/models/services/service-category-for-edit.model';

@Component({
  selector: 'spa-admin-service-category-edit',
  templateUrl: './admin-service-category-edit.component.html',
  styleUrls: ['./admin-service-category-edit.component.scss']
})
export class AdminServiceCategoryEditComponent implements OnInit, OnChanges {
  @Input() category: ServiceCategoryForEditModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveCategory: EventEmitter<ServiceCategoryForEditModel> = new EventEmitter<ServiceCategoryForEditModel>();

  private subs = new SubSink();

  @ViewChild('categoryModal') categoryModal: ModalDirective;

  title: string = 'Add Category';

  categoryForm: FormGroup;

  constructor(

  ) { }

  ngOnInit() {

    this.categoryForm = new FormGroup({
      ID: new FormControl(),
      Active: new FormControl(true),
      Name: new FormControl('', Validators.required),
    });

    if (this.category) {
      this.categoryForm.patchValue(this.category);
      this.title = (this.category.ID) ? 'Edit Category' : 'Add Category';
    }  

  }


  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.categoryModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.categoryModal.show();
      } else {
        this.categoryModal.hide();
      }
    }

    if (changes.category && this.categoryForm) {
      if (changes.category.currentValue) {
        this.category = { ...changes.category.currentValue }
        this.title = this.category.ID ? 'Edit Category' : 'Add Category';
        this.categoryForm.patchValue(this.category);    
      }

    }
  }

  onSave() {

    if (this.categoryForm.invalid) {
      this.categoryForm.markAllAsTouched();
      return;
    }

    var comp = new ServiceCategoryForEditModel();
    comp = Object.assign({}, this.category, this.categoryForm.value);

    this.saveCategory.emit(comp);

  }

  onClose() {
    this.category = null;
    this.categoryForm.reset();
    this.showModalChange.emit(false);
  }

}
