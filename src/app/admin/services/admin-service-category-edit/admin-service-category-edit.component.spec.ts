import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceCategoryEditComponent } from './admin-service-category-edit.component';

describe('AdminServiceCategoryEditComponent', () => {
  let component: AdminServiceCategoryEditComponent;
  let fixture: ComponentFixture<AdminServiceCategoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminServiceCategoryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceCategoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
