import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { ServiceNeedForViewEditModel } from '../../shared/models/services/service-need-for-view-edit.model';

@Component({
  selector: 'spa-admin-service-needs-edit',
  templateUrl: './admin-service-needs-edit.component.html',
  styleUrls: ['./admin-service-needs-edit.component.scss']
})
export class AdminServiceNeedsEditComponent implements OnInit, OnChanges {
  @Input() need: ServiceNeedForViewEditModel;
  @Input() categoryName: string;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveNeed: EventEmitter<ServiceNeedForViewEditModel> = new EventEmitter<ServiceNeedForViewEditModel>();

  @ViewChild('needModal') needModal: ModalDirective;

  title: string = 'Add Need';

  needForm: FormGroup;

  constructor() { }

  ngOnInit() {

    this.needForm = new FormGroup({
      ID: new FormControl(),
      Active: new FormControl(true),
      Name: new FormControl('', Validators.required)
    });

    if (this.need) {
      this.needForm.patchValue(this.need);
      this.title = (this.need.ID) ? 'Edit Need' : 'Add Need';
      this.title += ' for ' + this.categoryName;
    }

  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.needModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.needModal.show();
      } else {
        this.needModal.hide();
      }
    }

    if (changes.need && this.needForm) {

      if (changes.need.currentValue && changes.need.currentValue.ID) {
        this.need = { ...changes.need.currentValue };
        this.title = (this.need.ID) ? 'Edit Need' : 'Add Need';
        this.title += ' for ' + this.categoryName;
        this.needForm.patchValue(this.need);
      }

    }
  }

  onSave() {

    if (this.needForm.invalid) {
      this.needForm.markAllAsTouched();
      return;
    }

    var comp = new ServiceNeedForViewEditModel();
    comp = Object.assign({}, this.need, this.needForm.value);

    this.saveNeed.emit(comp);

  }

  onClose() {
    this.need = null;
    this.needForm.reset();
    this.showModalChange.emit(false);
  }

}

