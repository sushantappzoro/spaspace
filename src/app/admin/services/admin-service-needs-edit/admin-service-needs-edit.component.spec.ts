import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceNeedsEditComponent } from './admin-service-needs-edit.component';

describe('AdminServiceNeedsEditComponent', () => {
  let component: AdminServiceNeedsEditComponent;
  let fixture: ComponentFixture<AdminServiceNeedsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminServiceNeedsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceNeedsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
