import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { isNumeric } from 'rxjs/util/isNumeric';
import notify from 'devextreme/ui/notify';

import { DxDataGridComponent } from 'devextreme-angular';
import { ServiceCategoryModel } from '../shared/models/services/service-category.model';
import { AdminServiceCategoryService } from '../core/services/admin-service-category.service';
import { ActivatedRoute } from '@angular/router';
import { ServiceCategoryForAdminViewModel } from '../shared/models/services/service-category-for-admin-view.model';
import { AdminServicesService } from '../core/services/admin-services.service';
import { ServiceForEditModel } from '../shared/models/services/service-for-edit.model';
import { ServiceCategoryForEditModel } from '../shared/models/services/service-category-for-edit.model';
import { ServiceForAdminViewModel } from '../shared/models/services/service-for-admin-view.model';
import { AdminServiceAddonService } from '../core/services/admin-service-addon.service';
import { ServiceAddOnForEditModel } from '../shared/models/services/service-addon-for-edit.model';
import { ServiceAddOnForAddMmodel } from '../shared/models/services/service-addon-for-add.model';
import { ServiceForAddModel } from '../shared/models/services/service-for-add.model';
import { AdminServiceFocusAreaService } from '../core/services/admin-service-focusarea.service';
import { AdminServiceTypeService } from '../core/services/admin-service-type.service';
import { AdminServiceNeedService } from '../core/services/admin-service-need.service';
import { ServiceFocusAreaForViewEditModel } from '../shared/models/services/service-focusarea-for-view-edit.model';
import { ServiceNeedForViewEditModel } from '../shared/models/services/service-need-for-view-edit.model';
import { ServiceTypeForViewEditModel } from '../shared/models/services/service-type-for-view-edit.model';
import { AdminServiceLicenseService } from '../core/services/admin-service-license.service';
import { ServiceLicenseForViewEditModel } from '../shared/models/services/service-license-for-view-edit.model';
import { AdminLicenseService } from '../core/services/admin-license.service';
import { LicenseForViewEditModel } from '../shared/models/lists/license-for-viewedit.model';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-admin-services',
  templateUrl: './admin-services.component.html',
  styleUrls: ['./admin-services.component.scss']
})
export class AdminServicesComponent implements OnInit, OnDestroy {
  @ViewChild('categoriesGrid') categoriesGrid: DxDataGridComponent;
  @ViewChild('servicesGrid') servicesGrid: DxDataGridComponent;
  @ViewChild('addonsGrid') addonsGrid: DxDataGridComponent;
  @ViewChild('focusAreasGrid') focusAreasGrid: DxDataGridComponent;
  @ViewChild('needsGrid') needsGrid: DxDataGridComponent;
  @ViewChild('typeGrid') typeGrid: DxDataGridComponent;
  @ViewChild('servicesGrid') licenseGrid: DxDataGridComponent;

  private subs = new SubSink();

  category: ServiceCategoryForEditModel;
  service: ServiceForEditModel;
  addon: ServiceAddOnForEditModel;
  focusArea: ServiceFocusAreaForViewEditModel;
  need: ServiceNeedForViewEditModel;
  type: ServiceTypeForViewEditModel;
  license: ServiceLicenseForViewEditModel;

  licenses: LicenseForViewEditModel[];
  filteredLicenses: LicenseForViewEditModel[];

  serviceCategories: ServiceCategoryModel[];

  showCategoryModal: boolean = false;
  showServiceModal: boolean = false;
  showAddOnModal: boolean = false;
  showFocusAreaModal: boolean = false;
  showNeedModal: boolean = false;
  showTypeModal: boolean = false;
  showLicenseModal: boolean = false;

  deleteID: string;
  deleteType: string;
  showDeleteWarning: boolean = false;

  expandedCategoryKey: number = 0;
  expandedCategoryName: string;

  constructor(
    private route: ActivatedRoute,
    private categoryService: AdminServiceCategoryService,
    private servicesService: AdminServicesService,
    private addonService: AdminServiceAddonService,
    private focusAreaService: AdminServiceFocusAreaService,
    private typeService: AdminServiceTypeService,
    private needService: AdminServiceNeedService,
    private licenseService: AdminServiceLicenseService,
    private licensesService: AdminLicenseService
  ) {

    this.editCategory = this.editCategory.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);

    this.addService = this.addService.bind(this);
    this.editService = this.editService.bind(this);
    this.deleteService = this.deleteService.bind(this);

    this.editAddOn = this.editAddOn.bind(this);
    this.deleteAddOn = this.deleteAddOn.bind(this);

    this.addFocusArea = this.addFocusArea.bind(this);
    this.editFocusArea = this.editFocusArea.bind(this);
    this.deleteFocusArea = this.deleteFocusArea.bind(this);

    this.addNeed = this.addNeed.bind(this);
    this.editNeed = this.editNeed.bind(this);
    this.deleteNeed = this.deleteNeed.bind(this);

    this.addType = this.addType.bind(this);
    this.editType = this.editType.bind(this);
    this.deleteServiceType = this.deleteServiceType.bind(this);

    this.addLicense = this.addLicense.bind(this);
    this.deleteLicense = this.deleteLicense.bind(this);

  }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { categories: ServiceCategoryForAdminViewModel[] }) => {
        this.serviceCategories = data.categories;
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.licensesService.getList()
      .subscribe(resp => {
        this.licenses = resp;
      },
        error => {
          console.log(error);
        }
    )
    )

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  getServices(id) {
    console.log('getServices -id', id);
  }


  //
  //
  //   Process Categories
  //
  //

  categoriesGrid_onToolbarPreparing(e) {

    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New Category",
        hint: "Add New Category",
        onClick: () => {
          this.addCategory();
        }
      }
    });
  }

  addCategory() {
    this.category = new ServiceCategoryForEditModel();
    this.category.Active = true;
    this.showCategoryModal = true;
  }

  editCategory(e) {
    this.category = new ServiceCategoryForEditModel();
    this.category = { ...e.row.data };
    this.showCategoryModal = true;
  }

  deleteCategory(e) {

    this.deleteID = e.row.data.ID;
    this.deleteType = 'category';
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {

      switch (this.deleteType) {
        case 'category':
          this.removeCategory(this.deleteID);
          break;
        case 'service':
          this.removeService(this.deleteID);
          break;
        case 'addon':
          this.removeAddOn(+this.deleteID);
          break;
        case 'focusArea':
          this.removeFocusArea(+this.deleteID);
          break;
        case 'need':
          this.removeNeed(+this.deleteID);
          break;
        case 'type':
          this.removeNeed(+this.deleteID);
          break;
        case 'license':
          this.removeLicense(+this.deleteID);
          break;
        default:
      }
      this.deleteType = '';
      this.deleteID = '';
    }
  }

  saveCategory(category: ServiceCategoryForEditModel) {

    if (category.ID) {

      this.subs.add(this.categoryService.edit(category)
        .subscribe(resp => {
          let cat = this.serviceCategories.find(x => x.ID = resp.ID);

          if (cat) {
            cat.Name = resp.Name;
            cat.Active = resp.Active;

            //this.serviceCategories.splice(cat, 1, resp);
          }
          this.showCategoryModal = false;
        },
          error => {
            notify('An error occured updating the service category.', 'error', 3000);
          }));

    } else {

      this.subs.add(this.categoryService.add(category)
        .subscribe(resp => {
          let cat: ServiceCategoryModel = { Name: resp.Name, Active: resp.Active, AddOns: [], Services: [], FocusAreas: [], Needs: [], Types: [], AssociatedLicenses: [], ID: resp.ID}
          this.serviceCategories.unshift(cat);
          this.showCategoryModal = false;
        },
          error => {
            notify('An error occured adding the service category.', 'error', 3000);
          }));
    }
  }

  removeCategory(id: string) {

    this.subs.add(this.categoryService.remove(id)
      .subscribe(resp => {
        const cat = this.serviceCategories.findIndex(x => x.ID = +id);
        if (cat > -1) {
          this.serviceCategories.splice(cat, 1);
        }
      },
        error => {
          notify('An error occured deleting the service category.', 'error', 3000);
        }));

  }

  categoriesGrid_onRowExpanding(e) {
    this.expandedCategoryKey = e.key;
    var category = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);
    this.expandedCategoryName = category ? category.Name : '';
    this.categoriesGrid.instance.collapseAll(-1);
  }


  //
  //
  //   Process Services
  //
  //

  servicesGrid_onToolbarPreparing(e) {
    console.log('servicesGrid_onToolbarPreparing', e);
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New Service",
        hint: "Add New Service",
        onClick: this.addService.bind(this)

      }
    });
  }

  addService(e) {

    this.service = new ServiceForEditModel();
    this.service.CategoryID = this.expandedCategoryKey;
    this.service.Active = false;
    this.showServiceModal = true;
  }

  editService(e) {

    const holdService: ServiceForAdminViewModel = { ...e.row.data };
    holdService.CategoryID = this.expandedCategoryKey;

    this.service = {
      Active: holdService.Active,
      BasePrice: holdService.BasePrice,
      CategoryID: holdService.CategoryID,
      Description: holdService.Description,
      Duration: holdService.Duration,
      ProviderDuration: holdService.ProviderDuration,
      Name: holdService.Name,
      ServiceCharge: holdService.ServiceCharge,
      TherapistFlatRate: holdService.TherapistFlatRate,
      TherapistPercentOfBase: holdService.TherapistPercentOfBase,
      TherapistPercentOfServiceCharge: holdService.TherapistPercentOfServiceCharge,
      FacilityPercentOfBase: holdService.FacilityPercentOfBase,
      UID: holdService.UID
    };
    // this.service = { ...e.row.data };
    this.showServiceModal = true;
  }

  deleteService(e) {
    this.deleteID = e.row.data.UID;
    this.deleteType = 'service';
    this.showDeleteWarning = true;
  }

  saveService(service: ServiceForEditModel) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    if (service.TherapistPercentOfBase > 1) {
      service.TherapistPercentOfBase = service.TherapistPercentOfBase / 100;
    }

    if (service.TherapistPercentOfServiceCharge > 1) {
      service.TherapistPercentOfServiceCharge = service.TherapistPercentOfServiceCharge / 100;
    }

    if (service.FacilityPercentOfBase > 1) {
      service.FacilityPercentOfBase = service.FacilityPercentOfBase / 100;
    }

    if (!service.Active) {
      service.Active == false;
    }

    if (service.UID) {

      this.subs.add(this.servicesService.edit(this.expandedCategoryKey, service)
        .subscribe(resp => {
          if (cat) {
            const svc = cat.Services.findIndex(x => x.UID == service.UID);
            if (svc > -1) {
              cat.Services.splice(svc, 1, resp);
            }
          }
          this.showServiceModal = false;
        },
          error => {
            notify('An error occured updating the service.', 'error', 3000);
          }));

    } else {

      const svc: ServiceForAddModel = {
        Active: service.Active,
        BasePrice: service.BasePrice,
        Description: service.Description,
        CategoryID: service.CategoryID,
        CategoryName: service.CategoryName,
        Duration: service.Duration,
        ProviderDuration: service.ProviderDuration,
        Name: service.Name,
        ServiceCharge: service.ServiceCharge,
        TherapistPercentOfBase: service.TherapistPercentOfBase,
        TherapistPercentOfServiceCharge: service.TherapistPercentOfServiceCharge,
        TherapistFlatRate: service.TherapistFlatRate,
        FacilityPercentOfBase: service.FacilityPercentOfBase
      };

      this.subs.add(this.servicesService.add(this.expandedCategoryKey, svc)
        .subscribe(resp => {
          if (cat) {
            cat.Services.unshift(resp);
          }
          this.showServiceModal = false;
        },
          error => {
            notify('An error occured adding the service.', 'error', 3000);
          }));
    }
  }

  removeService(id: string) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    this.subs.add(this.servicesService.remove(this.expandedCategoryKey, id)
      .subscribe(resp => {
        if (cat) {
          const svc = cat.Services.findIndex(x => x.UID == id);
          if (svc > -1) {
            cat.Services.splice(svc, 1);
          }
        }
        //this.loadFacilities(this.therapistUID);
      },
        error => {
          notify('An error occured deleting the service.', 'error', 3000);
        }));

  }

  getPercentageOfBase(rowData) {

    if (!isNumeric(rowData.TherapistPercentOfBase)) {
      return null;
    }

    var therapistPercentOfBase: number = rowData.TherapistPercentOfBase;

    if (therapistPercentOfBase < 1) {
      therapistPercentOfBase = therapistPercentOfBase * 100;
    }

    return therapistPercentOfBase.toPrecision(2) + '%';

  }

  getFacilityPercentageOfBase(rowData) {

    if (!isNumeric(rowData.FacilityPercentOfBase)) {
      return null;
    }

    var facilityPercentOfBase: number = rowData.FacilityPercentOfBase;

    if (facilityPercentOfBase < 1) {
      facilityPercentOfBase = facilityPercentOfBase * 100;
    }

    return facilityPercentOfBase.toPrecision(2) + '%';
  }

  getPercentageOfServiceCharge(rowData) {
    if (!isNumeric(rowData.TherapistPercentOfServiceCharge)) {
      return null;
    }

    var therapistPercentOfBase: number = rowData.TherapistPercentOfServiceCharge;

    if (therapistPercentOfBase < 1) {
      therapistPercentOfBase = therapistPercentOfBase * 100;
    }

    return therapistPercentOfBase.toPrecision(2) + '%';
  }


  //
  //
  //   Process Addons
  //
  //

  addonsGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New AddOn",
        hint: "Add New AddOn",
        onClick: () => {
          this.addAddOn();
        }
      }
    });
  }

  addAddOn() {
    console.log('addAddOn');
    this.addon = new ServiceAddOnForEditModel();
    this.showAddOnModal = true;
  }

  editAddOn(e) {

    this.addon = { ...e.row.data };

    //this.service = { ...e.row.data };
    this.showAddOnModal = true;
  }

  deleteAddOn(e) {
    this.deleteID = e.row.data.ID;
    this.deleteType = 'addon';
    this.showDeleteWarning = true;
  }

  saveAddOn(addon: ServiceAddOnForEditModel) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    addon.CategoryID = this.expandedCategoryKey;
    addon.CategoryName = '';

    if (addon.ID) {

      this.subs.add(this.addonService.edit(this.expandedCategoryKey, addon)
        .subscribe(resp => {
          if (cat) {
            const svc = cat.AddOns.findIndex(x => x.ID === addon.ID);
            if (svc > -1) {
              cat.AddOns.splice(svc, 1, resp);
              this.addonsGrid.instance.refresh();
            }
          }
          this.showAddOnModal = false;
        },
          error => {
            notify('An error occured updating the add on.', 'error', 3000);
          }));

    } else {

      addon.ID = null;

      this.subs.add(this.addonService.add(this.expandedCategoryKey, addon)
        .subscribe(resp => {
          cat.AddOns.unshift(resp);
          this.showAddOnModal = false;
        },
          error => {
            notify('An error occured adding the add on.', 'error', 3000);
          }));
    }
  }

  removeAddOn(id: number) {

    const cat = this.serviceCategories.find(x => x.ID === this.expandedCategoryKey);

    this.subs.add(this.addonService.remove(this.expandedCategoryKey, id)
      .subscribe(resp => {
        if (cat) {
          const svc = cat.AddOns.findIndex(x => x.ID === id);
          if (svc > -1) {
            cat.AddOns.splice(svc, 1);
          }
        }
      },
        error => {
          notify('An error occured deleting the add on.', 'error', 3000);
        }));

  }

  //
  //
  //   Process Focus Areas
  //
  //

  focusAreasGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New Focus Area",
        hint: "Add New Focus Area",
        onClick: () => {
          this.addFocusArea();
        }
      }
    });
  }

  addFocusArea() {
    this.focusArea = new ServiceFocusAreaForViewEditModel();
    this.showFocusAreaModal = true;
  }

  editFocusArea(e) {
    //console.log('editAddOn', e);
    this.focusArea = { ...e.row.data };

    //this.service = { ...e.row.data };
    this.showFocusAreaModal = true;
  }

  deleteFocusArea(e) {
    this.deleteID = e.row.data.ID;
    this.deleteType = 'focusArea';
    this.showDeleteWarning = true;
  }

  saveFocusArea(focusArea: ServiceFocusAreaForViewEditModel) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    focusArea.CategoryID = this.expandedCategoryKey;
    focusArea.CategoryName = '';

    if (focusArea.ID) {

      this.subs.add(this.focusAreaService.edit(this.expandedCategoryKey, focusArea)
        .subscribe(resp => {
          if (cat) {
            const svc = cat.FocusAreas.findIndex(x => x.ID === focusArea.ID);
            if (svc > -1) {
              cat.FocusAreas.splice(svc, 1, resp);

            }
          }
          this.showFocusAreaModal = false;
        },
          error => {
            notify('An error occured updating the focus area.', 'error', 3000);
          }));

    } else {

      focusArea.ID = null;

      this.subs.add(this.focusAreaService.add(this.expandedCategoryKey, focusArea)
        .subscribe(resp => {
          cat.FocusAreas.unshift(resp);
          this.showFocusAreaModal = false;
        },
          error => {
            notify('An error occured adding the focus area.', 'error', 3000);
          }));
    }
  }

  removeFocusArea(id: number) {

    const cat = this.serviceCategories.find(x => x.ID === this.expandedCategoryKey);

    this.subs.add(this.focusAreaService.remove(this.expandedCategoryKey, id)
      .subscribe(resp => {
        if (cat) {
          const svc = cat.FocusAreas.findIndex(x => x.ID === id);
          if (svc > -1) {
            cat.FocusAreas.splice(svc, 1);
          }
        }
      },
        error => {
          notify('An error occured deleting the focus area.', 'error', 3000);
        }));

  }


  //
  //
  //   Process Needs
  //
  //

  needsGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New Need",
        hint: "Add New Need",
        onClick: () => {
          this.addNeed();
        }
      }
    });
  }

  addNeed() {
    this.need = new ServiceNeedForViewEditModel();
    this.showNeedModal = true;
  }

  editNeed(e) {
    //console.log('editAddOn', e);
    this.need = { ...e.row.data };

    //this.service = { ...e.row.data };
    this.showNeedModal = true;
  }

  deleteNeed(e) {
    this.deleteID = e.row.data.ID;
    this.deleteType = 'need';
    this.showDeleteWarning = true;
  }

  saveNeed(need: ServiceNeedForViewEditModel) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    need.CategoryID = this.expandedCategoryKey;
    need.CategoryName = '';

    if (need.ID) {

      this.subs.add(this.needService.edit(this.expandedCategoryKey, need)
        .subscribe(resp => {
          if (cat) {
            const svc = cat.FocusAreas.findIndex(x => x.ID === need.ID);
            if (svc > -1) {
              cat.Needs.splice(svc, 1, resp);
            }
          }
          this.showNeedModal = false;
        },
          error => {
            notify('An error occured updating the need.', 'error', 3000);
          }));

    } else {

      need.ID = null;

      this.subs.add(this.needService.add(this.expandedCategoryKey, need)
        .subscribe(resp => {
          cat.Needs.unshift(resp);
          this.showNeedModal = false;
        },
          error => {
            notify('An error occured adding the need.', 'error', 3000);
          }));
    }
  }

  removeNeed(id: number) {

    const cat = this.serviceCategories.find(x => x.ID === this.expandedCategoryKey);

    this.subs.add(this.needService.remove(this.expandedCategoryKey, id)
      .subscribe(resp => {
        if (cat) {
          const svc = cat.Needs.findIndex(x => x.ID === id);
          if (svc > -1) {
            cat.Needs.splice(svc, 1);
          }
        }
      },
        error => {
          notify('An error occured deleting the need.', 'error', 3000);
        }));

  }

  //
  //
  //   Process Types
  //
  //

  typeGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New Type",
        hint: "Add New Type",
        onClick: () => {
          this.addType();
        }
      }
    });
  }

  addType() {
    this.type = new ServiceTypeForViewEditModel();
    this.showTypeModal = true;
  }

  editType(e) {
    //console.log('editAddOn', e);
    this.type = { ...e.row.data };

    //this.service = { ...e.row.data };
    this.showTypeModal = true;
  }

  deleteServiceType(e) {
    this.deleteID = e.row.data.ID;
    this.deleteType = 'type';
    this.showDeleteWarning = true;
  }

  saveType(type: ServiceTypeForViewEditModel) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    type.CategoryID = this.expandedCategoryKey;
    type.CategoryName = '';

    if (type.ID) {

      this.subs.add(this.typeService.edit(this.expandedCategoryKey, type)
        .subscribe(resp => {
          if (cat) {
            const svc = cat.FocusAreas.findIndex(x => x.ID === type.ID);
            if (svc > -1) {
              cat.Types.splice(svc, 1, resp);
            }
          }
          this.showTypeModal = false;
        },
          error => {
            notify('An error occured updating the type.', 'error', 3000);
          }));

    } else {

      type.ID = null;

      this.subs.add(this.typeService.add(this.expandedCategoryKey, type)
        .subscribe(resp => {
          cat.Types.unshift(resp);
          this.showTypeModal = false;
        },
          error => {
            notify('An error occured adding the type.', 'error', 3000);
          }));
    }
  }

  removeType(id: number) {

    const cat = this.serviceCategories.find(x => x.ID === this.expandedCategoryKey);

    this.subs.add(this.typeService.remove(this.expandedCategoryKey, id)
      .subscribe(resp => {
        if (cat) {
          const svc = cat.Types.findIndex(x => x.ID === id);
          if (svc > -1) {
            cat.Types.splice(svc, 1);
          }
        }
      },
        error => {
          notify('An error occured deleting the type.', 'error', 3000);
        }));

  }

  //
  //
  //   Process Licenses
  //
  //

  filterLicenses() {

    this.filteredLicenses = [];

    let cat: ServiceCategoryModel = null;

    if (this.serviceCategories) {
      cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);
    }

    //console.log('')
    if (this.licenses && cat) {
      this.licenses.forEach(lic => {

        let temp = null;

        if (cat.AssociatedLicenses) {
          temp = cat.AssociatedLicenses.find(x => x.LicenseTypeID == lic.ID);
        }

        if (!temp) {
          this.filteredLicenses.push(lic);
        }
      });
    }
  }

  licenseGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New License",
        hint: "Add New License",
        onClick: () => {
          this.addLicense();
        }
      }
    });
  }

  addLicense() {
    this.filterLicenses();
    this.license = new ServiceLicenseForViewEditModel();
    this.showLicenseModal = true;
  }

  deleteLicense(e) {
    this.deleteID = e.row.data.ID;
    this.deleteType = 'license';
    this.showDeleteWarning = true;
  }

  saveLicense(license: ServiceLicenseForViewEditModel) {

    const cat = this.serviceCategories.find(x => x.ID == this.expandedCategoryKey);

    license.CategoryID = this.expandedCategoryKey;
    license.CategoryName = '';

    this.subs.add(this.licenseService.add(this.expandedCategoryKey, license)
      .subscribe(resp => {
        if (!cat.AssociatedLicenses) {
          cat.AssociatedLicenses = [];
        }
        cat.AssociatedLicenses.unshift(resp);
        this.showLicenseModal = false;
      },
        error => {
          notify('An error occured adding the license.', 'error', 3000);
        }));
  }

  removeLicense(id: number) {

    const cat = this.serviceCategories.find(x => x.ID === this.expandedCategoryKey);

    this.subs.add(this.licenseService.remove(this.expandedCategoryKey, id)
      .subscribe(resp => {
        if (cat) {
          const svc = cat.Types.findIndex(x => x.ID === id);
          if (svc > -1) {
            cat.AssociatedLicenses.splice(svc, 1);
          }
        }
      },
        error => {
          notify('An error occured deleting the license.', 'error', 3000);
        }));

  }

}
