import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceFocusAreaEditComponent } from './admin-service-focus-area-edit.component';

describe('AdminServiceFocusAreaEditComponent', () => {
  let component: AdminServiceFocusAreaEditComponent;
  let fixture: ComponentFixture<AdminServiceFocusAreaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminServiceFocusAreaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceFocusAreaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
