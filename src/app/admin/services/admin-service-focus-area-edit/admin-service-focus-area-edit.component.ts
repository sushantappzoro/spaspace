import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { ServiceFocusAreaForViewEditModel } from '../../shared/models/services/service-focusarea-for-view-edit.model';

@Component({
  selector: 'spa-admin-service-focus-area-edit',
  templateUrl: './admin-service-focus-area-edit.component.html',
  styleUrls: ['./admin-service-focus-area-edit.component.scss']
})
export class AdminServiceFocusAreaEditComponent implements OnInit, OnChanges {
  @Input() focusArea: ServiceFocusAreaForViewEditModel;
  @Input() categoryName: string;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveFocusArea: EventEmitter<ServiceFocusAreaForViewEditModel> = new EventEmitter<ServiceFocusAreaForViewEditModel>();

  @ViewChild('focusAreaModal') focusAreaModal: ModalDirective;

  title: string = 'Add Focus Area';

  focusAreaForm: FormGroup;

  constructor() { }

  ngOnInit() {

    this.focusAreaForm = new FormGroup({
      ID: new FormControl(),
      Active: new FormControl(true),
      Name: new FormControl('', Validators.required)
    });

    if (this.focusArea) {
      this.focusAreaForm.patchValue(this.focusArea);
      this.title = (this.focusArea.ID) ? 'Edit Focus Area' : 'Add Focus Area';
      this.title += ' for ' + this.categoryName;
    }

  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.focusAreaModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.focusAreaModal.show();
      } else {
        this.focusAreaModal.hide();
      }
    }

    if (changes.focusArea && this.focusAreaForm) {

      if (changes.focusArea.currentValue && changes.focusArea.currentValue.ID) {
        this.focusArea = { ...changes.focusArea.currentValue };
        this.title = (this.focusArea.ID) ? 'Edit Focus Area' : 'Add Focus Area';
        this.title += ' for ' + this.categoryName;
        this.focusAreaForm.patchValue(this.focusArea);
      }

    }
  }

  onSave() {

    if (this.focusAreaForm.invalid) {
      this.focusAreaForm.markAllAsTouched();
      return;
    }

    var comp = new ServiceFocusAreaForViewEditModel();
    comp = Object.assign({}, this.focusArea, this.focusAreaForm.value);

    this.saveFocusArea.emit(comp);

  }

  onClose() {
    this.focusArea = null;
    this.focusAreaForm.reset();
    this.showModalChange.emit(false);
  }

}
