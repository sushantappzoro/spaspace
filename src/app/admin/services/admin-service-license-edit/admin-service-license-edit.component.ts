import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';

import { AdminLicenseService } from '../../core/services/admin-license.service';
import { ServiceLicenseForAddModel } from '../../shared/models/services/service-license-for-add.model';
import { LicenseForViewEditModel } from '../../shared/models/lists/license-for-viewedit.model';

@Component({
  selector: 'spa-admin-service-license-edit',
  templateUrl: './admin-service-license-edit.component.html',
  styleUrls: ['./admin-service-license-edit.component.scss']
})
export class AdminServiceLicenseEditComponent implements OnInit, OnChanges {
  @Input() licenses: LicenseForViewEditModel[];
  @Input() categoryName: string;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveLicense: EventEmitter<ServiceLicenseForAddModel> = new EventEmitter<ServiceLicenseForAddModel>();

  @ViewChild('licenseModal') licenseModal: ModalDirective;

  title: string = 'Add License';

  licenseForm: FormGroup;

  constructor(
    
  ) { }

  ngOnInit() {

    this.licenseForm = new FormGroup({
      LicenseTypeID: new FormControl()
    });

  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.licenseModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.licenseModal.show();
      } else {
        this.licenseModal.hide();
      }
    }
  }

  onSave() {

    if (this.licenseForm.invalid) {
      this.licenseForm.markAllAsTouched();
      return;
    }

    var comp = new ServiceLicenseForAddModel();
    comp.LicenseTypeID = this.licenseForm.value.LicenseTypeID
    this.saveLicense.emit(comp);

  }

  onClose() {
    this.licenseForm.reset();
    this.showModalChange.emit(false);
  }

}
