import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceLicenseEditComponent } from './admin-service-license-edit.component';

describe('AdminServiceLicenseEditComponent', () => {
  let component: AdminServiceLicenseEditComponent;
  let fixture: ComponentFixture<AdminServiceLicenseEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminServiceLicenseEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceLicenseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
