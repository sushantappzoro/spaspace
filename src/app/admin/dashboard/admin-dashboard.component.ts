import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  approvalCount: number = 0;
  facilityApprovalCount: number = 0;
  facilityReviewCount: number = 0;
  therapistReviewCount: number = 0;

  constructor() { }

  ngOnInit() {
  }

  numberForApproval(val: number) {
    this.approvalCount = val;
  }

  numberForFacilityApproval(val: number) {
    this.facilityApprovalCount = val;
  }

  numberForFacilityReviews(val: number) {
    this.facilityReviewCount = val;
  }

  numberForTherapistReviews(val: number) {
    this.therapistReviewCount = val;
  }

}
