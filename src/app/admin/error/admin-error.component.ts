import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import { AdminErrorService } from '../core/services/admin-error.service';

@Component({
  selector: 'app-admin-error',
  templateUrl: './admin-error.component.html',
  styleUrls: ['./admin-error.component.scss']
})
export class AdminErrorComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  subscription: any;

  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private errorService: AdminErrorService
  ) { }

  ngOnInit() {

    this.subscription = this.route
      .params
      .subscribe(params => {
        var errorCode = params['code'];
        this.errorMessage = this.errorService.getErrorMessage(errorCode);

      });

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
