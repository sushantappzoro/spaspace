import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DxListModule,
  DxDataGridModule,
  DxAccordionModule,
  DxButtonModule,
  DxTextAreaModule,
  DxTabsModule,
  DxTabPanelModule,
  DxTemplateModule
} from 'devextreme-angular';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [
    DxDataGridModule,
    DxListModule,
    DxAccordionModule,
    DxButtonModule,
    DxTabsModule,
    DxTabPanelModule,
    DxTemplateModule,
    DxTextAreaModule
  ]
})
export class AdminDevexModule { }
