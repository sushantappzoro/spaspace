import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminWidgetsModule } from './widgets/admin-widgets.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminComponentsModule } from './components/admin-components.module';
import { MaterialModule } from '../../material-module.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AdminComponentsModule
  ],
  exports: [
    AdminWidgetsModule,
    AdminComponentsModule,
    SharedModule,
    MaterialModule,
    MDBBootstrapModulesPro,
    NgbModule
  ]
})
export class AdminSharedModule { }
