export class TherapistLicenseForApprovalModel {
  public StateCode: string;
  public AsOfDate: Date;
  public UntilDate: Date;
}
