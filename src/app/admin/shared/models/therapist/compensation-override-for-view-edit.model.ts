export class CompensationOverrideForViewEditModel {
  public UID: string;
  public Service: string;
  public ServiceUID: string;
  public PercentOfBase: number;
  public FlatRate: number;
  public PercentOfServiceCharge: number;
}
