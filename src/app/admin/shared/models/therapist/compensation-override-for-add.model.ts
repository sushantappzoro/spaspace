export class CompensationOverrideForAddModel {
  public ServiceUID: string;
  public PercentOfBase: number;
  public FlatRate: number;
  public PercentOfServiceCharge: number;
}
