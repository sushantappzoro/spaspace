import { TherapistCertificationForApprovalModel } from "./therapist-certification-for-approval.model";
//import { TherapistInsuranceForApprovalModel } from "./therapist-insurance-for-approval.model";
import { TherapistLicenseForApprovalModel } from "./therapist-license-for-approval.model";
import { TherapistProfilePhotoForApprovalModel } from "./therapist-profile-photo-for-approval.model";

export class TherapistForApprovalDetailModel {
  public UID: string;
  public Address: string;
  public Bio: string;
  public Certifications: TherapistCertificationForApprovalModel[];
  public City: string;
  public EmailAddress: string;
  public FirstName: string;
  //public Insurances: TherapistInsuranceForApprovalModel[];
  public LastName: string;
  public Licenses: TherapistLicenseForApprovalModel[];
  public MobilePhone: string;
  public ProfilePhoto: TherapistProfilePhotoForApprovalModel;
  public State: string;
  public YearsExperience: number;
  public ZipCode: string;
}
