export class TherapistCertificationForApprovalModel {
  public Name: string;
  public Description: string;
  public CertificateDate: Date;
  public CertifyingOrganization: string;
}
