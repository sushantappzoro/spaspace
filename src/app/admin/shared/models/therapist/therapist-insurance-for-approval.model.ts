export class TherapistInsuranceForApprovalModel {
  public UID: string;
  public ProviderName: string;
  public StartDate: Date;
  public ExpirationDate: Date;
  public CoverageAmount: Number;
  public FileUrl: string;
}
