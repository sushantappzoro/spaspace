export class PendingTherapistReviewForAdminViewModel {
  public UID: string;
  public CustomerName: string;
  public CustomerUID: string;
  public TherapistName: string;
  public TherapistUID: string;
  public Stars: number;
  public ReviewText: string;
  public AppointmentDate: Date;
  public SubmitDate: Date;
}
