export class TherapistForListModel {
  public UID: string;
  public Email: string;
  public Phone: string;
  public Name: string;
  public ProfilePhotoUrl: string;
}
