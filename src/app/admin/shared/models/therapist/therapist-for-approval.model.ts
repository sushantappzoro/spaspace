import { TherapistProfilePhotoForApprovalModel } from "./therapist-profile-photo-for-approval.model";

export class TherapistForApprovalModel {
  public UID: string;
  public Name: string;
  public YearsExperience: string;
  public SubmittedForApprovalDate: Date;
  public ProfilePhoto: TherapistProfilePhotoForApprovalModel;
}
