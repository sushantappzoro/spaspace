export class AdminFacilityInfoForEditModel {
  public UID: string;
  public Name: string;
  public City: string;
  public State: string;
  public Active: boolean;
  public ParentCompanyName: string;
  public ParentCompanyUID: string;
  public RegionName: string;
  public RegionUID: string;
  public MainPhotoUrl: string;
  public AmenityCharge: number;
  public FacilitysCut: number;
}
