export class PendingFacilityReviewForAdminView {
    public UID: string;
    public CustomerName: string;
    public CustomerUID: string;
    public FacilityName: string;
    public FacilityUID: string;
    public Stars: number;
    public ReviewText: string;
    public AppointmentDate: Date;
    public SubmitDate: Date;
}