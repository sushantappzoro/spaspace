export class AdminRequestForDeclineReviewModel {
    public UID: string;
    public DeclineReason: string;
}