export class AdminFacilityApprovalForViewModel {
  public FacilityUID: string;
  public FaciltyName: string;
  public City: string;
  public State: string;
  public MainPhotoUrl: string;
  public RequestedByUID: string;
  public RequestedByName: string;
}
