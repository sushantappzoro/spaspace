export class ServiceTypeForAddModel {
  public Active: boolean;
  public Name: string;
  public Description: string;
  public IconUrl: string;
  public CategoryName: string;
  public CategoryID: number;
}
