export class ServiceNeedForAddModel {
  public Active: boolean;
  public Name: string;
  public CategoryName: string;
  public CategoryID: number;
}
