import { ServiceForAdminViewModel } from "./service-for-admin-view.model";
import { ServiceAddOnForEditModel } from './service-addon-for-edit.model';
import { ServiceFocusAreaForViewEditModel } from './service-focusarea-for-view-edit.model';
import { ServiceNeedForViewEditModel } from './service-need-for-view-edit.model';
import { ServiceTypeForViewEditModel } from './service-type-for-view-edit.model';
import { ServiceLicenseForViewEditModel } from './service-license-for-view-edit.model';

export class ServiceCategoryModel {
  public ID: number ;
  public Name: string;
  public Active: boolean;
  public Services: ServiceForAdminViewModel[];
  public AddOns: ServiceAddOnForEditModel[];
  public FocusAreas: ServiceFocusAreaForViewEditModel[];
  public Needs: ServiceNeedForViewEditModel[];
  public Types: ServiceTypeForViewEditModel[];
  public AssociatedLicenses: ServiceLicenseForViewEditModel[];
}
