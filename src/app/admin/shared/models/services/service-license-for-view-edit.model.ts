export class ServiceLicenseForViewEditModel {
  public UID: string;
  public LicenseTypeID: number;
  public Name: string;
  public CategoryName: string;
  public CategoryID: number;
}
