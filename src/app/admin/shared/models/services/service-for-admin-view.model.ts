export class ServiceForAdminViewModel {
  public UID: string;
  public CategoryID?: number;
  public CategoryName?: string;
  public Name: string;
  public Description: string;
  public Duration: number;
  public ProviderDuration: number;
  public BasePrice: number;
  public ServiceCharge: number;
  public TherapistFlatRate: number;
  public TherapistPercentOfBase: number;
  public FacilityPercentOfBase: number;
  public TherapistPercentOfServiceCharge: number;
  public Active: boolean;
  public LastUpdateDate: Date;
  public LastUpdateBy: string;
}
