export class ServiceTypeForViewEditModel {
  public ID: number;
  public Active: boolean;
  public Name: string;
  public Description: string;
  public IconUrl: string;
  public CategoryName: string;
  public CategoryID: number;
}
