export class ServiceAddOnForAdminViewModel {
  public ID: number;
  public Description: string;
  public Name: string;
  public Price: number;
  public TherapistPercent: number;
  public CategoryName: string;
  public CategoryID: number;
  public Active: boolean; 
}
