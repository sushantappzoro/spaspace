export class ServiceFocusAreaForViewEditModel {
  public ID: number;
  public Active: boolean;
  public Name: string;
  public CategoryName: string;
  public CategoryID: number;
}
