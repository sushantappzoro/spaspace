export class ServiceCategoryForEditModel {
  public ID: number;
  public Active: boolean;
  public Name: string;
}
