export class ServiceForAddModel {
  public CategoryName?: string;
  public CategoryID?: number;
  public Name: string;
  public Description: string;
  public Duration: number;
  public ProviderDuration: number;
  public BasePrice: number;
  public ServiceCharge: number;
  public TherapistPercentOfBase: number;
  public TherapistPercentOfServiceCharge: number;
  public TherapistFlatRate: number;
  public FacilityPercentOfBase: number;
  public Active: boolean;
}
