export class ServiceNeedForViewEditModel {
  public ID: number;
  public Active: boolean;
  public Name: string;
  public CategoryName: string;
  public CategoryID: number;
}
