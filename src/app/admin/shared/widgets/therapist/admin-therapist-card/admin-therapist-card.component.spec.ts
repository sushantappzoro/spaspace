import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistCardComponent } from './admin-therapist-card.component';

describe('AdminTherapistCardComponent', () => {
  let component: AdminTherapistCardComponent;
  let fixture: ComponentFixture<AdminTherapistCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
