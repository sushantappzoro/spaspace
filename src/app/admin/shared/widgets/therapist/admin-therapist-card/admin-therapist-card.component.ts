import { Component, OnInit, Input } from '@angular/core';
import { TherapistForApprovalModel } from '../../../models/therapist/therapist-for-approval.model';

@Component({
  selector: 'spa-admin-therapist-card',
  templateUrl: './admin-therapist-card.component.html',
  styleUrls: ['./admin-therapist-card.component.scss']
})
export class AdminTherapistCardComponent implements OnInit {
  @Input() therapist: TherapistForApprovalModel;

  constructor() { }

  ngOnInit() {
  }

}
