import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminTherapistCardComponent } from './therapist/admin-therapist-card/admin-therapist-card.component';
import { RouterModule } from '@angular/router';
import { MaterialModuleModule } from '../../material-module.module';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { AdminFacilityCardComponent } from './facility/admin-facility-card/admin-facility-card.component';


@NgModule({
  declarations: [
    AdminTherapistCardComponent,
    AdminFacilityCardComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModuleModule,
    MDBBootstrapModulesPro.forRoot(),
  ],
  exports: [
    AdminTherapistCardComponent,
    AdminFacilityCardComponent
  ]
})
export class AdminWidgetsModule { }
