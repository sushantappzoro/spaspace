import { Component, OnInit, Input } from '@angular/core';
import { AdminFacilityApprovalForViewModel } from '../../../models/facility/admin-facility-approval-for-view.model';

@Component({
  selector: 'spa-admin-facility-card',
  templateUrl: './admin-facility-card.component.html',
  styleUrls: ['./admin-facility-card.component.scss']
})
export class AdminFacilityCardComponent implements OnInit {
  @Input() facility: AdminFacilityApprovalForViewModel;

  constructor() { }

  ngOnInit() {
  }

}
