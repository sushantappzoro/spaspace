import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFacilityCardComponent } from './admin-facility-card.component';

describe('AdminFacilityCardComponent', () => {
  let component: AdminFacilityCardComponent;
  let fixture: ComponentFixture<AdminFacilityCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFacilityCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFacilityCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
