import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AdminSharedModule } from '../admin-shared.module';
import { AdminTherapistCompensationsComponent } from './admin-therapist-compensations/admin-therapist-compensations.component';
import { AdminTherapistCompensationComponent } from './admin-therapist-compensations/admin-therapist-compensation/admin-therapist-compensation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DevexModuleModule } from '../../../shared/devex-module.module';
import { MaterialModule } from '../../../material-module.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { NgxCurrencyModule } from 'ngx-currency';
import { SharedModule } from '../../../shared/shared.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [    
    AdminTherapistCompensationsComponent,  
    AdminTherapistCompensationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SharedModule,
    DevexModuleModule,
    MaterialModule,
    NgbModule,
    MDBBootstrapModulesPro,
    NgxMaskModule.forRoot(options),
    NgxCurrencyModule
  ],
  exports: [
    AdminTherapistCompensationsComponent,
    AdminTherapistCompensationComponent
  ]
})
export class AdminComponentsModule { }
