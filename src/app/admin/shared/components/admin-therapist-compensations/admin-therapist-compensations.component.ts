import { Component, OnInit, Input, ViewChild, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { isNumeric } from 'rxjs/util/isNumeric';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';
import { TherapistProfileForEditModel } from '../../../../shared/models/therapist/therapist-profile-for-edit.model';
import { AdminTherapistCompensationService } from '../../../core/services/admin-therapist-compensation.service';
import { ServiceService } from '../../../../core/services/service.service';
import { ServiceCategoryModel } from '../../../../shared/models/offerings/service-category.model';
import { ServiceModel } from '../../../../shared/models/offerings/service.model';
import { CompensationOverrideForViewEditModel } from '../../models/therapist/compensation-override-for-view-edit.model';

@Component({
  selector: 'spa-admin-therapist-compensations',
  templateUrl: './admin-therapist-compensations.component.html',
  styleUrls: ['./admin-therapist-compensations.component.scss']
})
export class AdminTherapistCompensationsComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild("compensationsGrid") servicesGrid: DxDataGridComponent;

  @Input() therapist: TherapistProfileForEditModel;
  @Input() canEdit: boolean = true;
  @Input() compensations: CompensationOverrideForViewEditModel[];

  private subs = new SubSink();

  compensation: CompensationOverrideForViewEditModel;

  categories: ServiceCategoryModel[] = [];
  services: ServiceModel[] = [];

  showModal: boolean = false;

  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private compensationService: AdminTherapistCompensationService,
    private serviceService: ServiceService
  ) {
    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
  }

  ngOnInit() {

    //this.loadCompensation();
    this.loadCategories();

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.compensations) {

      this.loadServices(this.categories);
    }

  }


  loadCompensation() {

  }

  loadCategories() {
    this.subs.add(this.serviceService.getServices()
      .subscribe(resp => {
        this.categories = resp;
        this.loadServices(resp);
      },
        error => {
          notify('Unable to load services.', "error", 3000);
        }));
  }

  loadServices(categories: ServiceCategoryModel[]) {
    //console.log('categories', this.categories);
    this.services = [];

    if (categories) {
      categories.forEach(category => {
        if (category.Services) {
          category.Services.forEach(service => {
            if (this.compensations && this.compensations.length > 0) {
              let tmp = this.compensations.find(x => x.Service == service.Name);
              if (!tmp) {
                this.services.push(service);
              }
            } else {
              this.services.push(service);
            }            
          })
        }        
      })
    }
    //console.log('services', this.services);
  }

  servicesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add Compensation",
        hint: "Add Compensation",
        onClick: () => {
          this.addNew();
        }
      }
    });
  }

  addNew() {
    this.compensation = new CompensationOverrideForViewEditModel();
    this.showModal = true;
  }

  editRecord(e) {
    console.log('editRecord', e);
    this.compensation = { ...e.row.data };
    this.showModal = true;

  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveCompensation(compensation: CompensationOverrideForViewEditModel) {

    if (compensation.UID) {
      this.subs.add(this.compensationService.edit(this.therapist.UID, compensation)
        .subscribe(resp => {
          var i = this.compensations.findIndex(item => item.UID === resp.UID);
          this.compensations.splice(i, 1, resp);
          this.showModal = false;
        },
          error => {
            notify('An error occured updating the compensation record.', 'error', 3000);
          }));
    } else {
      this.subs.add(this.compensationService.add(this.therapist.UID, compensation)
        .subscribe(resp => {
          this.compensations.unshift(resp);
          this.showModal = false;
        },
          error => {
            notify('An error occured adding the compensation record.', 'error', 3000);
          }));
    }
  }

  deleteRow(uid: string) {

    this.subs.add(this.compensationService.remove(this.therapist.UID, uid)
      .subscribe(resp => {
        var i = this.compensations.findIndex(item => item.UID === uid);
        this.compensations.splice(i, 1);
      },
        error => {
          notify('Unable to delete treatment area.', 'error', 800);
        }));

  }

  getPercentageOfBase(rowData) {

    if (!isNumeric(rowData.TherapistPercentOfBase)) {
      return null;
    }

    var therapistPercentOfBase: number = rowData.TherapistPercentOfBase;

    if (therapistPercentOfBase < 1) {
      therapistPercentOfBase = therapistPercentOfBase * 100;
    }

    return therapistPercentOfBase.toPrecision(2) + '%';

  }

}
