import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistCompensationsComponent } from './admin-therapist-compensations.component';

describe('AdminTherapistCompensationsComponent', () => {
  let component: AdminTherapistCompensationsComponent;
  let fixture: ComponentFixture<AdminTherapistCompensationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistCompensationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistCompensationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
