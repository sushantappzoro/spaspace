import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistCompensationComponent } from './admin-therapist-compensation.component';

describe('AdminTherapistCompensationComponent', () => {
  let component: AdminTherapistCompensationComponent;
  let fixture: ComponentFixture<AdminTherapistCompensationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistCompensationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistCompensationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
