import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { isNumeric } from 'rxjs/util/isNumeric';
import { SubSink } from 'subsink';
import { ModalDirective } from 'ng-uikit-pro-standard';
import notify from 'devextreme/ui/notify';
import { AdminTherapistCompensationService } from '../../../../core/services/admin-therapist-compensation.service';
import { CompensationOverrideForViewEditModel } from '../../../models/therapist/compensation-override-for-view-edit.model';
import { AdminServiceCategoryApiService } from '../../../../core/api/admin-service-category-api.service';
import { ServiceModel } from '../../../../../shared/models/offerings/service.model';


@Component({
  selector: 'spa-admin-therapist-compensation',
  templateUrl: './admin-therapist-compensation.component.html',
  styleUrls: ['./admin-therapist-compensation.component.scss']
})

export class AdminTherapistCompensationComponent implements OnInit, OnChanges, OnDestroy {
  @Input() compensation: CompensationOverrideForViewEditModel;
  @Input() services: ServiceModel[];
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveCompensation: EventEmitter<CompensationOverrideForViewEditModel> = new EventEmitter<CompensationOverrideForViewEditModel>();

  @ViewChild('compensationModal') compensationModal: ModalDirective;

  private subs = new SubSink();

  title: string = 'Edit Therapist Compensation';

  compensationForm: FormGroup;

  isEditOnly: boolean;

  selectServices: ServiceModel[];

  constructor(
    private route: ActivatedRoute,
    private compensationService: AdminTherapistCompensationService,
    private servicesService: AdminServiceCategoryApiService
  ) { }

  ngOnInit() {

    this.compensationForm = new FormGroup({
      UID: new FormControl(),
      Service: new FormControl(),
      ServiceUID: new FormControl('', Validators.required),
      PercentOfBase: new FormControl('', this.EitherOrValidator),
      FlatRate: new FormControl('', this.EitherOrValidator),
      PercentOfServiceCharge: new FormControl(''),
    });

    this.compensationForm.setValidators(this.EitherOrValidator);

    this.title = (this.compensation?.UID) ? 'Edit Therapist Compensation' : 'Add Therapist Compensation';

    //console.log('services', this.services);
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log('ngOnChanges', changes);
    if (this.compensationModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.compensationModal.show();
      } else {
        this.compensationModal.hide();
      }
    }

    if (changes.compensation && this.compensationForm) {
      this.title = (this.compensation?.UID && this.compensation?.UID.length > 0) ? 'Edit Therapist Compensation' : 'Add Therapist Compensation';
      if (this.compensation?.UID && this.compensation?.UID.length > 0) {
        //this.compensationForm.controls.ServiceCategoryID.disable();
        this.compensationForm.controls.Service.disable();
      }
      this.populateForm(this.compensation);
    }

    if (changes.services) {
      this.selectServices = changes.services.currentValue;
      //console.log('services', this.services);
    }

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  EitherOrValidator(formControl: AbstractControl) {

    if (!formControl || !formControl.parent) {
      return null;
    }

    var percent = (formControl.parent.get('PercentOfBase') && formControl.parent.get('PercentOfBase').value) ? formControl.parent.get('PercentOfBase').value : null;
    var flat = (formControl.parent.get('FlatRate') && formControl.parent.get('FlatRate').value) ? formControl.parent.get('FlatRate').value : null;

    if (percent && flat) {
      return { notEitherOr: true };
    }
    return null;

  }

  populateForm(compensation: CompensationOverrideForViewEditModel) {
    var holdComp: CompensationOverrideForViewEditModel = {
      Service: compensation.Service,
      FlatRate: compensation.FlatRate,
      PercentOfBase: this.convertForDisplay(compensation.PercentOfBase),
      PercentOfServiceCharge: this.convertForDisplay(compensation.PercentOfServiceCharge),
      UID: compensation.UID,
      ServiceUID: compensation.ServiceUID
    };

    this.compensationForm.patchValue(holdComp);
  }

  convertForDisplay(value: number): number {
    if (value && isNumeric(value)) {
      return (value <= 1) ? value * 100 : value;
    }
  }

  convertForSave(value: number): number {
    if (value && isNumeric(value)) {
      return (value > 1) ? value / 100 : value;
    }
  }


  onClose() {
    this.compensation = null;
    this.showModalChange.emit(false);
  }

  onSave() {
    //console.log('onSave', this.compensationForm);
    if (this.compensationForm.invalid) {
      this.compensationForm.markAllAsTouched();
      return;
    }

    var comp: CompensationOverrideForViewEditModel = Object.assign({}, this.compensation, this.compensationForm.value);

    //comp.FlatRate = this.compensationForm..value.FlatRate;
    comp.PercentOfBase = this.convertForSave(this.compensationForm.value.PercentOfBase);
    comp.PercentOfServiceCharge = this.convertForSave(this.compensationForm.value.PercentOfServiceCharge);

    //comp = Object.assign({}, this.compensation, this.compensationForm.value);

    this.saveCompensation.emit(comp);

  }

}


interface ServiceSelectModel {
  value: string;
  viewValue: string;
}
