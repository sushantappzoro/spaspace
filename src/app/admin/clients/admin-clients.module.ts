import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminClientsRoutingModule } from './admin-clients-routing.module';
import { AdminClientsComponent } from './admin-clients.component';


@NgModule({
  declarations: [AdminClientsComponent],
  imports: [
    CommonModule,
    AdminClientsRoutingModule
  ]
})
export class AdminClientsModule { }
