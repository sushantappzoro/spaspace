import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import notify from 'devextreme/ui/notify';
import { AdminTherapistCompensationService } from '../../core/services/admin-therapist-compensation.service';
import { TherapistProfileForEditModel } from '../../../shared/models/therapist/therapist-profile-for-edit.model';
import { CompensationOverrideForViewEditModel } from '../../shared/models/therapist/compensation-override-for-view-edit.model';

@Component({
  selector: 'app-admin-therapist-management',
  templateUrl: './admin-therapist-management.component.html',
  styleUrls: ['./admin-therapist-management.component.scss']
})
export class AdminTherapistManagementComponent implements OnInit {

  private subs = new SubSink();

  therapist: TherapistProfileForEditModel;
  compensations: CompensationOverrideForViewEditModel[] = [];

  constructor(
    private route: ActivatedRoute,
    private compensationService: AdminTherapistCompensationService
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { therapist: TherapistProfileForEditModel }) => {
        this.therapist = data.therapist;
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.route.data
      .subscribe((data: { compensations: CompensationOverrideForViewEditModel[] }) => {
        this.compensations = data.compensations;
        console.log('compensations', this.compensations)
      },
        error => {
          console.log(error);
        }));

  }

  getTherapistImage() {
    return this.therapist.ProfilePhoto.Url;
  }

}
