import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistManagementComponent } from './admin-therapist-management.component';

describe('AdminTherapistManagementComponent', () => {
  let component: AdminTherapistManagementComponent;
  let fixture: ComponentFixture<AdminTherapistManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
