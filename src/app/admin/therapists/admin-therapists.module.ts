import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AdminTherapistsRoutingModule } from './admin-therapists-routing.module';
import { AdminTherapistsComponent } from './admin-therapists.component';
import { AdminTherapistApprovalsComponent } from './therapist-approvals/admin-therapist-approvals.component';
import { AdminSharedModule } from '../shared/admin-shared.module';
import { AdminTherapistApprovalDetailComponent } from './therapist-approvals/admin-therapist-approval-detail/admin-therapist-approval-detail.component';
import { AdminDevexModule } from '../admin-devex.module';
import { AdminTherapistDetailComponent } from './therapist-detail/admin-therapist-detail.component';
import { MaterialModuleModule } from '../material-module.module';
import { SharedModule } from '../../shared/shared.module';
import { AdminTherapistManagementComponent } from './therapist-management/admin-therapist-management.component';


@NgModule({
  declarations: [
    AdminTherapistsComponent,
    AdminTherapistApprovalsComponent,
    AdminTherapistApprovalDetailComponent,
    AdminTherapistDetailComponent,
    AdminTherapistManagementComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModuleModule,
    SharedModule,
    AdminTherapistsRoutingModule,
    AdminSharedModule,
    TabsModule,
    AdminDevexModule

  ],
  exports: [
    AdminTherapistApprovalsComponent
  ]
})
export class AdminTherapistsModule { }
