import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { TherapistApprovalsService } from '../../core/services/therapist-approvals.service';
import { SubSink } from 'subsink';
import { TherapistForApprovalModel } from '../../shared/models/therapist/therapist-for-approval.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'spa-admin-therapist-approvals',
  templateUrl: './admin-therapist-approvals.component.html',
  styleUrls: ['./admin-therapist-approvals.component.scss']
})
export class AdminTherapistApprovalsComponent implements OnInit, OnDestroy {
  @Output() numberForApproval: EventEmitter<number> = new EventEmitter<number>();

  private subs = new SubSink();

  therapists: TherapistForApprovalModel[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private approvalservice: TherapistApprovalsService
  ) { }

  ngOnInit() {
    this.loadTherapistsForApproval();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadTherapistsForApproval() {

    this.approvalservice.getTherapistsForApproval()
      .subscribe((therapists) => {
        
        this.numberForApproval.emit(therapists.length);

        this.therapists = therapists;
      }, error => {

      });
  }

  getTherapistImage(uid: string) {
    const therapist = this.therapists.find(x => x.UID == uid);
    return (therapist  && therapist.ProfilePhoto && therapist.ProfilePhoto.Url) ? therapist.ProfilePhoto.Url : 'assets/images/placeholder-person.jpg';
  }

  itemClicked(uid: string) {
    this.router.navigate(['../therapists', uid, 'approvals'], { relativeTo: this.route });
  }

}
