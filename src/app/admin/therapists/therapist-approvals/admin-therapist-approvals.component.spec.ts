import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistApprovalsComponent } from './admin-therapist-approvals.component';

describe('AdminTherapistApprovalsComponent', () => {
  let component: AdminTherapistApprovalsComponent;
  let fixture: ComponentFixture<AdminTherapistApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
