import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';

import { TherapistForApprovalDetailModel } from '../../../shared/models/therapist/therapist-for-approval-detail.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TherapistApprovalsService } from '../../../core/services/therapist-approvals.service';
import { TherapistForRejectionModel } from '../../../shared/models/therapist/therapist-for-rejection.model';

@Component({
  selector: 'app-admin-therapist-approval-detail',
  templateUrl: './admin-therapist-approval-detail.component.html',
  styleUrls: ['./admin-therapist-approval-detail.component.scss']
})
export class AdminTherapistApprovalDetailComponent implements OnInit {

  rejectReason: string;
  buttonVisible: boolean = true;
  rejectButtonVisible: boolean = true;
  rejecting: boolean;
  therapist: TherapistForApprovalDetailModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private approvalService: TherapistApprovalsService
  ) {
    this.rejecting = false;}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.therapist = data['therapist'];
    });
  }


  switchToRejectMode() {
    if (!this.rejecting) this.rejecting = true;
    this.buttonVisible = !this.rejecting
  }

  cancelRejectMode() {
    this.rejecting = false;
    this.buttonVisible = !this.rejecting
  }

  reject() {

    this.buttonVisible = false;
    this.rejectButtonVisible = false;

    var provider = new TherapistForRejectionModel();

    provider.Reason = this.rejectReason;
    provider.UID = this.therapist.UID;

    this.approvalService.rejectTherapist(provider)
      .subscribe(res => {        
        notify('Therapist rejected.', "success", 3000);
        this.router.navigate(['/admin']);
      },
        error => {
          this.buttonVisible = true;
          this.rejectButtonVisible = true;
          notify('Unable to reject therapist.', "error", 3000);
        });
    
  }

  approve() {

    this.buttonVisible = false;

    this.approvalService.approveTherapist(this.therapist.UID)
      .subscribe(res => {
        notify('Therapist approved.', "success", 3000);
        this.router.navigate(['/admin']);
      },
        error => {
          this.buttonVisible = true;
          notify('Unable to approve therapist.', "error", 3000);
        });

  }

}
