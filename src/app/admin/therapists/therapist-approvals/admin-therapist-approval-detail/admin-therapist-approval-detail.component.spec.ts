import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistApprovalDetailComponent } from './admin-therapist-approval-detail.component';

describe('AdminTherapistApprovalDetailComponent', () => {
  let component: AdminTherapistApprovalDetailComponent;
  let fixture: ComponentFixture<AdminTherapistApprovalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistApprovalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistApprovalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
