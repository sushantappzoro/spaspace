import { Component, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { AdminTherapistCustomStoreService } from '../core/store/admin-therapist-custom-store.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-therapists',
  templateUrl: './admin-therapists.component.html',
  styleUrls: ['./admin-therapists.component.scss']
})
export class AdminTherapistsComponent implements OnInit {

  gridDisabled: boolean = false;

  therapists: CustomStore

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private therapistStore: AdminTherapistCustomStoreService
  ) {
    this.goToProfile = this.goToProfile.bind(this);
    this.goToAdmin = this.goToAdmin.bind(this);
  }

  ngOnInit() {

    this.therapists = this.therapistStore.getStore();

  }

  therapistsGrid_onItemSelect(e) {
    //console.log('therapistsGrid_onItemSelect', e);
    this.router.navigate([e.key, 'detail'], { relativeTo: this.route });
  }

  goToProfile(e) {
    //console.log(e.row.data.UID);
    this.router.navigate([e.row.data.UID, 'detail'], { relativeTo: this.route });
  }

  goToAdmin(e) {
    this.router.navigate([e.row.data.UID, 'manage'], { relativeTo: this.route });
  }

}
