import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTherapistDetailComponent } from './admin-therapist-detail.component';

describe('AdminTherapistDetailComponent', () => {
  let component: AdminTherapistDetailComponent;
  let fixture: ComponentFixture<AdminTherapistDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTherapistDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTherapistDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
