import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { map } from 'rxjs/operators';
import { Route } from '@angular/compiler/src/core';
import { CanActivate, ActivatedRoute } from '@angular/router';
import { DxTabsComponent } from 'devextreme-angular';

import { TherapistProfileForEditModel } from '../../../shared/models/therapist/therapist-profile-for-edit.model';

export class Longtab {
  text: string;
}

@Component({
  selector: 'app-admin-therapist-detail',
  templateUrl: './admin-therapist-detail.component.html',
  styleUrls: ['./admin-therapist-detail.component.scss']
})
export class AdminTherapistDetailComponent implements OnInit, OnDestroy, CanActivate, AfterViewInit {
  @ViewChild("tabMenu") tabMenu: DxTabsComponent;

  private subs = new SubSink();

  //form stuff

  showCert: boolean = false;
  showSkills: boolean = false;
  showProfile: boolean = true;
  showAssocFacilities: boolean = false;
  showInsurance: boolean = false;

  longtabs: Longtab[] = [
    { text: "Profile" },
    { text: "Expertise" },
    { text: "Certifications" },
    { text: "Insurance" },
    { text: "Associated Facilities" }
  ];

  therapist: TherapistProfileForEditModel = new TherapistProfileForEditModel();

  lastSub: string = '';

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {    

    this.subs.add(this.route.data
      .subscribe((data: { therapist: TherapistProfileForEditModel }) => {
        this.therapist = data.therapist;
      },
        error => {
          console.log(error);
        }));

  }

  ngAfterViewInit() {
    //fix this 
    //this.tabMenu.selectedIndex = 0;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  canActivate() {
    return true;
  }

  onViewChanged(e) {
    console.log('onViewChanged', e.addedItems[0].text);

    if (e && e.addedItems && e.addedItems.length != 0) {

      const selection = e.addedItems[0].text;

      switch (selection) {
        case 'Profile':
          this.showProfile = true;
          this.showSkills = false;
          this.showCert = false;
          this.showAssocFacilities = false;
          this.showInsurance = false;
          break;
        case 'Certifications':
          //this.loadCertifications();
          //this.loadLicenses();
          this.showCert = true;
          this.showSkills = false;
          this.showProfile = false;
          this.showAssocFacilities = false;
          this.showInsurance = false;
          break;
        case 'Expertise':
          this.showSkills = true;
          this.showCert = false;
          this.showProfile = false;
          this.showAssocFacilities = false;
          this.showInsurance = false;
          break;
        case 'Associated Facilities':
          //this.loadFacilities();
          this.showAssocFacilities = true;
          this.showSkills = false;
          this.showCert = false;
          this.showProfile = false;
          this.showAssocFacilities = true;
          this.showInsurance = false;
          break;
        case 'Insurance':
          //this.loadInsurance();
          this.showAssocFacilities = true;
          this.showSkills = false;
          this.showCert = false;
          this.showProfile = false;
          this.showAssocFacilities = false;
          this.showInsurance = true;
          break;
        default:
      }
    }
  }

  // data handling


 

  

}
