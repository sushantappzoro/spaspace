import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxCurrencyModule } from "ngx-currency";

import { AdminFacilitiesRoutingModule } from './admin-facilities-routing.module';
import { AdminFacilitiesComponent } from './admin-facilities.component';
import { DevexModuleModule } from '../../shared/devex-module.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AdminFacilityDetailComponent } from './facility-detail/admin-facility-detail.component';
import { SharedModule } from '../../shared/shared.module';
import { AdminFacilityApprovalsComponent } from './facility-approvals/admin-facility-approvals.component';
import { AdminSharedModule } from '../shared/admin-shared.module';
import { AdminFacilityApprovalDetailComponent } from './facility-approvals/facility-approvals-detail/admin-facility-approval-detail.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AdminFacilityManagementComponent } from './facility-management/admin-facility-management.component';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminFacilityManageMainComponent } from './facility-management/main/admin-facility-manage-main.component';

@NgModule({
  declarations: [
    AdminFacilitiesComponent,
    AdminFacilityDetailComponent,
    AdminFacilityApprovalsComponent,
    AdminFacilityApprovalDetailComponent,
    AdminFacilityManagementComponent,
    AdminFacilityManageMainComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    MDBBootstrapModulesPro,
    DevexModuleModule,
    SharedModule,
    AdminSharedModule,
    AdminFacilitiesRoutingModule,
    TabsModule,
    NgxCurrencyModule
  ],
  exports: [
    AdminFacilityApprovalsComponent
  ]
})
export class AdminFacilitiesModule { }
