import { Component, OnInit, OnDestroy } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';

import { AdminFacilityApprovalsService } from '../../../core/services/admin-facility-approvals.service';
import { FacilityInfoForAdminModel } from '../../../../shared/models/facility/facility-info-for-admin.model';
import { FacilityForRejectionModel } from '../../../shared/models/facility/facility-for-rejection.model';

@Component({
  selector: 'app-admin-facility-approval-detail',
  templateUrl: './admin-facility-approval-detail.component.html',
  styleUrls: ['./admin-facility-approval-detail.component.scss']
})
export class AdminFacilityApprovalDetailComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facility: FacilityInfoForAdminModel;

  rejectReason: string;
  buttonVisible: boolean = true;
  rejectButtonVisible: boolean = true;
  rejecting: boolean;



  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private approvalService: AdminFacilityApprovalsService) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facility: FacilityInfoForAdminModel }) => {
        this.facility = data.facility;
      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  switchToRejectMode() {
    if (!this.rejecting) this.rejecting = true;
    this.buttonVisible = !this.rejecting
  }

  cancelRejectMode() {
    this.rejecting = false;
    this.buttonVisible = !this.rejecting
  }

  reject() {

    this.buttonVisible = false;
    this.rejectButtonVisible = false;

    var facility = new FacilityForRejectionModel();

    facility.Reason = this.rejectReason;
    facility.UID = this.facility.UID;

    this.approvalService.rejectFacility(facility)
      .subscribe(res => {
        notify('Facility rejected.', "success", 3000);
        this.router.navigate(['/admin']);
      },
        error => {
          this.buttonVisible = true;
          this.rejectButtonVisible = true;
          notify('Unable to reject facility.', "error", 3000);
        });

  }

  approve() {

    this.buttonVisible = false;

    this.approvalService.approveFacility(this.facility.UID)
      .subscribe(res => {
        notify('Facility approved.', "success", 3000);
        this.router.navigate(['/admin']);
      },
        error => {
          this.buttonVisible = true;
          notify('Unable to approve facility.', "error", 3000);
        });

  }

}
