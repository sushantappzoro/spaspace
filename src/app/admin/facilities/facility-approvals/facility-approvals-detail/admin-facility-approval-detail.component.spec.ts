import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFacilityApprovalDetailComponent } from './admin-facility-approval-detail.component';

describe('AdminFacilityApprovalDetailComponent', () => {
  let component: AdminFacilityApprovalDetailComponent;
  let fixture: ComponentFixture<AdminFacilityApprovalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFacilityApprovalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFacilityApprovalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
