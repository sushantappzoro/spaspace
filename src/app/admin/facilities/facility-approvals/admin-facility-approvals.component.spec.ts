import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFacilityApprovalsComponent } from './admin-facility-approvals.component';

describe('AdminFacilityApprovalsComponent', () => {
  let component: AdminFacilityApprovalsComponent;
  let fixture: ComponentFixture<AdminFacilityApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFacilityApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFacilityApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
