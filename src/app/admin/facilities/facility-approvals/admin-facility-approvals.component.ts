import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { AdminFacilityApprovalForViewModel } from '../../shared/models/facility/admin-facility-approval-for-view.model';
import { AdminFacilityApprovalsService } from '../../core/services/admin-facility-approvals.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'spa-admin-facility-approvals',
  templateUrl: './admin-facility-approvals.component.html',
  styleUrls: ['./admin-facility-approvals.component.scss']
})
export class AdminFacilityApprovalsComponent implements OnInit, OnDestroy {
  @Output() numberForApproval: EventEmitter<number> = new EventEmitter<number>();

  private subs = new SubSink();

  facilitiesRequiringApproval: AdminFacilityApprovalForViewModel[];

  constructor(
    private approvalservice: AdminFacilityApprovalsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.loadFacilitiesForApproval();

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadFacilitiesForApproval() {

    this.approvalservice.getFacilitiesForApproval()
      .subscribe((facilities) => {
        //console.log('numberForApproval.emit=' + facilities.length);
        this.numberForApproval.emit(facilities.length);

        this.facilitiesRequiringApproval = facilities;
      }, error => {

      });

  }

  facilityClicked(uid: string) {
    this.router.navigate(['/admin/facilities/approvals', uid]);
  }

}
