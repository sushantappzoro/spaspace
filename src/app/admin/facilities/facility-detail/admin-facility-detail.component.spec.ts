import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFacilityDetailComponent } from './admin-facility-detail.component';

describe('AdminFacilityDetailComponent', () => {
  let component: AdminFacilityDetailComponent;
  let fixture: ComponentFixture<AdminFacilityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFacilityDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFacilityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
