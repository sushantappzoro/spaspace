import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';

@Component({
  selector: 'app-admin-facility-detail',
  templateUrl: './admin-facility-detail.component.html',
  styleUrls: ['./admin-facility-detail.component.scss']
})
export class AdminFacilityDetailComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facility: FacilityInfoForAdminModel

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facility: FacilityInfoForAdminModel }) => {
        this.facility = data.facility;
      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
