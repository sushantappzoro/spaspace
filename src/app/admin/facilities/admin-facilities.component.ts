import { Component, OnInit, OnDestroy } from '@angular/core';
import { FacilityForListModel } from '../../shared/models/facility/facility-for-list.model';
import { SubSink } from 'subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminFacilityForListModel } from '../shared/models/facility/admin-facility-for-list.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin-facilities',
  templateUrl: './admin-facilities.component.html',
  styleUrls: ['./admin-facilities.component.scss']
})
export class AdminFacilitiesComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facilities: Observable<AdminFacilityForListModel[]>;
  gridDisabled: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {

    this.goToProfile = this.goToProfile.bind(this);
    this.goToAdmin = this.goToAdmin.bind(this);
    this.goToManage = this.goToManage.bind(this);
    this.goToResources = this.goToResources.bind(this);
    this.viewAppointments = this.viewAppointments.bind(this);
    this.searchAppointments = this.searchAppointments.bind(this);
  }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilities: Observable<AdminFacilityForListModel[]> }) => {
        this.facilities = data.facilities;
      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  facilitiesGrid_onItemSelect(e) {
    this.router.navigate(['./detail', e.key.UID], { relativeTo: this.route });
  }

  goToProfile(e) {
    console.log(e.row.data.UID);
    this.router.navigate(['./detail', e.row.data.UID], { relativeTo: this.route });
  }

  goToAdmin(e) {
    this.router.navigate(['./manage', e.row.data.UID], { relativeTo: this.route });
  }

  goToManage(e) {
    this.router.navigate(['./managers', e.row.data.UID], { relativeTo: this.route });
  }

  goToResources(e) {
    this.router.navigate([e.row.data.UID, 'treatmentareas'], { relativeTo: this.route });
  }

  viewAppointments(e) {
    //console.log('this.route', this.route)
    this.router.navigate([e.row.data.UID, 'appointments'], { relativeTo: this.route });
  }

  searchAppointments(e) {
    //console.log('this.route',this.route)
    this.router.navigate([e.row.data.UID, 'appointments', 'list'], { relativeTo: this.route });
  }


}
