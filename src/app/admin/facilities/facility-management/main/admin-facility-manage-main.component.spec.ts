import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFacilityManageMainComponent } from './admin-facility-manage-main.component';

describe('AdminFacilityManageMainComponent', () => {
  let component: AdminFacilityManageMainComponent;
  let fixture: ComponentFixture<AdminFacilityManageMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFacilityManageMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFacilityManageMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
