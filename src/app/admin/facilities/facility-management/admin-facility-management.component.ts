import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import notify from 'devextreme/ui/notify';

import { AdminFacilityService } from '../../core/services/admin-facility.service';
import { FacilityRegionModel } from '../../../shared/models/facility/facility-region.model';
import { FacilityParentModel } from '../../../shared/models/facility/facility-parent.model';
import { AdminFacilityInfoForEditModel } from '../../shared/models/facility/admin-facility-info-for-edit.model';


@Component({
  selector: 'app-admin-facility-management',
  templateUrl: './admin-facility-management.component.html',
  styleUrls: ['./admin-facility-management.component.scss']
})
export class AdminFacilityManagementComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facility: AdminFacilityInfoForEditModel;

  parents: FacilityParentModel[] = [];
  regions: FacilityRegionModel[] = [];

  imageUrl: string = '/spa/assets/images/location.jpg'; // image_placeholder.png';

  adminManageForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private facilityService: AdminFacilityService
  ) { }

  ngOnInit() {

    this.loadDropdowns();

    this.adminManageForm = new FormGroup({
      ParentCompanyUID: new FormControl(),
      RegionUID: new FormControl(),
      AmenityCharge: new FormControl(),
      FacilitysCut: new FormControl(),
    });

    this.subs.add(this.route.data
      .subscribe((data: { facility: AdminFacilityInfoForEditModel }) => {
        if (data.facility.FacilitysCut && data.facility.FacilitysCut > 0) {
          data.facility.FacilitysCut = data.facility.FacilitysCut * 100;
        }
        this.facility = data.facility;
        this.imageUrl = data.facility.MainPhotoUrl;
        this.adminManageForm.patchValue(this.facility);
      },
        error => {
          console.log(error);
        }));

  }

  loadDropdowns() {

    this.subs.add(this.facilityService.getRegions()
      .subscribe(resp => {
        this.regions = resp;
      },
        error => {
          //notify('Unable to save facility profile.', "error", 3000);
        }));

    this.subs.add(this.facilityService.getParents()
      .subscribe(resp => {
        this.parents = resp;
      },
        error => {
          //notify('Unable to save facility profile.', "error", 3000);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  getFacilityImage() {
    return this.imageUrl;
  }

  onSave() {

    console.log('save');
    if (this.adminManageForm.invalid) {
      this.adminManageForm.markAllAsTouched();
      return;
    }
    console.log('Here is the form');
    console.log(this.adminManageForm);
    this.facility.ParentCompanyUID = this.adminManageForm.value.ParentCompany;
    this.facility.RegionUID = this.adminManageForm.value.Region;
    this.facility.AmenityCharge = this.adminManageForm.value.AmenityCharge;
    if (this.adminManageForm.value.FacilitysCut > 0) {
        this.facility.FacilitysCut = this.adminManageForm.value.FacilitysCut / 100;
      }
    console.log(this.facility);
    // var fac = new AdminFacilityInfoForEditModel();
    // fac = Object.assign({}, this.facility, this.adminManageForm.value);

    this.subs.add(this.facilityService.editFacilityInfo(this.facility.UID, this.facility)
      .subscribe(resp => {
        notify('Facility saved succesfully.', 'success', 3000);
      },
        error => {
          notify('An error occured updating the facility.', 'error', 3000);
        }));
  }
}
