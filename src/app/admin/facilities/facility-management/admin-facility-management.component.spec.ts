import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFacilityManagementComponent } from './admin-facility-management.component';

describe('AdminFacilityManagementComponent', () => {
  let component: AdminFacilityManagementComponent;
  let fixture: ComponentFixture<AdminFacilityManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFacilityManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFacilityManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
