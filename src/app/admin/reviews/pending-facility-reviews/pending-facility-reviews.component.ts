import { Component, OnInit, EventEmitter, OnDestroy, Output, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import { ModalDirective } from 'ng-uikit-pro-standard';

import { AdminPendingReviewsService } from '../../core/services/admin-pending-reviews.service';
import { PendingFacilityReviewForAdminView } from '../../shared/models/facility/pending-facility-review-for-admin-view.model';
import { AdminRequestForDeclineReviewModel } from '../../shared/models/facility/admin-request-for-decline-review.model';

@Component({
  selector: 'spa-pending-facility-reviews',
  templateUrl: './pending-facility-reviews.component.html',
  styleUrls: ['./pending-facility-reviews.component.scss']
})
export class PendingFacilityReviewsComponent implements OnInit, OnDestroy {
  @Output() numberForFacilityReviews: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild('reviewModal') reviewModal: ModalDirective;

  private subs = new SubSink();

  pendingfacilityReviews: PendingFacilityReviewForAdminView[];

  max = 5;

  selectedReview: PendingFacilityReviewForAdminView;

  reviewForm: FormGroup;

  showReason: boolean = false;

  constructor(private pendingReviewsService: AdminPendingReviewsService) { }

  ngOnInit() {

    this.reviewForm = new FormGroup({
      Action: new FormControl(),
      RejectText: new FormControl('')
    });

    this.loadPendingReviews();

  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  loadPendingReviews() {
    this.subs.add(this.pendingReviewsService.getPendingFacilityReviews()
      .subscribe((reviews) => {
        this.numberForFacilityReviews.emit(reviews.length);
        this.pendingfacilityReviews = reviews;
      }, error => {

      }));
  }

  itemClicked(uid: string) {
    this.selectedReview = this.pendingfacilityReviews.find(x => x.UID == uid);
    if (this.selectedReview) {
      this.reviewModal.show();
    }
  }

  actionChanged(e) {
    this.showReason = (e.value == 'Reject') ? true : false;
  }

  onSave(uid: string) {

    if (this.reviewForm.get('Action').value == 'Approve') {
      this.subs.add(this.pendingReviewsService.approveFacilityReview(uid)
        .subscribe((resp) => {
          this.reviewForm.reset();
          this.selectedReview = null;
          this.reviewModal.hide();
          this.loadPendingReviews();
        }, error => {

        }));
    } else {
      var reason: AdminRequestForDeclineReviewModel = { DeclineReason: this.reviewForm.get('RejectText').value, UID: uid }

      this.subs.add(this.pendingReviewsService.declineFacilityReview(reason)
        .subscribe((resp) => {
          this.reviewForm.reset();
          this.selectedReview = null;
          this.reviewModal.hide();
          this.loadPendingReviews();
        }, error => {

        }));
    }

  }

  onClose() {
    this.selectedReview = null;
  }

  onCancel() {
    this.reviewModal.hide();
  }

}
