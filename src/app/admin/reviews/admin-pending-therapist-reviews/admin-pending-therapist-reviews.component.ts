import { Component, OnInit, EventEmitter, OnDestroy, Output, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { SubSink } from 'subsink';
import { AdminPendingReviewsService } from '../../core/services/admin-pending-reviews.service';
import { PendingTherapistReviewForAdminViewModel } from '../../shared/models/therapist/pending-therapist-review-for-admin-view.model';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { AdminRequestForDeclineReviewModel } from '../../shared/models/facility/admin-request-for-decline-review.model';

@Component({
  selector: 'spa-admin-pending-therapist-reviews',
  templateUrl: './admin-pending-therapist-reviews.component.html',
  styleUrls: ['./admin-pending-therapist-reviews.component.scss']
})
export class AdminPendingTherapistReviewsComponent implements OnInit {
  @Output() numberForTherapistReviews: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild('reviewModal') reviewModal: ModalDirective;

  private subs = new SubSink();

  pendingTherapistReviews: PendingTherapistReviewForAdminViewModel[];

  selectedReview: PendingTherapistReviewForAdminViewModel;

  max = 5;

  reviewForm: FormGroup;

  showReason: boolean = false;

  constructor(
    private pendingReviewsService: AdminPendingReviewsService
  ) { }

  ngOnInit() {

    this.reviewForm = new FormGroup({
      Action: new FormControl(),
      RejectText: new FormControl('')
    });

    this.loadPendingReviews();

  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  loadPendingReviews() {
    this.subs.add(this.pendingReviewsService.getPendingTherapistReviews()
      .subscribe((reviews) => {
        this.numberForTherapistReviews.emit(reviews.length);
        this.pendingTherapistReviews = reviews;
      }, error => {

      }));
  }

  itemClicked(uid: string) {
    this.selectedReview = this.pendingTherapistReviews.find(x => x.UID == uid);
    if (this.selectedReview) {
      this.reviewModal.show();
    }
  }

  actionChanged(e) {
    this.showReason = (e.value == 'Reject') ? true : false;
  }

  onSave(uid: string) {

    if (this.reviewForm.get('Action').value == 'Approve') {
      this.subs.add(this.pendingReviewsService.approveTherapistReview(uid)
        .subscribe((resp) => {
          this.reviewForm.reset();
          this.selectedReview = null;
          this.reviewModal.hide();
          this.loadPendingReviews();
        }, error => {

        }));
    } else {
      var reason: AdminRequestForDeclineReviewModel = { DeclineReason: this.reviewForm.get('RejectText').value, UID: uid }

      this.subs.add(this.pendingReviewsService.declineTherapistReview(reason)
        .subscribe((resp) => {
          this.reviewForm.reset();
          this.selectedReview = null;
          this.reviewModal.hide();
          this.loadPendingReviews();
        }, error => {

        }));
    }

  }

  onClose() {
    this.selectedReview = null;
  }

  onCancel() {
    this.reviewModal.hide();
  }

}
