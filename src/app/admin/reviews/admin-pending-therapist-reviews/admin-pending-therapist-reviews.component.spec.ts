import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPendingTherapistReviewsComponent } from './admin-pending-therapist-reviews.component';

describe('AdminPendingTherapistReviewsComponent', () => {
  let component: AdminPendingTherapistReviewsComponent;
  let fixture: ComponentFixture<AdminPendingTherapistReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPendingTherapistReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPendingTherapistReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
