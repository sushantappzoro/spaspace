import { Component, OnInit } from '@angular/core';
import themes from "devextreme/ui/themes";
import { Router, ActivatedRoute } from '@angular/router';
import { MenuListItemModel } from '../shared/models/common/menu-list-item.model';

@Component({
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  homePage = '/admin';

  selectedItems: any[] = [];

  navigation: MenuListItemModel[] = [
    { id: 1, text: "Dashboard", icon: "fas fa-home", link:'./dashboard', hasSubItems: false },
    { id: 2, text: "Therapists", icon: "fas fa-hands", link: './therapists', hasSubItems: false },
    { id: 3, text: "Facilities", icon: "fas fa-building", link: './facilities', hasSubItems: false },
    {
      id: 4,
      text: "Administrative",
      icon: "fas fa-clipboard",
      hasSubItems: true,
      subItems: [
        { id: 4.1, text: "Services", disabled: false, link: "./services" }
      ]
    },
    //{ id: 5, text: "Accounting", icon: "money", link: './dashboard' }
    //{ id: 6, text: "Standards", icon: "chart", status: ['Approved'] }
  ];

  constructor(private router: Router, private route: ActivatedRoute) {
    themes.current("material.admin.light");
  }

  ngOnInit() {
  }

  menu_selectionChanged(e) {
    this.router.navigate([e.itemData.link], { relativeTo: this.route });

    ////var selection = e.addedItems[0].text;
    //var selection = e.itemData.text;
    //switch (selection) {
    //  case 'Dashboard':
    //    this.router.navigate(['./dashboard'], { relativeTo: this.route });
    //    break;
    //  case 'Therapists':
    //    this.router.navigate(['./therapists'], { relativeTo: this.route });
    //    break;
    //  case 'Facilities':
    //    this.router.navigate(['./facilities'], { relativeTo: this.route });
    //    break;
    //  case 'Services':
    //    this.router.navigate(['./services'], { relativeTo: this.route });
    //    break;
    //  case 'Availability':
    //    this.router.navigate(['./availability'], { relativeTo: this.route });
    //    break;
    //  case 'Standards':
    //    this.router.navigate(['./standards'], { relativeTo: this.route });
    //    break;
    //  default:
    //}
  }

}

//export class MenuListItem {
//  id: number;
//  text: string;
//  icon: string;
//  status?: string[];
//  disabled?: boolean;
//  link?: string;
//}
