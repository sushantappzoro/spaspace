import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { LicenseForViewEditModel } from '../../shared/models/lists/license-for-viewedit.model';
import { LicenseForAddModel } from '../../shared/models/lists/license-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminLicenseApiService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<LicenseForViewEditModel[]> {
    return this.http.get<LicenseForViewEditModel[]>
    (configSettings.WebAPI_URL + '/auth/admin/lists/licenses');
  }

  public getSingle(ID: number): Observable<LicenseForViewEditModel> {
    return this.http.get<LicenseForViewEditModel>
    (configSettings.WebAPI_URL + '/auth/admin/lists/licenses/' + ID);
  }

  public add(license: LicenseForAddModel): Observable<LicenseForViewEditModel> {
    return this.http.post<LicenseForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/lists/licenses', license);
  }

  public edit(license: LicenseForViewEditModel): Observable<LicenseForViewEditModel> {
    return this.http.put<LicenseForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/lists/licenses', license);
  }

  public remove(ID: number): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/lists/licenses/' + ID);
  }
}
