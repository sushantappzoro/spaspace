import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceFocusAreaForViewEditModel } from '../../shared/models/services/service-focusarea-for-view-edit.model';
import { ServiceFocusAreaForAddModel } from '../../shared/models/services/service-focusarea-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceFocusAreaApiService {

  constructor(private http: HttpClient) { }

  public get(categoryID: number): Observable<ServiceFocusAreaForViewEditModel[]> {
    return this.http.get<ServiceFocusAreaForViewEditModel[]>
    (configSettings.WebAPI_URL + '/auth/servicecategories/' + categoryID + '/focusareas');
  }

  public add(categoryID: number, focusArea: ServiceFocusAreaForAddModel): Observable<ServiceFocusAreaForViewEditModel> {
    return this.http.post<ServiceFocusAreaForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/focusareas', focusArea);
  }

  public edit(categoryID: number, focusArea: ServiceFocusAreaForViewEditModel): Observable<ServiceFocusAreaForViewEditModel> {
    return this.http.put<ServiceFocusAreaForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/focusareas', focusArea);
  }

  public remove(categoryID: number, focusAreaID: number): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/focusareas/' + focusAreaID);
  }
}
