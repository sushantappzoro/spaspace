import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { FacilityServiceCompensationModel } from '../../../shared/models/facility/facility-service-compensation.model';


@Injectable({
  providedIn: 'root'
})
export class AdminFacilityCompensationApiService {

  constructor(private http: HttpClient) { }

  public get(facilityUID: string): Observable<FacilityServiceCompensationModel[]> {
    return this.http.get<FacilityServiceCompensationModel[]>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation');
  }

  public edit(facilityUID: string, compensation: FacilityServiceCompensationModel): Observable<FacilityServiceCompensationModel> {
    return this.http.put<FacilityServiceCompensationModel>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation', compensation);
  }

}
