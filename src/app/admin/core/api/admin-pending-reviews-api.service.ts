import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { configSettings } from '../../../app.config';
import { Observable } from 'rxjs';
import { PendingFacilityReviewForAdminView } from '../../shared/models/facility/pending-facility-review-for-admin-view.model';
import { AdminRequestForDeclineReviewModel } from '../../shared/models/facility/admin-request-for-decline-review.model';
import { PendingTherapistReviewForAdminView } from '../../shared/models/facility/pending-therapist-review-for-admin-view.model';

@Injectable({
    providedIn: 'root'
})
export class AdminPendingReviewsApiService {
    constructor(private http: HttpClient) { }

    public getPendingFacilityReviews(): Observable<PendingFacilityReviewForAdminView[]> {
        return this.http.get<PendingFacilityReviewForAdminView[]>(
            configSettings.WebAPI_URL + '/auth/admin/dashboard/pendingfacilityreviews');
    }

    public approveFacilityReview(uid: string): Observable<any[]> {
        return this.http.get<any>(configSettings.WebAPI_URL +
            '/auth/admin/dashboard/pendingfacilityreviews/' + uid + '/Approve');
      }

    public declineFacilityReview(declineReason: AdminRequestForDeclineReviewModel): Observable<any[]> {
        return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/dashboard/pendingfacilityreviews/decline', declineReason);
    }

    public getPendingTherapistReviews(): Observable<PendingTherapistReviewForAdminView[]> {
        return this.http.get<PendingTherapistReviewForAdminView[]>(
            configSettings.WebAPI_URL + '/auth/admin/dashboard/pendingtherapistreviews');
    }

    public approveTherapistReview(uid: string): Observable<any[]> {
        console.log('This is the damn URL: ' + configSettings.WebAPI_URL +
            '/auth/admin/dashboard/pendingtherapistreviews/' + uid + '/Approve');
        return this.http.get<any>(configSettings.WebAPI_URL +
            '/auth/admin/dashboard/pendingtherapistreviews/' + uid + '/Approve');
    }

    public declineTherapistReview(declineReason: AdminRequestForDeclineReviewModel): Observable<any[]> {
        return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/dashboard/pendingtherapistreviews/decline', declineReason);
    }

}
