import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceLicenseForAddModel } from '../../shared/models/services/service-license-for-add.model';
import { ServiceLicenseForViewEditModel } from '../../shared/models/services/service-license-for-view-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceLicenseApiService {

  constructor(private http: HttpClient) { }

  public getList(categoryID: number): Observable<ServiceLicenseForViewEditModel[]> {
    return this.http.get<ServiceLicenseForViewEditModel[]>
    (configSettings.WebAPI_URL + '/auth/servicecategories/' + categoryID + '/licenses');
  }

  public getSingle(categoryID: number, UID: string): Observable<ServiceLicenseForViewEditModel> {
    return this.http.get<ServiceLicenseForViewEditModel>
    (configSettings.WebAPI_URL + '/auth/servicecategories/' + categoryID + '/licenses/' + UID);
  }

  public add(categoryID: number, license: ServiceLicenseForAddModel): Observable<ServiceLicenseForViewEditModel> {
    return this.http.post<ServiceLicenseForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/licenses', license);
  }

  public edit(categoryID: number, license: ServiceLicenseForViewEditModel): Observable<ServiceLicenseForViewEditModel> {
    return this.http.put<ServiceLicenseForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/licenses', license);
  }

  public remove(categoryID: number, ID: number): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/licenses/' + ID);
  }
}
