import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { AdminFacilityForListModel } from '../../shared/models/facility/admin-facility-for-list.model';
import { FacilityParentModel } from '../../../shared/models/facility/facility-parent.model';
import { FacilityRegionModel } from '../../../shared/models/facility/facility-region.model';
import { AdminFacilityInfoForEditModel } from '../../shared/models/facility/admin-facility-info-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityApiService {

  constructor(private http: HttpClient) { }

  public getList(): Observable<AdminFacilityForListModel[]> {
    return this.http.get<AdminFacilityForListModel[]>(configSettings.WebAPI_URL + '/auth/admin/Facilities');
  }

  public getFacilityInfo(facilityUID: string): Observable<AdminFacilityInfoForEditModel> {
    return this.http.get<AdminFacilityInfoForEditModel>(configSettings.WebAPI_URL + '/auth/admin/Facilities/' + facilityUID + '/AdminInfo');
  }

  public editFacilityInfo(facilityUID: string, facility: AdminFacilityInfoForEditModel): Observable<AdminFacilityInfoForEditModel> {
    return this.http.put<AdminFacilityInfoForEditModel>(configSettings.WebAPI_URL + '/auth/admin/Facilities/' + facilityUID + '/AdminInfo', facility);
  }

  public getParents(): Observable<FacilityParentModel[]> {
    return this.http.get<FacilityParentModel[]>(configSettings.WebAPI_URL + '/auth/admin/Facilities/ParentCompanies');
  }

  public getRegions(): Observable<FacilityRegionModel[]> {
    return this.http.get<FacilityRegionModel[]>(configSettings.WebAPI_URL + '/auth/admin/Facilities/Regions');
  }

}
