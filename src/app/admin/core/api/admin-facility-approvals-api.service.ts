import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { AdminFacilityApprovalForViewModel } from '../../shared/models/facility/admin-facility-approval-for-view.model';
import { FacilityForAuthViewModel } from '../../../shared/models/facility/facility-for-auth-view.model';
import { FacilityForRejectionModel } from '../../shared/models/facility/facility-for-rejection.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityApprovalsApiService {

  constructor(private http: HttpClient) { }

  public getFacilitiesForApproval(): Observable<AdminFacilityApprovalForViewModel[]> {
    return this.http.get<AdminFacilityApprovalForViewModel[]>(configSettings.WebAPI_URL + '/auth/admin/dashboard/FacilityApprovals');
  }

  public getFacilitiesForApprovalDetail(uid: string): Observable<FacilityForAuthViewModel> {
    return this.http.get<FacilityForAuthViewModel>(configSettings.WebAPI_URL + '/auth/admin/dashboard/FacilityApprovals/' + uid);
  }

  public approveFacility(uid: string): Observable<any[]> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/dashboard/FacilityApprovals/Approve/' + uid, null);
  }

  public rejectFacility(facility: FacilityForRejectionModel): Observable<any[]> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/dashboard/FacilityApprovals/Reject/' + facility.UID, facility.Reason);
  }
}
