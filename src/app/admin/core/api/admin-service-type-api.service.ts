import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceTypeForViewEditModel } from '../../shared/models/services/service-type-for-view-edit.model';
import { ServiceTypeForAddModel } from '../../shared/models/services/service-type-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceTypeApiService {

  constructor(private http: HttpClient) { }

  public get(categoryID: number): Observable<ServiceTypeForViewEditModel[]> {
    return this.http.get<ServiceTypeForViewEditModel[]>
    (configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/types');
  }

  public add(categoryID: number, type: ServiceTypeForAddModel): Observable<ServiceTypeForViewEditModel> {
    return this.http.post<ServiceTypeForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/types', type);
  }

  public edit(categoryID: number, type: ServiceTypeForViewEditModel): Observable<ServiceTypeForViewEditModel> {
    return this.http.put<ServiceTypeForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/types', type);
  }

  public remove(categoryID: number, typeID: number): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/types/' + typeID);
  }
}
