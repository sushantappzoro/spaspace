import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceForAdminViewModel } from '../../shared/models/services/service-for-admin-view.model';
import { ServiceForAddModel } from '../../shared/models/services/service-for-add.model';
import { ServiceForEditModel } from '../../shared/models/services/service-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServicesApiService {

  constructor(private http: HttpClient) { }

  public get(): Observable<ServiceForAdminViewModel[]> {
    return this.http.get<ServiceForAdminViewModel[]>(configSettings.WebAPI_URL + '/auth/services');
  }

  public add(categoryID: number, service: ServiceForAddModel): Observable<ServiceForAdminViewModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/services', service);
  };

  public edit(categoryID: number, service: ServiceForEditModel): Observable<ServiceForAdminViewModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/services', service);
  };

  public remove(categoryID: number, serviceUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/services/' + serviceUID);
  };
}
