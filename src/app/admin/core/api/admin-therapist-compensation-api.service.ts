import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { CompensationOverrideForViewEditModel } from '../../shared/models/therapist/compensation-override-for-view-edit.model';
import { CompensationOverrideForAddModel } from '../../shared/models/therapist/compensation-override-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminTherapistCompensationApiService {

  constructor(private http: HttpClient) { }

  public getCompensationOverrides(therapistUID: string): Observable<CompensationOverrideForViewEditModel[]> {
    return this.http.get<CompensationOverrideForViewEditModel[]>(configSettings.WebAPI_URL + '/auth/admin/therapists/' + therapistUID + '/compensation');
  }

  public add(therapistUID: string, compensationOverride: CompensationOverrideForAddModel): Observable<CompensationOverrideForViewEditModel> {
    return this.http.post<CompensationOverrideForViewEditModel>(configSettings.WebAPI_URL + '/auth/admin/therapists/' + therapistUID + '/compensation', compensationOverride);
  }

  public edit(therapistUID: string, compensationOverride: CompensationOverrideForViewEditModel): Observable<CompensationOverrideForViewEditModel> {
    return this.http.put<CompensationOverrideForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/therapists/' + therapistUID + '/compensation', compensationOverride);
  }

  public remove(therapistUID: string, compensationOverrideUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/therapists/' + therapistUID + '/compensation/' + compensationOverrideUID);
  }

}
