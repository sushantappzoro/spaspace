import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceNeedForViewEditModel } from '../../shared/models/services/service-need-for-view-edit.model';
import { ServiceNeedForAddModel } from '../../shared/models/services/service-need-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceNeedApiService {

  constructor(private http: HttpClient) { }

  public get(categoryID: number): Observable<ServiceNeedForViewEditModel[]> {
    return this.http.get<ServiceNeedForViewEditModel[]>
    (configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/needs');
  }

  public add(categoryID: number, need: ServiceNeedForAddModel): Observable<ServiceNeedForViewEditModel> {
    return this.http.post<ServiceNeedForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/needs', need);
  }

  public edit(categoryID: number, focusArea: ServiceNeedForViewEditModel): Observable<ServiceNeedForViewEditModel> {
    return this.http.put<ServiceNeedForViewEditModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/needs', focusArea);
  }

  public remove(categoryID: number, needID: number): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/needs/' + needID);
  }
}
