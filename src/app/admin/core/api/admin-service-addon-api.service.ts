import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceAddOnForEditModel } from '../../shared/models/services/service-addon-for-edit.model';
import { ServiceAddOnForAdminViewModel } from '../../shared/models/services/service-addon-for-admin-view.model';
import { ServiceAddOnForAddMmodel } from '../../shared/models/services/service-addon-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceAddonApiService {

  constructor(private http: HttpClient) { }

  public get(): Observable<ServiceAddOnForAdminViewModel[]> {
    return this.http.get<ServiceAddOnForAdminViewModel[]>(configSettings.WebAPI_URL + '/auth/servicecategories');
  }

  public add(categoryID: number, serviceAddOn: ServiceAddOnForAddMmodel): Observable<ServiceAddOnForAdminViewModel> {
    return this.http.post<ServiceAddOnForAdminViewModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/addons', serviceAddOn);
  }

  public edit(categoryID: number, serviceAddOn: ServiceAddOnForEditModel): Observable<ServiceAddOnForAdminViewModel> {
    return this.http.put<ServiceAddOnForAdminViewModel>(
      configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/addons', serviceAddOn);
  }

  public remove(categoryID: number, serviceAddOnID: number): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + categoryID + '/addons/' + serviceAddOnID);
  }

}
