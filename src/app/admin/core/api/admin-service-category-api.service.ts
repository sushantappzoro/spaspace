import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { ServiceCategoryForAdminViewModel } from '../../shared/models/services/service-category-for-admin-view.model';
import { ServiceCategoryForAddModel } from '../../shared/models/services/service-category-for-add.model';
import { ServiceCategoryForEditModel } from '../../shared/models/services/service-category-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceCategoryApiService {

  constructor(private http: HttpClient) { }

  public get(): Observable<ServiceCategoryForAdminViewModel[]> {
    return this.http.get<ServiceCategoryForAdminViewModel[]>(configSettings.WebAPI_URL + '/auth/admin/servicecategories');
  }

  public add(serviceCategory: ServiceCategoryForAddModel): Observable<ServiceCategoryForAdminViewModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories', JSON.stringify(serviceCategory));
  };

  public edit(serviceCategory: ServiceCategoryForEditModel): Observable<ServiceCategoryForAdminViewModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories', JSON.stringify(serviceCategory));
  };

  public remove(serviceCategoryUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/servicecategories/' + serviceCategoryUID);
  };
}
