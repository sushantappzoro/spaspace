import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { TherapistForApprovalModel } from '../../shared/models/therapist/therapist-for-approval.model';
import { TherapistForApprovalDetailModel } from '../../shared/models/therapist/therapist-for-approval-detail.model';
import { TherapistForRejectionModel } from '../../shared/models/therapist/therapist-for-rejection.model';


@Injectable({
  providedIn: 'root'
})
export class TherapistApprovalsApiService {
  
  constructor(private http: HttpClient) { }

  public getTherapistsForApproval(): Observable<TherapistForApprovalModel[]> {
    return this.http.get<TherapistForApprovalModel[]>(configSettings.WebAPI_URL + '/auth/admin/dashboard/TherapistApprovals');
  }

  public getTherapistsForApprovalDetail(uid: string): Observable<TherapistForApprovalDetailModel[]> {
    return this.http.get<TherapistForApprovalDetailModel[]>(configSettings.WebAPI_URL + '/auth/admin/dashboard/TherapistApprovals/' + uid);
  }

  public approveTherapist(uid: string): Observable<any[]> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/dashboard/TherapistApprovals/Approve/' + uid, null);
  }

  public rejectTherapist(therapist: TherapistForRejectionModel): Observable<any[]> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/dashboard/TherapistApprovals/Reject', therapist);
  }

}
