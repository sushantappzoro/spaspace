import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';

import { TherapistApprovalsApiService } from '../api/therapist-approvals-api.service';
import { TherapistForApprovalModel } from '../../shared/models/therapist/therapist-for-approval.model';
import { TherapistForApprovalDetailModel } from '../../shared/models/therapist/therapist-for-approval-detail.model';
import { TherapistForRejectionModel } from '../../shared/models/therapist/therapist-for-rejection.model';


@Injectable({
  providedIn: 'root'
})
export class TherapistApprovalsService {

  constructor(private apiService: TherapistApprovalsApiService) { }

  public getTherapistsForApproval(): Observable<TherapistForApprovalModel[]> {
    return this.apiService.getTherapistsForApproval();
  }

  public getTherapistsForApprovalDetail(uid: string): Observable<TherapistForApprovalDetailModel[]> {
    return this.apiService.getTherapistsForApprovalDetail(uid);
  }

  public approveTherapist(uid: string): Observable<any[]> {
    return this.apiService.approveTherapist(uid);
  }

  public rejectTherapist(therapist: TherapistForRejectionModel): Observable<any[]> {
    return this.apiService.rejectTherapist(therapist);
  }
}
