import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminServiceLicenseApiService } from '../api/admin-service-license-api.service';
import { ServiceLicenseForViewEditModel } from '../../shared/models/services/service-license-for-view-edit.model';
import { ServiceLicenseForAddModel } from '../../shared/models/services/service-license-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceLicenseService {

  constructor(private api: AdminServiceLicenseApiService) { }

  public get(categoryID: number): Observable<ServiceLicenseForViewEditModel[]> {
    return this.api.getList(categoryID);
  }

  public add(categoryID: number, license: ServiceLicenseForAddModel): Observable<ServiceLicenseForViewEditModel> {
    return this.api.add(categoryID, license);
  }

  public edit(categoryID: number, license: ServiceLicenseForViewEditModel): Observable<ServiceLicenseForViewEditModel> {
    return this.api.edit(categoryID, license);
  }

  public remove(categoryID: number, ID: number): Observable<boolean> {
    return this.api.remove(categoryID, ID);
  }
}
