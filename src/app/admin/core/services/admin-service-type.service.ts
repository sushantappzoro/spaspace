import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminServiceTypeApiService } from '../api/admin-service-type-api.service';
import { ServiceTypeForViewEditModel } from '../../shared/models/services/service-type-for-view-edit.model';
import { ServiceTypeForAddModel } from '../../shared/models/services/service-type-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceTypeService {

  constructor(private api: AdminServiceTypeApiService) { }

  public get(categoryID: number): Observable<ServiceTypeForViewEditModel[]> {
    return this.api.get(categoryID);
  }

  public add(categoryID: number, type: ServiceTypeForAddModel): Observable<ServiceTypeForViewEditModel> {
    return this.api.add(categoryID, type);
  }

  public edit(categoryID: number, type: ServiceTypeForViewEditModel): Observable<ServiceTypeForViewEditModel> {
    return this.api.edit(categoryID, type);
  }

  public remove(categoryID: number, typeID: number): Observable<boolean> {
    return this.api.remove(categoryID, typeID);
  }
}
