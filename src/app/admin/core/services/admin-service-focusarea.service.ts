import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminServiceFocusAreaApiService } from '../api/admin-service-focusarea-api.service';
import { ServiceFocusAreaForViewEditModel } from '../../shared/models/services/service-focusarea-for-view-edit.model';
import { ServiceFocusAreaForAddModel } from '../../shared/models/services/service-focusarea-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceFocusAreaService {

  constructor(private api: AdminServiceFocusAreaApiService) { }

  public get(categoryID: number): Observable<ServiceFocusAreaForViewEditModel[]> {
    return this.api.get(categoryID);
  }

  public add(categoryID: number, focusArea: ServiceFocusAreaForAddModel): Observable<ServiceFocusAreaForViewEditModel> {
    return this.api.add(categoryID, focusArea);
  }

  public edit(categoryID: number, focusArea: ServiceFocusAreaForViewEditModel): Observable<ServiceFocusAreaForViewEditModel> {
    return this.api.edit(categoryID, focusArea);
  }

  public remove(categoryID: number, focusAreaID: number): Observable<boolean> {
    return this.api.remove(categoryID, focusAreaID);
  }
}
