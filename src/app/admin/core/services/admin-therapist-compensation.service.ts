import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminTherapistCompensationApiService } from '../api/admin-therapist-compensation-api.service';
import { CompensationOverrideForViewEditModel } from '../../shared/models/therapist/compensation-override-for-view-edit.model';
import { CompensationOverrideForAddModel } from '../../shared/models/therapist/compensation-override-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminTherapistCompensationService {

  constructor(
    private api: AdminTherapistCompensationApiService
  ) { }

  public getCompensationOverrides(therapistUID: string): Observable<CompensationOverrideForViewEditModel[]> {
    return this.api.getCompensationOverrides(therapistUID);
  }

  public add(therapistUID: string, compensationOverride: CompensationOverrideForAddModel): Observable<CompensationOverrideForViewEditModel> {
    return this.api.add(therapistUID, compensationOverride);
  }

  public edit(therapistUID: string, compensationOverride: CompensationOverrideForViewEditModel): Observable<CompensationOverrideForViewEditModel> {
    return this.api.edit(therapistUID, compensationOverride);
  }

  public remove(therapistUID: string, compensationOverrideUID: string): Observable<boolean> {
    return this.api.remove(therapistUID, compensationOverrideUID);
  }

}
