import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminServiceNeedApiService } from '../api/admin-service-need-api.service';
import { ServiceNeedForViewEditModel } from '../../shared/models/services/service-need-for-view-edit.model';
import { ServiceNeedForAddModel } from '../../shared/models/services/service-need-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceNeedService {

  constructor(private api: AdminServiceNeedApiService) { }

  public get(categoryID: number): Observable<ServiceNeedForViewEditModel[]> {
    return this.api.get(categoryID);
  }

  public add(categoryID: number, focusArea: ServiceNeedForAddModel): Observable<ServiceNeedForViewEditModel> {
    return this.api.add(categoryID, focusArea);
  }

  public edit(categoryID: number, focusArea: ServiceNeedForViewEditModel): Observable<ServiceNeedForViewEditModel> {
    return this.api.edit(categoryID, focusArea);
  }

  public remove(categoryID: number, focusAreaID: number): Observable<boolean> {
    return this.api.remove(categoryID, focusAreaID);
  }
}
