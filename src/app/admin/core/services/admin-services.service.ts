import { Injectable } from '@angular/core';
import { AdminServicesApiService } from '../api/admin-services-api.service';
import { ServiceForAdminViewModel } from '../../shared/models/services/service-for-admin-view.model';
import { Observable } from 'rxjs';
import { ServiceForAddModel } from '../../shared/models/services/service-for-add.model';
import { ServiceForEditModel } from '../../shared/models/services/service-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServicesService {

  constructor(private servicesApi: AdminServicesApiService) { }

  public get(): Observable<ServiceForAdminViewModel[]> {
    return this.servicesApi.get();
  }

  public add(categoryID: number, service: ServiceForAddModel): Observable<ServiceForAdminViewModel> {
    return this.servicesApi.add(categoryID, service);
  };

  public edit(categoryID: number, service: ServiceForEditModel): Observable<ServiceForAdminViewModel> {
    return this.servicesApi.edit(categoryID, service);
  };

  public remove(categoryID: number, serviceUID: string): Observable<boolean> {
    return this.servicesApi.remove(categoryID, serviceUID);
  };
}
