import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { PendingFacilityReviewForAdminView } from '../../shared/models/facility/pending-facility-review-for-admin-view.model';
import { AdminPendingReviewsApiService } from '../api/admin-pending-reviews-api.service';
import { AdminRequestForDeclineReviewModel } from '../../shared/models/facility/admin-request-for-decline-review.model';
import { PendingTherapistReviewForAdminView } from '../../shared/models/facility/pending-therapist-review-for-admin-view.model';

@Injectable({
    providedIn: 'root'
  })
  export class AdminPendingReviewsService {

    constructor(private pendingReviewApi: AdminPendingReviewsApiService) { }

    public getPendingFacilityReviews(): Observable<PendingFacilityReviewForAdminView[]> {
        return this.pendingReviewApi.getPendingFacilityReviews();
    }

    public approveFacilityReview(uid: string): Observable<any[]> {
      return this.pendingReviewApi.approveFacilityReview(uid);
    }

    public declineFacilityReview(declineReason: AdminRequestForDeclineReviewModel): Observable<any[]> {
      return this.pendingReviewApi.declineFacilityReview(declineReason);
    }

    public getPendingTherapistReviews(): Observable<PendingTherapistReviewForAdminView[]> {
      return this.pendingReviewApi.getPendingTherapistReviews();
    }

    public approveTherapistReview(uid: string): Observable<any[]> {
      return this.pendingReviewApi.approveTherapistReview(uid);
    }

    public declineTherapistReview(declineReason: AdminRequestForDeclineReviewModel): Observable<any[]> {
      return this.pendingReviewApi.declineTherapistReview(declineReason);
    }
  }
