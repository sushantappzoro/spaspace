import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AdminFacilityCompensationApiService } from '../api/admin-facility-compensation-api.service';
import { FacilityServiceCompensationModel } from '../../../shared/models/facility/facility-service-compensation.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityCompensationService {

  constructor(private api: AdminFacilityCompensationApiService) { }

  public get(facilityUID: string): Observable<FacilityServiceCompensationModel[]> {
    return this.api.get(facilityUID);
  }

  public edit(facilityUID: string, compensation: FacilityServiceCompensationModel): Observable<FacilityServiceCompensationModel> {
    return this.api.edit(facilityUID, compensation);
  }

}
