import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminFacilityApprovalForViewModel } from '../../shared/models/facility/admin-facility-approval-for-view.model';
import { AdminFacilityApprovalsApiService } from '../api/admin-facility-approvals-api.service';
import { FacilityForAuthViewModel } from '../../../shared/models/facility/facility-for-auth-view.model';
import { FacilityForRejectionModel } from '../../shared/models/facility/facility-for-rejection.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityApprovalsService {

  constructor(private facilityApprovalApi: AdminFacilityApprovalsApiService) { }

  public getFacilitiesForApproval(): Observable<AdminFacilityApprovalForViewModel[]> {
    return this.facilityApprovalApi.getFacilitiesForApproval();
  }

  public getFacilitiesForApprovalDetail(uid: string): Observable<FacilityForAuthViewModel> {
    return this.facilityApprovalApi.getFacilitiesForApprovalDetail(uid);
  }

  public approveFacility(uid: string): Observable<any[]> {
    return this.facilityApprovalApi.approveFacility(uid);
  }

  public rejectFacility(facility: FacilityForRejectionModel): Observable<any[]> {
    return this.facilityApprovalApi.rejectFacility(facility);
  }

}
