import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AdminServiceCategoryApiService } from '../api/admin-service-category-api.service';
import { ServiceCategoryForAdminViewModel } from '../../shared/models/services/service-category-for-admin-view.model';
import { ServiceCategoryForAddModel } from '../../shared/models/services/service-category-for-add.model';
import { ServiceCategoryForEditModel } from '../../shared/models/services/service-category-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceCategoryService {

  constructor(private serviceCategoryApi: AdminServiceCategoryApiService) { }

  public get(): Observable<ServiceCategoryForAdminViewModel[]> {
    return this.serviceCategoryApi.get();
  }

  public add(serviceCategory: ServiceCategoryForAddModel): Observable<ServiceCategoryForAdminViewModel> {
    return this.serviceCategoryApi.add(serviceCategory);
  };

  public edit(serviceCategory: ServiceCategoryForEditModel): Observable<ServiceCategoryForAdminViewModel> {
    return this.serviceCategoryApi.edit(serviceCategory);
  };

  public remove(serviceCategoryUID: string): Observable<boolean> {
    return this.serviceCategoryApi.remove(serviceCategoryUID);
  };

}
