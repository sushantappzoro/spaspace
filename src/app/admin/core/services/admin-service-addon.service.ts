import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ServiceAddOnForEditModel } from '../../shared/models/services/service-addon-for-edit.model';
import { ServiceAddOnForAdminViewModel } from '../../shared/models/services/service-addon-for-admin-view.model';
import { ServiceAddOnForAddMmodel } from '../../shared/models/services/service-addon-for-add.model';
import { AdminServiceAddonApiService } from '../api/admin-service-addon-api.service';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceAddonService {

  constructor(private api: AdminServiceAddonApiService) { }

  public get(): Observable<ServiceAddOnForAdminViewModel[]> {
    return this.api.get();
  }

  public add(categoryID: number, serviceAddOn: ServiceAddOnForAddMmodel): Observable<ServiceAddOnForAdminViewModel> {
    return this.api.add(categoryID, serviceAddOn);
  }

  public edit(categoryID: number, serviceAddOn: ServiceAddOnForEditModel): Observable<ServiceAddOnForAdminViewModel> {
    return this.api.edit(categoryID, serviceAddOn);
  }

  public remove(categoryID: number, serviceAddOnID: number): Observable<boolean> {
    return this.api.remove(categoryID, serviceAddOnID);
  }

}
