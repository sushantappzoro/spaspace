import { Injectable } from '@angular/core';
import { ErrorModel } from '../../../shared/models/common/error.model';

@Injectable({
  providedIn: 'root'
})
export class AdminErrorService {

  private errors: ErrorModel[];

  constructor() {
    this.loadErrors();
  }

  public loadErrorMessages(errors: ErrorModel[]) {
    this.errors = errors;
  }

  public getErrorMessage(code: string) {
    var theError = this.errors.find(err => err.Code == code);
    return theError ? theError.Message : 'An error occured';

  }

  loadErrors() {
    this.errors = [
      { Code: 'af00', Message: 'An error occured.' },
      { Code: 'af01', Message: 'There was an error loading the Facilities List.' }
    ]
  }

}


export enum AdminErrors {
  unknownError = 'af00',
  adminFaciltiesListError = 'af01'
}
