import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminLicenseApiService } from '../api/admin-license-api.service';
import { LicenseForViewEditModel } from '../../shared/models/lists/license-for-viewedit.model';
import { LicenseForAddModel } from '../../shared/models/lists/license-for-add.model';

@Injectable({
  providedIn: 'root'
})
export class AdminLicenseService {

  constructor(private api: AdminLicenseApiService) { }

  public getList(): Observable<LicenseForViewEditModel[]> {
    return this.api.getAll();
  }

  public getSingle(ID: number): Observable<LicenseForViewEditModel> {
    return this.api.getSingle(ID);
  }

  public add(license: LicenseForAddModel): Observable<LicenseForViewEditModel> {
    return this.api.add(license);
  }

  public edit(license: LicenseForViewEditModel): Observable<LicenseForViewEditModel> {
    return this.api.edit(license);
  }

  public remove(ID: number): Observable<boolean> {
    return this.api.remove(ID);
  }
}
