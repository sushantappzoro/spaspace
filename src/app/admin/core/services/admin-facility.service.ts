import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';

import { AdminFacilityApiService } from '../api/admin-facility-api.service';
import { AdminFacilityForListModel } from '../../shared/models/facility/admin-facility-for-list.model';
import { FacilityParentModel } from '../../../shared/models/facility/facility-parent.model';
import { FacilityRegionModel } from '../../../shared/models/facility/facility-region.model';
import { AdminFacilityInfoForEditModel } from '../../shared/models/facility/admin-facility-info-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityService {

  constructor(private api: AdminFacilityApiService) { }

  public getList(): Observable<AdminFacilityForListModel[]> {
    return this.api.getList();
  }

  public getFacilityInfo(facilityUID: string): Observable<AdminFacilityInfoForEditModel> {
    return this.api.getFacilityInfo(facilityUID);
  }

  public editFacilityInfo(facilityUID: string, facility: AdminFacilityInfoForEditModel): Observable<AdminFacilityInfoForEditModel> {
    console.log('In the service: ');
    console.log(facility);
    return this.api.editFacilityInfo(facilityUID, facility);
  }

  public getParents(): Observable<FacilityParentModel[]> {
    return this.api.getParents();
  }

  public getRegions(): Observable<FacilityRegionModel[]> {
    return this.api.getRegions();
  }

}
