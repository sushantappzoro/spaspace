import { Injectable } from '@angular/core';
import { createStore } from 'devextreme-aspnet-data-nojquery';
import CustomStore from 'devextreme/data/custom_store';

import { configSettings } from '../../../app.config';
//import { AdminModule } from '../../admin.module';

@Injectable({
  providedIn: 'root'
})
export class AdminTherapistCustomStoreService {

  store: CustomStore;

  constructor() { }

  getStore() {

    this.store = createStore({
      key: "UID",
      loadUrl: configSettings.WebAPI_URL + "/auth/therapists",
      insertUrl: configSettings.WebAPI_URL + "/auth/therapists",
      updateUrl: configSettings.WebAPI_URL + "/auth/therapists",
      deleteUrl: configSettings.WebAPI_URL + "/auth/therapists",
    })

    return this.store;
  }

}
