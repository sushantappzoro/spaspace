import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AdminErrors } from '../services/admin-error.service';
import { AdminFacilityService } from '../services/admin-facility.service';
import { AdminFacilityForListModel } from '../../shared/models/facility/admin-facility-for-list.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityListResolver implements Resolve<AdminFacilityForListModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facilityService: AdminFacilityService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<AdminFacilityForListModel[]> {
    return this.facilityService.getList().pipe(
      catchError(error => {
        //console.log(error);
        //console.log('FacilityListResolver', this.route);
        //this.router.navigate(['../error']);
        this.router.navigate(['./admin/error/' + AdminErrors.adminFaciltiesListError], { relativeTo: this.route });
        return of(null);
      })
    );
  }
}
