import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityForAuthViewModel } from "../../../shared/models/facility/facility-for-auth-view.model";
import { FacilityService } from "../../../core/services/facility/facility.service";
import { FacilityForAdminListModel } from "../../../shared/models/facility/facility-for-admin-list.model";
import { FacilityInfoService } from "../../../core/services/facility/facility-info.service";

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityDetailResolver implements Resolve<FacilityForAdminListModel>  {

  constructor(
    private router: Router,
    private facilityService: FacilityInfoService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityForAdminListModel> {
    return this.facilityService.getFacilityInfo(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}


//export class AdminFacilityDetailResolver implements Resolve<FacilityForAuthViewModel>  {

//  constructor(
//    private router: Router,
//    private facilityService: FacilityService
//  ) { }

//  resolve(route: ActivatedRouteSnapshot): Observable<FacilityForAuthViewModel> {
//    return this.facilityService.getFacility(route.params['uid']).pipe(
//      catchError(error => {
//        console.log(error);
//        this.router.navigate(['/home']);
//        return of(null);
//      })
//    );
//  }

//}
