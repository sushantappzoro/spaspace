import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityInfoForAdminModel } from "../../../shared/models/facility/facility-info-for-admin.model";
import { FacilityInfoService } from "../../../core/services/facility/facility-info.service";

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityApprovalDetailResolver implements Resolve<FacilityInfoForAdminModel> {

  constructor(
    private facilityService: FacilityInfoService,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityInfoForAdminModel> {
    return this.facilityService.getFacilityInfo(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
