import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { AdminFacilityCompensationService } from "../services/admin-facility-compensation.service";
import { FacilityServiceCompensationModel } from "../../../shared/models/facility/facility-service-compensation.model";

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityCompensationResolver implements Resolve<FacilityServiceCompensationModel[]> {

  constructor(
    private router: Router,
    private compensationService: AdminFacilityCompensationService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityServiceCompensationModel[]> {
    return this.compensationService.get(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
