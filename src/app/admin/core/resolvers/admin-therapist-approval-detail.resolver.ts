import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { TherapistForApprovalDetailModel } from "../../shared/models/therapist/therapist-for-approval-detail.model";
import { TherapistApprovalsService } from "../services/therapist-approvals.service";
import { AdminModule } from "../../admin.module";

@Injectable({
  providedIn: 'root'
})
export class AdminTherapistApprovalDetailResolver implements Resolve<TherapistForApprovalDetailModel> {

  constructor(
    private therapistApprovalsService: TherapistApprovalsService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<TherapistForApprovalDetailModel> {
    return this.therapistApprovalsService.getTherapistsForApprovalDetail(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }
}
