import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AdminErrors } from '../services/admin-error.service';
import { AdminFacilityService } from '../services/admin-facility.service';
import { AdminFacilityInfoForEditModel } from '../../shared/models/facility/admin-facility-info-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityInfoResolver implements Resolve<AdminFacilityInfoForEditModel> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facilityService: AdminFacilityService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<AdminFacilityInfoForEditModel> {

    return this.facilityService.getFacilityInfo(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        console.log('FacilityListResolver', this.route);
        //this.router.navigate(['../error']);
        this.router.navigate(['./admin/error/' + AdminErrors.adminFaciltiesListError], { relativeTo: this.route });
        return of(null);
      })
    );
  }
}
