import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AdminErrors } from '../services/admin-error.service';
import { FacilityManagerModel } from '../../../shared/models/facility/facility-manager.model';
import { FacilityManagerService } from '../../../core/services/facility/facility-manager.service';

@Injectable({
  providedIn: 'root'
})
export class AdminFacilityManagersResolver implements Resolve<FacilityManagerModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facilityService: FacilityManagerService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityManagerModel[]> {

    return this.facilityService.getFacilityManagers(route.params['uid']).pipe(
      catchError(error => {
        //this.router.navigate(['../error']);
        this.router.navigate(['./admin/error/' + AdminErrors.adminFaciltiesListError], { relativeTo: this.route });
        return of(null);
      })
    );
  }


}
