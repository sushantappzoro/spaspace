import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { AdminTherapistCompensationService } from '../services/admin-therapist-compensation.service';
import { CompensationOverrideForViewEditModel } from '../../shared/models/therapist/compensation-override-for-view-edit.model';

@Injectable({
  providedIn: 'root'
})
export class AdminTherapistCompensationResolver implements Resolve<CompensationOverrideForViewEditModel[]> {

  constructor(
    private router: Router,
    private compensationService: AdminTherapistCompensationService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<CompensationOverrideForViewEditModel[]> {
    return this.compensationService.getCompensationOverrides(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        //this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
