import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { TherapistProfileForEditModel } from "../../../shared/models/therapist/therapist-profile-for-edit.model";
import { TherapistProfileService } from "../../../core/services/therapist/therapist-profile.service";

@Injectable({
  providedIn: 'root'
})
export class AdminTherapistDetailResolver implements Resolve<TherapistProfileForEditModel> {

  constructor(
    private therapistService: TherapistProfileService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<TherapistProfileForEditModel> {
    return this.therapistService.getTherapistProfile(route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        //this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
