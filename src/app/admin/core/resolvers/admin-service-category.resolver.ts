import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { ServiceCategoryForAdminViewModel } from "../../shared/models/services/service-category-for-admin-view.model";
import { AdminServiceCategoryService } from "../services/admin-service-category.service";

@Injectable({
  providedIn: 'root'
})
export class AdminServiceCategoryResolver implements Resolve<ServiceCategoryForAdminViewModel[]> {

  constructor(
    private serviceService: AdminServiceCategoryService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ServiceCategoryForAdminViewModel[]> {
    return this.serviceService.get().pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
