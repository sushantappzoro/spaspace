import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminCoreModule } from './core/admin-core.module';
import { AdminSharedModule } from './shared/admin-shared.module';
import { AdminFacilitiesModule } from './facilities/admin-facilities.module';
import { AdminClientsModule } from './clients/admin-clients.module';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { AdminDevexModule } from './admin-devex.module';
import { AdminTherapistsModule } from './therapists/admin-therapists.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AdminTherapistDetailResolver } from './core/resolvers/admin-therapist-detail.resolver';
import { AdminTherapistCustomStoreService } from './core/store/admin-therapist-custom-store.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AdminServicesComponent } from './services/admin-services.component';
import { DevexModuleModule } from '../customer/devex-module.module';
import { ApiAuthorizationModule } from '../../api-authorization/api-authorization.module';
import { AdminErrorComponent } from './error/admin-error.component';
import { MaterialModule } from '../material-module.module';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminServiceEditComponent } from './services/admin-service-edit/admin-service-edit.component';
import { AdminServiceCategoryEditComponent } from './services/admin-service-category-edit/admin-service-category-edit.component';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { NgxCurrencyModule } from 'ngx-currency';
import { AdminServiceAddonEditComponent } from './services/admin-service-addon-edit/admin-service-addon-edit.component';
import { PendingFacilityReviewsComponent } from './reviews/pending-facility-reviews/pending-facility-reviews.component';
import { AdminComponentsModule } from './shared/components/admin-components.module';
import { AdminPendingTherapistReviewsComponent } from './reviews/admin-pending-therapist-reviews/admin-pending-therapist-reviews.component';
import { AdminServiceFocusAreaEditComponent } from './services/admin-service-focus-area-edit/admin-service-focus-area-edit.component';
import { AdminServiceNeedsEditComponent } from './services/admin-service-needs-edit/admin-service-needs-edit.component';
import { AdminServiceTypeEditComponent } from './services/admin-service-type-edit/admin-service-type-edit.component';
import { AdminServiceLicenseEditComponent } from './services/admin-service-license-edit/admin-service-license-edit.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AdminComponent,
    AdminDashboardComponent,
    AdminServicesComponent,
    AdminErrorComponent,
    AdminServiceEditComponent,
    AdminServiceCategoryEditComponent,
    AdminServiceAddonEditComponent,
    PendingFacilityReviewsComponent,
    AdminPendingTherapistReviewsComponent,
    AdminServiceFocusAreaEditComponent,
    AdminServiceNeedsEditComponent,
    AdminServiceTypeEditComponent,
    AdminServiceLicenseEditComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MDBBootstrapModulesPro.forRoot(),
    NgbModule,
    NgxMaskModule.forRoot(options),
    NgxCurrencyModule,
    DevexModuleModule,
    MaterialModule,
    ApiAuthorizationModule,
    AdminRoutingModule,
    AdminCoreModule,
    AdminSharedModule,
    AdminFacilitiesModule,
    AdminClientsModule,
    AdminDevexModule,
    AdminTherapistsModule,
    AccordionModule.forRoot(),
    TabsModule.forRoot(),
    AdminComponentsModule,
  ],
  providers: [
    // AdminTherapistDetailResolver,
    // AdminTherapistCustomStoreService
  ]
})
export class AdminModule { }
