import { Component, OnInit, ViewChild } from '@angular/core';
import themes from "devextreme/ui/themes";
import { NavBarService } from '../core/services/nav-bar.service';
import { MenuListItemModel } from '../shared/models/common/menu-list-item.model';
import { SidenavComponent } from 'ng-uikit-pro-standard';
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-prototype',
  templateUrl: './prototype.component.html',
  styleUrls: ['./prototype.component.scss']
})
export class PrototypeComponent implements OnInit {
  @ViewChild("sidenav") sidenav: SidenavComponent;

  public isAuthenticated: Observable<boolean>;
  public userName: Observable<string>;
  public userRole: string;

  navigation: MenuListItemModel[] = [
    { id: 1, text: "Dashboard", icon: "home", link: "/therapist/dashboard", status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    { id: 2, text: "My Profile", icon: "user", link: "/therapist/profile", status: ['New', 'Pending Approval', 'Approved'], disabled: true },
    { id: 3, text: "Availability", icon: "calendar", link: "/therapist/availability", status: ['Approved'], disabled: false },
    { id: 4, text: "Schedule", icon: "clock", link: "/therapist/schedule", status: ['Approved'], disabled: false },
    { id: 5, text: "Account", icon: "dollar-sign", link: "/therapist/account", status: ['Approved'], disabled: false },
    { id: 6, text: "Standards", icon: "fas fa-certificate", link: "/therapist/standards", status: ['Approved'], disabled: false }
  ];

  constructor(private authorizeService: AuthorizeService) { }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.firstName));
    this.authorizeService.getRole()
      .subscribe(role => {
        this.userRole = role;
      });
  }
  
}
