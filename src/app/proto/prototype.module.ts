import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrototypeComponent } from './prototype.component';
import { DummyPageComponent } from './dummy-page/dummy-page.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ApiAuthorizationModule } from '../../api-authorization/api-authorization.module';
import { ScreenService } from '../core/services/screen.service';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';


@NgModule({
  declarations: [
    PrototypeComponent,
    DummyPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MDBBootstrapModulesPro.forRoot(),
    ApiAuthorizationModule,
    RouterModule.forChild([
      {
        path: '',
        component: PrototypeComponent,
        children: [
          {
            path: '',
            component: DummyPageComponent
          },
          {
            path: 'dummy',
            component: DummyPageComponent
          }
        ]
      },
    
    ])
  ],
  providers: [ScreenService, MDBSpinningPreloader]
})
export class PrototypeModule { }
