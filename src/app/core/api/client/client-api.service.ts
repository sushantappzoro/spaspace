import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { configSettings } from '../../../app.config';
import { CustomerModel } from '../../../shared/models/client/customer.model';

@Injectable({
  providedIn: 'root'
})
export class ClientApiService {

  constructor(private http: HttpClient) { }

  public getCustomer(id: string) {
    return this.http.get<CustomerModel>(configSettings.WebAPI_URL + '/Customer/' + id);
  }

}
