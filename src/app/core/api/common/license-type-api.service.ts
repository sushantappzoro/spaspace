import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { LicenseTypeModel } from '../../../shared/models/common/license-type.model';

@Injectable({
  providedIn: 'root'
})
export class LicenseTypeApiService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<LicenseTypeModel[]> {
    return this.http.get<LicenseTypeModel[]>
      (configSettings.WebAPI_URL + '/auth/admin/lists/licenses');
  }

}
