import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ByroomForViewModel } from '../../../shared/models/appointment/byroom-for-view.model';
import { TherapistForViewModel } from '../../../shared/models/appointment/therapist-for-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityScheduleApiService {

  constructor(private http: HttpClient) { }

  public getByRoom(url: string, loadOptions: any): Observable<ByroomForViewModel> {
    return this.http.get<ByroomForViewModel>(url);
  }

  public getByTherapist(url: string, loadOptions: any): Observable<TherapistForViewModel> {
    return this.http.get<TherapistForViewModel>(url);
  }

}
