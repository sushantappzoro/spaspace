import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityAmenityForViewModel } from '../../../shared/models/facility/facility-amenity-for-view.model';
import { TherapistForProfileViewModel } from '../../../shared/models/therapist/therapist-for-profile-view.model';
import { FacilityPhotoForAdminModel } from '../../../shared/models/facility/facility-photo-for-admin.model';
import { FacilityProfileForViewModel } from '../../../shared/models/facility/facility-profile-for-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityProfileApiService {

  constructor(private http: HttpClient) { }

  public getProfile(facilityUID: string): Observable<FacilityProfileForViewModel> {
    return this.http.get<FacilityProfileForViewModel>(configSettings.WebAPI_URL + '/facilities/' + facilityUID + '/Profile');
  }

  public getAmenities(facilityUID: string): Observable<FacilityAmenityForViewModel[]> {
    return this.http.get<FacilityAmenityForViewModel[]>(configSettings.WebAPI_URL + '/facilities/' + facilityUID + '/Profile/Amenities');
  }

  public getFeaturedTherapists(facilityUID: string): Observable<TherapistForProfileViewModel[]> {
    return this.http.get<TherapistForProfileViewModel[]>(configSettings.WebAPI_URL + '/facilities/' + facilityUID + '/Profile/FeaturedTherapists');
  }

  public getPhotos(facilityUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.http.get<FacilityPhotoForAdminModel[]>(configSettings.WebAPI_URL + '/facilities/' + facilityUID + '/Profile/Photos');
  }

}
