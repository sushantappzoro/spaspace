import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TreatmentAreaAvailabilityForViewModel } from '../../../shared/models/facility/treatment-area-availability-for-view.model';
import { TreatmentAreaAvailabilityForAddModel } from '../../../shared/models/facility/treatment-area-availability-for-add.model';
import { TreatmentAreaAvailabilityForEditModel } from '../../../shared/models/facility/treatment-area-availability-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityAvailabilityApiService {

  constructor(private http: HttpClient) { }

  public getTreatmentAreaAvailabilities(facilityUid: string, treatmentAreaUID: string): Observable<TreatmentAreaAvailabilityForViewModel[]> {
    return this.http.get<TreatmentAreaAvailabilityForViewModel[]>(configSettings.WebAPI_URL + '/auth/Facilities/' + facilityUid + '/TreatmentAreas/' + treatmentAreaUID + '/availability');
  }

  public addTreatmentAreaAvailability(facilityUid: string, treatmentAreaUID: string, avalability: TreatmentAreaAvailabilityForAddModel): Observable<TreatmentAreaAvailabilityForEditModel> {
    return this.http.post<TreatmentAreaAvailabilityForEditModel>(configSettings.WebAPI_URL + '/auth/Facilities/' + facilityUid + '/TreatmentAreas/' + treatmentAreaUID + '/availability', avalability);
  }

  public editTreatmentAreaAvailability(facilityUid: string, treatmentAreaUID: string, avalability: TreatmentAreaAvailabilityForViewModel): Observable<TreatmentAreaAvailabilityForEditModel> {
    return this.http.put<TreatmentAreaAvailabilityForEditModel>(configSettings.WebAPI_URL + '/auth/Facilities/' + facilityUid + '/TreatmentAreas/' + treatmentAreaUID + '/availability', avalability);
  }

  public removeTreatmentAreaAvailability(facilityUid: string, treatmentAreaUID: string, availabilityUID: string): Observable<TreatmentAreaAvailabilityForEditModel> {
    return this.http.delete<TreatmentAreaAvailabilityForEditModel>(configSettings.WebAPI_URL + '/auth/Facilities/' + facilityUid + '/TreatmentAreas/' + treatmentAreaUID + '/availability/' + availabilityUID);
  }

}
