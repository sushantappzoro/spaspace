import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityForListModel } from '../../../shared/models/facility/facility-for-list.model';
import { FacilityForAuthViewModel } from '../../../shared/models/facility/facility-for-auth-view.model';
import { FacilityForAuthEditModel } from '../../../shared/models/facility/facility-for-auth-edit.model';
import { TimeZoneModel } from 'src/app/shared/models/facility/time-zone.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityApiService {

  constructor(private http: HttpClient) { }

  public get(): Observable<FacilityForListModel[]> {
    return this.http.get<FacilityForListModel[]>(configSettings.WebAPI_URL + '/auth/facilities');
  }

  public getActive(): Observable<FacilityForListModel[]> {
    return this.http.get<FacilityForListModel[]>(configSettings.WebAPI_URL + '/auth/facilities/GetActiveFacilities');
  }

  public getFacility(uid: string): Observable<FacilityForAuthViewModel> {
    return this.http.get<FacilityForAuthViewModel>(configSettings.WebAPI_URL + '/auth/facilities/' + uid);
  }

  public add(facility: FacilityForAuthEditModel): Observable<any> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/facilities', facility);
  }

  public edit(facility: FacilityForAuthEditModel): Observable<any> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/facilities', facility);
  }

  public submit(facility: FacilityForAuthEditModel): Observable<FacilityForAuthViewModel> {
    return this.http.post<FacilityForAuthViewModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facility.UID + '/submit', facility);
  }

  public getAllTimeZones(): Observable<TimeZoneModel[]> {
    return this.http.get<TimeZoneModel[]>(configSettings.WebAPI_URL + '/auth/timezones');
  }

  // public remove(therapistUid: string, facilityUID: string): Observable<boolean> {
  //  return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/AssociatedFacilities/' + facilityUID);
  // }

  // public getFacilitiesForList(): Observable<TherapistFacilityForListModel[]> {
  //  return this.http.get<TherapistFacilityForListModel[]>(configSettings.WebAPI_URL + '/Facilities/GetFacilitiesForList');
  // }
}
