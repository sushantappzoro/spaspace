import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistInfoForAdminModel } from '../../../shared/models/facility/therapist-info-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTherapistInfoApiService {

  constructor(private http: HttpClient) { }

  public getTherapistInfo(facilityUid: string): Observable<TherapistInfoForAdminModel> {
    return this.http.get<TherapistInfoForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/TherapistInfo');
  }

  public editTherapistInfo(facilityUid: string, therapistInfo: TherapistInfoForAdminModel): Observable<TherapistInfoForAdminModel> {
    return this.http.put<TherapistInfoForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/TherapistInfo', therapistInfo);
  }

}
