import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityAmenityForAdminModel } from '../../../shared/models/facility/facility-amenity-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityAmenitiesApiService {

  constructor(private http: HttpClient) { }

  public getFacilityAmenities(facilityUid: string): Observable<FacilityAmenityForAdminModel[]> {
    return this.http.get<FacilityAmenityForAdminModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/Amenities');
  }

  public updateFacilityAmenities(facilityUid: string, amenities: FacilityAmenityForAdminModel): Observable<FacilityAmenityForAdminModel> {
    console.log('updateFacilityAmenities', amenities);
    return this.http.put<FacilityAmenityForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/Amenities', amenities);
  }

}
