import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityManagerForAdminModel } from '../../../shared/models/facility/facility-manager-for-admin.model';


@Injectable({
  providedIn: 'root'
})
export class FacilityDashboardApiService {

  constructor(private http: HttpClient) { }

  public getPendingFacilityManagers(facilityManagerUid: string): Observable<FacilityManagerForAdminModel[]> {
    return this.http.get<FacilityManagerForAdminModel[]>(configSettings.WebAPI_URL + '/auth/FacilityManagers/' + facilityManagerUid + '/DashBoard/PendingFacilityManagers');
  }

}
