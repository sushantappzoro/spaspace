import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityPhotoForAdminModel } from '../../../shared/models/facility/facility-photo-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityPhotosApiService {

  constructor(private http: HttpClient) { }

  public getFacilityPhoto(facilityUID: string, photoUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.http.get<FacilityPhotoForAdminModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/Photos/' + photoUID);
  }

  public getFacilityPhotos(facilityUID: string): Observable<FacilityPhotoForAdminModel[]> {
    console.log('FacilityPhotosService- got here');
    return this.http.get<FacilityPhotoForAdminModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/Photos');
  }

  public removeFacilityPhoto(facilityUID: string, photoUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.http.delete<FacilityPhotoForAdminModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/Photos/' + photoUID);
  }

  public setMainFacilityPhoto(facilityUID: string, photoUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.http.get<FacilityPhotoForAdminModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/Photos/SetMain/' + photoUID);
  }

  //public saveFacilityPhoto(facilityUID, formData, description): Observable<FacilityPhotoForAdminModel[]> {

  //}

}
