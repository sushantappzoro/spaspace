import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityManagerForAdminModel } from '../../../shared/models/facility/facility-manager-for-admin.model';
import { FacilityForAdminListModel } from '../../../shared/models/facility/facility-for-admin-list.model';
import { FacilityManagerModel } from '../../../shared/models/facility/facility-manager.model';
import { FacilityManagerForEditModel } from '../../../shared/models/facility/facility-manager-for-edit.model';
import { FacilityManagerForAdminAddModel } from '../../../shared/models/facility/facility-manager-for-admin-add.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityManagerApiService {

  constructor(private http: HttpClient) { }

  public getManagedFacilities(facilityManagerUID: string): Observable<FacilityForAdminListModel[]> {
    return this.http.get<FacilityForAdminListModel[]>(configSettings.WebAPI_URL + '/auth/facilitymanagers/' + facilityManagerUID + '/facilities');
  }

  public getFacilityManager(facilityManagerUID: string): Observable<FacilityManagerForAdminModel> {
    return this.http.get<FacilityManagerForAdminModel>(configSettings.WebAPI_URL + '/auth/facilitymanagers/' + facilityManagerUID);
  }

  public getFacilityManagerForFacility(facilityUID: string, facilityManagerUID: string): Observable<FacilityManagerForAdminModel> {
    return this.http.get<FacilityManagerForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/FacilityManagers/' + facilityManagerUID);
  }

  public add(facilityUID: string, facilityManager: FacilityManagerForAdminAddModel): Observable<FacilityManagerForAdminModel> {
    return this.http.post<FacilityManagerForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/FacilityManagers', facilityManager);
  }

  public edit(facilityUID: string, facilityManager: FacilityManagerForEditModel): Observable<any> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/FacilityManagers', facilityManager);
  }

  public remove(facilityUID: string, facilityManagerUID: string): Observable<any> {
    return this.http.delete<FacilityManagerForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/FacilityManagers/' + facilityManagerUID);
  }

  public getFacilityManagers(facilityUID: string): Observable<FacilityManagerModel[]> {
    return this.http.get<FacilityManagerModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/FacilityManagers');
  }

}
