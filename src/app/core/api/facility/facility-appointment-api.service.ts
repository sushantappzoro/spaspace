import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { AppointmentForFacilityDetailModel } from '../../../shared/models/appointment/appointment-for-facility-detail.model';
import { AppointmentCancelByFacilityRequestModel } from '../../../shared/models/appointment/appointment-cancel-by-facility-request.model';
import { AppointmentChangeRequestModel } from '../../../shared/models/appointment/appointment-change-request.model';
import { AppointmentForFacilityListModel } from '../../../shared/models/appointment/appointment-for-facility-list.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityAppointmentApiService {

  constructor(private http: HttpClient) { }

  public getAppointments(facilityUid: string): Observable<AppointmentForFacilityListModel[]> {
    return this.http.get<AppointmentForFacilityListModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/appointments');
  }

  public getAppointmentDetail(facilityUid: string, appointmentUID: string): Observable<AppointmentForFacilityDetailModel> {
    return this.http.get<AppointmentForFacilityDetailModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/appointments/' + appointmentUID + '/detail');
  }

  public startCancel(facilityUID: string, appointmentUID: string, cancelRequest: AppointmentCancelByFacilityRequestModel): Observable<AppointmentCancelByFacilityRequestModel> {
    return this.http.post<AppointmentCancelByFacilityRequestModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/appointments/' + appointmentUID + '/startcancel', cancelRequest);
  }

  public finishCancel(facilityUID: string, appointmentUID: string, cancelRequest: AppointmentCancelByFacilityRequestModel): Observable<any> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/appointments/' + appointmentUID + '/finishcancel', cancelRequest);
  }

  public changeAppointment(facilityUID: string, appointmentUID: string, changeRequest: AppointmentChangeRequestModel): Observable<AppointmentForFacilityDetailModel> {
    return this.http.put<AppointmentForFacilityDetailModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/appointments/' + appointmentUID + '/change', changeRequest);
  }

}
