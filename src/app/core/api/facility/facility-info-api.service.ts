import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityInfoApiService {

  constructor(private http: HttpClient) { }

  public getFacilityInfo(facilityUid: string): Observable<FacilityInfoForAdminModel> {
    return this.http.get<FacilityInfoForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/FacilityInfo');
  }

  public editFacilityInfo(facilityUid: string, facilityInfo: FacilityInfoForAdminModel): Observable<FacilityInfoForAdminModel> {
    return this.http.put<FacilityInfoForAdminModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUid + '/FacilityInfo', facilityInfo);
  }

}
