import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityServiceCompensationModel } from '../../../shared/models/facility/facility-service-compensation.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityCompensationApiService {

  constructor(private http: HttpClient) { }

  public getServiceCompensations(facilityUID: string): Observable<FacilityServiceCompensationModel[]> {
    return this.http.get<FacilityServiceCompensationModel[]>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation');
  }

  public getServiceCompensation(facilityUID: string, compensationUID: string): Observable<FacilityServiceCompensationModel> {
    return this.http.get<FacilityServiceCompensationModel>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation/' + compensationUID);
  }

  public add(facilityUID: string, compensation: FacilityServiceCompensationModel): Observable<FacilityServiceCompensationModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation', JSON.stringify(compensation));
  };

  public edit(facilityUID: string, compensation: FacilityServiceCompensationModel): Observable<FacilityServiceCompensationModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation', JSON.stringify(compensation));
  };

  public remove(facilityUID: string, compensationUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/admin/facilities/' + facilityUID + '/compensation/' + compensationUID)
  };

}
