import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { TherapistForFacilityScheduleViewModel } from '../../../shared/models/facility/therapist-for-facility-schedule-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTherapistApiService {

  constructor(private http: HttpClient) { }

  public getAssociatedTherapists(facilityUID: string): Observable<TherapistForFacilityScheduleViewModel[]> {
    return this.http.get<TherapistForFacilityScheduleViewModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/therapists');
  }

}
