import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityTreatmentAreaModel } from '../../../shared/models/facility/facility-treatment-area.model';
import { FacilityTreatmentAreaViewModel } from '../../../shared/models/facility/facility-treatment-area-view.model';
import { FacilityTreatmentAreaForAddModel } from '../../../shared/models/facility/facility-treatment-area-for-add.model';
import { FacilityTreatmentAreaForEditModel } from '../../../shared/models/facility/facility-treatment-area-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTreatmentAreasApiService {

  constructor(private http: HttpClient) { }

  public getTreatmentAreas(facilityUID: string): Observable<FacilityTreatmentAreaViewModel[]> {
    return this.http.get<FacilityTreatmentAreaViewModel[]>(
      configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas');
  }

  public getTreatmentArea(facilityUID: string, treatmentAreaUID: string): Observable<FacilityTreatmentAreaModel> {
    return this.http.get<FacilityTreatmentAreaModel>(
      configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas/' + treatmentAreaUID);
  }

  public add(facilityUID: string, treatmentArea: FacilityTreatmentAreaForAddModel): Observable<FacilityTreatmentAreaForEditModel> {
    return this.http.post<any>(
      configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas', JSON.stringify(treatmentArea));
  }

  public newTreatmentArea(facilityUID: string): Observable<FacilityTreatmentAreaForEditModel> {
    return this.http.get<FacilityTreatmentAreaForEditModel>(
      configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas/new');
  }

  public edit(facilityUID: string, treatmentArea: FacilityTreatmentAreaForEditModel): Observable<FacilityTreatmentAreaForEditModel> {
    return this.http.put<any>(
      configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas', JSON.stringify(treatmentArea));
  }

  public remove(facilityUID: string, treatmentAreaUID: string): Observable<boolean> {
    return this.http.delete<any>(
      configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas/' + treatmentAreaUID)
  }
}
