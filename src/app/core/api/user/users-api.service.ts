import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { UserProfileForViewEditModel } from '../../../shared/models/user/user-profile-for-view-edit.model';
import { PasswordChangeRequest } from 'src/app/shared/models/user/password-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class UsersApiService {

  constructor(private http: HttpClient) { }

  public getProfile(userUid: string): Observable<UserProfileForViewEditModel> {
    return this.http.get<UserProfileForViewEditModel>(configSettings.WebAPI_URL + '/auth/users/' + userUid + '/profile');
  }

  public editProfile(userUid: string, userProfile: UserProfileForViewEditModel): Observable<UserProfileForViewEditModel> {
    return this.http.put<UserProfileForViewEditModel>(configSettings.WebAPI_URL + '/auth/users/' + userUid + '/profile', userProfile);
  }

  public updatePassword(passwordChangeRequest: PasswordChangeRequest): Observable<boolean> {
    return this.http.put<boolean>(configSettings.WebAPI_URL + 
      '/auth/users/' + passwordChangeRequest.UID + '/updatePassword', passwordChangeRequest);
  }
}
