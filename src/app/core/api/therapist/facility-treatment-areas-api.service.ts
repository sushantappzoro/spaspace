import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { FacilityTreatmentAreaModel } from '../../../shared/models/facility/facility-treatment-area.model';
//import { FacilityTreatmentAreaTypeModel } from '../../../shared/models/facility/facility-treatment-area-type.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTreatmentAreasApiService {

  constructor(private http: HttpClient) { }

  public getTreatmentArea(facilityUID: string, treatmentAreaUID: string): Observable<FacilityTreatmentAreaModel> {
    return this.http.get<FacilityTreatmentAreaModel>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas/' + treatmentAreaUID);
  }

  public getTreatmentAreas(facilityUID: string): Observable<FacilityTreatmentAreaModel[]> {
    return this.http.get<FacilityTreatmentAreaModel[]>(configSettings.WebAPI_URL + '/auth/facilities/' + facilityUID + '/TreatmentAreas');
  }

  public add(facilityUID: string, treatmentArea: FacilityTreatmentAreaModel): Observable<FacilityTreatmentAreaModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/therapists/' + facilityUID + '/TreatmentAreas', JSON.stringify(treatmentArea));
  };

  public edit(facilityUID: string, treatmentArea: FacilityTreatmentAreaModel): Observable<FacilityTreatmentAreaModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/therapists/' + facilityUID + '/TreatmentAreas', JSON.stringify(treatmentArea));
  };

  public remove(facilityUID: string, treatmentAreaUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/therapists/' + facilityUID + '/TreatmentAreas/' + treatmentAreaUID);
  };

  //public getTreatmentAreaTypes(): Observable<FacilityTreatmentAreaTypeModel[]> {
  //  return this.http.get<FacilityTreatmentAreaTypeModel[]>(configSettings.WebAPI_URL + '/auth/facilities/TreatmentAreaTypes');
  //}


  //public getResourceAvailability(): Observable<ApiResponseModel> {
  //  //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/Resources');

  //  let apiResponse: ApiResponseModel[] = [{
  //    ErrorResult: '',
  //    Errors: [],
  //    Message: '',
  //    StatusCode: 200,
  //    Result: null
  //  }]

  //  return from(apiResponse);
  //}

}
