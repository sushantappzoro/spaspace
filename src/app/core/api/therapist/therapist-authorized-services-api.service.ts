import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { AuthorizedServiceForViewEditModel } from 'src/app/shared/models/therapist/authorized-service-for-view-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAuthorizedServicesApiService {

  constructor(private http: HttpClient) { }

  public get(therapistUid: string): Observable<AuthorizedServiceForViewEditModel[]> {
    return this.http.get<AuthorizedServiceForViewEditModel[]>
    (configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/authorizedservices');
  }

  public update(therapistUid: string, service: AuthorizedServiceForViewEditModel): Observable<AuthorizedServiceForViewEditModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/authorizedservices', service);
  }
}
