import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistAssociatedFacilityModel } from '../../../shared/models/therapist/therapist-associated-facility.model';
import { TherapistFacilityForListModel } from '../../../shared/models/therapist/therapist-facility-for-list.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistFacilitiesApiService {

  constructor(private http: HttpClient) { }

  public getAssociatedFacilities(therapistUid: string): Observable<TherapistAssociatedFacilityModel[]> {
    return this.http.get<TherapistAssociatedFacilityModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/AssociatedFacilities');
  }

  public add(therapistUid: string, facility: TherapistAssociatedFacilityModel): Observable<TherapistAssociatedFacilityModel[]> {
    return this.http.post<TherapistAssociatedFacilityModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/AssociatedFacilities', JSON.stringify(facility));
  };

  public edit(therapistUid: string, facility: TherapistAssociatedFacilityModel): Observable<TherapistAssociatedFacilityModel[]> {
    return this.http.put<TherapistAssociatedFacilityModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/AssociatedFacilities', JSON.stringify(facility));
  };

  public remove(therapistUid: string, facilityUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/AssociatedFacilities/' + facilityUID);
  };

  public getFacilitiesForList(): Observable<TherapistFacilityForListModel[]> {
    return this.http.get<TherapistFacilityForListModel[]>(configSettings.WebAPI_URL + '/Facilities/GetFacilitiesForList');
  }
}
