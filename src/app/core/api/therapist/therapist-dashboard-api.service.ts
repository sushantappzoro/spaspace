import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { MessageForListModel } from '../../../shared/models/common/message-for-list.model';
import { TherapistAppointmentSummaryForViewModel } from '../../../shared/models/therapist/therapist-appointment-summary-for-view.model';
import { TherapistTransactionsForDashboardModel } from '../../../shared/models/therapist/therapist-transactions-for-dashboard.model';
import { TherapistAppointmentForDashboardModel } from '../../../shared/models/therapist/therapist-appointment-for-dashboard.model';
import { ReviewForDashboardModel } from '../../../shared/models/therapist/review-for-dashboard.model';


@Injectable({
  providedIn: 'root'
})
export class TherapistDashboardApiService {

  constructor(private http: HttpClient) { }

  public getMessages(therapistUid: string): Observable<MessageForListModel[]> {
    return this.http.get<MessageForListModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/dashboard/messages');
  }

  public getUpcomingAppointments(therapistUid: string): Observable<TherapistAppointmentForDashboardModel[]> {
    return this.http.get<TherapistAppointmentForDashboardModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/dashboard/UpcomingAppointments');
  }

  public getPastAppointments(therapistUid: string): Observable<TherapistAppointmentForDashboardModel[]> {
    return this.http.get<TherapistAppointmentForDashboardModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/dashboard/PastAppointments');
  }

  public getTransactions(therapistUid: string): Observable<TherapistTransactionsForDashboardModel> {
    return this.http.get<TherapistTransactionsForDashboardModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/dashboard/Transactions');
  }

  public getRatings(therapistUid: string): Observable<ReviewForDashboardModel[]> {
    return this.http.get<ReviewForDashboardModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/dashboard/Reviews');
  }


}
