import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistAvailabilityForAddModel } from '../../../shared/models/therapist/therapist-availability-for-add.model';
import { TherapistAvailabilityForViewModel } from '../../../shared/models/therapist/therapist-availability-for-view.model';
import { TherapistAvailabilityForEditModel } from '../../../shared/models/therapist/therapist-availability-for-edit.model';


@Injectable({
  providedIn: 'root'
})
export class TherapistAvailabilityApiService {

  constructor(private http: HttpClient) { }

  public getAvailability(therapistUid: string): Observable<TherapistAvailabilityForViewModel[]> {
    return this.http.get<TherapistAvailabilityForViewModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Availability');
  }

  public addAvailability(therapistUid: string, availability: TherapistAvailabilityForAddModel): Observable<TherapistAvailabilityForViewModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Availability', JSON.stringify(availability));
  }

  public editAvailability(therapistUid: string, availability: TherapistAvailabilityForEditModel): Observable<TherapistAvailabilityForViewModel> {
    return this.http.put<TherapistAvailabilityForViewModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Availability', JSON.stringify(availability));
  }

  public removeAvailability(therapistUid: string, availabilityUID: string): Observable<boolean> {
    return this.http.delete<boolean>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Availability/' + availabilityUID);
  }
}
