import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistAppointmentSummaryForViewModel } from '../../../shared/models/therapist/therapist-appointment-summary-for-view.model';
import { AppointmentForTherapistDetailModel } from '../../../shared/models/therapist/appointment-for-therapist-detail.model';
import { TherapistAppointmentForDashboardModel } from '../../../shared/models/therapist/therapist-appointment-for-dashboard.model';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';
import { StartCheckoutResponseModel } from '../../../shared/models/therapist/start-checkout-response.model';
import { CompleteCheckoutResponseModel } from '../../../shared/models/therapist/complete-checkout-response.model';
import { CompleteCheckoutRequestModel } from '../../../shared/models/therapist/complete-checkout-request.model';


@Injectable({
  providedIn: 'root'
})
export class TherapistAppointmentsApiService {

  constructor(private http: HttpClient) { }

  public getAllAppointments(therapistUid: string): Observable<TherapistScheduleAndAppointmentsModel> {
    return this.http.get<TherapistScheduleAndAppointmentsModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/WithAvailability');
  }

  public getUpcomingAppointments(therapistUid: string): Observable<TherapistAppointmentForDashboardModel[]> {
    return this.http.get<TherapistAppointmentForDashboardModel[]>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/upcoming');
  }

  public getAppointmentDetail(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.http.get<AppointmentForTherapistDetailModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/detail/' + appointmentUid);
  }

  public checkIn(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.http.post<AppointmentForTherapistDetailModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/' + appointmentUid + '/CheckIn', null);
  }

  public startCheckOut(therapistUid: string, appointmentUid: string): Observable<StartCheckoutResponseModel> {
    return this.http.post<StartCheckoutResponseModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/' + appointmentUid + '/StartCheckOut', null);
  }

  public completeCheckOut(therapistUid: string, appointmentUid: string, 
    requestModel: CompleteCheckoutRequestModel): Observable<CompleteCheckoutResponseModel> {
    return this.http.post<CompleteCheckoutResponseModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/' +
      appointmentUid + '/CompleteCheckOut', requestModel);
  }
  public cancel(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.http.get<AppointmentForTherapistDetailModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/' + appointmentUid + '/Cancel');
  }

  public noShow(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.http.get<AppointmentForTherapistDetailModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Appointments/' + appointmentUid + '/NoShow');
  }


}
