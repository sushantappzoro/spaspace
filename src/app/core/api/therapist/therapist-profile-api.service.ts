import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistProfileForEditModel } from '../../../shared/models/therapist/therapist-profile-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistProfileApiService {

  constructor(private http: HttpClient) { }

  public getTherapist(therapistUid: string): Observable<TherapistProfileForEditModel> {
    return this.http.get<TherapistProfileForEditModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid);
  }

  public edit(therapistProfile: TherapistProfileForEditModel): Observable<TherapistProfileForEditModel> {
    return this.http.put<TherapistProfileForEditModel>(configSettings.WebAPI_URL + '/auth/therapists', therapistProfile);
  }

  public submitTherapist(therapistProfile: TherapistProfileForEditModel): Observable<TherapistProfileForEditModel> {
    return this.http.post<TherapistProfileForEditModel>(configSettings.WebAPI_URL + '/auth/therapists/Submit', therapistProfile);
  }
}
