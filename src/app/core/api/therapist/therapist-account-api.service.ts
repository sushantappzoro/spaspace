import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { StripeInfoModel } from 'src/app/shared/models/therapist/stripe-info.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAccountApiService {

  constructor(private http: HttpClient) { }

  public getStripeInfo(therapistUid: string): Observable<StripeInfoModel> {
    return this.http.get<StripeInfoModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/StripeInfo');
  }

  public addStripeInfo(therapistUid: string, stripeInfo: StripeInfoModel): Observable<StripeInfoModel> {
    return this.http.post<StripeInfoModel>(
      configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/StripeInfo', stripeInfo);
  }

}
