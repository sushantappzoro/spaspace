import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistLicenseModel } from '../../../shared/models/therapist/therapist-license.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistLicensesApiService {

  constructor(private http: HttpClient) { }

  public getLicenses(therapistUid: string): Observable<TherapistLicenseModel[]> {
    return this.http.get<TherapistLicenseModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Licenses');
  }

  public getLicense(therapistUid: string, licenseUID: string): Observable<TherapistLicenseModel> {
    return this.http.get<TherapistLicenseModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Licenses/' + licenseUID);
  }

  public add(therapistUid: string, license: TherapistLicenseModel): Observable<TherapistLicenseModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Licenses', JSON.stringify(license));
  };

  public edit(therapistUid: string, license: TherapistLicenseModel): Observable<TherapistLicenseModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Licenses', JSON.stringify(license));
  };

  public remove(therapistUid: string, licenseUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Licenses/' + licenseUID);
  };
}
