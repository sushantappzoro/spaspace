import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { TherapistCertificationModel } from '../../../shared/models/therapist/therapist-certification.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistCertificatesApiService {

  constructor(private http: HttpClient) { }

  public getCertificates(therapistUid: string): Observable<TherapistCertificationModel[]> {
    return this.http.get<TherapistCertificationModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Certifications');
  }

  public getCertificate(therapistUid: string, certificateUID: string): Observable<TherapistCertificationModel> {
    return this.http.get<TherapistCertificationModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Certifications/' + certificateUID);
  }

  public add(therapistUid: string, certificate: TherapistCertificationModel): Observable<TherapistCertificationModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Certifications', JSON.stringify(certificate));
  };

  public edit(therapistUid: string, certificate: TherapistCertificationModel): Observable<TherapistCertificationModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Certifications', JSON.stringify(certificate));
  };

  public remove(therapistUid: string, certificateUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/Certifications/' + certificateUID);
  };
}
