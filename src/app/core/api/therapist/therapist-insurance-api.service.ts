import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';

import { TherapistInsuranceForViewModel } from '../../../shared/models/therapist/therapist-insurance-for-view.model';
import { TherapistInsuranceForAddModel } from '../../../shared/models/therapist/therapist-insurance-for-add.model';
import { TherapistInsuranceForEditModel } from '../../../shared/models/therapist/therapist-insurance-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistInsuranceApiService {

  constructor(private http: HttpClient) { }

  public getInsurances(therapistUid: string): Observable<TherapistInsuranceForViewModel[]> {
    return this.http.get<TherapistInsuranceForViewModel[]>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/insurance');
  }

  public getInsurance(therapistUid: string, insuranceUID: string): Observable<TherapistInsuranceForViewModel> {
    return this.http.get<TherapistInsuranceForViewModel>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/insurance/' + insuranceUID);
  }

  public add(therapistUid: string, insurance: TherapistInsuranceForAddModel): Observable<TherapistInsuranceForViewModel> {
    return this.http.post<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/insurance', JSON.stringify(insurance));
  };

  public edit(therapistUid: string, insurance: TherapistInsuranceForEditModel): Observable<TherapistInsuranceForViewModel> {
    return this.http.put<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/insurance', JSON.stringify(insurance));
  };

  public remove(therapistUid: string, insuranceUID: string): Observable<boolean> {
    return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/therapists/' + therapistUid + '/insurance/' + insuranceUID);
  };
}
