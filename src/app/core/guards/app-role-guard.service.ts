import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { Observable, of } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';
import { ApplicationPaths, QueryParameterNames } from '../../../api-authorization/api-authorization.constants';

@Injectable({
  providedIn: 'root'
})
export class AppRoleGuardService implements CanActivate {

  constructor(
    private authService: AuthorizeService,
    private router: Router
  ) { }

  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const expectedRole = _next.data.expectedRole;

    return this.authService.getUser()
      .pipe(take(1),
        mergeMap(user => {
          if (!user) {
            this.router.navigate(ApplicationPaths.LoginPathComponents, {
              queryParams: {
                [QueryParameterNames.ReturnUrl]: state.url
              }
            });
          }
          if (user.role != expectedRole) {
            this.router.navigate(['/main/unauthorized']);
          }
          return of(true);
        })
      );

  }
}
