import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StepperControlService {
  private validateSubject: BehaviorSubject<number | null> = new BehaviorSubject(null);

  private completeSubject: BehaviorSubject<number | null> = new BehaviorSubject(null);

  private validationSubject: BehaviorSubject<string | null> = new BehaviorSubject(null);

  constructor() { }

  public validateStep(stepNumber: number) {
    this.validateSubject.next(stepNumber);
  }

  public getValidationStep(): Observable<number | null> {
    return this.validateSubject.asObservable();
  }

  public completeStep(stepNumber: number) {
    this.completeSubject.next(stepNumber);
  }

  public getCompleteStep(): Observable<number | null> {
    return this.completeSubject.asObservable();
  }

  public reportError(error: string) {
    this.validationSubject.next(error);
  }

  public getError(): Observable<string | null> {
    return this.validationSubject.asObservable();
  }

}
