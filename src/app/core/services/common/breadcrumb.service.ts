import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BreadcrumbModel } from '../../../shared/models/common/breadcrumb.model';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  private breadcrumbs$: BehaviorSubject<BreadcrumbModel[]> = new BehaviorSubject([]);

  constructor() {
  }

  public GetBreadcrumbs(): Observable<BreadcrumbModel[]> {
    return this.breadcrumbs$;
  }

  public SetBreadcrumbs(activatedRoute: ActivatedRoute) {
    this.breadcrumbs$.next(this.buildBreadCrumb(activatedRoute));
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '',
    breadcrumbs: BreadcrumbModel[] = []): BreadcrumbModel[] {

    //console.log('breadcrumbs', route);

    if (route.routeConfig && route.routeConfig.path == '') {
      return route.firstChild ? this.buildBreadCrumb(route.firstChild, url, breadcrumbs) : breadcrumbs;
    }

    if (route.routeConfig && !route.routeConfig.data) {
      return;
    }

    if (route.routeConfig && route.routeConfig.data && !route.routeConfig.data['breadcrumb']) {
      return;
    }

    //If no routeConfig is avalailable we are on the root path
    const label = route.routeConfig ? route.routeConfig.data['breadcrumb'] : 'Home';
    const path = route.routeConfig ? route.routeConfig.path : '';
    //In the routeConfig the complete path is not available, 
    //so we rebuild it each time
    const nextUrl = `${url}${path}/`;
    const breadcrumb: BreadcrumbModel = {
      label: label,
      url: nextUrl,
      params: ''
    };

    const newBreadcrumbs: BreadcrumbModel[] = [...breadcrumbs, breadcrumb];

    if (route.firstChild) {
      //If we are not on our current path yet, 
      //there will be more children to look after, to build our breadcumb
      return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);

    }
    //console.log('breadcrumbs', newBreadcrumbs);
    return newBreadcrumbs;
  }

}
