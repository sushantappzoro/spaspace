import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideNavService {

  private _toggle = new Subject();
  private toggle$ = this._toggle.asObservable();

  constructor() { }

  toggle() {
    return this.toggle$;
  }

  toggleNext() {
    this._toggle.next();
  }
}
