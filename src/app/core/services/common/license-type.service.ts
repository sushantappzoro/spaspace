import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { LicenseTypeApiService } from '../../api/common/license-type-api.service';
import { LicenseTypeModel } from '../../../shared/models/common/license-type.model';

@Injectable({
  providedIn: 'root'
})
export class LicenseTypeService {

  constructor(
    private api: LicenseTypeApiService
  ) { }

  public getAll(): Observable<LicenseTypeModel[]> {
    return this.api.getAll();
  }

}
