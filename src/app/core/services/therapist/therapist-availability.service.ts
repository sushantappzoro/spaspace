import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TherapistAvailabilityForViewModel } from '../../../shared/models/therapist/therapist-availability-for-view.model';
import { TherapistAvailabilityApiService } from '../../api/therapist/therapist-availability-api.service';
import { TherapistAvailabilityForAddModel } from '../../../shared/models/therapist/therapist-availability-for-add.model';
import { TherapistAvailabilityForEditModel } from '../../../shared/models/therapist/therapist-availability-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAvailabilityService {

  constructor(private availabilityService: TherapistAvailabilityApiService) { }


  public getAvailability(therapistUid: string): Observable<TherapistAvailabilityForViewModel[]> {
    return this.availabilityService.getAvailability(therapistUid);
  }

  public add(therapistUid: string, availability: TherapistAvailabilityForAddModel): Observable<TherapistAvailabilityForViewModel> {
    return this.availabilityService.addAvailability(therapistUid, availability);
  }

  public edit(therapistUid: string, availability: TherapistAvailabilityForEditModel): Observable<TherapistAvailabilityForViewModel> {
    return this.availabilityService.editAvailability(therapistUid, availability);
  }

  public remove(therapistUid: string, availabilityUID: string): Observable<boolean> {
    return this.availabilityService.removeAvailability(therapistUid, availabilityUID);
  }

}
