import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError, take } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';

import { TherapistAssociatedFacilityModel } from '../../../shared/models/therapist/therapist-associated-facility.model';
import { TherapistFacilitiesApiService } from '../../api/therapist/therapist-facilities-api.service';

@Injectable({
  providedIn: 'root'
})
export class TherapistFacilitiesService extends ObservableStore<therapistFacilitiesState> {

  constructor(private therapistFacilitiesApi: TherapistFacilitiesApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  getTherapistFacilities(therapistUid: string): Observable<TherapistAssociatedFacilityModel[]> {
    if (this.getState() && this.getState().therapistFacilities && this.getState().therapistUid && this.getState().therapistUid == therapistUid) {
      const therapistFacilities = this.getState().therapistFacilities;
      return of(therapistFacilities);
    } else {
      return this.fetchtherapistFacilities(therapistUid);
    }
  }

  add(therapistUid: string, therapistFacilities: TherapistAssociatedFacilityModel): Observable<TherapistAssociatedFacilityModel[]> {

    return this.therapistFacilitiesApi.add(therapistUid, therapistFacilities)
      .pipe(take(1),
        map(factilities => {
          //let state = this.getState();
          //var certs: TherapistAssociatedFacilityModel[];

          //if (this.getState() && this.getState().therapistFacilities) {
          //  certs = cloneDeep(state.therapistFacilities);
          //  certs.unshift(cert);
          //} else {
          //  certs = [];
          //  certs.push(cert);
          //}

          this.setState({ therapistFacilities: factilities }, therapistFacilitiesStoreActions.AddTherapistFacilities);

          return factilities;
        }, catchError(this.handleError)));
  }

  edit(therapistUid: string, therapistFacilities: TherapistAssociatedFacilityModel): Observable<TherapistAssociatedFacilityModel[]> {

    return this.therapistFacilitiesApi.edit(therapistUid,therapistFacilities)
      .pipe(take(1),
        map(factilities => {
          //let state = this.getState();
          //var certs: TherapistAssociatedFacilityModel[];

          //if (this.getState() && this.getState().therapistFacilities) {
          //  certs = cloneDeep(state.therapistFacilities);

          //  var index = certs.findIndex(x => x.UID == therapistFacilities.UID );
          //  certs.splice(index, 1, cert);

          //} else {
          //  certs = [];
          //  certs.push(cert);
          //}

          this.setState({ therapistFacilities: factilities }, therapistFacilitiesStoreActions.EditTherapistFacilities);

          return factilities;
        }, catchError(this.handleError)));
  }

  remove(therapistUid: string, therapistFacilitiesUID: string) {

    return this.therapistFacilitiesApi.remove(therapistUid, therapistFacilitiesUID)
      .pipe(take(1),
        map(cert => {

          let state = this.getState();
          var certs: TherapistAssociatedFacilityModel[];

          if (this.getState() && this.getState().therapistFacilities) {
            certs = cloneDeep(state.therapistFacilities);

            var index = certs.findIndex(x => x.UID == therapistFacilitiesUID);
            certs.splice(index, 1);

          } else {
            certs = []; 
          }

          this.setState({ therapistFacilities: certs }, therapistFacilitiesStoreActions.EditTherapistFacilities);

          return cert;
        }, catchError(this.handleError)));
  }

  reloadTherapistFacilities(therapistUid: string) {
    this.fetchtherapistFacilities(therapistUid).subscribe();
  }


  private fetchtherapistFacilities(therapistUid: string) {
    //console.log('getTherapist');
    return this.therapistFacilitiesApi.getAssociatedFacilities(therapistUid)
      .pipe(
        map(therapistFacilities => {
          this.setState({ therapistFacilities }, therapistFacilitiesStoreActions.GettherapistFacilities);
          this.setState({ therapistUid }, therapistFacilitiesStoreActions.SetTherapistUid);
          return therapistFacilities;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }
}

export enum therapistFacilitiesStoreActions {
  SetTherapistUid = 'set_therapist_uid',
  GettherapistFacilities = 'get_therapist_certifications',
  AddTherapistFacilities = 'add_therapist_certification',
  RemoveTherapistFacilities = 'remove_therapist_certification',
  EditTherapistFacilities = 'save_therapist_certification',
  SavetherapistFacilities = 'save_therapist_certifications',
}

export interface therapistFacilitiesState {
  therapistUid: string;
  therapistFacilities: TherapistAssociatedFacilityModel[];
}
