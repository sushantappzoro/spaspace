import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { configSettings } from '../../../app.config';
import { TherapistAppointmentSummaryForViewModel } from '../../../shared/models/therapist/therapist-appointment-summary-for-view.model';
import { Observable } from 'rxjs';
import { TherapistAppointmentsApiService } from '../../api/therapist/therapist-appointments-api.service';
import { AppointmentForTherapistDetailModel } from '../../../shared/models/therapist/appointment-for-therapist-detail.model';
import { TherapistAppointmentForDashboardModel } from '../../../shared/models/therapist/therapist-appointment-for-dashboard.model';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';
import { StartCheckoutResponseModel } from 'src/app/shared/models/therapist/start-checkout-response.model';
import { CompleteCheckoutResponseModel } from 'src/app/shared/models/therapist/complete-checkout-response.model';
import { CompleteCheckoutRequestModel } from 'src/app/shared/models/therapist/complete-checkout-request.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAppointmentsService {

  constructor(private therapistService: TherapistAppointmentsApiService) { }

  public getAllAppointments(therapistUid: string): Observable<TherapistScheduleAndAppointmentsModel> {
    return this.therapistService.getAllAppointments(therapistUid);
  }

  public getUpcomingAppointments(therapistUid: string): Observable<TherapistAppointmentForDashboardModel[]> {
    return this.therapistService.getUpcomingAppointments(therapistUid);
  }

  public getAppointmentDetail(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.therapistService.getAppointmentDetail(therapistUid, appointmentUid);
  }

  public checkIn(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.therapistService.checkIn(therapistUid, appointmentUid);
  }

  public startCheckOut(therapistUid: string, appointmentUid: string): Observable<StartCheckoutResponseModel> {
    return this.therapistService.startCheckOut(therapistUid, appointmentUid);
  }

  public completeCheckOut(therapistUid: string, appointmentUid: string, requestModel: CompleteCheckoutRequestModel):
  Observable<CompleteCheckoutResponseModel> {
    return this.therapistService.completeCheckOut(therapistUid, appointmentUid, requestModel);
  }
  public cancel(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.therapistService.cancel(therapistUid, appointmentUid);
  }

  public noShow(therapistUid: string, appointmentUid: string): Observable<AppointmentForTherapistDetailModel> {
    return this.therapistService.noShow(therapistUid, appointmentUid);
  }

}
