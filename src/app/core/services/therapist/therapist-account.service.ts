import { Injectable } from '@angular/core';
import { TherapistAccountApiService } from '../../api/therapist/therapist-account-api.service';
import { Observable } from 'rxjs';
import { StripeInfoModel } from 'src/app/shared/models/therapist/stripe-info.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAccountService {

  constructor(private accountService: TherapistAccountApiService) { }

  public getStripeInfo(therapistUid: string): Observable<StripeInfoModel> {
    return this.accountService.getStripeInfo(therapistUid);
  }

  public addStripeInfo(therapistUid: string, stripeInfo: StripeInfoModel): Observable<StripeInfoModel> {
    return this.accountService.addStripeInfo(therapistUid, stripeInfo);
  }

}
