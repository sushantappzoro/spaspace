import { Injectable } from '@angular/core';
import { TherapistDashboardApiService } from '../../api/therapist/therapist-dashboard-api.service';
import { Observable } from 'rxjs';
import { MessageForListModel } from '../../../shared/models/common/message-for-list.model';
import { TherapistAppointmentSummaryForViewModel } from '../../../shared/models/therapist/therapist-appointment-summary-for-view.model';
import { TherapistTransactionsForDashboardModel } from '../../../shared/models/therapist/therapist-transactions-for-dashboard.model';
import { TherapistAppointmentForDashboardModel } from '../../../shared/models/therapist/therapist-appointment-for-dashboard.model';
import { ReviewForDashboardModel } from '../../../shared/models/therapist/review-for-dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistDashboardService {

  constructor(private api: TherapistDashboardApiService) { }

  public getMessages(therapistUid: string): Observable<MessageForListModel[]> {
    return this.api.getMessages(therapistUid);
  }

  public getUpcomingAppointments(therapistUid: string): Observable<TherapistAppointmentForDashboardModel[]> {
    return this.api.getUpcomingAppointments(therapistUid);
  }

  public getPastAppointments(therapistUid: string): Observable<TherapistAppointmentForDashboardModel[]> {
    return this.api.getPastAppointments(therapistUid);
  }

  public getTransactions(therapistUid: string): Observable<TherapistTransactionsForDashboardModel> {
    return this.api.getTransactions(therapistUid);
  }

  public getRatings(therapistUid: string): Observable<ReviewForDashboardModel[]> {
    return this.api.getRatings(therapistUid);
  }

}
