import { TherapistProfileForEditModel } from "../../../shared/models/therapist/therapist-profile-for-edit.model";


export interface ProviderState {
  provider: TherapistProfileForEditModel
}
