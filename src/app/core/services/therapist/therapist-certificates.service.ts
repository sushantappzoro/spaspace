import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError, take } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';

import { TherapistCertificationModel } from '../../../shared/models/therapist/therapist-certification.model';
import { TherapistCertificatesApiService } from '../../api/therapist/therapist-certificates-api.service';

@Injectable({
  providedIn: 'root'
})
export class TherapistCertificatesService extends ObservableStore<TherapistCertificationsState> {

  constructor(private therapistCertificatesApi: TherapistCertificatesApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  getTherapistCertifications(therapistUid: string): Observable<TherapistCertificationModel[]> {
    if (this.getState() && this.getState().therapistCertifications && this.getState().therapistUid && this.getState().therapistUid == therapistUid ) {
      const therapistCertifications = this.getState().therapistCertifications;
      return of(therapistCertifications);
    } else {
      return this.fetchTherapistCertifications(therapistUid);
    }
  }

  add(therapistUid: string, therapistCertification: TherapistCertificationModel): Observable<TherapistCertificationModel> {
    //console.log('add cert', therapistCertification);
    return this.therapistCertificatesApi.add(therapistUid, therapistCertification)
      .pipe(take(1),
        map(cert => {
          let state = this.getState();
          var certs: TherapistCertificationModel[];

          if (this.getState() && this.getState().therapistCertifications) {
            certs = cloneDeep(state.therapistCertifications);
            certs.unshift(cert);
          } else {
            certs = [];
            certs.push(cert);
          }

          this.setState({therapistCertifications: certs }, TherapistCertificationsStoreActions.AddTherapistCertification);

          return cert;
        }, catchError(this.handleError)));    
  }

  edit(therapistUid: string, therapistCertification: TherapistCertificationModel) {

    return this.therapistCertificatesApi.edit(therapistUid, therapistCertification)
      .pipe(take(1),
        map(cert => {
          let state = this.getState();
          var certs: TherapistCertificationModel[];

          if (this.getState() && this.getState().therapistCertifications) {
            certs = cloneDeep(state.therapistCertifications);
            
            var index = certs.findIndex(x => x.UID == therapistCertification.UID );
            certs.splice(index, 1, cert);

            console.log('thecerts-', certs);
          } else {
            certs = [];
            certs.push(cert);
          }

          this.setState({therapistCertifications: certs }, TherapistCertificationsStoreActions.EditTherapistCertification);

          return cert;
        }, catchError(this.handleError)));
  }

  remove(therapistUid: string, therapistCertificationUID: string) {
    console.log('certs before', this.getState().therapistCertifications);
    return this.therapistCertificatesApi.remove(therapistUid, therapistCertificationUID)
      .pipe(take(1),
        map(cert => {
          //this.reloadCertifications(therapistUid);
          let state = this.getState();
          var certs: TherapistCertificationModel[];

          if (this.getState() && this.getState().therapistCertifications) {
            certs = cloneDeep(state.therapistCertifications);

            var index = certs.findIndex(x => x.UID == therapistCertificationUID);
            certs.splice(index, 1);

            console.log('thecerts-', certs);
          } else {
            certs = [];
          }

          this.setState({ therapistCertifications: certs }, TherapistCertificationsStoreActions.EditTherapistCertification);

          return cert;
        }, catchError(this.handleError)));
  }

  reloadCertifications(therapistUid: string) {
    this.fetchTherapistCertifications(therapistUid).subscribe();
  }

  private fetchTherapistCertifications(Uid: string) {
    //console.log('getTherapist');
    return this.therapistCertificatesApi.getCertificates(Uid)
      .pipe(
        map(certs => {
          this.setState({ therapistCertifications: certs }, TherapistCertificationsStoreActions.GetTherapistCertifications);
          this.setState({ therapistUid: Uid }, TherapistCertificationsStoreActions.SetTherapistUid);
          return certs;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }
}

export enum TherapistCertificationsStoreActions {
  SetTherapistUid = 'set_therapist_uid',
  GetTherapistCertifications = 'get_therapist_certifications',
  AddTherapistCertification = 'add_therapist_certification',
  RemoveTherapistCertification = 'remove_therapist_certification',
  EditTherapistCertification = 'save_therapist_certification',
  SaveTherapistCertifications = 'save_therapist_certifications',
}

export interface TherapistCertificationsState {
  therapistUid: string;
 therapistCertifications: TherapistCertificationModel[];
}
