import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError, take } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';

import { TherapistInsuranceApiService } from '../../api/therapist/therapist-insurance-api.service';
import { TherapistInsuranceForViewModel } from '../../../shared/models/therapist/therapist-insurance-for-view.model';
import { TherapistInsuranceForAddModel } from '../../../shared/models/therapist/therapist-insurance-for-add.model';
import { TherapistInsuranceForEditModel } from '../../../shared/models/therapist/therapist-insurance-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistInsuranceService extends ObservableStore<TherapistInsurancesState> {

  constructor(private therapistInsuranceApi: TherapistInsuranceApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  getTherapistInsurances(therapistUid: string): Observable<TherapistInsuranceForViewModel[]> {
    if (this.getState() && this.getState().therapistInsurances && this.getState().therapistUid && this.getState().therapistUid == therapistUid) {
      const therapistInsurances = this.getState().therapistInsurances;
      return of(therapistInsurances);
    } else {
      return this.fetchTherapistInsurances(therapistUid);
    }
  }

  add(therapistUid: string, therapistInsurance: TherapistInsuranceForAddModel): Observable<TherapistInsuranceForViewModel> {

    return this.therapistInsuranceApi.add(therapistUid, therapistInsurance)
      .pipe(take(1),
        map(resp => {
          let state = this.getState();
          var insurances: TherapistInsuranceForViewModel[];

          if (this.getState() && this.getState().therapistInsurances) {
            insurances = cloneDeep(state.therapistInsurances);
            insurances.unshift(resp);
          } else {
            insurances = [];
            insurances.push(resp);
          }

          this.setState({ therapistInsurances: insurances }, TherapistInsurancesStoreActions.AddTherapistInsurance);

          return resp;
        }, catchError(this.handleError)));
  }

  edit(therapistUid: string, therapistInsurance: TherapistInsuranceForEditModel): Observable<TherapistInsuranceForViewModel> {

    return this.therapistInsuranceApi.edit(therapistUid, therapistInsurance)
      .pipe(take(1),
        map(resp => {
          let state = this.getState();
          var insurances: TherapistInsuranceForViewModel[];

          if (this.getState() && this.getState().therapistInsurances) {
            insurances = cloneDeep(state.therapistInsurances);

            var index = insurances.findIndex(x => x.UID == therapistInsurance.UID );

            insurances.splice(index, 1, resp);

          } else {
            insurances = [];
            insurances.push(resp);
          }

          this.setState({ therapistInsurances: insurances }, TherapistInsurancesStoreActions.EditTherapistInsurance);

          return resp;
        }, catchError(this.handleError)));
  }

  remove(therapistUid: string, therapistInsuranceUID: string) {

    return this.therapistInsuranceApi.remove(therapistUid, therapistInsuranceUID)
      .pipe(take(1),
        map(cert => {

          let state = this.getState();
          var insurances: TherapistInsuranceForViewModel[];

          if (this.getState() && this.getState().therapistInsurances) {
            insurances = cloneDeep(state.therapistInsurances);

            var index = insurances.findIndex(x => x.UID == therapistInsuranceUID);

            insurances.splice(index, 1);

          } else {
            insurances = [];
          }

          this.setState({ therapistInsurances: insurances }, TherapistInsurancesStoreActions.EditTherapistInsurance);

          return cert;
        }, catchError(this.handleError)));
  }

  reloadInsurances(therapistUid: string) {
    this.fetchTherapistInsurances(therapistUid).subscribe();
  }

  private fetchTherapistInsurances(therapistUid: string) {
    //console.log('getTherapist');
    return this.therapistInsuranceApi.getInsurances(therapistUid)
      .pipe(
        map(therapistInsurances => {
          this.setState({ therapistInsurances }, TherapistInsurancesStoreActions.GetTherapistInsurances);
          this.setState({ therapistUid }, TherapistInsurancesStoreActions.SetTherapistUid);
          return therapistInsurances;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }
}

export enum TherapistInsurancesStoreActions {
  SetTherapistUid = 'set_therapist_uid',
  GetTherapistInsurances = 'get_therapist_insurance',
  AddTherapistInsurance = 'add_therapist_certification',
  RemoveTherapistInsurance = 'remove_therapist_certification',
  EditTherapistInsurance = 'save_therapist_certification',
  SaveTherapistInsurances = 'save_therapist_insurance',
}

export interface TherapistInsurancesState {
  therapistUid: string;
  therapistInsurances: TherapistInsuranceForViewModel[];
}
