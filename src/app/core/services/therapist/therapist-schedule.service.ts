import { Injectable } from '@angular/core';
import { TherapistScheduleApiService } from '../../api/therapist/therapist-schedule-api.service';
import { Observable } from 'rxjs';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistScheduleService {

  constructor(private therapistScheduleApi: TherapistScheduleApiService) { }


  public getAppointments(UID: string): Observable<TherapistScheduleAndAppointmentsModel> {
    return this.therapistScheduleApi.getAppointments(UID);
  }
}
