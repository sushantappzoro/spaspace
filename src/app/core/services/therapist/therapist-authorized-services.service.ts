import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError, take } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';

import { TherapistAuthorizedServicesApiService } from '../../api/therapist/therapist-authorized-services-api.service';
import { AuthorizedServiceForViewEditModel } from 'src/app/shared/models/therapist/authorized-service-for-view-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAuthorizedServiceService extends ObservableStore<TherapistAuthorizedServiceState> {

  constructor(private api: TherapistAuthorizedServicesApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  getTherapistAuthorizedServices(therapistUid: string): Observable<AuthorizedServiceForViewEditModel[]> {
    if (this.getState() && this.getState().therapistAuthorizedServices && this.getState().therapistUid && this.getState().therapistUid == therapistUid) {
      const therapistAuthorizedServices = this.getState().therapistAuthorizedServices;
      return of(therapistAuthorizedServices);
    } else {
      return this.fetchTherapistAuthorizedServices(therapistUid);
    }
  }

  public reloadTherapistAuthorizedServices(therapistUid: string) {
    return this.fetchTherapistAuthorizedServices(therapistUid);
  }

  //public get(therapistUID: string): Observable<AuthorizedServiceForViewEditModel[]> {
  //  return this.api.get(therapistUID);
  //}

  public update(therapistUID: string, service: AuthorizedServiceForViewEditModel): Observable<AuthorizedServiceForViewEditModel> {
    return this.api.update(therapistUID, service);
  }

  edit(therapistUid: string, therapistAuthorizedService: AuthorizedServiceForViewEditModel) {

    return this.api.update(therapistUid, therapistAuthorizedService)
      .pipe(take(1),
        map(service => {
          let state = this.getState();
          var certs: AuthorizedServiceForViewEditModel[];

          if (this.getState() && this.getState().therapistAuthorizedServices) {
            certs = cloneDeep(state.therapistAuthorizedServices);

            var index = certs.findIndex(x => x.UID == therapistAuthorizedService.UID);
            certs.splice(index, 1, service);

            //console.log('thecerts-', certs);
          } else {
            certs = [];
            certs.push(service);
          }

          this.setState({ therapistAuthorizedServices: certs }, TherapistAuthorizedServiceStoreActions.EditTherapistAuthorizedService);

          return service;
        }, catchError(this.handleError)));
  }

  private fetchTherapistAuthorizedServices(Uid: string) {
    //console.log('getTherapist');
    return this.api.get(Uid)
      .pipe(
        map(services => {
          this.setState({ therapistAuthorizedServices: services }, TherapistAuthorizedServiceStoreActions.GetTherapistAuthorizedServices);
          this.setState({ therapistUid: Uid }, TherapistAuthorizedServiceStoreActions.SetTherapistUid);
          return services;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }

}

export enum TherapistAuthorizedServiceStoreActions {
  SetTherapistUid = 'set_therapist_uid',
  GetTherapistAuthorizedServices = 'get_therapist_authorizedServices',
  AddTherapistAuthorizedService = 'add_therapist_authorizedService',
  RemoveTherapistAuthorizedService = 'remove_therapist_authorizedService',
  EditTherapistAuthorizedService = 'save_therapist_authorizedService',
  SaveTherapistAuthorizedServices = 'save_therapist_authorizedServices',
}

export interface TherapistAuthorizedServiceState {
  therapistUid: string;
  therapistAuthorizedServices: AuthorizedServiceForViewEditModel[];
}
