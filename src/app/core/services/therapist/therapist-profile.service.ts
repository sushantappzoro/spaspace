import { Injectable } from '@angular/core';
import { Observable, from, of, throwError, observable } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError } from 'rxjs/operators';

import { TherapistProfileForEditModel } from '../../../shared/models/therapist/therapist-profile-for-edit.model';
import { TherapistProfileApiService } from '../../api/therapist/therapist-profile-api.service';
import { ApiErrorModel } from '../../../shared/models/common/api-error.model';
import { SpaTherapistExpertiseForViewModel } from '../../../../../projects/spaspace-lib/src/shared/models';

@Injectable({
  providedIn: 'root'
})
export class TherapistProfileService extends ObservableStore<TherapistState> {

  constructor(private therapistProfileApi: TherapistProfileApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  getTherapistProfile(UID: string): Observable<TherapistProfileForEditModel> {
    return this.getTherapist(UID)
      .pipe(
        map(therapist => {
          //var profile = this.toProfile(therapist);
          //return profile;
          return therapist;
        }));
  }

  getTherapistProfilePhotoURL(UID: string): Observable<string> {
    return this.getTherapist(UID)
      .pipe(
        map(therapist => {
          return therapist.ProfilePhoto.Url;
        }));
  }

  getTherapistProfileExpertise(UID: string): Observable<SpaTherapistExpertiseForViewModel[]> {
    return this.getTherapist(UID)
      .pipe(
        map(therapist => {
          return therapist.Expertises;
        }));
  }

  getTherapistStatus(UID: string): Observable<string> {
    return this.getTherapist(UID)
      .pipe(
        map(therapist => {
          return therapist.Status;
        }));
  }

  reloadTherapist(UID): Observable<TherapistProfileForEditModel> {
    return this.fetchTherapist(UID);
  }

  //public submitTherapist(therapistProfile: TherapistProfileForEditModel): Observable<TherapistProfileForEditModel> {
  //  //return this.therapistProfileApi.submitTherapist(therapistProfile);
  //  return this.therapistProfileApi.submitTherapist(therapistProfile)
  //    .pipe(
  //      map(therapist => {
  //        this.setState({ therapist }, TherapistStoreActions.GetTherapist);
  //        return therapist;
  //      }),
  //      catchError(this.handleError)
  //    );
  //}

  private getTherapist(UID: string): Observable<TherapistProfileForEditModel> {

    if (this.getState() && this.getState().therapist && this.getState().therapist.UID == UID) {
      const therapist = this.getState().therapist;
      return of(therapist);
    } else {    
      return this.fetchTherapist(UID);
    }

  }

  editTherapist(therapist: TherapistProfileForEditModel): Observable<TherapistProfileForEditModel> {

    //need to apply changes to model in store, then send to api

    if (this.getState() && this.getState().therapist) {
      const therapist = this.getState().therapist;

    }

    return this.therapistProfileApi.edit(therapist)
      .pipe(
        map(therapist => {
          this.setState({ therapist }, TherapistStoreActions.GetTherapist);
          return therapist;
        }),
        catchError(this.handleError)
      );
  }

  submitTherapist(therapist: TherapistProfileForEditModel): Observable<TherapistProfileForEditModel> {

    //need to apply changes to model in store, then send to api

    if (this.getState() && this.getState().therapist) {
      const therapist = this.getState().therapist;

    }

    return this.therapistProfileApi.submitTherapist(therapist)
      .pipe(
        map(therapist => {
          //this.setState({ therapist }, TherapistStoreActions.GetTherapist);
          return therapist;
        }),
        catchError(this.handleError)
      );
  }

  fetchTherapist(UID: string) {

    return this.therapistProfileApi.getTherapist(UID)
      .pipe(
        map(therapist => {
          console.log('fetchTherapist', therapist);
          this.setState({ therapist }, TherapistStoreActions.GetTherapist);
          return therapist;
        }),
        catchError(this.handleError)
      );
  }

  //private toProfile(therapist: TherapistProfileForEditModel): TherapistProfileForUIModel {
  //  return new TherapistProfileForUIModel(
  //    //therapist.UID,
  //    therapist.Bio,
  //    therapist.EmailAddress,
  //    therapist.MobilePhone,
  //    therapist.FirstName,
  //    therapist.LastName,
  //    //therapist.PressureCapability,
  //    therapist.YearsExperience
  //  );
  //}

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      console.error('server error2');
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    if (error.error instanceof ApiErrorModel) {
      console.error('server error3');
      return Observable.throw(error.error);
    }
    console.error('server error4');
    return throwError(error)
    //return Observable.throw(error || 'Server error');
  }
}

export interface TherapistState {
  therapist: TherapistProfileForEditModel
}

export enum TherapistStoreActions {
  GetTherapist = 'get_therapist',
  SaveTherapist = 'save_therapist',
}
