import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError, take } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';

import { TherapistLicenseModel } from '../../../shared/models/therapist/therapist-license.model';
import { TherapistLicensesApiService } from '../../api/therapist/therapist-licenses-api.service';

@Injectable({
  providedIn: 'root'
})
export class TherapistLicensesService extends ObservableStore<TherapistLicensesState> {

  constructor(private therapistLicensesApi: TherapistLicensesApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  getTherapistLicenses(therapistUid: string, reload: boolean = false): Observable<TherapistLicenseModel[]> {
    if (reload == false && this.getState() && this.getState().therapistLicenses && this.getState().therapistUid && this.getState().therapistUid == therapistUid) {
      const therapistLicenses = this.getState().therapistLicenses;
      return of(therapistLicenses);
    } else {
      return this.fetchTherapistLicenses(therapistUid);
    }
  }

  add(therapistUid: string, therapistLicenses: TherapistLicenseModel): Observable<TherapistLicenseModel> {

    return this.therapistLicensesApi.add(therapistUid, therapistLicenses)
      .pipe(take(1),
        map(cert => {
          let state = this.getState();
          var certs: TherapistLicenseModel[];

          if (this.getState() && this.getState().therapistLicenses) {
            certs = cloneDeep(state.therapistLicenses);
            certs.unshift(cert);
          } else {
            certs = [];
            certs.push(cert);
          }

          this.setState({ therapistLicenses: certs }, TherapistLicensesStoreActions.AddTherapistLicense);

          return cert;
        }, catchError(this.handleError)));
  }

  edit(therapistUid: string, therapistLicenses: TherapistLicenseModel) {

    return this.therapistLicensesApi.edit(therapistUid, therapistLicenses)
      .pipe(take(1),
        map(cert => {
          let state = this.getState();
          var certs: TherapistLicenseModel[];

          if (this.getState() && this.getState().therapistLicenses) {
            certs = cloneDeep(state.therapistLicenses);
            var index = certs.findIndex(x => x.UID == therapistLicenses.UID);
            certs.splice(index, 1, cert);

          } else {
            certs = [];
            certs.push(cert);
          }

          this.setState({ therapistLicenses: certs }, TherapistLicensesStoreActions.EditTherapistLicense);

          return cert;
        }, catchError(this.handleError)));
  }

  remove(therapistUid: string, therapistLicensesUID: string) {

    return this.therapistLicensesApi.remove(therapistUid, therapistLicensesUID)
      .pipe(take(1),
        map(cert => {
          let state = this.getState();
          var certs: TherapistLicenseModel[];

          if (this.getState() && this.getState().therapistLicenses) {
            certs = cloneDeep(state.therapistLicenses);
            var index = certs.findIndex(x => x.UID == therapistLicensesUID);
            certs.splice(index, 1);

          } else {
            certs = [];
          }

          this.setState({ therapistLicenses: certs }, TherapistLicensesStoreActions.EditTherapistLicense);

          return cert;
        }, catchError(this.handleError)));
  }

  reloadLicenses(therapistUid: string) {
    this.fetchTherapistLicenses(therapistUid).subscribe();
  }


  private fetchTherapistLicenses(Uid: string) {
    //console.log('getTherapist');
    return this.therapistLicensesApi.getLicenses(Uid)
      .pipe(
        map(licenses => {
          this.setState({ therapistLicenses: licenses }, TherapistLicensesStoreActions.GetTherapistLicenses);
          this.setState({ therapistUid: Uid }, TherapistLicensesStoreActions.SetTherapistUid);
          return licenses;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }
}

export enum TherapistLicensesStoreActions {
  SetTherapistUid = 'set_therapist_uid',
  GetTherapistLicenses = 'get_therapist_licenses',
  AddTherapistLicense = 'add_therapist_license',
  RemoveTherapistLicense = 'remove_therapist_license',
  EditTherapistLicense = 'save_therapist_license',
  SaveTherapistLicenses = 'save_therapist_licenses',
}

export interface TherapistLicensesState {
  therapistUid: string;
  therapistLicenses: TherapistLicenseModel[];
}
