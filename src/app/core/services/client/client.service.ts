import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';
import { CustomerModel } from '../../../shared/models/client/customer.model';
import { ObservableStore } from '@codewithdan/observable-store';
import { map } from 'rxjs/operators';
import { CustomerProfileModel } from '../../../shared/models/client/customer-profile.model';
import { CustomerFocusAreaModel } from '../../../shared/models/client/customer-focus-area.model';
import { CustomerNeedsModel } from '../../../shared/models/client/customer-needs.model';
import { CustomerFacilityPreferencesModel } from '../../../shared/models/client/customer-facility-preferences.model';
import { CustomerTherapistPreferencesModel } from '../../../shared/models/client/customer-therapist-preferences.model';
import { AuthorizeService, IUser } from '../../../../api-authorization/authorize.service';


export interface StoreState {
    currentCustomer: CustomerModel;
}

@Injectable({
  providedIn: 'root'
})
export class ClientService extends ObservableStore<StoreState> {

    user: IUser = null;

    constructor(private http: HttpClient) {
        super({ trackStateHistory: false, logStateChanges: true });
        console.log('ClientService - constructor');
    }

    public getCustomer(id: string) {
        console.log('ClientService.getCustomer');
        const state = this.getState();

        if (state && state.currentCustomer) {
            console.log('ClientService - customer from state', state.currentCustomer);
        } else {
            console.log('ClientService - no current state');
        }

        if (state && state.currentCustomer && state.currentCustomer.UserUID === id) {
            return of(state.currentCustomer);
        } else {
            if (!id || id.length == 0) {
                var currentCustomer = this.initCustomer()
                console.log('ClientService - set state on dummy customer', currentCustomer);
                this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
                return of(currentCustomer);
            } else {
                this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Customer/GetByUserID/' + id)
                    .pipe(map(u => u && u.Result))
                    .subscribe(currentCustomer => {
                        console.log('ClientService - set state on actual customer', currentCustomer);
                        this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
                        return of(currentCustomer);
                    });
            }
        }
    }

    updateCustomer(currentCustomer: CustomerModel) {
        //const state = this.getState();
        this.setState({ currentCustomer }, CustomersStoreActions.SetCustomer);
    }

    getCurrentCustomer() {
        console.log('ClientService.getCustomer');
        const state = this.getState();

        if (state && state.currentCustomer) {
            console.log('ClientService - customer from state', state.currentCustomer);
        } else {
            console.log('ClientService - no current state');
        }

        if (state && state.currentCustomer) {
            return of(state.currentCustomer);
        } else {

            var currentCustomer = this.initCustomer()

            console.log('ClientService - set state on dummy customer', currentCustomer);
            this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
            return of(currentCustomer);
        }
    }

    private initCustomer(): CustomerModel {
        let currentCustomer = new CustomerModel();
        currentCustomer.Profile = new CustomerProfileModel();
        currentCustomer.Profile.CurrentFocusArea = new CustomerFocusAreaModel();
        currentCustomer.Profile.CurrentNeeds = new CustomerNeedsModel();
        currentCustomer.Profile.FacilityPreferences = new CustomerFacilityPreferencesModel();
        currentCustomer.Profile.TherapistPreferences = new CustomerTherapistPreferencesModel();
        return currentCustomer;
    }

    //haveCustomer(): boolean {
    //    const state = this.getState();
    //    if (state && state.currentCustomer) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //}

    //public getCurrentCustomer() {
    //    console.log('getCurrentCustomer');
    //    const state = this.getState();
    //    if (state && state.currentCustomer) {
    //        console.log('stateHistory:', this.stateHistory);
    //        return of(state.currentCustomer);
    //    }
    //    // doesn't exist in store so create empty customer
    //    else {
    //        let cust = new CustomerModel();
    //        cust.Profile = new CustomerProfileModel();
    //        cust.Profile.CurrentFocusArea = new CustomerFocusAreaModel();
    //        cust.Profile.CurrentNeeds = new CustomerNeedsModel();
    //        cust.Profile.FacilityPreferences = new CustomerFacilityPreferencesModel();
    //        cust.Profile.TherapistPreferences = new CustomerTherapistPreferencesModel();
    //        return of(cust);
    //    }
    //}

    //private fetchCustomer(id: string): Observable<ApiResponseModel> {
    //    console.log('fetchCustomer');
    //    return this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Customer/' + id);
    //}

    //private updateCustomerFromService(id: string) {
    //    console.log('updateCustomerFromService');
    //    this.fetchCustomer(id)
    //        .subscribe(resp => {
    //            var currentCustomer: CustomerModel = resp.Result;
    //            const state = this.getState();
    //            this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer);
    //            //if (state && state.currentCustomer) {
    //            //  this.setState({ currentCustomer }, CustomersStoreActions.GetCustomer)
    //            //} else {
    //            //  // merge
    //            //}
    //    });
    //}

    //public getCustomer(id: string) {
    //    const state = this.getState();
    //    if (state && state.currentCustomer) {
    //        console.log('stateHistory:', this.stateHistory);
    //        return of(state.currentCustomer);
    //    }
    //    // doesn't exist in store so fetch from server
    //    else {
    //        // assu
    //        return this.fetchCustomer();
    //    }
    //}

    //getCustomers() {
    //    const state = this.getState();
    //    // pull from store cache
    //    if (state && state.currentCustomer) {
    //        console.log('stateHistory:', this.stateHistory);
    //        return of(state.currentCustomer);
    //    }
    //    // doesn't exist in store so fetch from server
    //    else {
    //        return this.fetchCustomers();
    //    }
    //}
}

export enum CustomersStoreActions {
    GetCustomers = 'get_customers',
    GetCustomer = 'get_customer',
    SetCustomer = 'set_customer'
}
