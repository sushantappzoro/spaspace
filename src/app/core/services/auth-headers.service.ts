import { Injectable } from '@angular/core';
import { AuthorizeService } from '../../../api-authorization/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class AuthHeadersService {

  headers: any;

  constructor(private authService: AuthorizeService) {

    this.authService.getAccessToken()
      .subscribe(token => {
        this.headers = { 'Authorization': 'Bearer ' + token };
      });
  }

  getHeaders() {
    return this.headers;
  }
}
