import { Injectable } from '@angular/core';
import { ErrorModel } from '../../shared/models/common/error.model';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  private errors: ErrorModel[];

  constructor() { }

  public loadErrorMessages(errors: ErrorModel[]) {
    this.errors = errors;
  }

  public getErrorMessage(code: string) {
    return this.errors.find(err => err.Code == code);
  }

}
