import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';
import { ResourceModel } from '../../../shared/interfaces/resource';
import { ResourceTypeModel } from '../../../shared/interfaces/resource-type';
import { ResourceAvailabilityModel } from '../../../shared/interfaces/resource-availability.model';
import { FacilityModel } from '../../../shared/models/facility/facility.model';
import { FacilityApiService } from '../../api/facility/facility-api.service';
import { FacilityForListModel } from '../../../shared/models/facility/facility-for-list.model';
import { FacilityForAuthViewModel } from '../../../shared/models/facility/facility-for-auth-view.model';
import { FacilityForAuthEditModel } from '../../../shared/models/facility/facility-for-auth-edit.model';
import { TimeZoneModel } from 'src/app/shared/models/facility/time-zone.model';

@Injectable({
    providedIn: 'root'
})

export class FacilityService {

  constructor(private facilityApi: FacilityApiService) { }

  public get(): Observable<FacilityForListModel[]> {
    return this.facilityApi.get();
  }

  public getActive(): Observable<FacilityForListModel[]> {
    return this.facilityApi.getActive();
  }

  public getFacility(uid: string): Observable<FacilityForAuthViewModel> {
    return this.facilityApi.getFacility(uid);
  }

  public add(facility: FacilityForAuthEditModel): Observable<any> {
    return this.facilityApi.add(facility);
  }

  public edit(facility: FacilityForAuthEditModel): Observable<any> {
    return this.facilityApi.edit(facility);
  }

  public submit(facility: FacilityForAuthEditModel): Observable<FacilityForAuthViewModel> {
    return this.facilityApi.submit(facility);
  }

  public getAllTimeZones(): Observable<TimeZoneModel[]> {
    return this.facilityApi.getAllTimeZones();
  }

    // public getPhoto(id: string): Observable<Blob> {
    //    return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/GetPhoto/' + id);
    // }

    //// Facility Services
    // public getFacilities(): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: null
    //    }]

    //    return from(apiResponse);
    // }

    // public getFacility(facilityCode: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //      Result: null
    //    }]

    //    return from(apiResponse);
    // }


    //// Profile Services
    // public getProfile(facilityId: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //      Result: null
    //    }]

    //    return from(apiResponse);
    // }

    //// public saveProfile(profile: FacilityProfile): Observable<ApiResponseModel> {
    ////    return this.http.post<any>(configSettings.WebAPI_URL + '/Facilities/Profile', profile);
    //// }

    // public updateProfile(profile: FacilityModel): Observable<ApiResponseModel> {
    //    return this.http.put<any>(configSettings.Auth_URL + '/Facilities/Profile/' , profile);
    // }

    //// Resource Type Services

}
