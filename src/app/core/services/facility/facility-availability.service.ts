import { Injectable } from '@angular/core';
import { TreatmentAreaAvailabilityForAddModel } from '../../../shared/models/facility/treatment-area-availability-for-add.model';
import { Observable, of } from 'rxjs';
import { TreatmentAreaAvailabilityForViewModel } from '../../../shared/models/facility/treatment-area-availability-for-view.model';
import { TreatmentAreaAvailabilityForEditModel } from '../../../shared/models/facility/treatment-area-availability-for-edit.model';
import { FacilityAvailabilityApiService } from '../../api/facility/facility-availability-api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityAvailabilityService {

  constructor(private api: FacilityAvailabilityApiService) { }

  public getTreatmentAreaAvailabilities(facilityUid: string, treatmentAreaUID: string): Observable<TreatmentAreaAvailabilityForViewModel[]> {
    return this.api.getTreatmentAreaAvailabilities(facilityUid, treatmentAreaUID);
  }

  public addTreatmentAreaAvailability(facilityUid: string, treatmentAreaUID: string, avalability: TreatmentAreaAvailabilityForAddModel): Observable<TreatmentAreaAvailabilityForEditModel> {
    return this.api.addTreatmentAreaAvailability(facilityUid, treatmentAreaUID, avalability);
  }

  public editTreatmentAreaAvailability(facilityUid: string, treatmentAreaUID: string, avalability: TreatmentAreaAvailabilityForViewModel): Observable<TreatmentAreaAvailabilityForEditModel> {
    return this.api.editTreatmentAreaAvailability(facilityUid, treatmentAreaUID, avalability);
  }

  public removeTreatmentAreaAvailability(facilityUid: string, treatmentAreaUID: string, availabilityUID: string): Observable<TreatmentAreaAvailabilityForEditModel> {
    return this.api.removeTreatmentAreaAvailability(facilityUid, treatmentAreaUID, availabilityUID);
  }


  public add(facilityUID: string, treatmentArea: TreatmentAreaAvailabilityForAddModel): Observable<TreatmentAreaAvailabilityForViewModel> {
    var temp = new TreatmentAreaAvailabilityForViewModel();
    return of(temp);
  };

  public edit(facilityUID: string, treatmentArea: TreatmentAreaAvailabilityForEditModel): Observable<TreatmentAreaAvailabilityForViewModel> {
    var temp = new TreatmentAreaAvailabilityForViewModel();
    return of(temp);
  };

  public remove(facilityUID: string, treatmentAreaUID: string): Observable<boolean> {
    return;
  };

}
