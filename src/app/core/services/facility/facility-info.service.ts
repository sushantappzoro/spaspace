import { Injectable } from '@angular/core';
import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';
import { Observable } from 'rxjs';
import { FacilityInfoApiService } from '../../api/facility/facility-info-api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityInfoService {

  constructor(private facilityInfoApi: FacilityInfoApiService) { }

  public getFacilityInfo(facilityUid: string): Observable<FacilityInfoForAdminModel> {
    return this.facilityInfoApi.getFacilityInfo(facilityUid);
  }

  public editFacilityInfo(facilityUid: string, facilityInfo: FacilityInfoForAdminModel): Observable<FacilityInfoForAdminModel> {
    return this.facilityInfoApi.editFacilityInfo(facilityUid, facilityInfo);
  }

}
