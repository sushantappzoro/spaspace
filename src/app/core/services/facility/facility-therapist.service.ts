import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { FacilityTherapistApiService } from '../../api/facility/facility-therapist-api.service';
import { TherapistForFacilityScheduleViewModel } from '../../../shared/models/facility/therapist-for-facility-schedule-view.model';


@Injectable({
  providedIn: 'root'
})
export class FacilityTherapistService {

  constructor(private api: FacilityTherapistApiService) { }

  public getAssociatedTherapists(facilityUID: string): Observable<TherapistForFacilityScheduleViewModel[]> {
    return this.api.getAssociatedTherapists(facilityUID);
  }

}
