import { Injectable } from '@angular/core';
import { FacilityTreatmentAreasApiService } from '../../api/facility/facility-treatment-areas-api.service';
import { FacilityTreatmentAreaModel } from '../../../shared/models/facility/facility-treatment-area.model';
import { Observable } from 'rxjs';
import { FacilityTreatmentAreaViewModel } from '../../../shared/models/facility/facility-treatment-area-view.model';
import { FacilityTreatmentAreaForAddModel } from '../../../shared/models/facility/facility-treatment-area-for-add.model';
import { FacilityTreatmentAreaForEditModel } from '../../../shared/models/facility/facility-treatment-area-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTreatmentAreasService {

  constructor(private treatmentAreasApi: FacilityTreatmentAreasApiService) { }

  public getTreatmentArea(facilityUID: string, treatmentAreaUID: string): Observable<FacilityTreatmentAreaModel> {
    return this.treatmentAreasApi.getTreatmentArea(facilityUID, treatmentAreaUID);
  }

  public getTreatmentAreas(facilityUID: string): Observable<FacilityTreatmentAreaViewModel[]> {
    return this.treatmentAreasApi.getTreatmentAreas(facilityUID);
  }

  public newTreatmentArea(facilityUID: string): Observable<FacilityTreatmentAreaForEditModel> {
    return this.treatmentAreasApi.newTreatmentArea(facilityUID);
  }

  public add(facilityUID: string, treatmentArea: FacilityTreatmentAreaForAddModel): Observable<FacilityTreatmentAreaForEditModel> {
    return this.treatmentAreasApi.add(facilityUID, treatmentArea);
  }

  public edit(facilityUID: string, treatmentArea: FacilityTreatmentAreaForEditModel): Observable<FacilityTreatmentAreaForEditModel> {
    return this.treatmentAreasApi.edit(facilityUID, treatmentArea);
  }

  public remove(facilityUID: string, treatmentAreaUID: string): Observable<boolean> {
    return this.treatmentAreasApi.remove(facilityUID, treatmentAreaUID);
  }
}
