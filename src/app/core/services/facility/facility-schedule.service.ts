import { Injectable } from '@angular/core';
import { FacilityScheduleApiService } from '../../api/facility/facility-schedule-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import * as AspNetData from "devextreme-aspnet-data-nojquery";
import CustomStore from "devextreme/data/custom_store";

import { ByroomForViewModel } from '../../../shared/models/appointment/byroom-for-view.model';
import { TherapistForViewModel } from '../../../shared/models/appointment/therapist-for-view.model';
import { AvailabilityForAppointmentModel } from '../../../shared/models/appointment/availability-for-appointment.model';
import { error } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class FacilityScheduleService {

  private $therapistData: BehaviorSubject<TherapistForViewModel | null> = new BehaviorSubject<TherapistForViewModel>(null);
  private $byroomData: BehaviorSubject<ByroomForViewModel | null> = new BehaviorSubject<ByroomForViewModel>(null);

  private $therapists: BehaviorSubject<AvailabilityForAppointmentModel[] | null> = new BehaviorSubject<AvailabilityForAppointmentModel[]>(null);
  private $treatmentAreas: BehaviorSubject<AvailabilityForAppointmentModel[] | null> = new BehaviorSubject<AvailabilityForAppointmentModel[]>(null); 

  constructor(private api: FacilityScheduleApiService) { }

  public getByRoom(url: string, loadOptions: any) {
    
    return this.api.getByRoom(url, loadOptions);



    //return new Promise((resolve, reject) => {
    //  this.api.getByRoom(url, loadOptions)
    //    .toPromise()
    //    .then((res: any) => {
    //      console.log('then', res);
    //      this.$treatmentAreas.next(res.TreatmentAreas);
    //      return res.data;
    //    }, (error) => {
    //        reject(error);
    //    })
    //  resolve();
    //});
    
    
  }


  //getPosts1(url: string, loadOptions: any) {
  //  const promise = new Promise((resolve, reject) => {
  //    this.api.getByRoom(url, loadOptions)
  //      .toPromise()
  //      .then((res: any) => {
  //        // Success
  //        this.data = res.map((res: any) => {
  //          return new Post(
  //            res.userId,
  //            res.id,
  //            res.title,
  //            res.body
  //          );
  //        });
  //        resolve();
  //      },
  //        err => {
  //          // Error
  //          reject(err);
  //        }
  //      );
  //  });
  //  return promise;
  //}

  //getPosts() {
  //  const promise = new Promise((resolve, reject) => {
  //    const apiURL = this.api;
  //    this.http
  //      .get<Post[]>(apiURL)
  //      .toPromise()
  //      .then((res: any) => {
  //        // Success
  //        this.data = res.map((res: any) => {
  //          return new Post(
  //            res.userId,
  //            res.id,
  //            res.title,
  //            res.body
  //          );
  //        });
  //        resolve();
  //      },
  //        err => {
  //          // Error
  //          reject(err);
  //        }
  //      );
  //  });
  //  return promise;
  //}

  public getTreatmentAreas(): Observable<AvailabilityForAppointmentModel[]> {
    return this.$treatmentAreas.asObservable();
  }

  public getByTherapist(url: string, loadOptions: any): Observable<TherapistForViewModel> {
    return this.api.getByTherapist(url, loadOptions);
    //this.api.getByTherapist(url, loadOptions)
    //  .subscribe(resp => {
    //    this.$therapists.next(resp.Therapists);
    //    this.$therapistData.next(resp);
    //  })
    //return this.$therapistData.asObservable();
  }

  public getTherapists(): Observable<AvailabilityForAppointmentModel[]> {
    return this.$therapists.asObservable();
  }

}
