import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FacilityAmenityForAdminModel } from '../../../shared/models/facility/facility-amenity-for-admin.model';
import { FacilityAmenitiesApiService } from '../../api/facility/facility-amenities-api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityAmenitiesService {

  constructor(private api: FacilityAmenitiesApiService) { }

  public getFacilityAmenities(facilityUid: string): Observable<FacilityAmenityForAdminModel[]> {
    return this.api.getFacilityAmenities(facilityUid);
  }

  public updateFacilityAmenities(facilityUid: string, amenities: FacilityAmenityForAdminModel): Observable<FacilityAmenityForAdminModel> {
    return this.api.updateFacilityAmenities(facilityUid, amenities);
  }

}
