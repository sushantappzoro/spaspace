import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';
import { map } from 'rxjs/operators';
import { FacilitySummaryModel } from '../../../shared/models/facility/facility-summary.model';

export interface StoreState {
    facilitySummary: FacilitySummaryByLocationObject;
}

class FacilitySummaryByLocationObject {
    longitude: number;
    latitude: number;
    summary: FacilitySummaryModel[];
}

@Injectable({
  providedIn: 'root'
})
export class FacilitySearchService extends ObservableStore<StoreState>  {

    constructor(private http: HttpClient) {
        super({ trackStateHistory: false, logStateChanges: false });
    }

    public getFacilitiesForLocation(latitude: number, longitude: number) {
        return this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Facilities/GetFacilitiesForLocation/' + latitude + "/" + longitude);
        //const state = this.getState();

        //if (state && state.facilitySummary && state.facilitySummary.latitude === latitude && state.facilitySummary.longitude === longitude) {
        //    return of(state.facilitySummary.summary);
        //} else {
        //    //return this.callService(latitude, longitude);

        //    this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Facilities/GetFacilitiesForLocation/' + latitude + "/" + longitude)
        //        .pipe(map(u => u && u.Result))
        //        .subscribe(res => {
        //            var facilitySummary: FacilitySummaryByLocationObject = new FacilitySummaryByLocationObject();
        //            facilitySummary.latitude = latitude;
        //            facilitySummary.longitude = longitude;
        //            facilitySummary.summary = res;

        //            this.setState({ facilitySummary }, FacilitySummaryStoreActions.searchByLocation);
        //            console.log('FacilitySearchService-after state set');
        //            return of(res);
        //        })
        //}
    }

    //callService(lat: number, long: number) {
    //    return this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Facilities/GetFacilitiesForLocation/' + lat + "/" + long)
    //        .pipe(map(u => u && u.Result))
    //        .subscribe(res => {
    //            var facilitySummary: FacilitySummaryByLocationObject = new FacilitySummaryByLocationObject();
    //            facilitySummary.latitude = lat;
    //            facilitySummary.longitude = long;
    //            facilitySummary.summary = res;

    //            this.setState({ facilitySummary }, FacilitySummaryStoreActions.searchByLocation);
    //            console.log('FacilitySearchService-after state set');
    //            return facilitySummary.summary;
    //        })
    //}

}


export enum FacilitySummaryStoreActions {
    searchByLocation = 'search-by-location'
}
