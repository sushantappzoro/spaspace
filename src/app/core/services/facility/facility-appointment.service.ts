import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { configSettings } from '../../../app.config';
import { AppointmentForFacilityDetailModel } from '../../../shared/models/appointment/appointment-for-facility-detail.model';
import { FacilityAppointmentApiService } from '../../api/facility/facility-appointment-api.service';
import { AppointmentCancelByFacilityRequestModel } from '../../../shared/models/appointment/appointment-cancel-by-facility-request.model';
import { AppointmentChangeRequestModel } from '../../../shared/models/appointment/appointment-change-request.model';
import { AppointmentForFacilityListModel } from '../../../shared/models/appointment/appointment-for-facility-list.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityAppointmentService {

  constructor(private api: FacilityAppointmentApiService) { }

  public getAppointments(facilityUid: string): Observable<AppointmentForFacilityListModel[]> {
    return this.api.getAppointments(facilityUid);
  }

  public getAppointmentDetail(facilityUid: string, appointmentUID: string): Observable<AppointmentForFacilityDetailModel> {
    return this.api.getAppointmentDetail(facilityUid, appointmentUID);
  }

  public startCancel(facilityUID: string, appointmentUID: string, cancelRequest: AppointmentCancelByFacilityRequestModel): Observable<AppointmentCancelByFacilityRequestModel> {
    return this.api.startCancel(facilityUID, appointmentUID, cancelRequest);
  }

  public finishCancel(facilityUID: string, appointmentUID: string, cancelRequest: AppointmentCancelByFacilityRequestModel): Observable<any> {
    return this.api.finishCancel(facilityUID, appointmentUID, cancelRequest);
  }

  public changeAppointment(facilityUID: string, appointmentUID: string, changeRequest: AppointmentChangeRequestModel): Observable<AppointmentForFacilityDetailModel> {
    return this.api.changeAppointment(facilityUID, appointmentUID, changeRequest);
  }

}
