import { Injectable } from '@angular/core';
import { FacilityManagerForAdminModel } from '../../../shared/models/facility/facility-manager-for-admin.model';
import { Observable } from 'rxjs';
import { FacilityManagerApiService } from '../../api/facility/facility-manager-api.service';
import { FacilityForAdminListModel } from '../../../shared/models/facility/facility-for-admin-list.model';
import { FacilityManagerModel } from '../../../shared/models/facility/facility-manager.model';
import { FacilityManagerForEditModel } from '../../../shared/models/facility/facility-manager-for-edit.model';
import { FacilityManagerForAdminAddModel } from '../../../shared/models/facility/facility-manager-for-admin-add.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityManagerService {

  constructor(private api: FacilityManagerApiService) { }

  public getFacilityManager(facilityManagerUID: string): Observable<FacilityManagerForAdminModel> {
    return this.api.getFacilityManager(facilityManagerUID);
  }

  public getManagedFacilities(facilityManagerUID: string): Observable<FacilityForAdminListModel[]> {
    return this.api.getManagedFacilities(facilityManagerUID);
  }

  public getFacilityManagerForFacility(facilityUID: string, facilityManagerUID: string): Observable<FacilityManagerForAdminModel> {
    return this.api.getFacilityManagerForFacility(facilityUID, facilityManagerUID);
  }

  public add(facilityUID: string, facilityManager: FacilityManagerForAdminAddModel): Observable<FacilityManagerForAdminModel> {
    return this.api.add(facilityUID, facilityManager);
  }

  public edit(facilityUID: string, facilityManager: FacilityManagerForEditModel): Observable<FacilityManagerForEditModel> {
    return this.api.edit(facilityUID, facilityManager);
  }

  public remove(facilityUID: string, facilityManagerUID: string): Observable<any> {
    return this.api.remove(facilityUID, facilityManagerUID);
  }

  public getFacilityManagers(facilityUID: string): Observable<FacilityManagerModel[]> {
    return this.api.getFacilityManagers(facilityUID);
  }

}
