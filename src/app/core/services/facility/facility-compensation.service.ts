import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FacilityServiceCompensationModel } from '../../../shared/models/facility/facility-service-compensation.model';
import { FacilityCompensationApiService } from '../../api/facility/facility-compensation-api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityCompensationService {

  constructor(private api: FacilityCompensationApiService) { }

  public getServiceCompensations(facilityUID: string): Observable<FacilityServiceCompensationModel[]> {
    return this.api.getServiceCompensations(facilityUID);
  }

  public getServiceCompensation(facilityUID: string, compensationUID: string): Observable<FacilityServiceCompensationModel> {
    return this.api.getServiceCompensation(facilityUID, compensationUID);
  }

  public add(facilityUID: string, compensation: FacilityServiceCompensationModel): Observable<FacilityServiceCompensationModel> {
    return this.api.add(facilityUID, compensation);
  };

  public edit(facilityUID: string, treatmentArea: FacilityServiceCompensationModel): Observable<FacilityServiceCompensationModel> {
    return this.api.edit(facilityUID, treatmentArea);
  };

  public remove(facilityUID: string, treatmentAreaUID: string): Observable<boolean> {
    return this.api.remove(facilityUID, treatmentAreaUID);
  };

}
