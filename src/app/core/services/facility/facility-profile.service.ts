import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FacilityProfileForViewModel } from '../../../shared/models/facility/facility-profile-for-view.model';
import { FacilityAmenityForViewModel } from '../../../shared/models/facility/facility-amenity-for-view.model';
import { TherapistForProfileViewModel } from '../../../shared/models/therapist/therapist-for-profile-view.model';
import { FacilityPhotoForAdminModel } from '../../../shared/models/facility/facility-photo-for-admin.model';
import { FacilityProfileApiService } from '../../api/facility/facility-profile-api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityProfileService {

  constructor(private api: FacilityProfileApiService) { }

  public getProfile(facilityUID: string): Observable<FacilityProfileForViewModel> {
    return this.api.getProfile(facilityUID);
  }

  public getAmenities(facilityUID: string): Observable<FacilityAmenityForViewModel[]> {
    return this.api.getAmenities(facilityUID);
  }

  public getFeaturedTherapists(facilityUID: string): Observable<TherapistForProfileViewModel[]> {
    return this.api.getFeaturedTherapists(facilityUID);
  }

  public getPhotos(facilityUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.api.getPhotos(facilityUID);
  }

}
