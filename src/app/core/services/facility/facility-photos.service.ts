import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FacilityPhotoForAdminModel } from '../../../shared/models/facility/facility-photo-for-admin.model';
import { FacilityPhotosApiService } from '../../api/facility/facility-photos-api.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityPhotosService {

  constructor(private api: FacilityPhotosApiService) { }

  public getFacilityPhoto(facilityUID: string, photoUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.api.getFacilityPhoto(facilityUID, photoUID);
  }

  public getFacilityPhotos(facilityUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.api.getFacilityPhotos(facilityUID);
  }

  public removeFacilityPhoto(facilityUID: string, photoUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.api.removeFacilityPhoto(facilityUID, photoUID);
  }

  public setMainFacilityPhoto(facilityUID: string, photoUID: string): Observable<FacilityPhotoForAdminModel[]> {
    return this.api.setMainFacilityPhoto(facilityUID, photoUID);
  }

}
