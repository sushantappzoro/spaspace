import { Injectable } from '@angular/core';
import { FacilityTherapistInfoApiService } from '../../api/facility/facility-therapist-info-api.service';
import { Observable } from 'rxjs';
import { TherapistInfoForAdminModel } from '../../../shared/models/facility/therapist-info-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTherapistInfoService {

  constructor(private api: FacilityTherapistInfoApiService) { }

  public getTherapistInfo(facilityUid: string): Observable<TherapistInfoForAdminModel> {
    return this.api.getTherapistInfo(facilityUid);
  }

  public editTherapistInfo(facilityUid: string, therapistInfo: TherapistInfoForAdminModel): Observable<TherapistInfoForAdminModel> {
    return this.api.editTherapistInfo(facilityUid, therapistInfo);
  }

}
