import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FacilityDashboardApiService } from '../../api/facility/facility-dashboard-api.service';
import { FacilityManagerForAdminModel } from '../../../shared/models/facility/facility-manager-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityDashboardService {

  constructor(private api: FacilityDashboardApiService) { }

  public getPendingFacilityManagers(facilityManagerUid: string): Observable<FacilityManagerForAdminModel[]> {
    return this.api.getPendingFacilityManagers(facilityManagerUid);
  }

}
