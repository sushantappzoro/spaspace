import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ApiResponseModel } from '../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../app.config';
import { ServiceCategoryModel } from '../../shared/models/offerings/service-category.model';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

    constructor(private http: HttpClient) { }

    public getServices(): Observable<ServiceCategoryModel[]> {
      return this.http.get<ServiceCategoryModel[]>(configSettings.WebAPI_URL + '/public/services');
    }

    //public getService(code: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.services.filter(x => (x.Code == code))
    //    }]

    //    return from(apiResponse);
    //}

    //public getServices(code: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.services.filter(x => (x.Code == code))
    //    }]

    //    return from(apiResponse);
    //}

    //public getChildServices(parent: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.services.filter(x => (x.ParentService == parent))
    //    }]

    //    return from(apiResponse);
    //}

    //public getOptions(serviceCode: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.options.filter(x => (x.ServiceCode == serviceCode))
    //    }]

    //    return from(apiResponse);
    //}


    //public getAddons(service: string): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/' + facilityId');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.addons.find(x => (x.s == parent))
    //    }]

    //    return from(apiResponse);
    //}


    //public saveProfile(profile: FacilityProfile): Observable<ApiResponseModel> {
    //    return this.http.post<any>(configSettings.WebAPI_URL + '/Facilities/Profile', profile);
    //}

    //public updateProfile(profile: FacilityModel): Observable<ApiResponseModel> {
    //    return this.http.put<any>(configSettings.Auth_URL + '/Facilities/Profile/' + profile.Uid, profile);
    //}

    //// Resource Type Services
    //public getResourceTypes(): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/Resources');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.resourceTypes
    //    }]

    //    return from(apiResponse);
    //}


    //// Resource Services
    //public getResources(): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/Resources');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.resources
    //    }]

    //    return from(apiResponse);
    //}

    //public getResourceAvailability(): Observable<ApiResponseModel> {
    //    //return this.http.get<any>(configSettings.WebAPI_URL + '/Facilities/Resources');

    //    let apiResponse: ApiResponseModel[] = [{
    //        ErrorResult: '',
    //        Errors: [],
    //        Message: '',
    //        StatusCode: 200,
    //        Result: this.availabilitiy
    //    }]

    //    return from(apiResponse);
    //}

    //public getResource(resourceId: string): Observable<ApiResponseModel> {
    //    return this.http.get<any>(configSettings.Auth_URL + '/Facilities/Resources/' + resourceId);
    //}

    //public saveResource(resource: ResourceModel): Observable<ApiResponseModel> {
    //    return this.http.post<any>(configSettings.WebAPI_URL + '/Facilities/Resources', resource);
    //}

    //public UpdateResource(resource: ResourceModel): Observable<ApiResponseModel> {
    //    return this.http.put<any>(configSettings.Auth_URL + '/Facilities/Resources/' + resource.Uid, resource);
    //}

    //public deleteResource(resourceId: string): Observable<ApiResponseModel> {
    //    return this.http.get<any>(configSettings.Auth_URL + '/Facilities/Resources/' + resourceId);
    //}
    //services: ServiceModel[] = [
    //    {
    //        Uid: '1',
    //        Name: 'Massage',
    //        Description: 'Massage',
    //        Code: 'MS',
    //        //CategoryUid: '1',
    //        ParentService: '',
    //        BasePrice: 50,
    //        HasChildren: true
    //    },
    //    {
    //        Uid: '2',
    //        Name: 'Facial',
    //        Description: 'Different Massage',
    //        Code: 'FC',
    //        //CategoryUid: '1',
    //        ParentService: '',
    //        BasePrice: 50,
    //        HasChildren: false
    //    },
    //    {
    //        Uid: '3',
    //        Name: 'Swedish Massage',
    //        Description: 'Swedish Massage',
    //        Code: 'MSSW',
    //        //CategoryUid: '1',
    //        ParentService: 'MS',
    //        BasePrice: 50,
    //        HasChildren: false
    //    },
    //    {
    //        Uid: '4',
    //        Name: 'Sports Massage',
    //        Description: 'Sports Massage',
    //        Code: 'MSSP',
    //        //CategoryUid: '1',
    //        ParentService: 'MS',
    //        BasePrice: 50,
    //        HasChildren: false
    //    },
    //    {
    //        Uid: '5',
    //        Name: 'Deep Tissue Massage',
    //        Description: 'Deep Tissue Massage',
    //        Code: 'MSDT',
    //        //CategoryUid: '1',
    //        ParentService: 'MS',
    //        BasePrice: 50,
    //        HasChildren: false
    //    }
    //]

    //addons: ServiceAddonModel[] = [];

    //options: ServiceOptionModel[] = [
    //    {
    //        Code: 'A',
    //        Description: 'Option A',
    //        Name: 'Option A',
    //        Price: 10,
    //        Uid: '1',
    //        ServiceCode: 'MSSW'
    //    },
    //    {
    //        Code: 'B',
    //        Description: 'Option B',
    //        Name: 'Option B',
    //        Price: 10,
    //        Uid: '2',
    //        ServiceCode: 'MSSW'
    //    },
    //    {
    //        Code: 'C',
    //        Description: 'Option C',
    //        Name: 'Option C',
    //        Price: 10,
    //        Uid: '3',
    //        ServiceCode: 'MSSW'
    //    }
    //];
}
