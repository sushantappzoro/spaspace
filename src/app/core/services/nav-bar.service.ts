import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {

  private _navItems: BehaviorSubject<NavBarItemModel[] | null> = new BehaviorSubject(null);
  private navItems$ = this._navItems.asObservable();

  private _toggle = new Subject();
  private toggle$ = this._toggle.asObservable();

  constructor() { }

  toggle() {
    return this.toggle$;
  }

  toggleNext() {
    this._toggle.next();
  }

  setNavigationItems(navItems: NavBarItemModel[]) {
    console.log('setNavigationItems', navItems)
    this._navItems.next(navItems);
  }

  getNavigationItems() {
    return this.navItems$;
  }

}

export class NavBarItemModel {
  id: number;
  text: string;
  icon: string;
  status?: string[];
  disabled?: boolean = false;
  visible?: boolean = true;
  link?: string;
}
