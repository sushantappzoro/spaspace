import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { UserProfileForViewEditModel } from '../../../shared/models/user/user-profile-for-view-edit.model';
import { UsersApiService } from '../../api/user/users-api.service';
import { PasswordChangeRequest } from 'src/app/shared/models/user/password-change-request.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private api: UsersApiService) { }

  public getProfile(userUid: string): Observable<UserProfileForViewEditModel> {
    return this.api.getProfile(userUid);
  }

  public editProfile(userUid: string, userProfile: UserProfileForViewEditModel): Observable<UserProfileForViewEditModel> {
    return this.api.editProfile(userUid, userProfile);
  }

  public updatePassword(passwordChangeRequest: PasswordChangeRequest): Observable<boolean> {
    return this.api.updatePassword(passwordChangeRequest);
  }
}
