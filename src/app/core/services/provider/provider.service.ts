import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';
import { TherapistRegistrationModel } from '../../../shared/models/therapist/therapist-registration.model';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

    constructor(private http: HttpClient) { }

    public getProviders(): Observable<ApiResponseModel> {
        return this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Therapist');
    }

  public registerProvider(provider: TherapistRegistrationModel) {
    return this.http.post<any>(configSettings.WebAPI_URL + '/Registration/RegisterAsTherapist', JSON.stringify(provider));
  };

}
