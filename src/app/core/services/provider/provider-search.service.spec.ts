import { TestBed } from '@angular/core/testing';

import { ProviderSearchService } from './provider-search.service';

describe('ProviderSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProviderSearchService = TestBed.get(ProviderSearchService);
    expect(service).toBeTruthy();
  });
});
