import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { ApiResponseModel } from '../../../shared/interfaces/api/apiResponse.model';
import { configSettings } from '../../../app.config';

@Injectable({
  providedIn: 'root'
})
export class ProviderSearchService {

    constructor(private http: HttpClient) { }


    public getTherapistsForLocation(latitude: number, longitude: number) {
        return this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Therapist/GetTherapistsForLocation/' + latitude + "/" + longitude);
        //const state = this.getState();

        //if (state && state.facilitySummary && state.facilitySummary.latitude === latitude && state.facilitySummary.longitude === longitude) {
        //    return of(state.facilitySummary.summary);
        //} else {
        //    //return this.callService(latitude, longitude);

        //    this.http.get<ApiResponseModel>(configSettings.WebAPI_URL + '/Facilities/GetFacilitiesForLocation/' + latitude + "/" + longitude)
        //        .pipe(map(u => u && u.Result))
        //        .subscribe(res => {
        //            var facilitySummary: FacilitySummaryByLocationObject = new FacilitySummaryByLocationObject();
        //            facilitySummary.latitude = latitude;
        //            facilitySummary.longitude = longitude;
        //            facilitySummary.summary = res;

        //            this.setState({ facilitySummary }, FacilitySummaryStoreActions.searchByLocation);
        //            console.log('FacilitySearchService-after state set');
        //            return of(res);
        //        })            
        //}
    }
}
