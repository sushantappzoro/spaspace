import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';
import { UserProfileForViewEditModel } from '../../../shared/models/user/user-profile-for-view-edit.model';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { UsersService } from '../../services/user/users.service';


@Injectable({
  providedIn: 'root'
})
export class UserProfileResolver implements Resolve<UserProfileForViewEditModel> {

  constructor(
    private authService: AuthorizeService,
    private userProfileService: UsersService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserProfileForViewEditModel> | Promise<UserProfileForViewEditModel> | UserProfileForViewEditModel {

    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.userProfileService.getProfile(user.sub).pipe(
            map(resp => resp),
            catchError(error => {
              console.log(error);
              this.router.navigate(['/home']);
              return of(null);
            })
          )
        })
      );
  }

}
