import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { TherapistFacilitiesApiService } from '../../api/therapist/therapist-facilities-api.service';
import { TherapistAssociatedFacilityModel } from '../../../shared/models/therapist/therapist-associated-facility.model';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TherapistAssociatedFacilitiesResolver implements Resolve<TherapistAssociatedFacilityModel[]> {

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistFacilitiesApiService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistAssociatedFacilityModel[]> | Observable<never> {


    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.therapistService.getAssociatedFacilities(user.sub).pipe(
            map(resp => resp),
            catchError(error => {

              if (error instanceof HttpErrorResponse) {
                if (error.status === 401) {
                  this.router.navigate(['/authentication/login']);
                }                
              }

              this.router.navigate(['./error']);
              return of(null);
            })
          )
        })
      );
  }

}
