import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { TherapistAppointmentsService } from '../../services/therapist/therapist-appointments.service';
import { TherapistAppointmentSummaryForViewModel } from '../../../shared/models/therapist/therapist-appointment-summary-for-view.model';
import { TherapistScheduleAndAppointmentsModel } from '../../../shared/models/therapist/therapist-schedule-and-appointments.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAppointmentsResolver implements Resolve<TherapistScheduleAndAppointmentsModel> {

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistAppointmentsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistScheduleAndAppointmentsModel> | Observable<never> {


    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.therapistService.getAllAppointments(user.sub).pipe(
            map(resp => resp),
            catchError(error => {
              console.log(error);
              this.router.navigate(['/home']);
              return of(null);
            })
          )
        })
      );
  }
}
