import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { TherapistProfileService } from '../../../core/services/therapist/therapist-profile.service';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { TherapistProfileForEditModel } from '../../../shared/models/therapist/therapist-profile-for-edit.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistResolverService implements Resolve<TherapistProfileForEditModel> {

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistProfileService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistProfileForEditModel> | Promise<TherapistProfileForEditModel> | TherapistProfileForEditModel{

    //return this.authService.getUser()
    //  .pipe(
    //    switchMap(user => {
    //      return this.therapistService.getTherapistProfile(user.sub)
    //        .pipe(
    //          map(ffff => {
    //            return ffff;
    //          })
    //        )            
    //    })
    //  );
    console.log('TherapistResolverService');
    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.therapistService.getTherapistProfile(user.sub).pipe(
            map(resp => resp),
            catchError(error => {
              console.log(error);
              this.router.navigate(['/home']);
              return of(null);
            })
          )
        })
      );

    //return this.therapistService.getTherapistProfile('4d4febee-6999-4f85-acae-c494a0b85c28').pipe(
    //  catchError(error => {
    //    console.log(error);
    //    this.router.navigate(['/home']);
    //    return of(null);
    //  })
    //)

    //return this.authService.getUser()
    //  .pipe(
    //    switchMap(user => {
    //      return this.therapistService.getTherapistProfile(user.sub)
    //        .pipe(
    //          mergeMap(response => {                
    //            if (response) {
    //              console.log('TherapistResolverService - got therapist profile', response);
    //              return of(response)
    //            } else {
    //              this.router.navigate(['/therapist/error']);
    //              return EMPTY;
    //            }
    //          })
    //        )
    //    })
    //  )
  }

}
