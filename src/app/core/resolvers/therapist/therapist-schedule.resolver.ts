import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY, forkJoin } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { TherapistAppointmentSummaryModel } from '../../../shared/models/therapist/therapist-appointment-summary.model';
import { TherapistScheduleService } from '../../services/therapist/therapist-schedule.service';

@Injectable({
  providedIn: 'root'
})
export class TherapistScheduleResolver implements Resolve<TherapistAppointmentSummaryModel[]>{

  constructor(
    private authService: AuthorizeService,
    private scheduleService: TherapistScheduleService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistAppointmentSummaryModel[]> | Observable<never> {

    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.scheduleService.getAppointments(user.sub).pipe(
            map(resp => resp),
            catchError(error => {
              console.log(error);
              this.router.navigate(['/home']);
              return of(null);
            })
          )
        })
      );
  }
}
