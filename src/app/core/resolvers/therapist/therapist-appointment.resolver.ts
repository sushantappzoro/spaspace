import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { TherapistAppointmentsService } from '../../services/therapist/therapist-appointments.service';
import { AppointmentForTherapistDetailModel } from '../../../shared/models/therapist/appointment-for-therapist-detail.model';

@Injectable({
  providedIn: 'root'
})
export class TherapistAppointmentResolver implements Resolve<AppointmentForTherapistDetailModel> {

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistAppointmentsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AppointmentForTherapistDetailModel> | Observable<never> {


    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.therapistService.getAppointmentDetail(user.sub, route.params['uid']).pipe(
            map(resp => resp),
            catchError(error => {
              console.log(error);
              this.router.navigate(['/home']);
              return of(null);
            })
          )
        })
      );
  }

}
