import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY, forkJoin } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { TherapistAvailabilityService } from '../../services/therapist/therapist-availability.service';
import { TherapistAvailabilityForViewModel } from '../../../shared/models/therapist/therapist-availability-for-view.model';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TherapistAvailabilityResolver implements Resolve<TherapistAvailabilityForViewModel[]> {

  constructor(
    private authService: AuthorizeService,
    private availabilityService: TherapistAvailabilityService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TherapistAvailabilityForViewModel[]> | Observable<never> {

    return this.authService.getUser()
      .pipe(
        take(1),
        switchMap(user => {
          return this.availabilityService.getAvailability(user.sub).pipe(
            map(resp => resp),
            catchError(error => {
              if (error instanceof HttpErrorResponse) {
                if (error.status === 401) {
                  this.router.navigate(['/authentication/login']);
                }
              }

              this.router.navigate(['./error']);
              return of(null);
            })
          )
        })
    );
  }
}
