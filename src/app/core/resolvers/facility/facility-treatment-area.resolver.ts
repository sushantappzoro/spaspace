import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityTreatmentAreaViewModel } from "../../../shared/models/facility/facility-treatment-area-view.model";
import { FacilityTreatmentAreasService } from "../../services/facility/facility-treatment-areas.service";

@Injectable({
  providedIn: 'root'
})
export class FacilityTreatmentAreaResolver implements Resolve<FacilityTreatmentAreaViewModel> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private treatmentAreaService: FacilityTreatmentAreasService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityTreatmentAreaViewModel> {
    //this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
    console.log(route.params)
    var facilityUID = route.params['facilityUID'];
    var uid = route.params['uid'];

    return this.treatmentAreaService.getTreatmentArea(facilityUID, uid).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
