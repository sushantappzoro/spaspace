import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { FacilityForListModel } from "../../../shared/models/facility/facility-for-list.model";
import { FacilityService } from "../../services/facility/facility.service";
import { AdminErrors } from "../../../admin/core/services/admin-error.service";

@Injectable({
  providedIn: 'root'
})
export class FacilityListResolver implements Resolve<FacilityForListModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facilityService: FacilityService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityForListModel[]> {
    return this.facilityService.get().pipe(
      catchError(error => {
        console.log(error);
        console.log('FacilityListResolver', this.route);
        //this.router.navigate(['../error']);
        this.router.navigate(['./admin/error/' + AdminErrors.adminFaciltiesListError], { relativeTo: this.route });
        return of(null);
      })
    );
  }

}
