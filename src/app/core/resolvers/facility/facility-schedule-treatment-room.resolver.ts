import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { FacilityScheduleService } from '../../services/facility/facility-schedule.service';
import { ByroomForViewModel } from '../../../shared/models/appointment/byroom-for-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityScheduleTreatmentRoomResolver implements Resolve<ByroomForViewModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: FacilityScheduleService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ByroomForViewModel[]> {
    //this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
    console.log(route.params)
    var facilityUID = route.params['facilityUID'];

    return this.service.getByRoom(facilityUID).pipe(
      catchError(error => {
        console.log(error);
        return of(null);
      })
    );
  }

}
