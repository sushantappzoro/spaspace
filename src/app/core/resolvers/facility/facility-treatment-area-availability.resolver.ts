import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityAvailabilityService } from '../../services/facility/facility-availability.service';
import { TreatmentAreaAvailabilityForViewModel } from '../../../shared/models/facility/treatment-area-availability-for-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityTreatmentAreaAvailabilityResolver implements Resolve<TreatmentAreaAvailabilityForViewModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private availabilityService: FacilityAvailabilityService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<TreatmentAreaAvailabilityForViewModel[]> {
    return this.availabilityService.getTreatmentAreaAvailabilities(route.params['facilityUID'],route.params['uid']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
