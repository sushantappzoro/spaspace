import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

import { FacilityScheduleService } from '../../services/facility/facility-schedule.service';
import { TherapistForViewModel } from '../../../shared/models/appointment/therapist-for-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityScheduleTherapistResolver implements Resolve<TherapistForViewModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: FacilityScheduleService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<TherapistForViewModel[]> {
    //this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
    console.log(route.params)
    var facilityUID = route.params['facilityUID'];
    var date = route.params['date'];

    return this.service.getByTherapist(facilityUID, date).pipe(
      catchError(error => {
        console.log(error);
        return of(null);
      })
    );
  }

}
