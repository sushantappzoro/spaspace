import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { AppointmentForFacilityDetailModel } from '../../../shared/models/appointment/appointment-for-facility-detail.model';
import { FacilityAppointmentService } from '../../services/facility/facility-appointment.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityAppointmentResolver implements Resolve<AppointmentForFacilityDetailModel> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facilityAppointmentService: FacilityAppointmentService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<AppointmentForFacilityDetailModel> {
    return this.facilityAppointmentService.getAppointmentDetail(route.params['facilityUID'], route.params['appointmentUID']).pipe(
      catchError(error => {
        console.log(error);
        //his.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
