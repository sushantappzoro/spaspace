import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FacilityCoreModule } from './core/facility-core.module';
import { FacilitySharedModule } from './shared/facility-shared.module';
import { DevexModuleModule } from './devex-module.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ApiAuthorizationModule } from '../../api-authorization/api-authorization.module';
import { FacilitiesModule } from './facilities/facilities.module';
import { FacilityMgrProfileComponent } from './profile/facility-mgr-profile.component';
import { FacilityMgrDashboardComponent } from './dashboard/facility-mgr-dashboard.component';
import { FacilityMgrAdminComponent } from './admin/facility-mgr-admin.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { FacilityPendingManagersResolver } from './core/resolvers/facility-pending-managers.resolver';
import { FacilityDashboardService } from '../core/services/facility/facility-dashboard.service';
import { FacilityDashboardApiService } from '../core/api/facility/facility-dashboard-api.service';
import { FacilityManagerFacilitiesResolver } from './core/resolvers/facility-manager-facilities.resolver';
import { FacilityManagerResolver } from './core/resolvers/facility-manager.resolver';
import { FacilityManagerService } from '../core/services/facility/facility-manager.service';
import { FacilityManagerApiService } from '../core/api/facility/facility-manager-api.service';
import { FacilityManagerComponent } from './facility-manager.component';
import { FacilityManagerRoutingModule } from './facility-manager-routing.module';


@NgModule({
  declarations: [
    FacilityManagerComponent,
    FacilityMgrProfileComponent,
    FacilityMgrDashboardComponent,
    FacilityMgrAdminComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro,
    ApiAuthorizationModule,
    SharedModule,
    FacilityManagerRoutingModule,
    FacilityCoreModule,
    FacilitySharedModule,
    DevexModuleModule,
    FacilitiesModule
  ],
  providers: [
    // FacilityPendingManagersResolver,
    // FacilityDashboardService,
    // FacilityDashboardApiService,
    // FacilityManagerResolver,
    // FacilityManagerFacilitiesResolver,
    // FacilityManagerService,
    // FacilityManagerApiService
  ]
})
export class FacilityManagerModule { }
