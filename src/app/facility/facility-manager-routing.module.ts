import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacilityMgrProfileComponent } from './profile/facility-mgr-profile.component';
import { FacilityMgrAdminComponent } from './admin/facility-mgr-admin.component';
import { FacilityManagerFacilitiesResolver } from './core/resolvers/facility-manager-facilities.resolver';
import { FacilityManagerResolver } from './core/resolvers/facility-manager.resolver';
import { FacilityUserGuard } from './core/guards/facility-user.guard';
import { FacilityManagerComponent } from './facility-manager.component';
import { FacilitiesFacilityComponent } from './facilities/facility/facilities-facility.component';
import { FacilityTreatmentAreasResolver } from '../core/resolvers/facility/facility-treatment-areas.resolver';
import { FacilityListComponent } from './facilities/list/facility-list.component';
import { FacilityDashboardComponent } from './facilities/dashboard/facility-dashboard.component';
import { FacilitySchedulingComponent } from './facilities/scheduling/facility-scheduling.component';
import { FacilityProfileStepperComponent } from './facilities/profile/facility-profile-stepper.component';
import { FacilityProfileComponent } from './facilities/profile/facility-profile.component';
import { FacilityPreviewComponent } from './facilities/preview/facility-preview.component';
import { FacilityImagesComponent } from './facilities/images/facility-images.component';
import { FacilityTreatmentAreasComponent } from '../shared/components/facility/facility-treatment-areas/facility-treatment-areas.component';
import { FacilitiesComponent } from './facilities/facilities.component';
import { FacilityTreatmentAreaAvailabilityResolver } from '../core/resolvers/facility/facility-treatment-area-availability.resolver';
import { FacilityInfoResolver } from './core/resolvers/facility-info.resolver';
import { FacilityTreatmentAreaResolver } from '../core/resolvers/facility/facility-treatment-area.resolver';
import { UserProfileComponent } from '../shared/components/user/user-profile/user-profile.component';
import { UserProfileResolver } from '../core/resolvers/user/user-profile.resolver';
import { FacilityAppointmentResolver } from '../core/resolvers/facility/facility-appointment.resolver';
import { FacilityAppointmentsResolver } from './core/resolvers/facility-appointments.resolver';
import { FacilityAvailabilityComponent } from '../shared/components/facility/facility-availability/facility-availability.component';
import { FacilityAppointmentsViewComponent } from '../shared/components/facility/facility-appointments-view/facility-appointments-view.component';
import { FacilityAppointmentsListComponent } from '../shared/components/facility/facility-appointments-list/facility-appointments-list.component';
import { FacilityAppointmentDetailComponent } from '../shared/components/facility/facility-appointment-detail/facility-appointment-detail.component';


const routes: Routes = [
  {
    path: '',
    component: FacilityManagerComponent,
    canActivateChild: [FacilityUserGuard],
    children: [
      //{
      //  path: '',
      //  redirectTo: 'dashboard'
      //},
      {
        path: '',
        redirectTo: 'facilities', pathMatch: 'full'
      },
      {
        path: 'dashboard',
        redirectTo: 'facilities', pathMatch: 'full'
      },
      //{
      //  path: 'dashboard',
      //  component: FacilityMgrDashboardComponent,
      //  resolve: {
      //    facilityManagers: FacilityPendingManagersResolver
      //  },
      //  data: {
      //    breadcrumb: 'dashboard'
      //  },
      //},
      {
        path: 'profile',
        component: FacilityMgrProfileComponent,
        resolve: {
          facilityManager: FacilityManagerResolver,
          facilities: FacilityManagerFacilitiesResolver
        },
      },
      {
        path: 'userprofile',
        component: UserProfileComponent,
        resolve: {
          userProfile: UserProfileResolver
        },
        data: {
          breadcrumb: 'User Profile'
        }
      },
      {
        path: 'admin',
        component: FacilityMgrAdminComponent,
        data: {
          breadcrumb: 'Administrative'
        }
      },
      //{
      //  path: 'facilities',
      //  loadChildren: () => import('./facilities/facilities.module').then(m => m.FacilitiesModule),
      //  resolve: {
      //    facilities: FacilityManagerFacilitiesResolver
      //  },
      //  data: {
      //    breadcrumb: 'Facilities'
      //  }
      //},
      {
        path: 'facilities',
        component: FacilitiesComponent,
        data: {
          breadcrumb: 'Facilities'
        },
        children: [
          {
            path: '',
            component: FacilityListComponent,
            resolve: {
              facilities: FacilityManagerFacilitiesResolver
            }
          },
          {
            path: 'list',
            component: FacilityListComponent,
            resolve: {
              facilities: FacilityManagerFacilitiesResolver
            },
            data: {
              breadcrumb: 'Facilities'
            }
          },
          {
            path: 'dashboard',
            component: FacilityDashboardComponent,
            //resolve: {
            //  facilityManagers: FacilityPendingManagersResolver
            //},
            data: {
              breadcrumb: 'Dashboard'
            }
          },
          {
            path: 'images',
            component: FacilityImagesComponent,
            data: {
              breadcrumb: 'Images'
            }
          },
          {
            path: 'preview/:uid',
            component: FacilityPreviewComponent,
            //resolve: FacilityInfoResolver,
            data: {
              breadcrumb: 'Preview'
            }
          },
          {
            path: 'profile',
            component: FacilityProfileComponent,
            data: {
              breadcrumb: 'Profile'
            }
          },
          {
            path: 'profile/:uid',
            component: FacilityProfileComponent,
            //resolve: FacilityInfoResolver,
            data: {
              breadcrumb: 'Profile'
            }
          },
          {
            path: 'profilestepper/:uid',
            component: FacilityProfileStepperComponent,
            //resolve: FacilityInfoResolver,
            data: {
              breadcrumb: 'Profile'
            }
          },
          //{
          //  path: 'resources',
          //  component: FacilityResourcesComponent
          //},
          {
            path: 'scheduling/:uid',
            component: FacilitySchedulingComponent,
            resolve: { treatmentAreas: FacilityTreatmentAreasResolver },
            data: {
              breadcrumb: 'Scheduling'
            }
          },
          {
            path: ':facilityUID/appointments',
            component: FacilityAppointmentsViewComponent,
          },
          {
            path: ':facilityUID/appointments/list',
            component: FacilityAppointmentsListComponent,
            resolve: {
              appointments: FacilityAppointmentsResolver
            },
          },
          {
            path: ':facilityUID/appointments/:appointmentUID/detail',
            component: FacilityAppointmentDetailComponent,
            resolve: {
              appointment: FacilityAppointmentResolver
            },
          },
          {
            path: ':facilityUID/treatmentareas',
            component: FacilityTreatmentAreasComponent,
            resolve: {
              treatmentAreas: FacilityTreatmentAreasResolver,
              facility: FacilityInfoResolver
            },
          },
          {
            path: ':facilityUID/treatmentareas/:uid/availability',
            component: FacilityAvailabilityComponent,
            resolve: {
              availability: FacilityTreatmentAreaAvailabilityResolver,
              treatmentArea: FacilityTreatmentAreaResolver
            },
          },
          //{
          //  path: '**',
          //  redirectTo: 'dashboard', pathMatch: 'full'
          //},
        ]
      },
      {
        path: 'facilities/:facilityUID',
        component: FacilitiesFacilityComponent,
        children: [
          {
            path: 'treatmentareas',
            component: FacilityTreatmentAreasComponent,
            resolve: {
              treatmentAreas: FacilityTreatmentAreasResolver
            },
          }
        ]
      },
      //{
      //  path: '', redirectTo: 'dashboard', pathMatch: 'full'
      //}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacilityManagerRoutingModule { }
