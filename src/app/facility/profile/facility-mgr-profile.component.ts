import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

import { FacilityManagerForAdminModel, FacilityManagerStatus } from '../../shared/models/facility/facility-manager-for-admin.model';
import { ActivatedRoute } from '@angular/router';
import { FacilityForAdminListModel } from '../../shared/models/facility/facility-for-admin-list.model';

@Component({
  selector: 'app-facility-mgr-profile',
  templateUrl: './facility-mgr-profile.component.html',
  styleUrls: ['./facility-mgr-profile.component.scss']
})
export class FacilityMgrProfileComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facilityUID: string;

  facilityManager: FacilityManagerForAdminModel;

  canEdit: boolean;
  showSaveButton: boolean;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilityManager: FacilityManagerForAdminModel }) => {
        this.facilityManager = data.facilityManager;

        this.setStatus();
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.route.data
      .subscribe((data: { facilities: FacilityForAdminListModel[] }) => {
        if (data.facilities && data.facilities.length > 0) {
          this.facilityUID = data.facilities[0].UID;
        }

      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  setStatus() {

    switch (this.facilityManager.Status) {

      case FacilityManagerStatus.approved:
      case FacilityManagerStatus.new:
        this.canEdit = true;
        this.showSaveButton = true;
        break;
      case FacilityManagerStatus.pending:
        this.canEdit = false;
        this.showSaveButton = false;
        break;
      default:
    }

  }
}
