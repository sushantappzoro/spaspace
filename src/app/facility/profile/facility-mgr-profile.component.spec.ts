import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityMgrProfileComponent } from './facility-mgr-profile.component';

describe('FacilityMgrProfileComponent', () => {
  let component: FacilityMgrProfileComponent;
  let fixture: ComponentFixture<FacilityMgrProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityMgrProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityMgrProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
