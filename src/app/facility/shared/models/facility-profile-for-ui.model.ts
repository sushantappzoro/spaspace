export class FacilityProfileForUiModel {
  public Name: string;
  public Description: string;
  //public RegionUID: string;
  //public RegionName: string;
  public Address1: string;
  public Address2: string;
  public City: string;
  public StateCode: string;
  public ZipCode: string;
  public URL: string;
}
