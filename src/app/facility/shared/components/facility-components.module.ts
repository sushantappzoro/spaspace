import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { FacilityMaterialModule } from '../facility-material.module';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DxDateBoxModule } from 'devextreme-angular'

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    FacilityMaterialModule,
    MDBBootstrapModulesPro,
    DxDateBoxModule
  ],
  exports: [

  ]
})
export class FacilityComponentsModule { }
