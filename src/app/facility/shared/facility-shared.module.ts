import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FacilityComponentsModule } from './components/facility-components.module';
import { FacilityMaterialModule } from './facility-material.module';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FacilityComponentsModule,
    FacilityMaterialModule,
    MDBBootstrapModulesPro
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    FacilityComponentsModule
  ]
})
export class FacilitySharedModule { }
