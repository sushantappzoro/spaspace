import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityMgrAdminComponent } from './facility-mgr-admin.component';

describe('FacilityMgrAdminComponent', () => {
  let component: FacilityMgrAdminComponent;
  let fixture: ComponentFixture<FacilityMgrAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityMgrAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityMgrAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
