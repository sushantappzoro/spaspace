import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import themes from "devextreme/ui/themes";

import { MenuListItemModel } from '../shared/models/common/menu-list-item.model';
import { FacilityForListModel } from '../shared/models/facility/facility-for-list.model';
import { FacilityManagerForAdminModel, FacilityManagerStatus } from '../shared/models/facility/facility-manager-for-admin.model';
import { DxListComponent } from 'devextreme-angular';

@Component({
  selector: 'app-facility-manager',
  templateUrl: './facility-manager.component.html',
  styleUrls: ['./facility-manager.component.scss']
})
export class FacilityManagerComponent implements OnInit, OnDestroy {
  @ViewChild("menuList") menuList: DxListComponent;

  private subs = new SubSink();

  homePage = '/facility/facilities';

  facilityManager: FacilityManagerForAdminModel;

  showList: boolean = false;
  facilityName: string;

  selectedItems: any[] = [];

  navigation: MenuListItemModel[] = [];

  menuItems: NavBarItemModel[] = [
    //{ id: 1, text: "Dashboard", icon: "fas fa-home", link: "./dashboard", status: ['New', 'Pending Approval', 'Approved'], disabled: false },
    { id: 1, text: "Dashboard", icon: "fas fa-home", link: './dashboard', enabled: ['Approved'], show: ['New', 'Pending Approval', 'Approved'] },
    //{ id: 2, text: "Profile", icon: "fas fa-user", link: './profile', enabled: ['New', 'Pending Approval', 'Approved'], show: ['New', 'Pending Approval', 'Approved'] },
    { id: 3, text: "Facilities", icon: "fas fa-clock", link: './facilities', enabled: ['New', 'Approved'], show: ['New', 'Pending Approval', 'Approved'] }
    //{ id: 4, text: "Administrative", icon: "far fa-folder", link: './admin', enabled: ['Approved'], show: ['New', 'Pending Approval', 'Approved'] }
  ];


  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    themes.current("material.admin.light");
  }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilityManager: FacilityManagerForAdminModel }) => {
        this.facilityManager = data.facilityManager;
        this.buildMenu(this.facilityManager.Status);
        //this.navigation.forEach(st => {
        //  st.disabled = !st.status.includes(data.facilityManager.Status);
        //});

      },
        error => {
          console.log(error);
        }));

    //this.chooseMenuItem();

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  buildMenu(status: string) {
    this.navigation = [];

    this.menuItems.forEach(item => {
      if (item.show.includes(status)) {
        var newItem: MenuListItemModel = { icon: item.icon, id: item.id, text: item.text, disabled: !item.enabled.includes(status), link: item.link };
        this.navigation.push(newItem);
      }
    });
  }

  //chooseMenuItem() {

  //  if (this.facilityManager.Status = FacilityManagerStatus.new) {
  //    this.selectedItems = [3];
  //    this.router.navigate(['./facilities'], { relativeTo: this.route });
  //  }

  //  if (this.facilityManager.Status = FacilityManagerStatus.pending) {
  //    this.selectedItems = [2];
  //    this.router.navigate(['./profile'], { relativeTo: this.route });
  //  }

  //  //if (this.menuList && this.menuList.instance) {
  //  //  this.menuList.instance.selectItem(2);
  //  //}
  //}

  //menu_selectionChanged(e) {
  //  this.router.navigate([e.itemData.link], { relativeTo: this.route });
  //}
}

export class NavBarItemModel {
  id: number;
  text: string;
  icon: string;
  show?: string[];
  enabled?: string[];
  link?: string;
}
