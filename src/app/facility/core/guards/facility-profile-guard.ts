import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { take, mergeMap, switchMap, map } from 'rxjs/operators';

import { FacilityManagerService } from '../../../core/services/facility/facility-manager.service';
import { FacilityManagerStatus } from '../../../shared/models/facility/facility-manager-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityProfileGuard implements CanActivate{

  constructor(
    private authService: AuthorizeService,
    private facilityService: FacilityManagerService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const allowedStatus: any[] = next.data.allowedStatus;
    const url = state.url;

    let parentUrl = state.url.slice(0, state.url.indexOf(next.url[next.url.length - 1].path));

    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          return this.facilityService.getFacilityManager(user.sub)
            .pipe(take(1),
              switchMap(resp => {
                if (allowedStatus.includes(resp.Status)) {
                  return of(true);
                }

                if (url.indexOf("profile") > 0 && resp.Status === FacilityManagerStatus.new) {
                  this.router.navigate([parentUrl, 'stepper']);
                  return of(null);
                }

                if (url.indexOf("stepper") > 0) {
                  this.router.navigate([parentUrl, 'profile']);
                  return of(null);
                }

                this.router.navigate([parentUrl, 'error']);
                return of(null);
              })
            )
        }));

  }

}
