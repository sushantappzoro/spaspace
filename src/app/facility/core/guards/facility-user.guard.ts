import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute,
  CanActivateChild,
  UrlTree
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { FacilityManagerService } from '../../../core/services/facility/facility-manager.service';
import { FacilityManagerForAdminModel, FacilityManagerStatus } from '../../../shared/models/facility/facility-manager-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityUserGuard implements CanActivateChild {

  constructor(
    private authService: AuthorizeService,
    private facilityManagerService: FacilityManagerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


    //return true;

    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          return this.facilityManagerService.getFacilityManager(user.sub)
            .pipe(
              mergeMap(response => {
                if (response.Status == FacilityManagerStatus.pending || response.Status == FacilityManagerStatus.rejected) {
                  console.log('facility manager not authorized, status=' + response.Status);
                  this.router.navigate(['/facility/error']);
                  return EMPTY;                  
                } else {
                  return of(true);
                }
              })
            )
        })
      )





  }
  
}
