import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { FacilityForListModel } from '../../../shared/models/facility/facility-for-list.model';
import { FacilityFacilityApiService } from '../api/facility-facility-api.service';
import { ObservableStore } from '@codewithdan/observable-store';
import { FacilityForAuthEditModel } from '../../../shared/models/facility/facility-for-auth-edit.model';
import { map, catchError } from 'rxjs/operators';
import { FacilityForAuthViewModel } from '../../../shared/models/facility/facility-for-auth-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityFacilityService extends ObservableStore<FacilityState> {

  constructor(private facilityApi: FacilityFacilityApiService) {
    super({ logStateChanges: true, trackStateHistory: true });
  }

  public get(): Observable<FacilityForListModel[]> {
    if (this.getState() && this.getState().facilities) {
      const facilities = this.getState().facilities;
      return of(facilities);
    } else {
      return this.fetchFacilities();
    }
  }

  public getFacility(UID: string): Observable<FacilityForAuthViewModel> {

    if (this.getState() && this.getState().currentFacility && this.getState().currentFacility.UID == UID) {
      const facility = this.getState().currentFacility;
      return of(facility);
    } else {
      return this.fetchFacility(UID);
    }
  }

  public getCurrentFacility(): Observable<FacilityForAuthViewModel> {

    if (this.getState() && this.getState().currentFacility) {
      const facility = this.getState().currentFacility;
      return of(facility);
    } else {
      if (this.getState() && this.getState().facilities && this.getState().facilities.length > 0) {
        const facility = this.getState().facilities[0];
        return this.fetchFacility(facility.UID);
      } else {
        return of(null);
      }
    }
  }

  private fetchFacilities() {

    return this.facilityApi.get()
      .pipe(
        map(facilities => {
          this.setState({ facilities }, FacilityStoreActions.GetFacilities);
          return facilities;
        }),
        catchError(this.handleError)
      );
  }

  private fetchFacility(UID: string) {

    return this.facilityApi.getFacility(UID)
      .pipe(
        map(currentFacility => {
          this.setState({ currentFacility }, FacilityStoreActions.GetFacility);
          return currentFacility;
        }),
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }
}

export interface FacilityState {
  facilities: FacilityForListModel[];
  currentFacility: FacilityForAuthViewModel;
}

export enum FacilityStoreActions {
  GetFacilities = 'get_facilities',
  GetFacility = 'get_facility',
  SaveFacility = 'save_facility',
}
