import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { configSettings } from '../../../app.config';
import { FacilityForListModel } from '../../../shared/models/facility/facility-for-list.model';
import { FacilityForAuthViewModel } from '../../../shared/models/facility/facility-for-auth-view.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityFacilityApiService {

  constructor(private http: HttpClient) { }

  public get(): Observable<FacilityForListModel[]> {
    return this.http.get<FacilityForListModel[]>(configSettings.WebAPI_URL + '/auth/facilities');
  }

  public getFacility(id: string): Observable<FacilityForAuthViewModel> {
    return this.http.get<FacilityForAuthViewModel>(configSettings.WebAPI_URL + '/auth/facilities/' + id);
  }

  //public add(service: ServiceForAddModel): Observable<ServiceForAdminViewModel> {
  //  return this.http.post<any>(configSettings.WebAPI_URL + '/auth/services', JSON.stringify(service));
  //};

  //public edit(service: ServiceForEditModel): Observable<ServiceForAdminViewModel> {
  //  return this.http.put<any>(configSettings.WebAPI_URL + '/auth/services', JSON.stringify(service));
  //};

  //public remove(serviceUID: string): Observable<boolean> {
  //  return this.http.delete<any>(configSettings.WebAPI_URL + '/auth/services' + serviceUID);
  //};
}
