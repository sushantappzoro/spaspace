import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityForListModel } from "../../../shared/models/facility/facility-for-list.model";
import { FacilityFacilityService } from "../services/facility-facility.service";

@Injectable({
  providedIn: 'root'
})
export class FacilityManagerResolver implements Resolve<FacilityForListModel[]> {

  constructor(
    private router: Router,
    private facilityService: FacilityFacilityService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FacilityForListModel[]> {
    console.log('FacilityManagerResolver');
    return this.facilityService.get().pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
