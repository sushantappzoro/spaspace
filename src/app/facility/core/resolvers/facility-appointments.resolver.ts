import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityAppointmentService } from '../../../core/services/facility/facility-appointment.service';
import { AppointmentForFacilityListModel } from '../../../shared/models/appointment/appointment-for-facility-list.model';


@Injectable({
  providedIn: 'root'
})
export class FacilityAppointmentsResolver implements Resolve<AppointmentForFacilityListModel[]> {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facilityAppointmentService: FacilityAppointmentService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<AppointmentForFacilityListModel[]> {
    return this.facilityAppointmentService.getAppointments(route.params['facilityUID']).pipe(
      catchError(error => {
        console.log(error);
        //his.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
