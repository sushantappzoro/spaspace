import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';

import { FacilityDashboardService } from '../../../core/services/facility/facility-dashboard.service';
import { FacilityManagerForAdminModel } from '../../../shared/models/facility/facility-manager-for-admin.model';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityPendingManagersResolver implements Resolve<FacilityManagerForAdminModel[]> {

  constructor(
    private authService: AuthorizeService,
    private router: Router,
    private facilityService: FacilityDashboardService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FacilityManagerForAdminModel[]> {
    
    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          return this.facilityService.getPendingFacilityManagers(user.sub)
            .pipe(
              mergeMap(response => {
                if (response) {
                  return of(response)
                } else {
                  this.router.navigate(['/facility/error']);
                  return EMPTY;
                }
              })
            )
        })
      )
  }

}
