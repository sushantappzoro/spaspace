import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { FacilityForAuthViewModel } from "../../../shared/models/facility/facility-for-auth-view.model";
import { FacilityFacilityService } from "../services/facility-facility.service";


@Injectable({
  providedIn: 'root'
})
export class FacilityCurrentFacilityResolver implements Resolve<FacilityForAuthViewModel> {

  constructor(
    private router: Router,
    private facilityService: FacilityFacilityService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FacilityForAuthViewModel> {
    return this.facilityService.getCurrentFacility().pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
