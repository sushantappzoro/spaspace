import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { FacilityInfoService } from '../../../core/services/facility/facility-info.service';
import { FacilityForAdminListModel } from '../../../shared/models/facility/facility-for-admin-list.model';
import { FacilityManagerService } from '../../../core/services/facility/facility-manager.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityManagerFacilitiesResolver implements Resolve<FacilityForAdminListModel[]>  {

  constructor(
    private authService: AuthorizeService,
    private facilityManagerService: FacilityManagerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FacilityForAdminListModel[]> | Observable<never> {
    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          return this.facilityManagerService.getManagedFacilities(user.sub)
            .pipe(
              mergeMap(response => {
                if (response) {
                  return of(response)
                } else {
                  this.router.navigate(['/facility/error']);
                  return EMPTY;
                }
              })
            )
        })
      )
  }
}
