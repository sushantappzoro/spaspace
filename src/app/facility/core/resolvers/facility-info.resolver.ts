import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError } from 'rxjs/operators';
import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';
import { FacilityInfoService } from '../../../core/services/facility/facility-info.service';

@Injectable({
  providedIn: 'root'
})
export class FacilityInfoResolver implements Resolve<FacilityInfoForAdminModel> {

  constructor(
    private facilityInfoService: FacilityInfoService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    console.log('FacilityInfoResolver-constructor');}

  resolve(route: ActivatedRouteSnapshot): Observable<FacilityInfoForAdminModel> {

    var uid = route.params['uid'];
    if (!uid) {
      uid = route.params['facilityUID'];
    }

    return this.facilityInfoService.getFacilityInfo(uid).pipe(
      catchError(error => {
        console.log(error);
        //this.router.navigate(['/home']);
        return of(null);
      })
    );
  }

}
