import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap } from 'rxjs/operators';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { FacilityManagerService } from '../../../core/services/facility/facility-manager.service';
import { FacilityManagerForAdminModel } from '../../../shared/models/facility/facility-manager-for-admin.model';

@Injectable({
  providedIn: 'root'
})
export class FacilityManagerResolver implements Resolve<FacilityManagerForAdminModel> {

  constructor(
    private authService: AuthorizeService,
    private facilityManagerService: FacilityManagerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FacilityManagerForAdminModel> | Observable<never> {
    return this.authService.getUser()
      .pipe(take(1),
        switchMap(user => {
          // need to get facility uid
          return this.facilityManagerService.getFacilityManager(user.sub)
            .pipe(take(1),
              mergeMap(response => {
                if (response) {
                  return of(response)
                } else {
                  this.router.navigate(['/facility/error']);
                  return EMPTY;
                }
              })
            )
        })
      )
  }

}
