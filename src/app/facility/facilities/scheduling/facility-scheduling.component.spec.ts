import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitySchedulingComponent } from './facility-scheduling.component';

describe('FacilitySchedulingComponent', () => {
  let component: FacilitySchedulingComponent;
  let fixture: ComponentFixture<FacilitySchedulingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitySchedulingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitySchedulingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
