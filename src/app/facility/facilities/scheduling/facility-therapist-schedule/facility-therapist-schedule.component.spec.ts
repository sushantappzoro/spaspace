import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityTherapistScheduleComponent } from './facility-therapist-schedule.component';

describe('FacilityTherapistScheduleComponent', () => {
  let component: FacilityTherapistScheduleComponent;
  let fixture: ComponentFixture<FacilityTherapistScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityTherapistScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityTherapistScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
