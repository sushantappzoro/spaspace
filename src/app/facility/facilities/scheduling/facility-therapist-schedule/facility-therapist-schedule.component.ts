import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Query from 'devextreme/data/query';
import { map } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { TherapistForViewModel } from '../../../../shared/models/appointment/therapist-for-view.model';
import { ByroomForViewModel } from '../../../../shared/models/appointment/byroom-for-view.model';

@Component({
  selector: 'app-facility-therapist-schedule',
  templateUrl: './facility-therapist-schedule.component.html',
  styleUrls: ['./facility-therapist-schedule.component.scss']
})
export class FacilityTherapistScheduleComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  bytherapist: TherapistForViewModel[];
  byroom: ByroomForViewModel[];

  currentDate: Date = new Date();

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { byroom: ByroomForViewModel[] }) => {
        this.byroom = data.byroom;

      }));

    this.subs.add(this.route.data
      .subscribe((data: { bytherapist: TherapistForViewModel[] }) => {
        this.bytherapist = data.bytherapist;

      }));
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onAppointmentFormOpeningByTherapist($event) {

  }

  onAppointmentFormOpeningByRoom($event) {

  }

  
}
