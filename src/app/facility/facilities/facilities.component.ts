import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import { FacilityForAdminListModel } from '../../shared/models/facility/facility-for-admin-list.model';

@Component({
  selector: 'spa-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.scss']
})
export class FacilitiesComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facilityInfo: FacilityForAdminListModel[]

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilities: FacilityForAdminListModel[] }) => {
        this.facilityInfo = data.facilities;
        
        if (this.facilityInfo && this.facilityInfo.length == 1) {
          var facility = this.facilityInfo[0];
          if (facility.Status == 'New') {
            console.log('new facility-' + facility.UID );
            //this.router.navigate(['../profilestepper/' + facility.UID], { relativeTo: this.route });
            this.router.navigate(['/facility/facilities/profilestepper/' + facility.UID]);
          } else {
            this.router.navigate(['./profile/' + facility.UID], { relativeTo: this.route });
          }          
        }

      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
