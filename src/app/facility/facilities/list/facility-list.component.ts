import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { DatePipe } from '@angular/common';

import { FacilityForAdminListModel } from '../../../shared/models/facility/facility-for-admin-list.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-facility-list',
  templateUrl: './facility-list.component.html',
  styleUrls: ['./facility-list.component.scss']
})
export class FacilityListComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facilities: FacilityForAdminListModel[];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe
  ) {
    this.goToResources = this.goToResources.bind(this);
    this.viewAppointments = this.viewAppointments.bind(this);
    this.goToProfile = this.goToProfile.bind(this);
    this.searchAppointments = this.searchAppointments.bind(this);
  }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilities: FacilityForAdminListModel[] }) => {
        this.facilities = data.facilities;
        if (this.facilities.length == 1 && this.facilities[0].Status == 'New') {
          this.router.navigate(['../profilestepper',this.facilities[0].UID], { relativeTo: this.route });
        }
      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  facilitiesGrid_onItemSelect(e) {
    this.routeToProfile(e.data.UID); 
  }

  goToProfile(e) {
    this.routeToProfile(e.row.data.UID);
  }

  goToResources(e) {
    this.router.navigate([e.row.data.UID, 'treatmentareas'], { relativeTo: this.route });
  }

  viewAppointments(e) {
    //console.log('this.route', this.route)
    this.router.navigate([e.row.data.UID, 'appointments'], { relativeTo: this.route });
  }

  searchAppointments(e) {
    //console.log('this.route',this.route)
    this.router.navigate([e.row.data.UID, 'appointments', 'list'], { relativeTo: this.route });
  }

  routeToProfile(uid: string) {
    let fac = this.facilities.find(x => x.UID == uid);
    if (!fac) {
      return;
    }

    if (fac.Status != 'New') {
      this.router.navigate(['../profile', uid], { relativeTo: this.route });
    } else {
      this.router.navigate(['../profilestepper', uid], { relativeTo: this.route });
    }    
  }
}
