import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { FacilitiesComponent } from './facilities.component';
//import { FacilityAvailabilityComponent } from './availability/facility-availability.component';
//import { FacilityDashboardComponent } from './dashboard/facility-dashboard.component';
//import { FacilityImagesComponent } from './images/facility-images.component';
//import { FacilityPreviewComponent } from './preview/facility-preview.component';
//import { FacilityProfileComponent } from './profile/facility-profile.component';
////import { FacilityResourcesComponent } from './resources/facility-resources.component';
//import { FacilitySchedulingComponent } from './scheduling/facility-scheduling.component';
//import { FacilityInfoResolver } from '../core/resolvers/facility-info.resolver';
//import { FacilityProfileStepperComponent } from './profile/facility-profile-stepper.component';
//import { FacilityListComponent } from './list/facility-list.component';
//import { FacilityManagerFacilitiesResolver } from '../core/resolvers/facility-manager-facilities.resolver';
//import { FacilityPendingManagersResolver } from '../core/resolvers/facility-pending-managers.resolver';
//import { FacilityTreatmentAreasResolver } from '../../core/resolvers/facility/facility-treatment-areas.resolver';
//import { FacilityTreatmentAreaComponent } from '../../shared/components/facility/facility-treatment-areas/facility-treatment-area/facility-treatment-area.component';
//import { FacilitiesFacilityComponent } from './facility/facilities-facility.component';
//import { FacilityTreatmentAreasComponent } from '../../shared/components/facility/facility-treatment-areas/facility-treatment-areas.component';


const routes: Routes = [
  //{
  //path: '',
  //component: FacilitiesComponent,
  //children: [
  //  {
  //    path: '',
  //    redirectTo: 'list',
  //  },
  //  {
  //    path: ':uid',
  //    component: FacilitiesFacilityComponent,
  //    //resolve: {
  //    //  services: ServicesResolver
  //    //},,
  //    data: {
  //      breadcrumb: 'Facility'
  //    },
  //    children: [
  //      {
  //        path: 'treatmentareas',
  //        component: FacilityTreatmentAreasComponent,
  //        resolve: {
  //          treatmentAreas: FacilityTreatmentAreasResolver
  //        },
  //        data: {
  //          breadcrumb: 'Treatment Area'
  //        }
  //      }
  //    ]
  //  },
  //  {
  //    path: 'treatmentareas/:uid',
  //    component: FacilityTreatmentAreasComponent,
  //    resolve: {
  //      treatmentAreas: FacilityTreatmentAreasResolver
  //    },
  //    data: {
  //      breadcrumb: 'Treatment Area'
  //    }
  //  },
  //  {
  //    path: 'availability/:uid',
  //    component: FacilityAvailabilityComponent,
  //    //resolve: {
  //    //  treatmentAreas: FacilityTreatmentAreasResolver
  //    //},
  //    data: {
  //      breadcrumb: 'Availability'
  //    }
  //  },
  //  {
  //    path: 'list',
  //    component: FacilityListComponent,
  //    resolve: {
  //      facilities: FacilityManagerFacilitiesResolver
  //    },
  //    data: {
  //      breadcrumb: 'Facilities'
  //    }
  //  },
  //  {
  //    path: 'dashboard',
  //    component: FacilityDashboardComponent,
  //    //resolve: {
  //    //  facilityManagers: FacilityPendingManagersResolver
  //    //},
  //    data: {
  //      breadcrumb: 'Dashboard'
  //    }
  //  },
  //  {
  //    path: 'images',
  //    component: FacilityImagesComponent,
  //    data: {
  //      breadcrumb: 'Images'
  //    }
  //  },
  //  {
  //    path: 'preview/:uid',
  //    component: FacilityPreviewComponent,
  //    //resolve: FacilityInfoResolver,
  //    data: {
  //      breadcrumb: 'Preview'
  //    }
  //  },
  //  {
  //    path: 'profile',
  //    component: FacilityProfileComponent,
  //    data: {
  //      breadcrumb: 'Profile'
  //    }
  //  },
  //  {
  //    path: 'profile/:uid',
  //    component: FacilityProfileComponent,
  //    //resolve: FacilityInfoResolver,
  //    data: {
  //      breadcrumb: 'Profile'
  //    }
  //  },
  //  {
  //    path: 'profilestepper/:uid',
  //    component: FacilityProfileStepperComponent,
  //    //resolve: FacilityInfoResolver,
  //    data: {
  //      breadcrumb: 'Profile'
  //    }
  //  },
  //  //{
  //  //  path: 'resources',
  //  //  component: FacilityResourcesComponent
  //  //},
  //  {
  //    path: 'scheduling/:uid',
  //    component: FacilitySchedulingComponent,
  //    resolve: { treatmentAreas: FacilityTreatmentAreasResolver },
  //    data: {
  //      breadcrumb: 'Scheduling'
  //    }
  //  },
  //  {
  //    path: '**',
  //    redirectTo: 'dashboard',
  //  },
  //  //{
  //  //  path: '', redirectTo: 'profile', pathMatch: 'full'
  //  //}
  //]
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacilitiesRoutingModule { }
