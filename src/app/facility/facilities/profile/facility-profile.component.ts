import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DxTabsComponent } from 'devextreme-angular';
import { SubSink } from 'subsink';

import { ActivatedRoute, Router } from '@angular/router';
import { FacilityFacilityService } from '../../core/services/facility-facility.service';
import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';
import { FacilityInfoService } from '../../../core/services/facility/facility-info.service';

export class Longtab {
  text: string;
}

@Component({
  selector: 'spa-facility-profile',
  templateUrl: './facility-profile.component.html',
  styleUrls: ['./facility-profile.component.scss']
})
export class FacilityProfileComponent implements OnInit, OnDestroy {
  @ViewChild("tabMenu") tabMenu: DxTabsComponent;

  private subs = new SubSink();

  subscription: any;

  facilityUid: string;

  facility: FacilityInfoForAdminModel;

  longtabs: Longtab[] = [
    { text: "Profile" },
    { text: "Amenities" },
    { text: "Therapist Info" },
    { text: "Images" }
  ];

  showProfile: boolean = true;
  showAmenities: boolean = true;
  showImages: boolean = true;
  showTherapistInfo: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private facilityInfoService: FacilityInfoService
  ) { }

  ngOnInit() {


    this.subscription = this.route
      .params
      .subscribe(params => {
        this.facilityUid = params['uid'];
      });



    this.facilityInfoService.getFacilityInfo(this.facilityUid)
      .subscribe(resp => {
        this.facility = resp;
      },
        error => {

        });

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  //onViewChanged(e) {
  //  console.log('onViewChanged', e.addedItems[0].text);

  //  if (e && e.addedItems && e.addedItems.length != 0) {

  //    const selection = e.addedItems[0].text;

  //    switch (selection) {
  //      case 'Profile':
  //        this.showProfile = true;
  //        this.showAmenities = false;
  //        this.showImages = false;
  //        this.showTherapistInfo = false;
  //        break;
  //      case 'Amenities':
  //        this.showAmenities = true;
  //        this.showImages = false;
  //        this.showProfile = false;
  //        this.showTherapistInfo = false;
  //        break;
  //      case 'Images':
  //        this.showImages = true;
  //        this.showAmenities = false;
  //        this.showProfile = false;
  //        this.showTherapistInfo = false;
  //        break;   
  //      case 'Therapist Info':
  //        this.showTherapistInfo = true;
  //        this.showImages = false;
  //        this.showAmenities = false;
  //        this.showProfile = false;
  //        break;        
  //      default:
  //    }
  //  }

  //}

}
