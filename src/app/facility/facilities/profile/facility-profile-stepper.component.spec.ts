import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityProfileStepperComponent } from './facility-profile-stepper.component';

describe('FacilityProfileStepperComponent', () => {
  let component: FacilityProfileStepperComponent;
  let fixture: ComponentFixture<FacilityProfileStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityProfileStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityProfileStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
