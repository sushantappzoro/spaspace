import { Component, OnInit, OnDestroy, ApplicationInitStatus, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { MatStepper } from '@angular/material/stepper';

import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';
import { StepperControlService } from '../../../core/services/stepper-control.service';
import { FacilityInfoService } from '../../../core/services/facility/facility-info.service';

@Component({
  selector: 'app-facility-profile-stepper',
  templateUrl: './facility-profile-stepper.component.html',
  styleUrls: ['./facility-profile-stepper.component.scss']
})
export class FacilityProfileStepperComponent implements OnInit, OnDestroy {
  @ViewChild("stepper") stepper: MatStepper;

  private subs = new SubSink();

  stepIsComplete: boolean[] = [false, false, false, false, false, false];

  facility: FacilityInfoForAdminModel;
  facilityUid: string;

  subscription: any;

  errorMessage: string;

  thanksPopupVisible: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private stepperService: StepperControlService,
    private facilityInfoService: FacilityInfoService
    //private facilityService: FacilityFacilityService
  ) { }

  ngOnInit() {

    this.subscription = this.route
      .params
      .subscribe(params => {
        this.facilityUid = params['uid'];
      });



    this.facilityInfoService.getFacilityInfo(this.facilityUid)
      .subscribe(resp => {        
        this.facility = resp;
      },
        error => {

        });

    //this.subs.add(this.route.data
    //  .subscribe((data: { facility: FacilityInfoForAdminModel }) => {
    //    this.facility = data.facility;
    //  },
    //    error => {
    //      console.log(error);
    //    }));

    this.subs.add(this.stepperService.getError()
      .subscribe(error => {
        this.errorMessage = error;
        //if (error) {
        //  notify(error, "error", 3000);
        //}
      }));


    this.subs.add(this.stepperService.getCompleteStep()
      .subscribe(step => {
        this.processCompleteStep(step);
      }));


  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  selectionChange(e) {
    this.stepIsComplete[e] = false;
    this.errorMessage = '';
  }

  processCompleteStep(step: number) {

    this.stepIsComplete[step] = true;
    if (this.stepper && this.stepper.selected) {
      this.stepper.selected.completed = true;
    }

    //console.log('processCompleteStep-stepIsComplete=' + this.stepIsComplete);

    if (this.stepper != null) {
      this.stepper.next();
    }
  }

  next(step: number) {
    this.stepperService.validateStep(this.stepper.selectedIndex);
  }

  back() {
    this.stepper.previous();
  }

}
