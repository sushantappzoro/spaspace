import { Component, OnInit, Input } from '@angular/core';
import { SubSink } from 'subsink';
import { FacilityProfileForUiModel } from '../../shared/models/facility-profile-for-ui.model';
import { FacilityInfoForAdminModel } from '../../../shared/models/facility/facility-info-for-admin.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FacilityInfoService } from '../../../core/services/facility/facility-info.service';

@Component({
  selector: 'app-facility-preview',
  templateUrl: './facility-preview.component.html',
  styleUrls: ['./facility-preview.component.scss']
})
export class FacilityPreviewComponent implements OnInit {

  private subs = new SubSink();

  facilityUid: string;

  facility: FacilityInfoForAdminModel;

  subscription: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private facilityInfoService: FacilityInfoService
  ) { }

  ngOnInit() {

    this.subscription = this.route
      .params
      .subscribe(params => {        
        this.facilityUid = params['uid'];
        console.log('FacilityPreviewComponent', this.facilityUid);
        this.facilityInfoService.getFacilityInfo(this.facilityUid)
          .subscribe(resp => {
            this.facility = resp;
          },
            error => {

            });
      });



  

    //this.subs.add(this.route.data
    //  .subscribe((data: { facility: FacilityInfoForAdminModel }) => {
    //    this.facility = data.facility;

    //    if (this.facility.Status == 'New') {
    //      this.router.navigate(['./profilestepper/' + this.route.params['uid']], { relativeTo: this.route });
    //    }

    //  },
    //    error => {
    //      console.log(error);
    //    }));

  }

}
