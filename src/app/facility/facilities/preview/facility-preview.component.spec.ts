import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityPreviewComponent } from './facility-preview.component';

describe('FacilityPreviewComponent', () => {
  let component: FacilityPreviewComponent;
  let fixture: ComponentFixture<FacilityPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
