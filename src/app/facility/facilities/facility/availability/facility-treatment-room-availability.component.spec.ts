import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityTreatmentRoomAvailabilityComponent } from './facility-treatment-room-availability.component';

describe('FacilityTreatmentRoomAvailabilityComponent', () => {
  let component: FacilityTreatmentRoomAvailabilityComponent;
  let fixture: ComponentFixture<FacilityTreatmentRoomAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityTreatmentRoomAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityTreatmentRoomAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
