import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

import { FacilityForAdminListModel } from '../../../shared/models/facility/facility-for-admin-list.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-facilities-facility',
  templateUrl: './facilities-facility.component.html',
  styleUrls: ['./facilities-facility.component.scss']
})
export class FacilitiesFacilityComponent implements OnInit, OnDestroy {
  private subs = new SubSink();

  facilities: FacilityForAdminListModel[];

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilities: FacilityForAdminListModel[] }) => {
        this.facilities = data.facilities;
      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  facilitiesGrid_onItemSelect(e) {
    this.router.navigate(['../profile', e.key.UID], { relativeTo: this.route });
  }

  goToResources(e) {
    this.router.navigate(['./treatmentareas'], { relativeTo: this.route });
  }


}
