import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityTreatmentRoomComponent } from './facility-treatment-room.component';

describe('FacilityTreatmentRoomComponent', () => {
  let component: FacilityTreatmentRoomComponent;
  let fixture: ComponentFixture<FacilityTreatmentRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityTreatmentRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityTreatmentRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
