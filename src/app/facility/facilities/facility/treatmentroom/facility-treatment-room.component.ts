import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Query from 'devextreme/data/query';
import { map } from 'rxjs/operators';
import { SubSink } from 'subsink';


import { FacilityTreatmentAreasService } from '../../../../core/services/facility/facility-treatment-areas.service';
import { FacilityTreatmentAreaViewModel } from '../../../../shared/models/facility/facility-treatment-area-view.model';
import { TreatmentAreaAvailabilityForViewModel } from '../../../../shared/models/facility/treatment-area-availability-for-view.model';
import { TreatmentAreaAvailabilityForAddModel } from '../../../../shared/models/facility/treatment-area-availability-for-add.model';
import { TreatmentAreaAvailabilityForEditModel } from '../../../../shared/models/facility/treatment-area-availability-for-edit.model';
import { FacilityAvailabilityService } from '../../../../core/services/facility/facility-availability.service';

@Component({
  selector: 'app-facility-treatment-room',
  templateUrl: './facility-treatment-room.component.html',
  styleUrls: ['./facility-treatment-room.component.scss']
})
export class FacilityTreatmentRoomComponent implements OnInit, OnDestroy {

  userUID: string;

  private subs = new SubSink();

  currentDate: Date = new Date();

  colors: string[] = [
    "#26A69A", "#00BCD4", "#03A9F4", "#2196F3", "#3F51B5"
  ]

  facilityUID: string;

  treatmentAreas: FacilityTreatmentAreaViewModel[] = [];
  availability: TreatmentAreaAvailabilityForViewModel[] = [];

  areaswcolor: AreaForAvailability[];

  draggingGroupName: string = "appointmentsGroup";

  constructor(
    private route: ActivatedRoute,
    private availabilityService: FacilityAvailabilityService
  ) { }

  ngOnInit() {

    if (!this.facilityUID) {
      this.route.params.subscribe(params => {
        this.facilityUID = params['uid'];
      });
    };

    var x: number = 0;

    this.subs.add(this.route.data
      .subscribe((data: { treatmentAreas: FacilityTreatmentAreaViewModel[] }) => {
        this.treatmentAreas = data.treatmentAreas;

        this.areaswcolor = [];

        this.treatmentAreas.forEach(item => {
          var newItem: AreaForAvailability = new AreaForAvailability();
          newItem.TreatmentAreaName = item.Name;
          newItem.TreatmentAreaUID = item.FacilityUID;
          newItem.UID = item.UID;

          var color = this.colors[x];
          newItem.Color = color;
          x += 1;
          this.areaswcolor.push(newItem);

        },
          error => {
            console.log(error);
          });

      }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onAppointmentFormOpening(data) {
    var that = this;
    var form = data.form;
    var facilityInfo = that.getAreaByUID(data.appointmentData.FacilityUID) || {}

    var elements: Array<any> = form.option('items');

    //console.log('onAppointmentFormOpening', elements);

    if (elements.length == 11) {
      elements.splice(0, 1);
      elements.splice(4, 3);
      elements.splice(6, 1);

      var newElement = {
        label: {
          text: "Facility"
        },
        colSpan: 2,
        editorType: "dxSelectBox",
        dataField: "FacilityUID",
        editorOptions: {
          items: that.areaswcolor,
          displayExpr: "FacilityName",
          valueExpr: "FacilityUID",
          onValueChanged: function (args) {
            facilityInfo = that.getAreaByUID(args.value);
          }.bind(this)
        }
      };

      elements.unshift(newElement);

      var hold = elements[1]['colSpan'] = 2;
      var hold = elements[3]['colSpan'] = 2;
      var hold = elements[6]['colSpan'] = 1;


      //console.log('onAppointmentFormOpening', elements);
    }
  }

  //
  // drag and drop
  //


  getColor(uid: string) {
    console.log()
    return this.areaswcolor.find(item => item.TreatmentAreaUID == uid).Color;
  }

  onAppointmentRemove(e) {
    //const index = this.appointments.indexOf(e.itemData);

    //if (index >= 0) {
    //  this.appointments.splice(index, 1);
    //  this.tasks.push(e.itemData);
    //}
  }

  onAppointmentAdd(e) {
    //console.log('onAppointmentAdd', e);
    //console.log('onAppointmentAdd', this.availability);
    //var startTime: Date = new Date(e.itemData.StartTime);
    //var endTime: Date = new Date(startTime);
    endTime.setHours(startTime.getHours() + 1);

    let tempdate = new Date(item.StartTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    var startTime: Date = new Date(tempdate);

    tempdate = new Date(item.EndTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    var endTime: Date = new Date(tempdate);


    var item: TreatmentAreaAvailabilityForAddModel = { EndTime: endTime, StartTime: startTime, TreatmentAreaUID: e.itemData.FacilityUID, RecurrenceRule: e.itemData.recurrenceRule };

    //item.FacilityUID = e.appointmentData.FacilityName;  // this.getFacilityByName(e.appointmentData.FacilityName).FacilityUID;

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.add(this.userUID, item)
        .subscribe(resp => {

          this.availability.push(resp);

          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);            
            resolve(true);
          }));
    });

  }

  onListDragStart(e) {
    console.log('onListDragStart', e);
    e.cancel = true;
  }

  onItemDragStart(e) {
    console.log('onItemDragStart', e);
    e.itemData = e.fromData;
  }

  onItemDragEnd(e) {
    console.log('onItemDragEnd', e);
    if (e.toData) {
      e.cancel = true;
    }
  }

  //
  // end drag and drop
  //

  //getFacilityByName(name: string): TherapistAssociatedFacilityModel {
  //  return Query(this.facswcolor).filter(["Name", "=", name]).toArray()[0];
  //  //return this.facilityData.find(x => { x.Name = name });
  //}

  getAreaByUID(uid: string): FacilityTreatmentAreaViewModel {
    return Query(this.areaswcolor).filter(["TreatmentAreaUID", "=", uid]).toArray()[0];
    //return this.facilityData.find(x => { x.Name = name });
  }

  onAppointmentAdding(e) {
    console.log('onAppointmentAdding', e);
    console.log('useruid=' + this.userUID);
    var item: TreatmentAreaAvailabilityForAddModel = { ...e.appointmentData }

    let tempdate = new Date(item.StartTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    item.StartTime = new Date(tempdate);

    tempdate = new Date(item.EndTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    item.EndTime = new Date(tempdate);

    //item.FacilityUID = e.appointmentData.FacilityName;  // this.getFacilityByName(e.appointmentData.FacilityName).FacilityUID;

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.add(this.userUID, item)
        .subscribe(resp => {

          e.appointmentData.UID = resp.UID;
          e.appointmentData.TreatmentAreaName = resp.TreatmentAreaName;

          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);            
            resolve(true);
          }));
    });
  }

  onAppointmentUpdating(e) {
    console.log('onAppointmentUpdating', e);

    //var item: ProviderAvailabilityForViewModel = { ...e.oldData, ...e.newData }

    //var editItem: ProviderAvailabilityForEditModel = new ProviderAvailabilityForEditModel();

    //editItem.EndTime = item.EndTime;
    //editItem.FacilityUID = this.getFacilityByName(item.FacilityName).UID;
    //editItem.StartTime = item.StartTime;
    //editItem.UID = item.UID;

    var item: TreatmentAreaAvailabilityForViewModel = { ...e.oldData, ...e.newData }
    var editItem: TreatmentAreaAvailabilityForEditModel = { ...e.oldData, ...e.newData }

    //editItem.FacilityUID = this.getFacilityByName(item.FacilityName).FacilityUID;

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.edit(this.userUID, editItem)
        .subscribe(resp => {
          e.newData.TreatmentAreaName = resp.TreatmentAreaName;
          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);            
            resolve(true);
          }));
    });
  }

  onAppointmentDeleting(e) {
    console.log('onAppointmentDeleting', e);
    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.remove(this.userUID, e.appointmentData.UID)
        .subscribe(resp => {
          resolve(false);
        },
          error => {
            //notify('An error occured updating the attendees. Please try again', 'error', 800);            
            resolve(true);
          }));
    });
  }


}


export class AreaForAvailability {
  public UID: string;
  public TreatmentAreaUID: string;
  public TreatmentAreaName: string;
  public Color: string;
}
