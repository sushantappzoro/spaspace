import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesFacilityComponent } from './facilities-facility.component';

describe('FacilitiesFacilityComponent', () => {
  let component: FacilitiesFacilityComponent;
  let fixture: ComponentFixture<FacilitiesFacilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesFacilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesFacilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
