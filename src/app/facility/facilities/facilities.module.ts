import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';

import { FacilitiesRoutingModule } from './facilities-routing.module';
import { FacilitiesComponent } from './facilities.component';
import { FacilityImagesComponent } from './images/facility-images.component';
import { FacilityDashboardComponent } from './dashboard/facility-dashboard.component';
import { FacilityPreviewComponent } from './preview/facility-preview.component';
import { FacilitySchedulingComponent } from './scheduling/facility-scheduling.component';
import { FacilityProfileComponent } from './profile/facility-profile.component';
//import { FacilityResourcesComponent } from './resources/facility-resources.component';
import { DevexModuleModule } from '../devex-module.module';
import { FacilitySharedModule } from '../shared/facility-shared.module';
import { FacilityProfileStepperComponent } from './profile/facility-profile-stepper.component';
import { FacilityMaterialModule } from '../shared/facility-material.module';
import { FacilityListComponent } from './list/facility-list.component';
import { FacilitiesFacilityComponent } from './facility/facilities-facility.component';
import { FacilityTreatmentRoomComponent } from './facility/treatmentroom/facility-treatment-room.component';
import { FacilityTreatmentRoomAvailabilityComponent } from './facility/availability/facility-treatment-room-availability.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FacilityTherapistScheduleComponent } from './scheduling/facility-therapist-schedule/facility-therapist-schedule.component';

@NgModule({
  declarations: [
    FacilitiesComponent,
    FacilityImagesComponent,
    FacilityDashboardComponent,
    FacilityPreviewComponent,
    FacilitySchedulingComponent,
    FacilityProfileComponent,
    FacilityProfileStepperComponent,
    FacilityDashboardComponent,
    //FacilityResourcesComponent,
    FacilityListComponent,
    FacilitiesFacilityComponent,
    FacilityTreatmentRoomComponent,
    FacilityTreatmentRoomAvailabilityComponent,
    FacilityTherapistScheduleComponent,
  ],
  imports: [
    CommonModule,
    DevexModuleModule,
    CommonModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro,
    FacilityMaterialModule,
    FacilitiesRoutingModule,
    FacilitySharedModule
  ]
})
export class FacilitiesModule { }
