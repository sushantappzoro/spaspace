import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { Router, ActivatedRoute } from '@angular/router';
import { FacilityManagerForAdminModel } from '../../shared/models/facility/facility-manager-for-admin.model';

@Component({
  selector: 'app-facility-mgr-dashboard',
  templateUrl: './facility-mgr-dashboard.component.html',
  styleUrls: ['./facility-mgr-dashboard.component.scss']
})
export class FacilityMgrDashboardComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  facilityManagers: FacilityManagerForAdminModel[];

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.subs.add(this.route.data
      .subscribe((data: { facilityManagers: FacilityManagerForAdminModel[] }) => {
        console.log('got data', data);
        this.facilityManagers = data.facilityManagers;
      },
        error => {
          console.log(error);
        }));


  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
