import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityMgrDashboardComponent } from './facility-mgr-dashboard.component';

describe('FacilityMgrDashboardComponent', () => {
  let component: FacilityMgrDashboardComponent;
  let fixture: ComponentFixture<FacilityMgrDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityMgrDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityMgrDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
