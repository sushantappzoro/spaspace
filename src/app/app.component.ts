import { Component, OnInit } from '@angular/core';
import { configSettings } from './app.config';
import { AuthorizeService } from '../api-authorization/authorize.service';
import { filter, pairwise } from 'rxjs/operators';
import { RoutesRecognized, Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';
import { SpinnerOverlayService } from './core/services/common/spinner-overlay.service';
import { BreadcrumbService } from './core/services/common/breadcrumb.service';
import { SpaConfigService } from '../../projects/spaspace-lib/src/core/services/spa-config.service';

import { environment } from '../environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    title = 'app';

    isAuth: any;

  constructor(
    private router: Router,
    private authService: AuthorizeService,
    private spinnerService: SpinnerOverlayService,
    private breadcrumbService: BreadcrumbService,
    private spaconfig: SpaConfigService
  ) {

    //console.log('app.config constructor');
    this.checkLogin();

    //console.log('app.config after checkLogin');

    this.router.events.subscribe(
      event => {
        if (event instanceof RouteConfigLoadStart) {
          this.spinnerService.show();
          return;
        }
        if (event instanceof RouteConfigLoadEnd) {
          this.spinnerService.hide();
          return;
        }
      }
    );

    this.router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe(_v => {
        //console.log('_v', _v);
        this.breadcrumbService.SetBreadcrumbs(this.router.routerState.root);
      });

  }

  ngOnInit() {

    //if (!this.authService.checkSignedIn()) {
    //  const signin = this.authService.signInSilent(null);
    //}
    
    //this.loadAppSettings();
    this.isAuth = this.isAuthorized();
    this.spaconfig.ApiServer = environment.ClientAppSettings.apiServer;
    this.spaconfig.MobileURL = environment.ClientAppSettings.mobileUrl;
    configSettings.WebAPI_URL = environment.ClientAppSettings.apiServer;
    configSettings.Auth_URL = environment.ClientAppSettings.authority;
    configSettings.Mobile_URL = environment.ClientAppSettings.mobileUrl;

  }

    // async loadAppSettings() {

    //   const response = await fetch(`${window.location.origin}/api/AppConfiguration`);
    //   if (!response.ok) {
    //       throw new Error('Could not load settings');
    //   }

    //   const settings: any = await response.json();

    //   //this.spaconfig.ApiServer = settings.apiServer;

    //   configSettings.WebAPI_URL = settings.apiServer;
    //   configSettings.Auth_URL = settings.authority;
    //   configSettings.Mobile_URL = settings.mobileURL;
    // }

    isAuthorized() {
        return this.authService.isAuthenticated;
  }

  private async checkLogin() {
    //console.log('app.config checkLogin');
    //const si = this.authService.checkSignedIn();
    //const signin = await this.authService.signInSilent(null);
    const check = await this.authService.checkSignedIn().then(value => {
      return;
    });

  }
}
