import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facility-pitch',
  templateUrl: './facility-pitch.component.html',
  styleUrls: ['./facility-pitch.component.scss']
})
export class FacilityPitchComponent implements OnInit {

  selectedOne: boolean = true;
  selectedTwo: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.selectedOne = !this.selectedOne;
    this.selectedTwo = !this.selectedTwo;
  }

}
