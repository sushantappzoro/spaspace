import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityPitchComponent } from './facility-pitch.component';

describe('FacilityPitchComponent', () => {
  let component: FacilityPitchComponent;
  let fixture: ComponentFixture<FacilityPitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityPitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityPitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
