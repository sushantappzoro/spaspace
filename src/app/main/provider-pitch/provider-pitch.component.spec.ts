import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderPitchComponent } from './provider-pitch.component';

describe('ProviderPitchComponent', () => {
  let component: ProviderPitchComponent;
  let fixture: ComponentFixture<ProviderPitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderPitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderPitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
