import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-provider-pitch',
  templateUrl: './provider-pitch.component.html',
  styleUrls: ['./provider-pitch.component.scss']
})
export class ProviderPitchComponent implements OnInit {

  selectedOne: boolean = true;
  selectedTwo: boolean = false;
  selectedThree: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onRequirementsClick() {

  }

  onRegisterClick() {

  }
}
