import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AuthorizeService } from '../../../api-authorization/authorize.service';
import { map, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

    private unsubscribe$ = new Subject();

    interval: any; // NodeJS.Timeout;

    constructor(private router: Router, private authorizeService: AuthorizeService) { }

    ngOnInit() {
        //console.log('LandingComponent.ngOnInit');
        //this.startTimer();

        this.authorizeService.getUser()
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(user => {
            //console.log('LandingComponent.got user');
            this.unsubscribe$.next();
            this.unsubscribe$.complete();            
            if (!!user && !!user.role) {
              //clearTimeout(this.interval);
              switch (user.role) {
                case 'admin':
                  this.router.navigate(['/admin']);
                  break;
                case 'therapist':
                  //console.log('got user', user);
                  this.router.navigate(['/therapist/dashboard']);
                  break;
                case 'customer':
                  var lastUrl = sessionStorage.getItem('lastUrl');
                  sessionStorage.removeItem('lastUrl');
                  if (lastUrl) {
                    this.router.navigateByUrl(lastUrl, {
                      replaceUrl: true
                    });
                  } else {
                    this.router.navigate(['/portal']);
                  }
                  break;
                case 'facilitymanager':
                  this.router.navigate(['/facility']);
                  break;
                default:
                  this.router.navigate(['/']);
                  break;
              }
            } else {
              this.router.navigate(['/']);
            }
          });
    }

    //startTimer() {        
    //    this.interval = setInterval(() => {
    //        console.log('fire timer');
    //        this.router.navigate(['/']);
    //    }, 5000)
    //}

}
