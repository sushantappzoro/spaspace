import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ApiAuthorizationModule } from '../../api-authorization/api-authorization.module';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
//import { BrowserModule } from '@angular/platform-browser';
import { MainComponent } from './main.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { DxGalleryModule, DxButtonModule, DxTextAreaModule, DxTextBoxModule, DxValidatorModule } from 'devextreme-angular';
import { LandingComponent } from './landing/landing.component';
import { MaterialModule } from '../material-module.module';
import { PricingComponent } from './pricing/pricing.component';
import { FacilityPitchComponent } from './facility-pitch/facility-pitch.component';
import { ProviderPitchComponent } from './provider-pitch/provider-pitch.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { TherapistRequirementsComponent } from './therapist/therapist-requirements/therapist-requirements.component';
import { MainTherapistsComponent } from './therapist/main-therapists/main-therapists.component';
import { TherapistJoinusComponent } from './therapist/therapist-joinus/therapist-joinus.component';
import { TherapistThanksComponent } from './therapist/therapist-thanks/therapist-thanks.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NewsComponent } from './news/news.component';
import { TermsComponent } from './terms/terms.component';
import { SafetyComponent } from './safety/safety.component';
import { FaqComponent } from './faq/faq.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { SpaSpaceComponentsModule } from '../../../projects/spaspace-lib/src/shared/components/spaspace-components.module';
import { SpaSpaceSharedModule } from '../../../projects/spaspace-lib/src/public-api';
import { MainContentComponent } from './content/main-content.component';
import { MainFaqComponent } from './content/main-faq/main-faq.component';
import { MainServiceStandardsComponent } from './content/main-service-standards/main-service-standards.component';
import { MainProcessComponent } from './content/main-process/main-process.component';
import { MainHealthSafetyComponent } from './content/main-health-safety/main-health-safety.component';
import { MainResourcesComponent } from './content/main-resources/main-resources.component';
import { MainSanitizationComponent } from './content/main-sanitization/main-sanitization.component';
import { MainAboutComponent } from './content/main-about/main-about.component';
import { MainStoryComponent } from './content/main-story/main-story.component';

@NgModule({
    declarations: [
        MainComponent,
        HomeComponent,
        LandingComponent,
        PricingComponent,
        FacilityPitchComponent,
        ProviderPitchComponent,
        UnauthorizedComponent,
        TherapistRequirementsComponent,
        MainTherapistsComponent,
        TherapistJoinusComponent,
        TherapistThanksComponent,
        AboutUsComponent,
        NewsComponent,
        TermsComponent,
        SafetyComponent,
        FaqComponent,
        PrivacyComponent,
        MainContentComponent,
        MainFaqComponent,
        MainServiceStandardsComponent,
        MainProcessComponent,
        MainHealthSafetyComponent,
        MainResourcesComponent,
        MainSanitizationComponent,
        MainAboutComponent,
        MainStoryComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
      FlexLayoutModule,
      MDBBootstrapModulesPro,
        ApiAuthorizationModule,
        SharedModule,
        MaterialModule,
        DxGalleryModule,
        DxButtonModule,
        DxTextAreaModule,
        DxTextBoxModule,
      DxValidatorModule,
      SpaSpaceSharedModule,
        SpaSpaceComponentsModule
    ]
})
export class MainModule { }
