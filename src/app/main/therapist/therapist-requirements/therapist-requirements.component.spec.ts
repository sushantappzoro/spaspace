import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistRequirementsComponent } from './therapist-requirements.component';

describe('TherapistRequirementsComponent', () => {
  let component: TherapistRequirementsComponent;
  let fixture: ComponentFixture<TherapistRequirementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistRequirementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistRequirementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
