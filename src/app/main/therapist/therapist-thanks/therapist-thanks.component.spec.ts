import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistThanksComponent } from './therapist-thanks.component';

describe('TherapistThanksComponent', () => {
  let component: TherapistThanksComponent;
  let fixture: ComponentFixture<TherapistThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
