import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistJoinusComponent } from './therapist-joinus.component';

describe('TherapistJoinusComponent', () => {
  let component: TherapistJoinusComponent;
  let fixture: ComponentFixture<TherapistJoinusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistJoinusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistJoinusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
