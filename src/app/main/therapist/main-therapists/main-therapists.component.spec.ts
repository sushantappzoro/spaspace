import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainTherapistsComponent } from './main-therapists.component';

describe('MainTherapistsComponent', () => {
  let component: MainTherapistsComponent;
  let fixture: ComponentFixture<MainTherapistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainTherapistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainTherapistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
