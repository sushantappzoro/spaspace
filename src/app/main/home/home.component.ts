import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';

import { SpaHomeStore, SpaMassageTypeForCardModel, SpaTherapistForCardModel, SpaConfigService } from '../../../../projects/spaspace-lib/src/public-api';
import { SpaFacilityForCardModel } from '../../../../projects/spaspace-lib/src/shared/models/facility/spa-facility-for-card.model';

import { DatePipe } from '@angular/common'
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  private subs = new SubSink();

  facilities: SpaFacilityForCardModel[] = [];
  massageTypes: SpaMassageTypeForCardModel[] = [];
  therapists: SpaTherapistForCardModel[] = [];

  constructor(
    private homeService: SpaHomeStore,
    private configService: SpaConfigService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {

    this.subs.add(this.homeService.getFacilitiesNearMe()
      .subscribe(facilities => {
        this.facilities = facilities;
      })
    )

    this.subs.add(this.homeService.getMassageTypes()
      .subscribe(massageTypes => {
        this.massageTypes = massageTypes;
      })
    )

    this.subs.add(this.homeService.getTherapistsNearMe()
      .subscribe(therapists => {
        this.therapists = therapists;
      })
    )

  }

  ngAfterViewInit() {

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onBookMassageClick() {
    window.location.assign(this.configService.MobileURL);
  }

  dateChange(e) {

    const date: Date = e.value;

    const dateString: string = this.datePipe.transform(date, 'yyyy-MM-dd');
    const url = environment.ClientAppSettings.mobileUrl + '/schedule/bydate/' + dateString;
    window.location.href = url;
  }

  onBookNowClick() {
    const url = environment.ClientAppSettings.mobileUrl + '/schedule/bydate';
    window.location.href = url;
  }

  onSubscribeClick() {

  } 
}
