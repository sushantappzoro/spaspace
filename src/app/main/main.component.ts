import { Component, OnInit, ViewChild } from '@angular/core';
import { faUserCircle, faBuilding, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import themes from "devextreme/ui/themes";
import { AuthorizeService } from '../../api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { MatDrawerContent, MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @ViewChild("sideMenu") sideMenu: MatDrawer;

  faUserCircle = faUserCircle;
  faBuilding = faBuilding;
  faQuestionCircle = faQuestionCircle;

  userRole: string;

  public isAuthenticated: Observable<boolean>;

  constructor(
    private router: Router,
    private authorizeService: AuthorizeService
  ) {
    //themes.current("material.main.light");
  }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.authorizeService.getRole()
      .subscribe(role => {
        this.userRole = role;
      });
  }

  onSubscribeClick() {

  }

  menuButtonClick() {
    this.sideMenu.toggle();
  }

  profileClick() {
    this.sideMenu.toggle();
    switch (this.userRole) {
      case 'admin':
        this.router.navigate(['/admin']);
        break;
      case 'customer':
        this.router.navigate(['/portal']);
        break;
      case 'facilitymanager':
        this.router.navigate(['/facility']);
        break;
      case 'therapist':
        this.router.navigate(['/therapist']);
        break;
      default:
    }
  }

  goHome() {
    //if (this.homePage) {
    //  this.router.navigate([this.homePage]);
    //} else {
    //  this.router.navigate(['/']);
    //}
  }

}
