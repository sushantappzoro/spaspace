import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSanitizationComponent } from './main-sanitization.component';

describe('MainSanitizationComponent', () => {
  let component: MainSanitizationComponent;
  let fixture: ComponentFixture<MainSanitizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainSanitizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSanitizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
