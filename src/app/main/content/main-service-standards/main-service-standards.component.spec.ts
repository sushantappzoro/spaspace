import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainServiceStandardsComponent } from './main-service-standards.component';

describe('MainServiceStandardsComponent', () => {
  let component: MainServiceStandardsComponent;
  let fixture: ComponentFixture<MainServiceStandardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainServiceStandardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainServiceStandardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
