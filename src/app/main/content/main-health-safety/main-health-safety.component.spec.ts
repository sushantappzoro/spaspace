import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHealthSafetyComponent } from './main-health-safety.component';

describe('MainHealthSafetyComponent', () => {
  let component: MainHealthSafetyComponent;
  let fixture: ComponentFixture<MainHealthSafetyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainHealthSafetyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHealthSafetyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
