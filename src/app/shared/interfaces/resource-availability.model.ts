export interface ResourceAvailabilityModel {
    Uid: string;
    ResourceUid: string;
    StartDate: Date;
    EndDate: Date;
    AllDay: boolean;
}
