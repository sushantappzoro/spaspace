
import { AppointmentModel } from "./appointment.model";
import { ServiceModel } from "../../models/offerings/service.model";

export interface OrderModel {
    Uid: string;
    ClientUid: string;
    Appointment: AppointmentModel;
    Service: ServiceModel[];
    Addons: any[];
    Options: any[];
    Booked: boolean;
    TotalPrice: number;
}
