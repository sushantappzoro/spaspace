export interface AppointmentModel {
    Uid: string;
    ResourceUid: string;
    ProviderUid: string;
    StartTime: Date;
    EndTime: Date;
}
