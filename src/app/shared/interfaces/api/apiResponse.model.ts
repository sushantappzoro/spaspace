export interface ApiResponseModel {
  Errors: string[];
  StatusCode: number;
  Message: string;
  Result: any;
  ErrorResult: any;
}
