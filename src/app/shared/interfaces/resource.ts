import { ResourceAvailabilityModel } from "./resource-availability.model";

export interface ResourceModel {
  //Id: number;  //Uid: string;
  //ResourceUid: number;
  Uid: string;
  FacilityUid: string;
  Name: string;
  Description: string;
  ResourceTypeUid: string;

  Rate: number;
  Availability: ResourceAvailabilityModel[];
}
