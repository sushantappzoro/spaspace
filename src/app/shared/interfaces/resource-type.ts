export interface ResourceTypeModel {
    Uid: string;
    Name: string;
    Description: string;
}
