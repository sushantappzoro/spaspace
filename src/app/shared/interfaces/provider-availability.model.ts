export interface ProviderAvailabilityModel {
    Uid: string;
    ProviderUid: string;
    FaciilityUid
    StartDate: Date;
    EndDate: Date;
    AllDay: boolean;
}
