import { NgModule } from '@angular/core';
import {
  DxButtonModule,
  DxDataGridModule,
  DxDraggableModule,
  DxDrawerModule,
  DxFileUploaderModule,
  DxListModule,
  DxNumberBoxModule,
  DxPopupModule,
  DxRadioGroupModule,
  DxSchedulerModule,
  DxScrollViewModule,
  DxTabPanelModule,
  DxTabsModule,
  DxTextAreaModule,
  DxTextBoxModule,
  DxToolbarModule,
  DxValidatorModule,
  DxTemplateModule,
  DxCheckBoxModule,
  DxDateBoxModule,
  DxGalleryModule,
  DxTileViewModule,
  DxValidationGroupModule,
  DxContextMenuModule,
  DxSelectBoxModule,
  DxSpeedDialActionModule,
  DxTooltipModule
} from 'devextreme-angular';



@NgModule({
  declarations: [],
  exports: [
    DxButtonModule,
    DxContextMenuModule,
    DxCheckBoxModule,
    DxDataGridModule,
    DxDateBoxModule,
    DxDraggableModule,
    DxDrawerModule,
    DxFileUploaderModule,
    DxGalleryModule,
    DxListModule,
    DxNumberBoxModule,
    DxPopupModule,
    DxRadioGroupModule,
    DxSchedulerModule,
    DxScrollViewModule,
    DxSelectBoxModule,
    DxTabPanelModule,
    DxTabsModule,
    DxTemplateModule,
    DxTextAreaModule,
    DxTextBoxModule,
    DxTileViewModule,
    DxToolbarModule,
    DxValidatorModule,
    DxValidationGroupModule,
    DxSpeedDialActionModule,
    DxTooltipModule
  ]
})
export class DevexModuleModule { }
