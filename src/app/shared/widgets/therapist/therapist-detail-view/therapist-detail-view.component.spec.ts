import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistDetailViewComponent } from './therapist-detail-view.component';

describe('TherapistDetailViewComponent', () => {
  let component: TherapistDetailViewComponent;
  let fixture: ComponentFixture<TherapistDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
