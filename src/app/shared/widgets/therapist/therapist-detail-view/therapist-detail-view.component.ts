import { Component, OnInit, Input } from '@angular/core';
import { SpaTherapistForCardModel } from 'projects/spaspace-lib/src/shared/models';

@Component({
  selector: 'spa-therapist-detail-view',
  templateUrl: './therapist-detail-view.component.html',
  styleUrls: ['./therapist-detail-view.component.scss']
})
export class TherapistDetailViewComponent implements OnInit {
  @Input() therapist: SpaTherapistForCardModel;

  constructor() { }

  ngOnInit() {
  }

  getPhotoUrl() {
    return (this.therapist && this.therapist.MainPhotoUrl && this.therapist.MainPhotoUrl.length > 0) ? this.therapist.MainPhotoUrl : 'assets/images/placeholder-person.jpg';
  }

}
