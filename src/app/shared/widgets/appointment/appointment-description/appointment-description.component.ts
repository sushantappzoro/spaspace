import { Component, OnInit, Input } from '@angular/core';
import { AppointmentForTherapistDetailModel } from '../../../models/therapist/appointment-for-therapist-detail.model';

@Component({
  selector: 'spa-appointment-description',
  templateUrl: './appointment-description.component.html',
  styleUrls: ['./appointment-description.component.scss']
})
export class AppointmentDescriptionComponent implements OnInit {
  @Input() appointmentDetail: AppointmentForTherapistDetailModel;
  @Input() showTherapist: boolean =false;
  
  therapist: any;

  constructor() { }

  ngOnInit() {
    
  }

}
