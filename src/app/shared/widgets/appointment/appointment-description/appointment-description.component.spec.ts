import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentDescriptionComponent } from './appointment-description.component';

describe('AppointmentDescriptionComponent', () => {
  let component: AppointmentDescriptionComponent;
  let fixture: ComponentFixture<AppointmentDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
