import { Component, OnInit, Input } from '@angular/core';

import { TherapistAppointmentSummaryModel } from '../../../models/therapist/therapist-appointment-summary.model';

@Component({
  selector: 'spa-appointment-card',
  templateUrl: './appointment-card.component.html',
  styleUrls: ['./appointment-card.component.scss']
})
export class AppointmentCardComponent implements OnInit {
  @Input() appointmentSummary: TherapistAppointmentSummaryModel;

  constructor() { }

  ngOnInit() {
    this.appointmentSummary = new TherapistAppointmentSummaryModel();
  }

  getClientImage(): string {
    return 'assets/images/placeholder-person.jpg';
  }

  onClick() {

  }
}
