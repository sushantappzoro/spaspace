import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { AppointmentForTherapistDetailModel } from '../../../models/therapist/appointment-for-therapist-detail.model';

@Component({
  selector: 'spa-appointment-therapist-info',
  templateUrl: './appointment-therapist-info.component.html',
  styleUrls: ['./appointment-therapist-info.component.scss']
})
export class AppointmentTherapistInfoComponent implements OnInit, OnChanges {
  @Input() uniformInfo: string;
  @Input() treatmentRoom: string;
  @Input() parkingInfo: string;
  @Input() checkInInfo: string;
  @Input() additionalInfo: string;

  defaultText: string = 'None provided';

  constructor() { }

  ngOnInit() {
    
  }

  ngOnChanges() {
    this.uniformInfo = (this.uniformInfo && this.uniformInfo.length) ? this.uniformInfo : this.defaultText;
    this.treatmentRoom = (this.treatmentRoom && this.treatmentRoom.length) ? this.treatmentRoom : this.defaultText;
    this.parkingInfo = (this.parkingInfo && this.parkingInfo.length) ? this.parkingInfo : this.defaultText;
    this.checkInInfo = (this.checkInInfo && this.checkInInfo.length) ? this.checkInInfo : this.defaultText;
    this.additionalInfo = (this.additionalInfo && this.additionalInfo.length) ? this.additionalInfo : this.defaultText;
  }

}
