import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentTherapistInfoComponent } from './appointment-therapist-info.component';

describe('AppointmentTherapistInfoComponent', () => {
  let component: AppointmentTherapistInfoComponent;
  let fixture: ComponentFixture<AppointmentTherapistInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentTherapistInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentTherapistInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
