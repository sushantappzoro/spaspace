import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAmenityCardComponent } from './facility-amenity-card.component';

describe('FacilityAmenityCardComponent', () => {
  let component: FacilityAmenityCardComponent;
  let fixture: ComponentFixture<FacilityAmenityCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAmenityCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAmenityCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
