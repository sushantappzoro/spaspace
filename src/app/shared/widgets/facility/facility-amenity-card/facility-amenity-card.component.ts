import { Component, OnInit, Input } from '@angular/core';
import { FacilityAmenityForViewModel } from '../../../models/facility/facility-amenity-for-view.model';

@Component({
  selector: 'spa-facility-amenity-card',
  templateUrl: './facility-amenity-card.component.html',
  styleUrls: ['./facility-amenity-card.component.scss']
})
export class FacilityAmenityCardComponent implements OnInit {
  @Input() amenity: FacilityAmenityForViewModel
  
  constructor() { }

  ngOnInit() {
  }

}
