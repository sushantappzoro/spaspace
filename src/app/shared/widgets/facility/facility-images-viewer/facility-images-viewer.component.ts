import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { FacilityPhotosService } from '../../../../core/services/facility/facility-photos.service';
import { FacilityPhotoForAdminModel } from '../../../models/facility/facility-photo-for-admin.model';
import { async } from '@angular/core/testing';

@Component({
  selector: 'spa-facility-images-viewer',
  templateUrl: './facility-images-viewer.component.html',
  styleUrls: ['./facility-images-viewer.component.scss']
})
export class FacilityImagesViewerComponent implements OnInit, AfterViewInit {
  @Input() facilityUid: string;

  imageList: string[] = [];
  images: FacilityPhotoForAdminModel[];

  constructor(private photoService: FacilityPhotosService) { }

  ngOnInit() {



  }

  onChanges() {
   

  }

  ngAfterViewInit() {
    this.photoService.getFacilityPhotos(this.facilityUid)
      .subscribe(photos => {
        this.images = photos;
        this.imageList = [];
        this.images.forEach(photo => {
          this.imageList.push(photo.Url);
        });

      })
  }

}
