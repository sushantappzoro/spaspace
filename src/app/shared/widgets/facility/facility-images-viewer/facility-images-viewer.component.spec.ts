import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityImagesViewerComponent } from './facility-images-viewer.component';

describe('FacilityImagesViewerComponent', () => {
  let component: FacilityImagesViewerComponent;
  let fixture: ComponentFixture<FacilityImagesViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityImagesViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityImagesViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
