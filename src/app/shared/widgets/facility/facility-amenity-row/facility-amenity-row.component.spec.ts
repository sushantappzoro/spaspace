import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAmenityRowComponent } from './facility-amenity-row.component';

describe('FacilityAmenityRowComponent', () => {
  let component: FacilityAmenityRowComponent;
  let fixture: ComponentFixture<FacilityAmenityRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAmenityRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAmenityRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
