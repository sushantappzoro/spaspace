import { Component, OnInit, Input } from '@angular/core';
import { SpaFacilityAmenityForCardModel } from '../../../../../../projects/spaspace-lib/src/shared/models';

@Component({
  selector: 'spa-facility-amenity-row',
  templateUrl: './facility-amenity-row.component.html',
  styleUrls: ['./facility-amenity-row.component.scss']
})
export class FacilityAmenityRowComponent implements OnInit {
  @Input() amenities: SpaFacilityAmenityForCardModel[];

  constructor() { }

  ngOnInit() {
  }

}
