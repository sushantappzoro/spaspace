import { Component, OnInit, Input } from '@angular/core';
import { FacilityProfileForUiModel } from '../../../../facility/shared/models/facility-profile-for-ui.model';

@Component({
  selector: 'app-facility-summary-block',
  templateUrl: './facility-summary-block.component.html',
  styleUrls: ['./facility-summary-block.component.scss']
})
export class FacilitySummaryBlockComponent implements OnInit {
  @Input() facility: FacilityProfileForUiModel;

  constructor() { }

  ngOnInit() {
  }

}
