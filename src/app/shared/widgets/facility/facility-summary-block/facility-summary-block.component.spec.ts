import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitySummaryBlockComponent } from './facility-summary-block.component';

describe('FacilitySummaryBlockComponent', () => {
  let component: FacilitySummaryBlockComponent;
  let fixture: ComponentFixture<FacilitySummaryBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitySummaryBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitySummaryBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
