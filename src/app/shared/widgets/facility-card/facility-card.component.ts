import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';

import { SpaFacilityForCardModel } from '../../../../../projects/spaspace-lib/src/shared/models/facility/spa-facility-for-card.model';

@Component({
  selector: 'spa-facility-card',
  templateUrl: './facility-card.component.html',
  styleUrls: ['./facility-card.component.scss']
})
export class FacilityCardComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() facilityForCard: SpaFacilityForCardModel;
  @Input() showFooter: boolean = true;

  
  @Output() selected: EventEmitter<any> = new EventEmitter<string>();
  @Output() loaded: EventEmitter<any> = new EventEmitter<boolean>();

  currentRate = 4;
  max = 5;

  constructor(
  ) { }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  ngAfterViewInit() {
    this.loaded.emit(true);
  }

  onClick() {
    this.selected.emit(this.facilityForCard.UID);
  }

  getFacilityImage() {
    if (this.facilityForCard && this.facilityForCard.MainPhotoUrl && this.facilityForCard.MainPhotoUrl.length > 0) {
      return this.facilityForCard.MainPhotoUrl;
    } else {
      return '/assets/images/image_placeholder.png'
    }    
  }
}
