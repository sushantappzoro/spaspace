import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit, AfterViewInit } from '@angular/core';

import { SpaTherapistForCardModel } from '../../../../../projects/spaspace-lib/src/shared/models/therapist/spa-therapist-for-card.model';

@Component({
  selector: 'spa-therapist-card',
  templateUrl: './therapist-card.component.html',
  styleUrls: ['./therapist-card.component.scss']
})
export class TherapistCardComponent implements OnInit, AfterViewInit {
  @Input() therapist: SpaTherapistForCardModel;
  @Output() click: EventEmitter<any> = new EventEmitter<boolean>();
  @Output() loaded: EventEmitter<any> = new EventEmitter<boolean>();

  currentRate = 4;
  max = 5;

  constructor() { }

  ngOnInit() {
    if (this.therapist) {
      if (!this.therapist.AverageRating || this.therapist.AverageRating == 0) {
        this.therapist.AverageRating = 5;
      }
    }
  }

  ngAfterViewInit() {
    this.loaded.emit(true);
  }
  getTherapistImage() {
    if (this.therapist && this.therapist.MainPhotoUrl && this.therapist.MainPhotoUrl.length > 0) {
      return this.therapist.MainPhotoUrl;
    } else {
      return '/assets/images/placeholder-person.jpg'
    }   
  }

  onClick() {
    this.click.emit(this.therapist.UID);
  }
}
