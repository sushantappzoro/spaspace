import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spa-client-quick-view',
  templateUrl: './client-quick-view.component.html',
  styleUrls: ['./client-quick-view.component.scss']
})
export class ClientQuickViewComponent implements OnInit {
  @Input() name: string;
  @Input() photoUrl: string;
  @Input() totalAppointments: string;

  constructor() { }

  ngOnInit() {
  }

  getPhotoUrl() {
    return (this.photoUrl && this.photoUrl.length > 0) ? this.photoUrl : 'assets/images/placeholder-person.jpg';
  }

}
