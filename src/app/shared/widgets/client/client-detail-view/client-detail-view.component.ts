import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spa-client-detail-view',
  templateUrl: './client-detail-view.component.html',
  styleUrls: ['./client-detail-view.component.scss']
})
export class ClientDetailViewComponent implements OnInit {
  @Input() name: string;
  @Input() photoUrl: string;
  @Input() totalAppointments: string;
  @Input() phone: string;
  @Input() email: string;

  constructor() { }

  ngOnInit() {
  }

  getPhotoUrl() {
    return (this.photoUrl && this.photoUrl.length > 0) ? this.photoUrl : 'assets/images/placeholder-person.jpg';
  }

}
