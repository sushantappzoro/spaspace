import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDetailViewComponent } from './client-detail-view.component';

describe('ClientDetailViewComponent', () => {
  let component: ClientDetailViewComponent;
  let fixture: ComponentFixture<ClientDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
