import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';

import { AuthHeadersService } from '../../../core/services/auth-headers.service';
import { DxFileUploaderComponent } from 'devextreme-angular';
import { title } from 'process';

@Component({
  selector: 'spa-file-upload-popup',
  templateUrl: './file-upload-popup.component.html',
  styleUrls: ['./file-upload-popup.component.scss']
})
export class FileUploadPopupComponent implements OnInit, OnChanges {
  @ViewChild("fileUploader") fileUploader: DxFileUploaderComponent;

  @Input() title: string;
  @Input() uploadApiUrl: string;
  @Input() popupVisible: boolean;

  holdTitle: string;
  holdUploadApiUrl: string;

  //@Output() popupVisibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output() onHiding: EventEmitter<any> = new EventEmitter<any>();
  @Output() fileUploaded: EventEmitter<string> = new EventEmitter<string>();
  @Output() popupVisibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  values: any[] = [];
  headers: any;

  showPopup: boolean = false;

  constructor(private authHeaderService: AuthHeadersService) { }

  ngOnInit() {
    this.headers = this.authHeaderService.getHeaders();
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log('changes', changes);
    this.headers = this.authHeaderService.getHeaders();
    if (changes.uploadApiUrl && changes.uploadApiUrl.currentValue) {
      this.holdUploadApiUrl = this.uploadApiUrl;
    }
    if (changes.popupVisible && changes.popupVisible.currentValue) {
      //console.log('changes', changes);
    }
  }

  fileUploader_onUploaded() {
    //close popup?
    notify('File uploaded successfully.', "success", 3000);
    this.fileUploader.resetOptions();
    this.fileUploaded.emit(this.title);
    this.popupVisible = false;
    this.popupVisibleChange.emit(false);
  }

  fileUploader_onUploadError() {
    notify('Unable to upload file.', "error", 3000);
  }

  onPopupHiding() {
    this.fileUploader.instance.reset();
    //console.log('onhiding');
    this.onHiding.emit();
    this.popupVisibleChange.emit(false);
  }

}
