import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { SwiperConfigInterface, SwiperDirective } from 'ngx-swiper-wrapper';

import { SpaFacilityForCardModel } from '../../../../../projects/spaspace-lib/src/shared/models/facility/spa-facility-for-card.model';

@Component({
  selector: 'spa-facility-slider',
  templateUrl: './facility-slider.component.html',
  styleUrls: ['./facility-slider.component.scss']
})
export class FacilitySliderComponent implements OnInit, AfterViewInit {
  //@ViewChild(SwiperDirective) swiper: SwiperDirective;
  @Input() facilities: SpaFacilityForCardModel[];

  index: number = 1;

  //public config: SwiperConfigInterface = {
  //  a11y: true,
  //  direction: 'horizontal',
  //  slidesPerView: 'auto',
  //  loop: false,
  //  spaceBetween: 30,
  //  keyboard: true,
  //  mousewheel: true,
  //  scrollbar: false,
  //  navigation: true,
  //  pagination: false,
  //  speed: 600,
  //  lazy: {
  //    loadPrevNext: true,
  //    loadPrevNextAmount: 1,
  //    loadOnTransitionStart: true
  //  },
  //   //  breakpoints: {
  //   //  // when window width is >= 320px
  //   //  320: {
  //   //    slidesPerView: 1,
  //   //    spaceBetween: 20
  //   //  },
  //   //  // when window width is >= 480px
  //   //  600: {
  //   //    slidesPerView: 2,
  //   //    spaceBetween: 30
  //   //  },
  //   //  // when window width is >= 640px
  //   //  900: {
  //   //     slidesPerView: 3
  //   //  },
  //   //   1200: {
  //   //     slidesPerView: 4
  //   //   }
  //   //}
  //};

  constructor() { }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    //this.swiper.update();
  }

  cardLoaded() {
    //this.swiper.update();
  }
}
