import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistSliderComponent } from './therapist-slider.component';

describe('TherapistSliderComponent', () => {
  let component: TherapistSliderComponent;
  let fixture: ComponentFixture<TherapistSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
