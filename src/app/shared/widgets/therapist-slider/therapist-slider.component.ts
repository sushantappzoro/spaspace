import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SwiperDirective, SwiperConfigInterface, SwiperConfig } from 'ngx-swiper-wrapper';
import { SpaTherapistForCardModel } from '../../../../../projects/spaspace-lib/src/public-api';


@Component({
  selector: 'spa-therapist-slider',
  templateUrl: './therapist-slider.component.html',
  styleUrls: ['./therapist-slider.component.scss']
})
export class TherapistSliderComponent implements OnInit {

  //@ViewChild(SwiperDirective) swiper: SwiperDirective;
  @Input() therapists: SpaTherapistForCardModel[];

  index: number = 1;

  //public config: SwiperConfigInterface = {
  //  slidesPerView: 'auto',
  //  autoHeight: true,
  //  spaceBetween: 30,
  //  keyboard: true,
  //  mousewheel: true,
  //  scrollbar: false,
  //  navigation: false,
  //  pagination: false,
  //  speed: 600,
  //   //  breakpoints: {
  //   //  // when window width is >= 320px
  //   //  320: {
  //   //    slidesPerView: 1,
  //   //    spaceBetween: 20
  //   //  },
  //   //  // when window width is >= 480px
  //   //  600: {
  //   //    slidesPerView: 2,
  //   //    spaceBetween: 30
  //   //  },
  //   //  // when window width is >= 640px
  //   //  900: {
  //   //     slidesPerView: 3
  //   //  },
  //   //   1200: {
  //   //     slidesPerView: 4
  //   //   }
  //   //}
  //};

   //public swiperConfig = new SwiperConfig({
   //  // Default parameters
   //  slidesPerView: 'auto',
   //  spaceBetween: 10,
   //  // Responsive breakpoints
   //  //breakpoints: {
   //  //  // when window width is >= 320px
   //  //  320: {
   //  //    slidesPerView: 2,
   //  //    spaceBetween: 20
   //  //  },
   //  //  // when window width is >= 480px
   //  //  480: {
   //  //    slidesPerView: 3,
   //  //    spaceBetween: 30
   //  //  },
   //  //  // when window width is >= 640px
   //  //  640: {
   //  //    slidesPerView: 4,
   //  //    spaceBetween: 40
   //  //  }
   //  //}
   //})

  constructor() { }

  ngOnInit() {
    var swiper = new SwiperConfig({
      // Default parameters
      slidesPerView: 1,
      spaceBetween: 10,
      // Responsive breakpoints
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        // when window width is >= 480px
        480: {
          slidesPerView: 3,
          spaceBetween: 30
        },
        // when window width is >= 640px
        640: {
          slidesPerView: 4,
          spaceBetween: 40
        }
      }
    })
  }

  ngAfterViewInit() {
    //this.swiper.update();
  }

  cardLoaded() {
    //this.swiper.update();
  }

  therapistClick(e) {

  }

}
