import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spa-how-they-booked',
  templateUrl: './how-they-booked.component.html',
  styleUrls: ['./how-they-booked.component.scss']
})
export class HowTheyBookedComponent implements OnInit {
  @Input() howTheyBooked: string;

  constructor() { }

  ngOnInit() {
  }

}
