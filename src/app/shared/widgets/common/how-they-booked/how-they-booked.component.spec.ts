import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowTheyBookedComponent } from './how-they-booked.component';

describe('HowTheyBookedComponent', () => {
  let component: HowTheyBookedComponent;
  let fixture: ComponentFixture<HowTheyBookedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowTheyBookedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowTheyBookedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
