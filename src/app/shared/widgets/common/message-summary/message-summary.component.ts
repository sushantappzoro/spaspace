import { Component, OnInit, Input } from '@angular/core';
import { MessageForListModel } from '../../../models/common/message-for-list.model';

@Component({
  selector: 'spa-message-summary',
  templateUrl: './message-summary.component.html',
  styleUrls: ['./message-summary.component.scss']
})
export class MessageSummaryComponent implements OnInit {
  @Input() messages: MessageForListModel[];

  constructor() { }

  ngOnInit() {
  }

  getSenderimage(uid: string): string {
    var url = this.messages.find(x => x.UID == uid).SenderPhotoUrl;

    if (url && url.length > 0) {
      return url;
    }

    return '/spa/assets/images/placeholder-person.jpg';
  }

}
