import { Component, OnInit, Output, EventEmitter, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ModalDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'spa-delete-warning-modal',
  templateUrl: './delete-warning-modal.component.html',
  styleUrls: ['./delete-warning-modal.component.scss']
})
export class DeleteWarningModalComponent implements OnInit, OnChanges {
  @Input() showWarning: boolean;
  @Output() continue: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() showWarningChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('warningModal') warningModal: ModalDirective;

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.warningModal) {
      if (changes.showWarning?.currentValue && changes.showWarning.currentValue == true) {
        this.warningModal.show();
      } else {
        this.warningModal.hide();
      }
    }
  }

  clickYes() {
    this.continue.emit(true);
    this.showWarningChange.emit(false);
  }

  clickNo() {
    this.continue.emit(false);
    this.showWarningChange.emit(false);
  }

}
