import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TherapistCardComponent } from './therapist-card/therapist-card.component';
import { FacilitySliderComponent } from './facility-slider/facility-slider.component';
import { SwiperModule, SWIPER_CONFIG, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { TherapistSliderComponent } from './therapist-slider/therapist-slider.component';
import { FileUploadPopupComponent } from './file-upload-popup/file-upload-popup.component';
import { DevexModuleModule } from '../devex-module.module';
import { FacilityImagesViewerComponent } from './facility/facility-images-viewer/facility-images-viewer.component';
import { FacilitySummaryBlockComponent } from './facility/facility-summary-block/facility-summary-block.component';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { SampleCardComponent } from './sample-card/sample-card.component';
import { FacilityAmenityCardComponent } from './facility/facility-amenity-card/facility-amenity-card.component';
import { DeleteWarningModalComponent } from './common/delete-warning-modal/delete-warning-modal.component';
import { ClientQuickViewComponent } from './client/client-quick-view/client-quick-view.component';
import { AppointmentDescriptionComponent } from './appointment/appointment-description/appointment-description.component';
import { AppointmentTherapistInfoComponent } from './appointment/appointment-therapist-info/appointment-therapist-info.component';
import { MessageSummaryComponent } from './common/message-summary/message-summary.component';
import { AppointmentCardComponent } from './appointment/appointment-card/appointment-card.component';
import { FacilityAmenityRowComponent } from './facility/facility-amenity-row/facility-amenity-row.component';
import { HowTheyBookedComponent } from './common/how-they-booked/how-they-booked.component';
import { SpaSpaceSharedModule } from '../../../../projects/spaspace-lib/src/public-api';
import { ClientDetailViewComponent } from './client/client-detail-view/client-detail-view.component';
import { TherapistDetailViewComponent } from './therapist/therapist-detail-view/therapist-detail-view.component';
//import { TherapistAppointmentListComponent } from '../components/therapist/therapist-appointment-list/therapist-appointment-list.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

@NgModule({
    declarations: [
      TherapistCardComponent,
      FacilitySliderComponent,
      TherapistSliderComponent,
      FileUploadPopupComponent,
      FacilityImagesViewerComponent,
      FacilitySummaryBlockComponent,
      SampleCardComponent,
      FacilityAmenityCardComponent,
      DeleteWarningModalComponent,
      ClientQuickViewComponent,
      AppointmentDescriptionComponent,
      AppointmentTherapistInfoComponent,
      MessageSummaryComponent,
      AppointmentCardComponent,
      FacilityAmenityRowComponent,
      HowTheyBookedComponent,
      ClientDetailViewComponent,
      TherapistDetailViewComponent,
      //TherapistAppointmentListComponent
    ],
    imports: [
      CommonModule,
      ReactiveFormsModule,
      FormsModule,
      FlexLayoutModule,
      MDBBootstrapModulesPro,
      FontAwesomeModule,
      NgbModule,
      MatCardModule,
      MatButtonModule,
      MatDividerModule,
      DevexModuleModule,
      SwiperModule,
      SpaSpaceSharedModule
   
    ],
    exports:[
      TherapistCardComponent,
      FacilitySliderComponent,
      TherapistSliderComponent,
      FileUploadPopupComponent,
      FacilityImagesViewerComponent,
      FacilitySummaryBlockComponent,
      SampleCardComponent,
      FacilityAmenityCardComponent,
      DeleteWarningModalComponent,
      ClientQuickViewComponent,
      AppointmentDescriptionComponent,
      AppointmentTherapistInfoComponent,
      MessageSummaryComponent,
      AppointmentCardComponent,
      HowTheyBookedComponent,
      ClientDetailViewComponent,
      TherapistDetailViewComponent,
      //TherapistAppointmentListComponent
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class WidgetsModule { }
