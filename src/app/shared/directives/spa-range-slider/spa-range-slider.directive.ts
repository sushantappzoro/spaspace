import { Directive, HostListener, ElementRef, OnInit, Renderer2, HostBinding } from '@angular/core';

@Directive({
  selector: '[spaRangeSlider]'
})
export class SpaRangeSliderDirective implements OnInit {

    @HostBinding('style.backgroundColor') backgroundColor: string = 'transparent';

    @HostListener('click') onClick(eventData: Event) {

    }

    @HostListener('mouseenter') mouseover(eventData: Event) {
        //this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'blue');
        this.backgroundColor = 'blue'
    }

    @HostListener('mouseleave') mouseoff(eventData: Event) {
        //this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'green');
        this.backgroundColor = 'transparent'
    }

    constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

    ngOnInit() {
        this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'blue');
    }
}


//var sheet = document.createElement('style'),
//    $rangeInput = $('.range input'),
//    prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

//document.body.appendChild(sheet);

//var getTrackStyle = function (el) {
//    var curVal = el.value,
//        val = (curVal - 1) * 16.666666667,
//        style = '';

//    // Set active label
//    $('.range-labels li').removeClass('active selected');

//    var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

//    curLabel.addClass('active selected');
//    curLabel.prevAll().addClass('selected');

//    // Change background gradient
//    for (var i = 0; i < prefs.length; i++) {
//        style += '.range {background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #fff ' + val + '%, #fff 100%)}';
//        style += '.range input::-' + prefs[i] + '{background: linear-gradient(to right, #37adbf 0%, #37adbf ' + val + '%, #b2b2b2 ' + val + '%, #b2b2b2 100%)}';
//    }

//    return style;
//}

//$rangeInput.on('input', function () {
//    sheet.textContent = getTrackStyle(this);
//});

//// Change input value on label click
//$('.range-labels li').on('click', function () {
//    var index = $(this).index();

//    $rangeInput.val(index + 1).trigger('input');

//});
