import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaRangeSliderDirective } from './spa-range-slider/spa-range-slider.directive';
import { NumberOfAppointmentsDirective } from './number-of-appointments/number-of-appointments.directive';

@NgModule({
  declarations: [
    SpaRangeSliderDirective,
    NumberOfAppointmentsDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SpaRangeSliderDirective,
    NumberOfAppointmentsDirective
  ]
})
export class DirectivesModule { }
