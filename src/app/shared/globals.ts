import { Options, LabelType  } from 'ng5-slider';

//export const TherapistProficiencyValues: Options = {
//  showTicksValues: true,
//  stepsArray: [
//    { value: 1, legend: 'Novice' },
//    { value: 2, legend: 'Advanced Beginner' },
//    { value: 3, legend: 'Competent' },
//    { value: 4, legend: 'Proficient' },
//    { value: 5, legend: 'Expert' }
//  ]
//};
export const TherapistProficiencyValues: Options = {
  showTicksValues: true,
  //floor: 1,
  //ceil: 5,
  stepsArray: [
    { value: 1, legend: 'Novice' },
    { value: 2 },
    { value: 3 },
    { value: 4 },
    { value: 5, legend: 'Expert' }
  ],
  //translate: (value: number): string => {
  //  switch (value) {
  //    case 1:
  //      return 'Novice';
  //    case 2:
  //      return 'Advanced Beginner';
  //    case 3:
  //      return 'Competent';
  //    case 4:
  //      return 'Proficient';
  //    case 5:
  //      return 'Expert';
  //    default:
  //  }
    
  //}
};


//export const TherapistPressureValues: Options = {
//  showTicksValues: true,
//  disabled: false,
//  stepsArray: [
//    { value: 1, legend: 'Light' },
//    { value: 2, legend: 'Light/Medium' },
//    { value: 3, legend: 'Medium' },
//    { value: 4, legend: 'Medium/Deep' },
//    { value: 5, legend: 'Deep' }
//  ]
//};

export const TherapistPressureValues: Options = {
  showTicksValues: true,
  disabled: false,
  stepsArray: [
    { value: 1, legend: 'Light' },
    { value: 2 },
    { value: 3, legend: 'Medium'  },
    { value: 4 },
    { value: 5, legend: 'Deep' }
  ]
};


export const MaxImageSizeForUpload: number = 10400000;
export const InvalidMaxFileSizeMessage: string = 'File too large.  Please limit image size to 10mb or less.'
