import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxFileUploaderComponent } from 'devextreme-angular';
import { UserProfileForViewEditModel } from '../../../models/user/user-profile-for-view-edit.model';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../../../core/services/user/users.service';
import { MaxImageSizeForUpload, InvalidMaxFileSizeMessage } from '../../../globals';
import { configSettings } from '../../../../app.config';
import { CustomValidator } from '../../../classes/custom-validator';
import { AuthorizeService } from 'src/api-authorization/authorize.service';

@Component({
  selector: 'spa-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})

export class UserProfileComponent implements OnInit {
  @ViewChild("imageUploader") imageUploader: DxFileUploaderComponent;

  private subs = new SubSink();

  userProfile: UserProfileForViewEditModel;

  photoUrl: string;
  userProfileForm: FormGroup;
  //phonePattern: any = /^\+\s*1\s*\(\s*[02-9]\d{2}\)\s*\d{3}\s*-\s*\d{4}$/;

  //showSaveButton: boolean = false;
  showSubmitButton: boolean = false;
  uploadDisabled: boolean = false;

  images: any[] = [];
  fileUploadApiPath: string;

  maxLength = 200;

  headers: any;

  showPasswordResetModal: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private userProfileService: UsersService,
    private authService: AuthorizeService,
  ) { }

  ngOnInit() {

    this.subs.add(this.authService.getAccessToken()
    .subscribe(token => {
      this.headers = { 'Authorization': 'Bearer ' + token };
    }));

    this.userProfileForm = new FormGroup({
      UID: new FormControl(''),
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      Email: new FormControl('', [Validators.required, Validators.email]),
      Phone: new FormControl('', [Validators.required, CustomValidator.phoneValidator]),
    });

    this.subs.add(this.route.data
      .subscribe((data: { userProfile: UserProfileForViewEditModel }) => {
        this.userProfile = data.userProfile;
        this.photoUrl = this.userProfile.ProfilePhotoURL;
        this.userProfileForm.patchValue(this.userProfile);
        this.fileUploadApiPath = configSettings.WebAPI_URL + '/auth/users/' + this.userProfile.UID + '/profile/photo';
      },
        error => {
          console.log(error);
        }));

  }

  //ngOnChanges(changes: SimpleChanges) {
  //  if (changes.userProfile?.currentValue) {
  //    this.userProfileForm.patchValue(changes.userProfile);
  //  }
  //}

  onSave() {

    if (this.userProfileForm.invalid) {
      return;
    }

    var profile = new UserProfileForViewEditModel();
    profile = Object.assign({}, this.userProfile, this.userProfileForm.value);

    this.userProfileService.editProfile(profile.UID, profile)
      .subscribe(res => {
        this.userProfile = res;
        this.userProfileForm.markAsPristine();
        if (res.UpdateSuccessful == true) {
          notify(res.UpdateMessage, "success", 3000);
        } else {
          notify(res.UpdateMessage, "error", 3000);
        }
      },
        error => {
          console.log('error saving profile', error);
          notify('Unable to save user profile.', "error", 3000);
        });

  }

  getUserPicture() {
    //return this.photoUrl;
    if (this.photoUrl && this.photoUrl.length > 0) {
      return this.photoUrl;
    } else {
      return '/assets/images/placeholder-person.jpg';
    }
  }

  getMaxFileSize() {
    return MaxImageSizeForUpload;
  }

  getMaxFileSizeMessage() {
    return InvalidMaxFileSizeMessage;
  }


  fileUploader_onUploaded(e): void {
    //console.log('photouploaded', e);
    this.imageUploader.instance.reset();

    var response: UserProfileForViewEditModel = JSON.parse(e.request.response);

    if (response) {
      //console.log('response.Url', response.ProfilePhotoURL);
      this.photoUrl = response.ProfilePhotoURL;

    }

    notify('Therapist photo uploaded successfully.', "success", 3000);
  }

  fileUploader_onUploadError(e): void {
    notify('Unable to upload therapist photo.', "error", 3000);
  }

  fileUploader_onValueChanged(e) {
    // console.log('fileUploader_onValueChanged', e);

  }

  startResetPassword() {
    // console.log('startResetPassword');
    this.showPasswordResetModal = true;
  }

}
