import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { CustomValidator } from '../../../classes/custom-validator'
import { ModalDirective } from 'ng-uikit-pro-standard';
import { AuthorizeService, IUser } from '../../../../../api-authorization/authorize.service';
import { UsersService } from '../../../../core/services/user/users.service';
import { PasswordChangeRequest } from 'src/app/shared/models/user/password-change-request.model';

@Component({
  selector: 'spa-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit, OnChanges {
  @Input() showModal: boolean;
  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('passwordModal') passwordModal: ModalDirective;

  private subs = new SubSink();

  resetPasswordForm: FormGroup;
  passwordChangeRequest: PasswordChangeRequest;

  user: IUser;

  constructor(
    private authService: AuthorizeService,
    private userService: UsersService
  ) { }

  ngOnInit() {

    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.user = user;
      })
    );

    this.resetPasswordForm = new FormGroup({
      password: new FormControl('', [ Validators.required ]),
      confirm: new FormControl('', [ Validators.required, this.confirmPasswordValidator ]),
    },
    //{
    //  validators: [ CustomValidator.matchPassword ]
    //}
    );

    this.resetPasswordForm.setValidators(this.confirmPasswordValidator);

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    if (changes.showModal && this.passwordModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.passwordModal.show();
      } else {
        this.passwordModal.hide();
      }
    }

  }

  confirmPasswordValidator(formControl: AbstractControl) {

    if (!formControl || !formControl.parent) {
      return null;
    }

    var password = (formControl.parent.get('password') && formControl.parent.get('password').value) ? formControl.parent.get('password').value : null;
    var confirm = (formControl.parent.get('confirm') && formControl.parent.get('confirm').value) ? formControl.parent.get('confirm').value : null;

    if (password != confirm) {
      return { notMatched: true };
    }
    return null;

  }

  onClose() {

    this.resetPasswordForm.reset();
    this.showModalChange.emit(false);
  }

  onSubmit() {

    if (this.resetPasswordForm.invalid) {
      this.resetPasswordForm.markAllAsTouched();
      return;
    }

    this.passwordChangeRequest = new PasswordChangeRequest();
    this.passwordChangeRequest.UID = this.user.sub;
    this.passwordChangeRequest.NewPassword = this.resetPasswordForm.value.password;


    this.subs.add(this.userService.updatePassword(this.passwordChangeRequest)
      .subscribe(resp => {
        if (resp == true) {
          notify('Password updated.', 'success', 3000);
          this.showModalChange.emit(false);
        } else {
          notify('Unable to update password.', 'error', 3000);
        }
      },
        error => {
          notify('Unable to update password.', 'error', 3000);
        })
    );
  }

}
