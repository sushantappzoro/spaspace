import { Component, OnInit, Input, OnDestroy, OnChanges, Output, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { TherapistForFacilityScheduleViewModel } from '../../../../shared/models/facility/therapist-for-facility-schedule-view.model';
import { FacilityTreatmentAreaViewModel } from '../../../../shared/models/facility/facility-treatment-area-view.model';
import { FacilityTreatmentAreasService } from '../../../../core/services/facility/facility-treatment-areas.service';
import { FacilityTherapistService } from '../../../../core/services/facility/facility-therapist.service';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FacilityAppointmentService } from '../../../../core/services/facility/facility-appointment.service';
import { AppointmentForFacilityDetailModel } from '../../../../shared/models/appointment/appointment-for-facility-detail.model';
import { AppointmentChangeRequestModel } from '../../../../shared/models/appointment/appointment-change-request.model';

@Component({
  selector: 'spa-facility-appointment-change',
  templateUrl: './facility-appointment-change.component.html',
  styleUrls: ['./facility-appointment-change.component.scss']
})
export class FacilityAppointmentChangeComponent implements OnInit, OnDestroy, OnChanges {
  @Input() showModal: boolean;
  @Input() facilityUID: string;
  @Input() appointment: AppointmentForFacilityDetailModel;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() appointmentChange: EventEmitter<AppointmentForFacilityDetailModel> = new EventEmitter<AppointmentForFacilityDetailModel>();

  @ViewChild('changeModal') changeModal: ModalDirective;

  private subs = new SubSink();

  therapists: TherapistForFacilityScheduleViewModel[] = [];
  treatmentAreas: FacilityTreatmentAreaViewModel[] = [];

  changeForm: FormGroup;

  appointmentTime: Date;

  constructor(
    private treatmentAreaService: FacilityTreatmentAreasService,
    private therapistsService: FacilityTherapistService,
    private appointmentService: FacilityAppointmentService
  ) { }

  ngOnInit() {

    if (this.facilityUID) {
      this.loadAreas(this.facilityUID);
      this.loadTherapists(this.facilityUID);
    }

    this.changeForm = new FormGroup({
      AppointmentDate: new FormControl(),
      TherapistUID: new FormControl('', Validators.required),
      TreatmentAreaUID: new FormControl('', Validators.required),
      AppointmentTime: new FormControl('', Validators.required)
    });

    if (this.appointment && this.changeForm) {
      //console.log('appointment', this.appointment);
      if (this.appointment.StartDateTime) {
        this.changeForm.controls['AppointmentDate'].patchValue(this.appointment.StartDateTime);
        //this.changeForm.controls['AppointmentTime'].patchValue(this.appointment.StartDateTime);
        this.appointmentTime = this.appointment.StartDateTime;
      }
      if (this.appointment.Therapist) {
        this.changeForm.controls['TherapistUID'].patchValue(this.appointment.Therapist.UID);
      }
      if (this.appointment.TreatmentArea && this.treatmentAreas) {
        console.log('treatmentAreas', this.treatmentAreas);
        let area = this.treatmentAreas.find(x => x.Name == this.appointment.TreatmentArea);
        console.log('area', area);
        if (area) {
          this.changeForm.controls['TreatmentAreaUID'].patchValue(area.UID);
        }
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.changeModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue === true) {
        this.changeModal.show();
      } else {
        this.changeModal.hide();
      }
    }

    if (changes.facilityUID) {
      if (changes.facilityUID?.currentValue && changes.facilityUID.currentValue.length > 0) {
        this.loadAreas(changes.facilityUID.currentValue);
        this.loadTherapists(changes.facilityUID.currentValue);
      }
    }

    if (changes.appointment && this.changeForm) {

      if (this.appointment.StartDateTime) {
        this.changeForm.controls['AppointmentDate'].patchValue(this.appointment.StartDateTime);
        //this.changeForm.controls['AppointmentTime'].patchValue(this.appointment.StartDateTime);
        this.appointmentTime = this.appointment.StartDateTime;
      }

      if (changes.appointment.currentValue.Therapist) {
        this.changeForm.controls['TherapistUID'].patchValue(this.appointment.Therapist.UID);
      }
      if (changes.appointment.currentValue.TreatmentArea && this.treatmentAreas) {
        let area = this.treatmentAreas.find(x => x.Name == changes.appointment.currentValue.TreatmentArea);
        if (area) {
          this.changeForm.controls['TreatmentAreaUID'].patchValue(area.UID);
        }
      }
    }

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadAreas(facilityUID: string) {

    if (!this.treatmentAreas || this.treatmentAreas.length == 0) {
      this.subs.add(this.treatmentAreaService.getTreatmentAreas(facilityUID)
        .subscribe(areas => {
          this.treatmentAreas = areas;

          if (this.appointment.TreatmentArea && this.treatmentAreas) {
            //console.log('treatmentAreas', this.treatmentAreas);
            let area = this.treatmentAreas.find(x => x.Name == this.appointment.TreatmentArea);
            //console.log('area', area);
            if (area) {
              this.changeForm.controls['TreatmentAreaUID'].patchValue(area.UID);
            }
          }

        },
          error => {
            console.log(error);
          }));
    }

  }

  loadTherapists(facilityUID: string) {

    if (!this.therapists || this.therapists.length == 0) {
      this.subs.add(this.therapistsService.getAssociatedTherapists(facilityUID)
        .subscribe(therapists => {
          this.therapists = therapists;
        },
          error => {
            console.log(error);
          }));
    }

  }

  onSave() {

    let change = new AppointmentChangeRequestModel();
    change.NewTreatmentAreaUID = this.changeForm.value.TreatmentAreaUID;
    change.NewTherapistUID = this.changeForm.value.TherapistUID;

    //change.NewTime = new Date(this.changeForm.value.AppointmentDate);

    console.log('time=' + this.appointmentTime);
    let time = new Date(this.appointmentTime);
    console.log('time=' + time);
    let outDate = new Date(this.changeForm.value.AppointmentDate);

    let finalDate = new Date(outDate.getFullYear(), outDate.getMonth(), outDate.getDate(), time.getHours(), time.getMinutes());
    finalDate.setMinutes(finalDate.getMinutes() - finalDate.getTimezoneOffset());

    console.log('time=' + finalDate);
    change.NewTime = finalDate;

    //tempdate = new Date(item.StartTime);
    //tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    //item.StartTime = tempdate;

    console.log('change', change);
    this.subs.add(this.appointmentService.changeAppointment(this.facilityUID, this.appointment.UID, change)
      .subscribe(appointment => {
        this.appointmentChange.emit(appointment);
        this.showModalChange.emit(false);
        this.changeForm.reset();
      },
        error => {
          console.log(error);
          notify('Unable to save changes.', "error", 3000);
        }));
  }

  onClose() {
    this.changeForm.reset();
    this.showModalChange.emit(false);
  }
}
