import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentChangeComponent } from './facility-appointment-change.component';

describe('FacilityAppointmentChangeComponent', () => {
  let component: FacilityAppointmentChangeComponent;
  let fixture: ComponentFixture<FacilityAppointmentChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
