import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityManagerProfileEditComponent } from './facility-manager-profile-edit.component';

describe('FacilityManagerProfileEditComponent', () => {
  let component: FacilityManagerProfileEditComponent;
  let fixture: ComponentFixture<FacilityManagerProfileEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityManagerProfileEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityManagerProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
