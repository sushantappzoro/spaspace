import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { FacilityManagerForAdminModel } from '../../../models/facility/facility-manager-for-admin.model';
import { FacilityManagerService } from '../../../../core/services/facility/facility-manager.service';
import { FacilityManagerForEditModel } from '../../../models/facility/facility-manager-for-edit.model';
import { CustomValidator } from '../../../classes/custom-validator';

@Component({
  selector: 'spa-facility-manager-profile-edit',
  templateUrl: './facility-manager-profile-edit.component.html',
  styleUrls: ['./facility-manager-profile-edit.component.scss']
})
export class FacilityManagerProfileEditComponent implements OnInit, AfterViewInit {
  @Input() facilityUID: string;
  @Input() facilityManager: FacilityManagerForAdminModel;
  @Input() canEdit: boolean;
  @Input() showSaveButton: boolean;

  personalInfoForm: FormGroup;

  constructor(private facilityManagerService: FacilityManagerService) { }

  ngOnInit() {

    this.personalInfoForm = new FormGroup({
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      Email: new FormControl('', Validators.required),
      PhoneNumber: new FormControl('', [Validators.required, CustomValidator.phoneValidator]),
      Active: new FormControl(''),
      Status: new FormControl(''),
      DisplayName: new FormControl(''),
      UID: new FormControl(''),
    });

  }

  ngAfterViewInit() {
    this.showProfile();
  }

  showProfile() {


    if (this.facilityManager && this.personalInfoForm) {
      this.personalInfoForm.setValue(this.facilityManager);

      if (!this.canEdit ) {
        this.personalInfoForm.controls['FirstName'].disable();
        this.personalInfoForm.controls['LastName'].disable();
        this.personalInfoForm.controls['Email'].disable();
        this.personalInfoForm.controls['PhoneNumber'].disable();
        this.personalInfoForm.controls['DisplayName'].disable();
        this.personalInfoForm.controls['Active'].disable();
      }
    }
  }

  onSave() {

    var newFacilityMgr = new FacilityManagerForEditModel();

    newFacilityMgr = Object.assign({}, this.facilityManager, this.personalInfoForm.value);

    this.facilityManagerService.edit(this.facilityUID, newFacilityMgr)
      .subscribe(res => {
        //this.facilityManager = res;
        
        this.personalInfoForm.markAsPristine();

      },
        error => {
          notify('Unable to save facility manager profile.', "error", 3000);
        });

  }

}
