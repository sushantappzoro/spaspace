import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityProfileEditComponent } from './facility-profile-edit.component';

describe('FacilityProfileEditComponent', () => {
  let component: FacilityProfileEditComponent;
  let fixture: ComponentFixture<FacilityProfileEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityProfileEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
