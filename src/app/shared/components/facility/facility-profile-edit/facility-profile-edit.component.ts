import { Component, OnInit, Input, OnDestroy, AfterViewInit, ViewChild, OnChanges } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { StepperControlService } from '../../../../core/services/stepper-control.service';
import { FacilityInfoForAdminModel } from '../../../models/facility/facility-info-for-admin.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DxValidationGroupComponent } from 'devextreme-angular';
import { FacilityInfoService } from '../../../../core/services/facility/facility-info.service';
import { StateModel, StatesService } from '../../../../core/services/states.service';
import { CustomValidator } from '../../../classes/custom-validator';
import { TimeZoneModel } from 'src/app/shared/models/facility/time-zone.model';
import { FacilityService } from 'src/app/core/services/facility/facility.service';


@Component({
  selector: 'spa-facility-profile-edit',
  templateUrl: './facility-profile-edit.component.html',
  styleUrls: ['./facility-profile-edit.component.scss']
})
export class FacilityProfileEditComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() showSaveButton: boolean
  @Input() showSubmitButton: boolean
  @Input() canEdit: boolean = false;
  @Input() canSubmit: boolean = false;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;
  @Input() facility: FacilityInfoForAdminModel;

  private subs = new SubSink();

  personalInfoForm: FormGroup;

  states: StateModel[];
  timezones: TimeZoneModel[];

  constructor(
    private stepperService: StepperControlService,
    private facilityInfoService: FacilityInfoService,
    private stateService: StatesService,
    private facilityService: FacilityService
  ) { }

  ngOnInit() {

    this.states = this.stateService.getStates();
    this.subs.add(this.facilityService.getAllTimeZones()
      .subscribe(zones => {
        this.timezones = zones;
      },
      error => { console.log(error); } ));

    this.personalInfoForm = new FormGroup({
      Address1: new FormControl('', Validators.required),
      Address2: new FormControl(),
      City: new FormControl('', Validators.required),
      StateCode: new FormControl('', Validators.required),
      ZipCode: new FormControl('', [Validators.required, CustomValidator.zipCodeValidator]),
      Description: new FormControl('', Validators.required),
      ShortDescription: new FormControl('', Validators.required),
      PhoneNumber: new FormControl('', [Validators.required, CustomValidator.phoneValidator]),
      Name: new FormControl('', Validators.required),
      URL: new FormControl('', Validators.required),
      Latitude: new FormControl(''),
      Longitude: new FormControl(''),
      TimeZoneID: new FormControl('Eastern Standard Time', Validators.required),
      //Status: new FormControl(''),
      UID: new FormControl(''),
    });

    this.showProfile();

    if (this.runAsStep) {
      this.subs.add(this.stepperService.getValidationStep()
        .subscribe(step => {
          if (step === this.stepNumber) {
            this.performValidation();
          }
        }));
    }
  }

  ngAfterViewInit() {
    //this.showProfile();
  }

  ngOnChanges() {
    this.showProfile();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  showProfile() {


    if (this.facility && this.personalInfoForm) {

      if (!this.facility.PhoneNumber) {
        this.facility.PhoneNumber = '';
      }

      console.log('the facility', this.facility);
      this.personalInfoForm.patchValue(this.facility);

      if (this.canEdit) {
        //this.showSaveButton = true;
        this.showSubmitButton = false;
      } else {
        this.personalInfoForm.controls['Address1'].disable();
        this.personalInfoForm.controls['Address2'].disable();
        this.personalInfoForm.controls['City'].disable();
        this.personalInfoForm.controls['StateCode'].disable();
        this.personalInfoForm.controls['ZipCode'].disable();
        this.personalInfoForm.controls['Description'].disable();
        this.personalInfoForm.controls['ShortDescription'].disable();
        this.personalInfoForm.controls['PhoneNumber'].disable();
        this.personalInfoForm.controls['Name'].disable();
        this.personalInfoForm.controls['URL'].disable();
        this.personalInfoForm.controls['Latitude'].disable();
        this.personalInfoForm.controls['Longitude'].disable();

        this.showSubmitButton = false;
      }

      this.showSubmitButton = this.canSubmit;

    }

  }

  performValidation() {

    if (!this.personalInfoForm.errors && !this.personalInfoForm.dirty && this.personalInfoForm.valid) {
      this.stepperService.completeStep(this.stepNumber);
      return;
    }

    if (this.personalInfoForm.errors || !this.personalInfoForm.valid) {
      this.personalInfoForm.markAllAsTouched();
      this.stepperService.reportError('Cannot proceed to next step until all required fields are completed.');
      return;
    }

    if (this.personalInfoForm.dirty) {
      this.onSave();
      return;
    }

  }

  onSave() {

    var newFacility = new FacilityInfoForAdminModel();
    newFacility = Object.assign({}, this.facility, this.personalInfoForm.value);

    this.facilityInfoService.editFacilityInfo(this.facility.UID, newFacility)
      .subscribe(res => {
        this.facility = res;
        //this.isCompleteChange.emit(true);
        this.personalInfoForm.markAsPristine();
        if (this.runAsStep) {
          this.stepperService.completeStep(this.stepNumber);
        } else {
          notify('Facility updated successfully.', "success", 3000);
        }
      },
        error => {
          if (this.runAsStep) {
            this.stepperService.reportError('Unable to save facility profile.');
          } else {
            notify('Unable to save facility profile.', "error", 3000);
          }
        });

  }

}
