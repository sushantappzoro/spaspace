import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DxTabsComponent } from 'devextreme-angular';
import { FacilityInfoForAdminModel } from '../../../models/facility/facility-info-for-admin.model';


export class Longtab {
  text: string;
}

@Component({
  selector: 'spa-facility-detail',
  templateUrl: './facility-detail.component.html',
  styleUrls: ['./facility-detail.component.scss']
})
export class FacilityDetailComponent implements OnInit {
  @Input() facility: FacilityInfoForAdminModel;
  @ViewChild("tabMenu") tabMenu: DxTabsComponent;

  subscription: any;

  constructor(

  ) { }

  ngOnInit() {

  }
}
