import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentsByRoomViewComponent } from './facility-appointments-by-room-view.component';

describe('FacilityAppointmentsByRoomViewComponent', () => {
  let component: FacilityAppointmentsByRoomViewComponent;
  let fixture: ComponentFixture<FacilityAppointmentsByRoomViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentsByRoomViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentsByRoomViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
