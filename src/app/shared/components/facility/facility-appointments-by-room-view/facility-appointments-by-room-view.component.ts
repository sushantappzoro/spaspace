import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Query from 'devextreme/data/query';
import { map } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { DxSchedulerModule } from 'devextreme-angular';
import CustomStore from "devextreme/data/custom_store";
import { createStore } from 'devextreme-aspnet-data-nojquery';
import { data } from 'devextreme/core/element_data';

import { configSettings } from '../../../../app.config';
import * as AspNetData from "devextreme-aspnet-data-nojquery";
import { FacilityScheduleService } from '../../../../core/services/facility/facility-schedule.service';
import { AvailabilityForAppointmentModel } from '../../../../shared/models/appointment/availability-for-appointment.model';
import { AppointmentForByroomViewModel } from '../../../../shared/models/appointment/appointment-for-byroom-view.model';
import { start } from 'repl';
import { FacilityTreatmentAreasService } from '../../../../core/services/facility/facility-treatment-areas.service';
import { FacilityTreatmentAreaViewModel } from '../../../../shared/models/facility/facility-treatment-area-view.model';
import { AppointmentForTherapistViewModel } from '../../../../shared/models/appointment/appointment-for-therapist-view.model';
import { SpinnerOverlayService } from '../../../../core/services/common/spinner-overlay.service';

@Component({
  selector: 'spa-facility-appointments-by-room-view',
  templateUrl: './facility-appointments-by-room-view.component.html',
  styleUrls: ['./facility-appointments-by-room-view.component.scss']
})
export class FacilityAppointmentsByRoomViewComponent implements OnInit, OnDestroy {
  @Input() facilityUID: string;
  @Output() showDetail: EventEmitter<string> = new EventEmitter<string>();

  private subs = new SubSink();

  treatmentAreas: FacilityTreatmentAreaViewModel[] = [];

  byroomData: AppointmentForByroomViewModel[] = [];
  treatmentAreasAvailability: AvailabilityForAppointmentModel[] = [];

  currentDate: Date = new Date();

  //byroomURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/byroom?loadOptions='
  //bytherapistURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/bytherapistURL?loadOptions='

  byroomURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/byroom'

  constructor(
    private route: ActivatedRoute,
    private facilityScheduleService: FacilityScheduleService,
    private treatmentAreasService: FacilityTreatmentAreasService,
    private spinnerService: SpinnerOverlayService,
  ) { }

  ngOnInit() {

    this.spinnerService.show();

    //this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
    this.byroomURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/byroom'

    // get the list of treatment rooms for grouping
    this.subs.add(this.treatmentAreasService.getTreatmentAreas(this.facilityUID)
      .subscribe(resp => {
        this.treatmentAreas = resp;
      })
    )

    this.subs.add(this.facilityScheduleService.getByRoom(this.byroomURL, null)
      .subscribe(resp => {
        this.byroomData = resp.Appointments;
        this.treatmentAreasAvailability = resp.TreatmentAreas;
        //console.log('availability=', this.treatmentAreasAvailability);
        this.spinnerService.hide();
      },
        error => {
          this.spinnerService.hide();
        }
      )
    );



    //this.facilityUID = this.route.params["facilityUID"];

    //this.byroomData = AspNetData.createStore({
    //  key: "ID",
    //  loadUrl: this.byroomURL,
    //  onBeforeSend: function (method, ajaxOptions) {
    //    ajaxOptions.xhrFields = { withCredentials: true };
    //  }
    //});

    //this.bytherapistData = createStore({
    //  key: "ID",
    //  loadUrl: this.bytherapistURL,
    //  onBeforeSend: function (method, ajaxOptions) {
    //    ajaxOptions.xhrFields = { withCredentials: true };
    //  }
    //});

    //this.byroomData = new CustomStore({
    //  key: "UID",
    //  load: (loadOptions) => {
    //    //console.log('loadoptions', loadOptions);
    //    return this.facilityScheduleService.getByRoom(this.byroomURL, loadOptions);
    //  }
    //});

    //this.bytherapistData = new CustomStore({
    //  key: "UID",
    //  load: (loadOptions) => {
    //    return this.facilityScheduleService.getByTherapist(this.bytherapistURL, loadOptions).subscribe()
    //  }
    //});
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  markAvailability(cellData) {
    //console.log('celldata', cellData);

    let markit: Boolean = false;

    if (cellData.groups) {
      markit = this.hasAvailability(cellData.startDate, cellData.endDate, cellData.groups.TreatmentAreaUID)
    }
    var classObject = {};
    classObject['appointment-shading-1'] = markit
    return classObject;

  }

  hasAvailability(startDate, endDate, treatmentAreaUID) {
    //return true;
    try {
     
      if (this.treatmentAreasAvailability) {
        //console.log('treatmentAreaUID=', treatmentAreaUID);
        //var room = this.treatmentAreas.find(room => room.UID = treatmentAreaUID);
        //console.log('room', room);

        var item = this.treatmentAreasAvailability.find(appointment => appointment.UID === treatmentAreaUID && new Date(startDate) >= new Date(appointment.AvailabilityStart) && new Date(endDate) <= new Date(appointment.AvailabilityEnd) )
        //console.log('startdate=' + startDate + ' enddate=' + endDate + ' treatmentAreaUID=' + treatmentAreaUID);
        //console.log('startdate=', this.treatmentAreasAvailability[0]);
        //var item = this.treatmentAreasAvailability.find(appointment =>
        //  appointment.UID === treatmentAreaUID ) // &&
           // (new Date(startDate) >= new Date(appointment.AvailabilityStartTime)) &&
           // (new Date(endDate) <= new Date(appointment.AvailabilityEndTime)));
        //console.log('item', item);
        if (item) { // && item[0]) {
          //console.log('item', item);
          return true;
        } else {
          return false;
        }
      }
    }
    catch {
      //console.log('error')
    }
    return false;
  }

  onAppointmentRendered(e) {
    setTimeout(() => {
      const appointmentData = data(e.appointmentElement);
      const settings = appointmentData.dxAppointmentSettings
      const cellWidth = appointmentData.dxSchedulerAppointment.option("cellWidth");
      const count = settings.count;
      const oldWidth = e.appointmentElement.clientWidth;
      const newWidth = cellWidth / count;
      const diff = newWidth - oldWidth;
      e.appointmentElement.style.transform = "translate(" + (appointmentData.dxTranslator.x + diff * settings.index) + "px, " + appointmentData.dxTranslator.y + "px)";
      e.appointmentElement.style.width = cellWidth / count + "px";
      e.appointmentElement.classList.remove("dx-scheduler-appointment-empty");
    })
  }

  onAppointmentFormOpeningByRoom(e) {
    //console.log('onAppointmentFormOpeningByRoom', e);
    this.showDetail.emit(e.appointmentData.UID);
    e.cancel = true;    
  }

}
