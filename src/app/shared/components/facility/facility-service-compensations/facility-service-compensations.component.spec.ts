import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityServiceCompensationsComponent } from './facility-service-compensations.component';

describe('FacilityServiceCompensationsComponent', () => {
  let component: FacilityServiceCompensationsComponent;
  let fixture: ComponentFixture<FacilityServiceCompensationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityServiceCompensationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityServiceCompensationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
