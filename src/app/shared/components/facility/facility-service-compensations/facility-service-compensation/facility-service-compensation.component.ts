import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { isNumeric } from 'rxjs/util/isNumeric';
import { SubSink } from 'subsink';
import { ModalDirective } from 'ng-uikit-pro-standard';
import notify from 'devextreme/ui/notify';

import { FacilityCompensationService } from '../../../../../core/services/facility/facility-compensation.service';
import { ServiceCategoryModel } from '../../../../models/offerings/service-category.model';
import { FacilityServiceCompensationModel } from '../../../../models/facility/facility-service-compensation.model';

@Component({
  selector: 'spa-facility-service-compensation',
  templateUrl: './facility-service-compensation.component.html',
  styleUrls: ['./facility-service-compensation.component.scss']
})

export class FacilityServiceCompensationComponent implements OnInit, OnChanges, OnDestroy {
  @Input() compensation: FacilityServiceCompensationModel;
  @Input() categories: ServiceCategoryModel[] = [];
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveCompensation: EventEmitter<FacilityServiceCompensationModel> = new EventEmitter<FacilityServiceCompensationModel>();

  @ViewChild('compensationModal') compensationModal: ModalDirective;

  private subs = new SubSink();

  masterCategories: ServiceCategoryModel[] = [];

  services: ServiceSelectModel[] = [];

  title: string = 'Edit Service Compensation';

  compensationForm: FormGroup;

  isEditOnly: boolean;

  constructor(
    private route: ActivatedRoute,
    private compensationService: FacilityCompensationService
  ) { }

  ngOnInit() {

    //this.subs.add(this.route.data
    //  .subscribe((data: { categories: ServiceCategoryModel[] }) => {
    //    this.masterCategories = data.categories;
    //    this.categories = [];
    //    this.masterCategories.forEach(cat => {
    //      this.categories.push({ value: cat.ID.toString(), viewValue: cat.Name })
    //    })
    //  },
    //    error => {
    //      console.log(error);
    //    }));

    this.compensationForm = new FormGroup({
      UID: new FormControl(),
      ServiceUID: new FormControl('', Validators.required),
      BasePrice: new FormControl('', Validators.required),
      TherapistPercentOfBase: new FormControl('', this.EitherOrValidator),
      FaciltyPercentOfBase: new FormControl(''),
      TherapistFlatRate: new FormControl('', this.EitherOrValidator),
      ServiceChargePercent: new FormControl(''),
      TherapistPercentOfServiceCharge: new FormControl(''),
    },
      //{
      //  validators: [this.EitherOrValidator]
      //}
    );

    this.compensationForm.setValidators(this.EitherOrValidator);

    this.title = (this.compensation?.UID) ? 'Edit Service Compensation' : 'Add Service Compensation';

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges', changes);
    if (this.compensationModal && changes.showModal?.currentValue && changes.showModal.currentValue == true) {
      this.compensationModal.show();
    } else {
      this.compensationModal.hide();
    }

    if (changes.compensation && this.compensationForm) {

      if (this.compensation?.UID && this.compensation?.UID.length > 0) {
        //this.compensationForm.controls.ServiceCategoryID.disable();
        this.compensationForm.controls.ServiceUID.disable();
      } else
      {
        this.compensationForm.controls.ServiceUID.enable();
      }
      this.populateForm(this.compensation);
    }

    if (changes.categories) {

      this.loadServices(changes.categories.currentValue);
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  EitherOrValidator(formControl: AbstractControl) {

    if (!formControl || !formControl.parent) {
      return null;
    }

    var percent = (formControl.parent.get('TherapistPercentOfBase') && formControl.parent.get('TherapistPercentOfBase').value) ? formControl.parent.get('TherapistPercentOfBase').value : null;
    var flat = (formControl.parent.get('TherapistFlatRate') && formControl.parent.get('TherapistFlatRate').value) ? formControl.parent.get('TherapistFlatRate').value : null;

    console.log('percent', percent);
    console.log('flat', flat);

    if (percent && flat) {
      return { notEitherOr: true };
    }
    return null;

  }

  //catagorySelectionChange(e) {
  //  if (e && e.value) {
  //    this.services = [];
  //    var cat = this.masterCategories.find(master => master.ID === e.value);
  //    if (cat) {
  //      cat.Services.forEach(service => {
  //        this.services.push({ value: service.UID, viewValue: service.Name });
  //      });
  //    }
  //  }
  //}

  loadServices(categories: ServiceCategoryModel[]) {
    if (categories) {
      categories.forEach(category => {
        category.Services.forEach(service => {
          this.services.push({ value: service.UID, viewValue: service.Name });
        });
      })
    }
  }

  //getPercentageOfBase(rowData) {

  //  if (!isNumeric(rowData.TherapistPercentOfBase)) {
  //    return null;
  //  }

  //  var therapistPercentOfBase: number = rowData.TherapistPercentOfBase;

  //  if (therapistPercentOfBase < 1) {
  //    therapistPercentOfBase = therapistPercentOfBase * 100;
  //  }

  //  return therapistPercentOfBase.toPrecision(2) + '%';

  //}

  populateForm(compensation: FacilityServiceCompensationModel) {
    var holdComp: FacilityServiceCompensationModel = {
      BasePrice: compensation.BasePrice,
      FaciltyPercentOfBase: this.convertForDisplay(compensation.FaciltyPercentOfBase),
      InheritingFromService: compensation.InheritingFromService,
      ServiceChargePercent: this.convertForDisplay(compensation.ServiceChargePercent),
      ServiceName: compensation.ServiceName,
      ServiceUID: compensation.ServiceUID,
      TherapistFlatRate: compensation.TherapistFlatRate,
      TherapistPercentOfBase: this.convertForDisplay(compensation.TherapistPercentOfBase),
      TherapistPercentOfServiceCharge: this.convertForDisplay(compensation.TherapistPercentOfServiceCharge),
      UID: compensation.UID
    };

    this.compensationForm.patchValue(holdComp);
  }

  convertForDisplay(value: number): number {
    if (value && isNumeric(value)) {
      return (value <= 1) ? value * 100 : value;
    }
  }

  convertForSave(value: number): number {
    if (value && isNumeric(value)) {
      return (value > 1) ? value / 100 : value;
    }
  }


  onClose() {
    this.compensation = null;
    this.showModalChange.emit(false);
  }

  onSave() {
    //console.log('onSave', this.compensationForm);
    if (this.compensationForm.invalid) {
      this.compensationForm.markAllAsTouched();
      return;
    }

    var comp: FacilityServiceCompensationModel = Object.assign({}, this.compensation, this.compensationForm.value);


    comp.BasePrice = this.compensationForm.value.BasePrice;
    comp.FaciltyPercentOfBase = this.convertForSave(this.compensationForm.value.FaciltyPercentOfBase);
    comp.InheritingFromService = this.compensationForm.value.InheritingFromService;
    comp.ServiceChargePercent = this.convertForSave(this.compensationForm.value.ServiceChargePercent);
    comp.TherapistFlatRate = this.compensationForm.get('TherapistFlatRate').value;
    comp.TherapistPercentOfBase = this.convertForSave(this.compensationForm.value.TherapistPercentOfBase);
    comp.TherapistPercentOfServiceCharge = this.convertForSave(this.compensationForm.value.TherapistPercentOfServiceCharge);

    //comp = Object.assign({}, this.compensation, this.compensationForm.value);

    this.saveCompensation.emit(comp);

  }

}


interface CategorySelectModel {
  value: string;
  viewValue: string;
}

interface ServiceSelectModel {
  value: string;
  viewValue: string;
}
