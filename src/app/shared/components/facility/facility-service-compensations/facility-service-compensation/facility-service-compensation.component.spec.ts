import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityServiceCompensationComponent } from './facility-service-compensation.component';

describe('FacilityServiceCompensationComponent', () => {
  let component: FacilityServiceCompensationComponent;
  let fixture: ComponentFixture<FacilityServiceCompensationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityServiceCompensationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityServiceCompensationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
