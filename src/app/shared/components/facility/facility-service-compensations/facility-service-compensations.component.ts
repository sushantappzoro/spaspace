import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { isNumeric } from 'rxjs/util/isNumeric';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';

import { FacilityCompensationService } from '../../../../core/services/facility/facility-compensation.service';
import { FacilityServiceCompensationModel } from '../../../models/facility/facility-service-compensation.model';
import { ServiceCategoryModel } from '../../../models/offerings/service-category.model';
import { ServiceModel } from '../../../models/offerings/service.model';
import { ServiceService } from '../../../../core/services/service.service';

@Component({
  selector: 'spa-facility-service-compensations',
  templateUrl: './facility-service-compensations.component.html',
  styleUrls: ['./facility-service-compensations.component.scss']
})
export class FacilityServiceCompensationsComponent implements OnInit, OnDestroy {
  @ViewChild("servicesGrid") servicesGrid: DxDataGridComponent;

  @Input() facilityUID: string;
  @Input() canEdit: boolean;

  private subs = new SubSink();

  compensations: FacilityServiceCompensationModel[];

  compensation: FacilityServiceCompensationModel;

  categories: ServiceCategoryModel[] = [];
  services: ServiceModel[] = [];

  showModal: boolean = false;  

  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private compensationService: FacilityCompensationService,
    private serviceService: ServiceService
  ) {
    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
  }

  ngOnInit() {

    this.loadCompensation();
    this.loadServices();

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }


  loadCompensation() {

    this.subs.add(this.compensationService.getServiceCompensations(this.facilityUID)
      .subscribe(resp => {
        this.compensations = resp;
      },
        error => {
          notify('Unable to load compensation records.', "error", 3000);
        }));
  } 

  loadServices() {
    this.subs.add(this.serviceService.getServices()
      .subscribe(resp => {
        this.categories = resp;
      },
        error => {
          notify('Unable to load compensation records.', "error", 3000);
        }));
  }

  servicesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        //text: "Add Compensation",
        hint: "Add Compensation",
        onClick: () => {
          this.addNew();
        }
      }
    });
  }

  addNew() {
    this.compensation = new FacilityServiceCompensationModel();
    this.showModal = true;
  }

  editRecord(e) {
    console.log('editRecord', e);
    this.compensation = { ...e.row.data };
    this.showModal = true;

  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveCompensation(compensation: FacilityServiceCompensationModel) {

    if (compensation.UID) {
      this.subs.add(this.compensationService.edit(this.facilityUID, compensation)
        .subscribe(resp => {
          var i = this.compensations.findIndex(item => item.UID === resp.UID);
          this.compensations.splice(i, 1, resp);
          this.showModal = false;
        },
          error => {
            notify('An error occured updating the compensation record.', 'error', 3000);
          }));
    } else {
      this.subs.add(this.compensationService.add(this.facilityUID, compensation)
        .subscribe(resp => {
          this.compensations.unshift(resp);
          this.showModal = false;
        },
          error => {
            notify('An error occured adding the compensation record.', 'error', 3000);
          }));
    }
  }

  deleteRow(uid: string) {

    this.subs.add(this.compensationService.remove(this.facilityUID, uid)
      .subscribe(resp => {
        var i = this.compensations.findIndex(item => item.UID === uid);
        this.compensations.splice(i, 1);
      },
        error => {
          notify('Unable to delete treatment area.', 'error', 800);
        }));

  }

  getPercentageOfBase(rowData) {

    if (!isNumeric(rowData.TherapistPercentOfBase)) {
      return null;
    }

    var therapistPercentOfBase: number = rowData.TherapistPercentOfBase;

    if (therapistPercentOfBase < 1) {
      therapistPercentOfBase = therapistPercentOfBase * 100;
    }

    return therapistPercentOfBase.toPrecision(2) + '%';

  }  

}
