import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityUpgradeCompensationComponent } from './facility-upgrade-compensation.component';

describe('FacilityUpgradeCompensationComponent', () => {
  let component: FacilityUpgradeCompensationComponent;
  let fixture: ComponentFixture<FacilityUpgradeCompensationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityUpgradeCompensationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityUpgradeCompensationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
