import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spa-facility-upgrades-compensation',
  templateUrl: './facility-upgrades-compensation.component.html',
  styleUrls: ['./facility-upgrades-compensation.component.scss']
})
export class FacilityUpgradesCompensationComponent implements OnInit {
  @Input() facilityUID: string;

  compensation: any[] = [];

  constructor() { }

  ngOnInit() {

    this.compensation = [];
  }


  upgradesGrid_onToolbarPreparing(e) {

  }

  editRecord() {

  }

  deleteRecord() {

  }
}
