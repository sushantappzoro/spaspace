import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityUpgradesCompensationComponent } from './facility-upgrades-compensation.component';

describe('FacilityUpgradesCompensationComponent', () => {
  let component: FacilityUpgradesCompensationComponent;
  let fixture: ComponentFixture<FacilityUpgradesCompensationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityUpgradesCompensationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityUpgradesCompensationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
