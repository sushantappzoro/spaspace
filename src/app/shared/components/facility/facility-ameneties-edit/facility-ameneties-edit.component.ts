import { Component, OnInit, Input, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';

import { StepperControlService } from '../../../../core/services/stepper-control.service';
import { FacilityAmenitiesService } from '../../../../core/services/facility/facility-amenities.service';
import { FacilityAmenityForAdminModel } from '../../../models/facility/facility-amenity-for-admin.model';
import { configSettings } from '../../../../app.config';


@Component({
  selector: 'spa-facility-amenities-edit',
  templateUrl: './facility-ameneties-edit.component.html',
  styleUrls: ['./facility-ameneties-edit.component.scss']
})
export class FacilityAmenetiesEditComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() showSaveButton: boolean
  @Input() showSubmitButton: boolean
  @Input() canEdit: boolean = false;
  @Input() canSubmit: boolean = false;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;
  @Input() facilityUID: string;

  @ViewChild("amenitiesGrid") facilitiesGrid: DxDataGridComponent;

  private subs = new SubSink();

  amenities: FacilityAmenityForAdminModel[];

  gridDisabled: boolean = false;

  amenityUploadApiPath: string;

  photoUrl: string;
  photoPopupVisible: boolean = false;

  popupVisible: boolean;
  title: string;
  uploadPath: string;

  constructor(
    private stepperService: StepperControlService,
    private amenitiesService: FacilityAmenitiesService
  ) {
    this.uploadImageClick = this.uploadImageClick.bind(this);
    this.showImageClick = this.showImageClick.bind(this);
  }

  ngOnInit() {


    this.amenityUploadApiPath = configSettings.WebAPI_URL + '/auth/therapists/';

    if (this.runAsStep) {
      this.subs.add(this.stepperService.getValidationStep()
        .subscribe(step => {
          if (step == this.stepNumber) {
            this.stepperService.completeStep(this.stepNumber);
          }
        }));
    }
    this.loadAmenities();
  }

  ngAfterViewInit() {

    

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadAmenities() {
    this.amenitiesService.getFacilityAmenities(this.facilityUID)
      .subscribe(resp => {
        this.amenities = resp;
      },
        error => {

        });
  }

  amenitiesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    });
  }

  facilityValidationCallback(e) {
    return true;
  }

  onAmenitiesRowUpdating(e: any) {

    var item: FacilityAmenityForAdminModel = { ...e.oldData, ...e.newData }

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.amenitiesService.updateFacilityAmenities(this.facilityUID, item)
        .subscribe(resp => {
          this.loadAmenities();
          resolve(false);
        },
          error => {
            notify('An error occured updating the attendees. Please try again', 'error', 3000);            
            resolve(true);
          }));
    });
  }

  isSearchButtonVisible(e) {
    if (this.canEdit && e.row.data.PhotoUrl && e.row.data.PhotoUrl.length > 0) {
      return true;
    }
    return false;
  }

  isUploadButtonVisible(e) {

    if (this.canEdit == false) {
      return false;
    }

    if (e.row.isNewRow) {
      return false;
    }

    if (!e.row.data.UID || e.row.data.UID.length === 0) {
      return false;
    }

    return true;
  }

  uploadImageClick(e) {
    e.event.preventDefault();
    //console.log('uploadCertificateClick', e.row.data.UID);
    this.title = 'Upload Amenity Image';
    this.uploadPath = this.amenityUploadApiPath + this.facilityUID + '/amenity/' + e.row.data.UID;
    this.popupVisible = true;
  }



  showImageClick(e) {

    this.title = "Amenity image on file";
    this.photoUrl = e.row.data.FileUrl;
    this.photoPopupVisible = true;

  }

}
