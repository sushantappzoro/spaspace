import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAmenetiesEditComponent } from './facility-ameneties-edit.component';

describe('FacilityAmenetiesEditComponent', () => {
  let component: FacilityAmenetiesEditComponent;
  let fixture: ComponentFixture<FacilityAmenetiesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAmenetiesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAmenetiesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
