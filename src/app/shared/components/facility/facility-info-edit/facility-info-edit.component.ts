import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { StepperControlService } from '../../../../core/services/stepper-control.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FacilityTherapistInfoService } from '../../../../core/services/facility/facility-therapist-info.service';
import { TherapistInfoForAdminModel } from '../../../models/facility/therapist-info-for-admin.model';

@Component({
  selector: 'spa-facility-info-edit',
  templateUrl: './facility-info-edit.component.html',
  styleUrls: ['./facility-info-edit.component.scss']
})
export class FacilityInfoEditComponent implements OnInit, AfterViewInit {
  @Input() facilityUID: string;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;
  @Input() showSaveButton: boolean

  private subs = new SubSink();

  therapistInfoForm: FormGroup;

  therapistInfo: TherapistInfoForAdminModel;

  constructor(
    private stepperService: StepperControlService,
    private infoService: FacilityTherapistInfoService
  ) { }

  ngOnInit() {

    if (this.runAsStep) {
      this.subs.add(this.stepperService.getValidationStep()
        .subscribe(step => {
          if (step == this.stepNumber) {
            this.performValidation();
          }
        }));
    }

    this.therapistInfoForm = new FormGroup({
      FacilityUID: new FormControl(''),
      ParkingInfo: new FormControl(''),
      UniformInfo: new FormControl(''),
      CheckInInfo: new FormControl(''),
      AdditionalInfo: new FormControl(''),
    }); 

    this.loadInfo();

  }

  ngAfterViewInit() {
    
  }

  loadInfo() {

    this.infoService.getTherapistInfo(this.facilityUID)
      .subscribe(resp => {     
        this.therapistInfo = resp;
        this.showInfo();
      },
      error => {

      });
  }

  showInfo() {
    
    if (this.therapistInfo) {
      this.therapistInfoForm.setValue(this.therapistInfo);
    }
  }

  performValidation() {   

    if (this.therapistInfoForm && this.therapistInfoForm.dirty) {
      this.onSave();
      return;
    }
    this.stepperService.completeStep(this.stepNumber);
  }

  onSave() {

    var info = new TherapistInfoForAdminModel();
    info = Object.assign({}, this.therapistInfo, this.therapistInfoForm.value);

    this.infoService.editTherapistInfo(this.facilityUID, info)
      .subscribe(res => {
        this.therapistInfo = res;
        this.showInfo();
        //this.isCompleteChange.emit(true);
        this.therapistInfoForm.markAsPristine();
        if (this.runAsStep) {
          this.stepperService.completeStep(this.stepNumber);
        } else {
          notify('Facility updated successfully.', "success", 3000);
        }
      },
        error => {
          if (this.runAsStep) {
            this.stepperService.reportError('Unable to save facility profile.');
          } else {
            notify('Unable to save facility profile.', "error", 3000);
          }
        });

  }

}
