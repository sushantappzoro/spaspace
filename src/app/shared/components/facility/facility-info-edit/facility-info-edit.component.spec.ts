import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityInfoEditComponent } from './facility-info-edit.component';

describe('FacilityInfoEditComponent', () => {
  let component: FacilityInfoEditComponent;
  let fixture: ComponentFixture<FacilityInfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityInfoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
