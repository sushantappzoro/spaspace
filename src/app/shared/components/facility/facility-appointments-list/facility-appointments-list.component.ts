import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { AppointmentForFacilityListModel } from '../../../../shared/models/appointment/appointment-for-facility-list.model';

@Component({
  selector: 'app-facility-appointments-list',
  templateUrl: './facility-appointments-list.component.html',
  styleUrls: ['./facility-appointments-list.component.scss']
})
export class FacilityAppointmentsListComponent implements OnInit {

  private subs = new SubSink();

  appointments: AppointmentForFacilityListModel[];

  currentFilter: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    //this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');

    this.subs.add(this.route.data
      .subscribe((data: { appointments: AppointmentForFacilityListModel[] }) => {
        this.appointments = data.appointments;

      }));

  }

  selectionChanged(e) {
    console.log(e.selectedRowKeys[0]);
    let uid = e.selectedRowKeys[0].toString();
    this.router.navigate(['../' ,uid, 'detail'], { relativeTo: this.route });
  }

  seeSchedule() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
