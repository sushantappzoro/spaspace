import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentsListComponent } from './facility-appointments-list.component';

describe('FacilityAppointmentsListComponent', () => {
  let component: FacilityAppointmentsListComponent;
  let fixture: ComponentFixture<FacilityAppointmentsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
