import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-facility-appointments-view',
  templateUrl: './facility-appointments-view.component.html',
  styleUrls: ['./facility-appointments-view.component.scss']
})
export class FacilityAppointmentsViewComponent implements OnInit {

  facilityUID: string;

  repaintTherapistView: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
  }

  showDetail(e) {
    //console.log('showDetail', e);
    this.router.navigate([e, 'detail'], { relativeTo: this.route });
  }

  seeList() {
    this.router.navigate(['list'], { relativeTo: this.route });
  }

  onTabShown(e) {
    //if second tab repaint

    this.repaintTherapistView = true;
  }

}
