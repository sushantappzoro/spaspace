import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentsViewComponent } from './facility-appointments-view.component';

describe('FacilityAppointmentsViewComponent', () => {
  let component: FacilityAppointmentsViewComponent;
  let fixture: ComponentFixture<FacilityAppointmentsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
