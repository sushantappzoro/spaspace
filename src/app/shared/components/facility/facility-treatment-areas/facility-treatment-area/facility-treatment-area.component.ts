import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';

import { Component, OnInit, ViewChild, Input, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { ModalDirective } from 'ng-uikit-pro-standard';
import { FacilityTreatmentAreaModel } from '../../../../models/facility/facility-treatment-area.model';
import { FacilityTreatmentAreaServiceCategoryModel } from '../../../../models/facility/facility-treatment-area-service-category.model';
import { AdminServiceCategoryService } from '../../../../../admin/core/services/admin-service-category.service';
import { ServiceCategoryForAdminViewModel } from '../../../../../admin/shared/models/services/service-category-for-admin-view.model';
import { retry } from 'rxjs/operators';
import { FacilityTreatmentAreaServiceForEditModel } from '../../../../models/facility/facility-treatment-area-service-for-edit.model';

@Component({
  selector: 'spa-facility-treatment-area',
  templateUrl: './facility-treatment-area.component.html',
  styleUrls: ['./facility-treatment-area.component.scss']
})
export class FacilityTreatmentAreaComponent implements OnInit, OnDestroy, OnChanges {
  @Input() treatmentArea: FacilityTreatmentAreaModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveTreatmentArea: EventEmitter<FacilityTreatmentAreaModel> = new EventEmitter<FacilityTreatmentAreaModel>();

  @ViewChild('treatmentAreaModal') treatmentAreaModal: ModalDirective;

  private subs = new SubSink();

  title: string = 'Add Treatment Area';

  treatmentAreaForm: FormGroup;

  categories: ServiceCategoryForAdminViewModel[] = [];

  //
  // material tree shit
  //

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: TodoItemFlatNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<TodoItemFlatNode>;

  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);




  constructor(
    private formBuilder: FormBuilder,
    private categoryService: AdminServiceCategoryService
  ) {

    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  }

  ngOnInit() {

    //this.treatmentAreaForm = new FormGroup({
    //  UID: new FormControl(),
    //  //FacilityUID: new FormControl(),
    //  Name: new FormControl('', Validators.required),
    //  Description: new FormControl('', Validators.required),
    //  Active: new FormControl(true),
    //  Accommodates: new FormControl('', [ Validators.required, Validators.min(1) ]),
    //  ServiceCategories: this.formBuilder.array([])
    //});

    //this.subs.add(this.categoryService.get()
    //  .subscribe(cats => {
    //    this.categories = cats;
    //    this.dataSource.data = this.buildDataset(cats);
    //    this.markSelected();
    //    console.log('datasource', this.dataSource);
    //  },
    //    error => {
    //      console.log(error);
    //    }
    //)
    //)

    if (this.treatmentArea) {
      //console.log('treatmentArea', this.treatmentArea);
      this.populateForm(this.treatmentArea);
      this.title = (this.treatmentArea?.UID) ? 'Edit Treatment Area' : 'Add Treatment Area';
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    if (this.treatmentAreaModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.treatmentAreaModal.show();
      } else {
        this.treatmentAreaModal.hide();
      }
    }

    if (changes.treatmentArea ) {
      this.populateForm(changes.treatmentArea.currentValue);
      if (this.treatmentArea && this.treatmentArea.Services) {
        this.dataSource.data = this.buildDataset(this.treatmentArea.Services);
        this.markSelected();
      }      
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  populateForm(treatmentArea: FacilityTreatmentAreaModel) {

    this.treatmentAreaForm = new FormGroup({
      UID: new FormControl(),
      //FacilityUID: new FormControl(),
      Name: new FormControl('', Validators.required),
      Description: new FormControl('', Validators.required),
      Active: new FormControl(true),
      Accommodates: new FormControl('', [Validators.required, Validators.min(1)]),
      //ServiceCategories: this.formBuilder.array([])
    });

    this.treatmentAreaForm.reset();
    if (treatmentArea) {
      this.treatmentAreaForm.controls["UID"].setValue(treatmentArea.UID);
      this.treatmentAreaForm.controls["Name"].setValue(treatmentArea.Name);
      this.treatmentAreaForm.controls["Description"].setValue(treatmentArea.Description);
      this.treatmentAreaForm.controls["Active"].setValue(treatmentArea.Active);
      this.treatmentAreaForm.controls["Accommodates"].setValue(treatmentArea.Accommodates);

    }
    
    

    //treatmentArea.ServiceCategories.forEach(category => {
    //   //var need = this.userSession.Needs.find(x => x.ID === reason.ID)
    //   //reason.IsChecked = (need && need.IsChecked === true) ? true : false;
    //  this.itemArray.push(this.addFormControl(category));
    //});
  }

  //get itemArray() {
  //  return this.treatmentAreaForm.get('ServiceCategories') as FormArray;
  //}

  //addFormControl(exp: FacilityTreatmentAreaServiceCategoryModel) {
  //  //console.log('addFormControl', exp);
  //  return this.formBuilder.group({
  //    UID: exp.UID,
  //    CategoryName: exp.CategoryName,
  //    CategoryID: exp.CategoryID,
  //    Accommodates: exp.Accommodates
  //  });
  //}

  onClose() {
    this.treatmentArea = null;
    this.treatmentAreaForm.reset();
    this.showModalChange.emit(false);
  }

  onSave() {
    

  }

  onSaveForReal() {
    if (this.treatmentAreaForm.invalid) {
      this.treatmentAreaForm.markAllAsTouched();
      return;
    }

    var area = new FacilityTreatmentAreaModel();
    //console.log('treatmentAreaForm', this.treatmentAreaForm.value);
    area = Object.assign({}, this.treatmentArea, this.treatmentAreaForm.value);
    //console.log('onSave', area);

    this.getSelected(area);
    //console.log('area to save', area);
    this.saveTreatmentArea.emit(area);
    //this.treatmentAreaForm.reset();
  }

  getSelected(area: FacilityTreatmentAreaModel) {
    this.treeControl.dataNodes.forEach(node => {
      const descendants = this.treeControl.getDescendants(node);
      if (!descendants || descendants.length == 0) {
        let service = area.Services.find(x => x.ServiceName == node.item);
        if (service) {
          service.Accommodates = this.checklistSelection.isSelected(node);
        }
        //console.log('node=' + node.item + ' selected=' + this.checklistSelection.isSelected(node));
      }
    
    });
  }

  //
  // material tree shit
  //

  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;

  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

  /**
 * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
 */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.item
      ? existingNode
      : new TodoItemFlatNode();
    flatNode.item = node.item;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    //flatNode.isSelected = node.isSelected;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    if (this.treeControl) {
      try {
        const descendants = this.treeControl.getDescendants(node);
        if (!descendants || descendants.length == 0) {
          return false;
        }
        const descAllSelected = descendants.every(child =>
          this.checklistSelection.isSelected(child)
        );
        return descAllSelected;
      }
      catch {
        return false;
      }

    }
    return false;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    if (this.treeControl) {
      try {
        const descendants = this.treeControl.getDescendants(node);
        const result = descendants.some(child => this.checklistSelection.isSelected(child));
        return result && !this.descendantsAllSelected(node);
      } catch {
        return false;
      }
    }
    return false;
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    //console.log('todoItemSelectionToggle', node);
    //console.log('isSelected=' + this.checklistSelection.isSelected(node));
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    //console.log('isSelected=' + this.checklistSelection.isSelected(node));
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
    //console.log('todoLeafItemSelectionToggle', node);
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: TodoItemFlatNode): void {
    //console.log('checkAllParentsSelection', node);
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: TodoItemFlatNode): void {
    //console.log('checkRootNodeSelection', node);
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
    //console.log('getParentNode', node);
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }


  buildDataset(services: FacilityTreatmentAreaServiceForEditModel[]): TodoItemNode[] {

    let items: TodoItemNode[] = [];

    services.forEach(service => {

      let category = items.find(x => x.item == service.CategoryName);
      if (category) {
        category.children.push({ item: service.ServiceName, children: null });
      } else {
        let newCat = new TodoItemNode();
        newCat.item = service.CategoryName;
        newCat.children = [];
        newCat.children.push({ item: service.ServiceName, children: null });
        items.push(newCat);
      }
    });

    //console.log('items', items);
    return items;

  }

  markSelected() {

    //console.log('markSelected', this.treeControl);
    this.treeControl.dataNodes.forEach(node => {
      if (!node.expandable) {
        let service = this.treatmentArea.Services.find(x => x.ServiceName == node.item);
        //console.log('service', service);
        //console.log('service', service.Accommodates);
        if (service && service.Accommodates == true) {
          //console.log('select', node);
          this.checklistSelection.select(node);
          this.checkAllParentsSelection(node);
        }
      }
    });
  }

}


/**
 * Node for to-do item
 */
export class TodoItemNode {
  children: TodoItemNode[];
  item: string;
  //isSelected: boolean;
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  item: string;
  level: number;
  expandable: boolean;
  //isSelected: boolean;
}


