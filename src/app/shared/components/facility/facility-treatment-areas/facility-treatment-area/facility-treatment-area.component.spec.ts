import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityTreatmentAreaComponent } from './facility-treatment-area.component';

describe('FacilityTreatmentAreaComponent', () => {
  let component: FacilityTreatmentAreaComponent;
  let fixture: ComponentFixture<FacilityTreatmentAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityTreatmentAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityTreatmentAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
