import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityTreatmentAreasComponent } from './facility-treatment-areas.component';

describe('FacilityTreatmentAreasComponent', () => {
  let component: FacilityTreatmentAreasComponent;
  let fixture: ComponentFixture<FacilityTreatmentAreasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityTreatmentAreasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityTreatmentAreasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
