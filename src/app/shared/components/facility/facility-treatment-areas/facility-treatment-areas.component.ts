import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';

import { FacilityTreatmentAreasService } from '../../../../core/services/facility/facility-treatment-areas.service';
import { FacilityTreatmentAreaModel } from '../../../models/facility/facility-treatment-area.model';
import { FacilityTreatmentAreaViewModel } from '../../../models/facility/facility-treatment-area-view.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FacilityTreatmentAreaForAddModel } from '../../../models/facility/facility-treatment-area-for-add.model';
import { FacilityTreatmentAreaForEditModel } from '../../../models/facility/facility-treatment-area-for-edit.model';
import { FacilityInfoForAdminModel } from '../../../models/facility/facility-info-for-admin.model';
import { take } from 'rxjs/operators';


@Component({
  selector: 'spa-facility-treatment-areas',
  templateUrl: './facility-treatment-areas.component.html',
  styleUrls: ['./facility-treatment-areas.component.scss']
})
export class FacilityTreatmentAreasComponent implements OnInit, OnDestroy {
  @Input() facilityUID: string;
  @Input() canEdit = true;

  @ViewChild("treatmentAreasGrid") treatmentAreasGrid: DxDataGridComponent;

  private subs = new SubSink();

  facility: FacilityInfoForAdminModel;

  treatmentAreas: FacilityTreatmentAreaViewModel[] = [];

  treatmentArea: FacilityTreatmentAreaForEditModel;

  showModal: boolean = false;

  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private treatmentAreaService: FacilityTreatmentAreasService
  ) {
    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
    this.manageAvailability = this.manageAvailability.bind(this);
  }

  ngOnInit() {

    if (!this.facilityUID) {
      this.route.params.subscribe(params => {
        this.facilityUID = params['uid'];
      });
    }

    if (!this.facilityUID) {
      this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
    }

    this.subs.add(this.route.data
      .subscribe((data: { treatmentAreas: FacilityTreatmentAreaViewModel[] }) => {
        this.treatmentAreas = data.treatmentAreas;
      },
        error => {
          console.log(error);
        }));

    this.subs.add(this.route.data
      .subscribe((data: { facility: FacilityInfoForAdminModel }) => {
        this.facility = data.facility;
      },
        error => {
          console.log(error);
        }));

  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  treatmentAreasGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    }, {
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        visible: this.canEdit,
        icon: "add",
        stylingMode: "outline",
        text: "Add New Treatment Area",
        hint: "Add New Treatment Area",
        onClick: () => {
          this.addNew();
        }
      }
    });
  }

  addNew() {
    this.subs.add(this.treatmentAreaService.newTreatmentArea(this.facilityUID)
      .pipe(take(1))
      .subscribe(resp => {
        this.treatmentArea = { Accommodates: resp.Accommodates, Active: true, Description: resp.Description, FacilityUID: this.facilityUID, Name: null, UID: null, Services: resp.Services };
        this.showModal = true;
    }));
   
  }

  editRecord(e) {

    this.subs.add(this.treatmentAreaService.getTreatmentArea(this.facilityUID, e.row.data.UID)
      .subscribe(resp => {

        this.treatmentArea = resp;
        //console.log('editRecord', this.treatmentArea);
        this.showModal = true;
      },
        error => {
          notify('Unable to edit treatment area.', 'error', 3000);
        }));

  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveTreatmentArea(area: FacilityTreatmentAreaModel) {

    if (area.UID) {

      var eta: FacilityTreatmentAreaForEditModel = 
        { Accommodates: area.Accommodates, Description: area.Description, Name: area.Name, Active: area.Active, UID: area.UID, Services: area.Services, FacilityUID: area.FacilityUID };

      this.subs.add(this.treatmentAreaService.edit(this.facilityUID, eta)
        .subscribe(resp => {
          var i = this.treatmentAreas.findIndex(item => item.UID === eta.UID);
          this.treatmentAreas.splice(i, 1, this.toArea(eta));
          this.showModal = false;
        },
          error => {
            notify('An error occured updating the treatment area.', 'error', 800);
          }));
    } else {

      var ata: FacilityTreatmentAreaForAddModel = { Accommodates: area.Accommodates, Description: area.Description, Name: area.Name, Services: area.Services };

      this.subs.add(this.treatmentAreaService.add(this.facilityUID, ata)
        .subscribe(resp => {
          this.treatmentAreas.unshift(this.toArea(resp));
          this.showModal = false;
        },
          error => {
            notify('An error occured adding the treatment area.', 'error', 800);            
          }));
    }
  }


  toArea(area: FacilityTreatmentAreaForEditModel): FacilityTreatmentAreaViewModel {
    var newArea: FacilityTreatmentAreaViewModel = { Active: area.Active ? 'Active' : 'Not Active', Accommodates: area.Accommodates, Description: area.Description, FacilityUID: this.facilityUID, Name: area.Name, UID: area.UID  };
    return newArea;
  }

  deleteRow(uid: string) {
    
    this.subs.add(this.treatmentAreaService.remove(this.facilityUID, uid)
      .subscribe(resp => {
        var i = this.treatmentAreas.findIndex(item => item.UID === uid);
        this.treatmentAreas.splice(i, 1);
      },
        error => {
          notify('Unable to delete treatment area.', 'error', 800);            
        }));
  
  }

  manageAvailability(e) {
    this.router.navigate([e.row.data.UID, 'availability'], { relativeTo: this.route });
  }

}
