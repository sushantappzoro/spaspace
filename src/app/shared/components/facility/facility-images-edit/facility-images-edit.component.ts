import { Component, OnInit, Input, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { SubSink } from 'subsink';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryComponent } from '@kolkov/ngx-gallery';
import { StepperControlService } from '../../../../core/services/stepper-control.service';
import { FacilityPhotosService } from '../../../../core/services/facility/facility-photos.service';
import { FacilityPhotoForAdminModel } from '../../../models/facility/facility-photo-for-admin.model';
import { configSettings } from '../../../../app.config';
import { AuthHeadersService } from '../../../../core/services/auth-headers.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthorizeService } from '../../../../../api-authorization/authorize.service';
import { MaxImageSizeForUpload, InvalidMaxFileSizeMessage } from '../../../globals';

@Component({
  selector: 'spa-facility-images-edit',
  templateUrl: './facility-images-edit.component.html',
  styleUrls: ['./facility-images-edit.component.scss']
})
export class FacilityImagesEditComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() canEdit: boolean = true;
  @Input() facilityUID: string;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  @ViewChild("gallery") gallery: NgxGalleryComponent;

  private subs = new SubSink();

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  imageCount: number;

  photos: FacilityPhotoForAdminModel[];

  photoUploadForm: FormGroup;

  popupVisible: boolean;
  title: string;
  uploadPath: string;
  //defaultVisible = false;

  photoUploadApiPath: string;

  currentPhotoUrl: string;

  items: any;

  values: any[] = [];
  headers: any;

  token: string;

  deleteURL: string;
  showDeleteWarning: boolean = false;

  constructor(
    private authService: AuthorizeService,
    private stepperService: StepperControlService,
    private photosService: FacilityPhotosService,
    private authHeaderService: AuthHeadersService
  ) { }

  ngOnInit() {

    this.headers = this.authHeaderService.getHeaders();

    this.photoUploadForm = new FormGroup({
      Description: new FormControl('', Validators.required),
    });    

    //this.items = [{
    //  text: 'Share',
    //  items: [
    //    { text: 'Facebook' },
    //    { text: 'Twitter' }]
    //},
    //{ text: 'Make Default' },
    //{ text: 'Delete' }
    //];

    this.items = [
    { text: 'Make Default' },
    { text: 'Delete' }
    ];

    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      { "imageDescription": true },
      // max-width 800
      {
        breakpoint: 800,
        width: '500px',
        height: '400px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 500,
        width: '300px',
        height: '300px',
        thumbnailsColumns: 3
      }
    ];

    if (this.runAsStep) {
      this.subs.add(this.stepperService.getValidationStep()
        .subscribe(step => {
          if (step == this.stepNumber) {
            this.performValidation();
          }
        }));
    }

    this.photoUploadApiPath = configSettings.WebAPI_URL + '/auth/facilities/';
    this.uploadPath = this.photoUploadApiPath + this.facilityUID + '/Photos';

  }

  ngAfterViewInit() {
    this.loadPhotos();

    this.subs.add(this.authService.getAccessToken()
      .subscribe(token => {
        if (token) {
          this.token = token;
          this.headers = { 'Authorization': 'Bearer ' + this.token }
        }
      }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  performValidation() {

    if (this.photos && this.photos.length > 0) {
      this.stepperService.completeStep(this.stepNumber);
      return;
    }

    this.stepperService.reportError('Cannot proceed to next step until at least one image uploaded.');

  }

  loadPhotos() {    
    if (this.facilityUID) {      

      this.photosService.getFacilityPhotos(this.facilityUID).subscribe(resp => {
        this.photos = resp;

        this.buildPhotosArray();
      },
        error => {

        });
    }

  }

  getMaxFileSize() {
    return MaxImageSizeForUpload;
  }

  getMaxFileSizeMessage() {
    return InvalidMaxFileSizeMessage;
  }

  buildPhotosArray() {

    var newPhotos: NgxGalleryImage[] = [];    

    this.photos.forEach(item => {
      var photo = new NgxGalleryImage({ big: item.Url, small: item.Url, medium: item.Url });
      photo.description = item.IsMain ? '** Default Image ** ' + item.Description : item.Description;
      newPhotos.push(photo);
    });
    
    this.galleryImages = newPhotos;
    if (this.galleryImages && this.galleryImages.length > 0) {
      this.currentPhotoUrl = this.galleryImages[0].small.toString();
    }
    this.imageCount = 0;

    if (this.galleryImages) {
      this.imageCount = this.galleryImages.length;
    }
  }

  previewOpen(e) {
    console.log('previewOpen', e);
  }

  itemClick(e) {
    //if (!e.itemData.items) {
    //  notify("The \"" + e.itemData.text + "\" item was clicked", "success", 1500);
    //}

    if (e.itemData.text == 'Delete') {
      this.deletePhoto(this.currentPhotoUrl);
    } else {
      this.markAsDefault();
    }

  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deletePhoto(this.currentPhotoUrl);
    }
  }

  deleteImage() {
    this.deleteURL = this.currentPhotoUrl;
    this.showDeleteWarning = true;
  }

  deletePhoto(url: string) {
    //console.log('delete', url);
    //console.log('photos', this.photos);
    var del = this.photos.find(item =>  item.Url === url );
    if (del) {
      //console.log('delete found', del);
      this.photosService.removeFacilityPhoto(this.facilityUID, del.UID).subscribe(resp => {
        this.photos = resp;

        this.loadPhotos();
      },
        error => {

        });
    }

  }

  markAsDefault() {
   // console.log('markAsDefault', this.photos);
    var del = this.photos.find(item => item.Url == this.currentPhotoUrl );
    if (del) {
      this.photosService.setMainFacilityPhoto(this.facilityUID, del.UID).subscribe(resp => {
        this.photos = resp;
        this.buildPhotosArray();

        this.gallery.show(0);
        let i = this.photos.findIndex(x => x.UID == del.UID);

        if (this.gallery.selectedIndex != i) {
          //console.log('show', i);
          //this.gallery.show(i);
        }        

        //this.gallery.showNext();
        //this.loadPhotos(del.UID);
      },
        error => {

        });
    }
  }

  change(e) {    
    this.currentPhotoUrl = e.image.small;
  }


  uploadPhotoClick(e) {
    e.event.preventDefault();
    //console.log('uploadCertificateClick', e.row.data.UID);
    this.title = 'Upload Facility Image';
    this.uploadPath = this.photoUploadApiPath + this.facilityUID + '/Photos';
    this.popupVisible = true;
  }

  onHiding() {

  }

  fileUploaded() {

  }

  onSave() {

  }

  fileUploader_onUploaded(e) {
    this.loadPhotos();
  }

  fileUploader_onUploadError(e) {
  }

  fileUploader_onUploadStarted(e) {
    console.log('fileUploader_onUploadStarted', e);

    this.headers = { 'Authorization': 'Bearer ' + this.token,  'Description': this.photoUploadForm.controls['Description'].value }
  }

  addDescription() {

  }

  //submitInputFile(fileUploader: DxFileUploaderComponent) {

  //  const formData = new FormData();

  //  for (const file of fileUploader.value) {
  //    formData.append(file.name, file);
  //  }

  //  // upload the file  
  //  this._myCustomServiceInChargeOfHttpRequest.postInputFileAndGetResult(formData).subscribe(res => {
  //    if (res.type === HttpEventType.Response) {
  //      // get the response of HTTP request and do anything with  
  //    }
  //  },
  //    error => {
  //      alert('Failed :-/');
  //    });
  //}  

}
