import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityImagesEditComponent } from './facility-images-edit.component';

describe('FacilityImagesEditComponent', () => {
  let component: FacilityImagesEditComponent;
  let fixture: ComponentFixture<FacilityImagesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityImagesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityImagesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
