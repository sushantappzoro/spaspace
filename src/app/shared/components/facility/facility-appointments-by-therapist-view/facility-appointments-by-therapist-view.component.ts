import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Query from 'devextreme/data/query';
import { map } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { DxSchedulerModule, DxSchedulerComponent } from 'devextreme-angular';
import CustomStore from "devextreme/data/custom_store";
import { createStore } from 'devextreme-aspnet-data-nojquery';
import { data } from 'devextreme/core/element_data';

import { configSettings } from '../../../../app.config';
import * as AspNetData from "devextreme-aspnet-data-nojquery";
import { FacilityScheduleService } from '../../../../core/services/facility/facility-schedule.service';
import { AvailabilityForAppointmentModel } from '../../../../shared/models/appointment/availability-for-appointment.model';
import { AppointmentForTherapistViewModel } from '../../../../shared/models/appointment/appointment-for-therapist-view.model';
import { TherapistForFacilityScheduleViewModel } from '../../../models/facility/therapist-for-facility-schedule-view.model';
import { FacilityTherapistService } from '../../../../core/services/facility/facility-therapist.service';

@Component({
  selector: 'spa-facility-appointments-by-therapist-view',
  templateUrl: './facility-appointments-by-therapist-view.component.html',
  styleUrls: ['./facility-appointments-by-therapist-view.component.scss']
})
export class FacilityAppointmentsByTherapistViewComponent implements OnInit, OnChanges, OnDestroy {
  @Input() facilityUID: string;
  @Input() repaint: boolean;

  @Output() showDetail: EventEmitter<string> = new EventEmitter<string>();
  @Output() repaintChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('bytherapist') bytherapist: DxSchedulerComponent;

  private subs = new SubSink();

  therapistAvailability: AvailabilityForAppointmentModel[] = [];

  bytherapistData: AppointmentForTherapistViewModel[];

  therapists: TherapistForFacilityScheduleViewModel[];


  currentDate: Date = new Date();

  //byroomURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/byroom?loadOptions='
  //bytherapistURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/bytherapistURL?loadOptions='

  bytherapistURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/bytherapist'

  constructor(
    private route: ActivatedRoute,
    private facilityScheduleService: FacilityScheduleService,
    private therapistService: FacilityTherapistService
  ) { }

  ngOnInit() {

    this.bytherapistURL = configSettings.WebAPI_URL + '/auth/facilities/' + this.facilityUID + '/schedule/bytherapist'

    this.subs.add(this.facilityScheduleService.getByTherapist(this.bytherapistURL, null)
      .subscribe(resp => {
        this.bytherapistData = resp.Appointments;
        this.therapistAvailability = resp.Therapists;
      })
    )

    // get the list of associated therapists for grouping
    this.subs.add(this.therapistService.getAssociatedTherapists(this.facilityUID)
      .subscribe(resp => {
        this.therapists = resp;
      })
    )

    //this.facilityUID = this.route.params["facilityUID"];

    //this.byroomData = AspNetData.createStore({
    //  key: "ID",
    //  loadUrl: this.byroomURL,
    //  onBeforeSend: function (method, ajaxOptions) {
    //    ajaxOptions.xhrFields = { withCredentials: true };
    //  }
    //});

    //this.bytherapistData = createStore({
    //  key: "ID",
    //  loadUrl: this.bytherapistURL,
    //  onBeforeSend: function (method, ajaxOptions) {
    //    ajaxOptions.xhrFields = { withCredentials: true };
    //  }
    //});

    //this.byroomData = new CustomStore({
    //  key: "UID",
    //  load: (loadOptions) => {
    //    //console.log('loadoptions', loadOptions);
    //    return this.facilityScheduleService.getByRoom(this.byroomURL, loadOptions);
    //  }
    //});

    //this.bytherapistData = new CustomStore({
    //  key: "UID",
    //  load: (loadOptions) => {
    //    return this.facilityScheduleService.getByTherapist(this.bytherapistURL, loadOptions).subscribe()
    //  }
    //});
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.repaint) {
      if (changes.repaint?.currentValue && changes.repaint.currentValue === true) {
        this.refresh();      
        //this.repaintChange.emit(false);
      } else {
        //this.changeModal.hide();
      }
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  refresh() {
    if (this.bytherapist) {
      console.log('refresh')
      this.bytherapist.instance.repaint();
    }
  }

  markAvailability(cellData) {
    //console.log('celldata', cellData);

    let markit: Boolean = false;

    if (cellData.groups) {
      markit = this.hasAvailability(cellData.startDate, cellData.endDate, cellData.groups.TherapistUID)
    }

    var classObject = {};
    classObject['appointment-shading-1'] = markit;
    return classObject;

  }

  hasAvailability(startDate, endDate, therapistUID) {
    try {

      //console.log('startdate=' + startDate + ' enddate=' + endDate);
      if (this.therapistAvailability) {
        var item = this.therapistAvailability.find(appointment => appointment.UID == therapistUID && new Date(startDate) >= new Date(appointment.AvailabilityStart) && new Date(endDate) <= new Date(appointment.AvailabilityEnd));
        if (item) { // && item[0]) {
          //console.log('item', item);
          return true;
        } else {
          return false;
        }
      }
    }
    catch {

    }
    return false;
  }

  onAppointmentRendered(e) {
    setTimeout(() => {
      const appointmentData = data(e.appointmentElement);
      const settings = appointmentData.dxAppointmentSettings
      const cellWidth = appointmentData.dxSchedulerAppointment.option("cellWidth");
      const count = settings.count;
      const oldWidth = e.appointmentElement.clientWidth;
      const newWidth = cellWidth / count;
      const diff = newWidth - oldWidth;
      e.appointmentElement.style.transform = "translate(" + (appointmentData.dxTranslator.x + diff * settings.index) + "px, " + appointmentData.dxTranslator.y + "px)";
      e.appointmentElement.style.width = cellWidth / count + "px";
      e.appointmentElement.classList.remove("dx-scheduler-appointment-empty");
    })
  }

  onAppointmentFormOpeningByTherapist(e) {
    //console.log('onAppointmentFormOpeningByRoom', e);
    this.showDetail.emit(e.appointmentData.UID);
    e.cancel = true;
  }

}
