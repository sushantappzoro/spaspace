import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentsByTherapistViewComponent } from './facility-appointments-by-therapist-view.component';

describe('FacilityAppointmentsByTherapistViewComponent', () => {
  let component: FacilityAppointmentsByTherapistViewComponent;
  let fixture: ComponentFixture<FacilityAppointmentsByTherapistViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentsByTherapistViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentsByTherapistViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
