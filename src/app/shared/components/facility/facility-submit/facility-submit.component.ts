import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { StepperControlService } from '../../../../core/services/stepper-control.service';
import { Router } from '@angular/router';
import { AuthorizeService } from '../../../../../api-authorization/authorize.service';
import { FacilityService } from 'src/app/core/services/facility/facility.service';
import { FacilityForAuthEditModel } from 'src/app/shared/models/facility/facility-for-auth-edit.model';

@Component({
  selector: 'spa-facility-submit',
  templateUrl: './facility-submit.component.html',
  styleUrls: ['./facility-submit.component.scss']
})
export class FacilitySubmitComponent implements OnInit, OnDestroy {
  @Input() facility: FacilityForAuthEditModel;
  @Input() facilityStatus: boolean;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  private subs = new SubSink();

  thanksPopupVisible: boolean = false;

  //userStatus: string;

  constructor(
    private authService: AuthorizeService,
    private stepperService: StepperControlService,
    private facilityService: FacilityService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {
        if (step == this.stepNumber) {
          this.stepperService.completeStep(this.stepNumber);
        }
      }));
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  click() {
    this.facilityService.submit(this.facility).subscribe(resp => {
      this.thanksPopupVisible = true;
    },
      error => {
        notify('There was an error submitting the facility for review.', "error", 3000);
      });
  }

  closePopup() {
    this.thanksPopupVisible = false;
  }

  onHidden() {
    this.router.navigate(['/authentication/logout']);
  }

}
