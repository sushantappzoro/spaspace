import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitySubmitComponent } from './facility-submit.component';

describe('FacilitySubmitComponent', () => {
  let component: FacilitySubmitComponent;
  let fixture: ComponentFixture<FacilitySubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitySubmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitySubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
