import { Component, OnInit, Input } from '@angular/core';
import { FacilityProfileForUiModel } from '../../../../facility/shared/models/facility-profile-for-ui.model';
import { FacilityProfileService } from '../../../../core/services/facility/facility-profile.service';
import { FacilityAmenityForViewModel } from '../../../models/facility/facility-amenity-for-view.model';
import { TherapistForProfileViewModel } from '../../../models/therapist/therapist-for-profile-view.model';
import { FacilityPhotoForAdminModel } from '../../../models/facility/facility-photo-for-admin.model';

@Component({
  selector: 'spa-facility-profile-view',
  templateUrl: './facility-profile-view.component.html',
  styleUrls: ['./facility-profile-view.component.scss']
})
export class FacilityProfileViewComponent implements OnInit {
  @Input() facility: FacilityProfileForUiModel;
  @Input() facilityUid: string;

  amenities: FacilityAmenityForViewModel[];
  therapists: TherapistForProfileViewModel[];
  photos: FacilityPhotoForAdminModel[];

  constructor(private profileService: FacilityProfileService) { }

  ngOnInit() {

    this.profileService.getAmenities(this.facilityUid)
      .subscribe(resp => {
        this.amenities = resp;
      },
        error => {

        });

    this.profileService.getFeaturedTherapists(this.facilityUid)
      .subscribe(resp => {
        this.therapists = resp;
      },
        error => {

        });

    this.profileService.getPhotos(this.facilityUid)
      .subscribe(resp => {
        this.photos = resp;
      },
        error => {

        });


  }

}
