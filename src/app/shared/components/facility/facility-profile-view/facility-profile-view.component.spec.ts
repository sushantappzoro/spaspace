import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityProfileViewComponent } from './facility-profile-view.component';

describe('FacilityProfileViewComponent', () => {
  let component: FacilityProfileViewComponent;
  let fixture: ComponentFixture<FacilityProfileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityProfileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityProfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
