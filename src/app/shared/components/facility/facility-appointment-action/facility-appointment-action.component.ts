import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'spa-facility-appointment-action',
  templateUrl: './facility-appointment-action.component.html',
  styleUrls: ['./facility-appointment-action.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FacilityAppointmentActionComponent implements OnInit, OnChanges {
  @Input() step: string;

  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() reschedule: EventEmitter<any> = new EventEmitter<any>();

  title: string;
  description: string;
  showCancel: boolean;
  showReschedule: boolean;

  constructor() { }

  ngOnInit() {
    if (this.step) {
      this.showAction(this.step);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.step) {
      this.showAction(this.step);
    }
  }

  showAction(step) {

    this.showCancel = false;
    this.showReschedule = false;

    switch (step) {
      case 'Upcoming':
      case 'Pending':
        this.title = "Scheduled Appointment";
        this.description = "Upcoming appointment at your facility.";
        this.showCancel = true;
        this.showReschedule = true;
        break;
      case 'In Process':
        this.title = "This appointment is active.";
        this.description = "" // "Check out when the session is complete.";
        //this.showCheckout = true;
        break;
      case 'Checking Out':
        this.title = "Time to check out.";
        this.description = "Complete the transaction below.";
        //this.showFinalize = true;
        break;
      case 'Cancelled':
        this.title = "This session was cancelled.";
        this.description = "This session was cancelled.";
        //this.showFinalize = true;
        break;
      case 'Completed':
        //this.title = "Your session is complete. Rate Client";
        //this.description = "Please rate the client.";
        this.title = "Your session is complete.";
        this.description = "";
        //this.showReview = true;
        break;
      case 'finalize':
        this.title = "Finalize the session.";
        this.description = "Finalize the appointment.";
        //this.showFinalize = true;
        break;
      default:
      //this.title = "It's time to start the massage.";
      //this.description = "Check-in to your massage or notify us if the client didn't show";
      //this.showCheckin = true;

    }
  }

  onCancel() {
    this.cancel.emit(null);
  }

  onReschedule() {
    this.reschedule.emit(null);
  }
  //onFinailize() {
  //  this.finalize.emit(null);
  //}
}
