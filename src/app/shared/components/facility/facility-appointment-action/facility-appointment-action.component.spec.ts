import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentActionComponent } from './facility-appointment-action.component';

describe('FacilityAppointmentActionComponent', () => {
  let component: FacilityAppointmentActionComponent;
  let fixture: ComponentFixture<FacilityAppointmentActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
