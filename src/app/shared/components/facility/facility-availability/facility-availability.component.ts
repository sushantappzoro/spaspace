import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Query from 'devextreme/data/query';
import { map } from 'rxjs/operators';
import { SubSink } from 'subsink';


import { FacilityTreatmentAreasService } from '../../../../core/services/facility/facility-treatment-areas.service';
import { FacilityTreatmentAreaViewModel } from '../../../../shared/models/facility/facility-treatment-area-view.model';
import { TreatmentAreaAvailabilityForViewModel } from '../../../../shared/models/facility/treatment-area-availability-for-view.model';
import { TreatmentAreaAvailabilityForAddModel } from '../../../../shared/models/facility/treatment-area-availability-for-add.model';
import { TreatmentAreaAvailabilityForEditModel } from '../../../../shared/models/facility/treatment-area-availability-for-edit.model';
import { FacilityAvailabilityService } from '../../../../core/services/facility/facility-availability.service';
import { StringifyOptions } from 'querystring';
import { SpinnerOverlayService } from 'src/app/core/services/common/spinner-overlay.service';

@Component({
  selector: 'spa-facility-availability',
  templateUrl: './facility-availability.component.html',
  styleUrls: ['./facility-availability.component.scss']
})
export class FacilityAvailabilityComponent implements OnInit, OnDestroy {

  userUID: string;

  private subs = new SubSink();

  currentDate: Date = new Date();

  facilityUID: string;
  treatmentAreaUID: string;

  treatmentArea: FacilityTreatmentAreaViewModel;
  availability: TreatmentAreaAvailabilityForViewModel[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private availabilityService: FacilityAvailabilityService,
    private spinnerService: SpinnerOverlayService,
  ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
      this.treatmentAreaUID = this.route.snapshot.paramMap.get('uid');
    });

    this.subs.add(this.route.data
      .subscribe((data: { availability: TreatmentAreaAvailabilityForViewModel[] }) => {
        this.availability = data.availability;

      }));

    this.subs.add(this.route.data
      .subscribe((data: { treatmentArea: FacilityTreatmentAreaViewModel }) => {
        this.treatmentArea = data.treatmentArea;

      }));

  }

  ngOnDestroy() {
    this.spinnerService.hide();
    this.subs.unsubscribe();
  }

  goBack() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  onAppointmentFormOpening(data) {
    var that = this;
    var form = data.form;

    var elements: Array<any> = form.option('items');

    if (elements.length == 10) {
      elements.splice(0, 1);
      elements.splice(4, 3);
      elements.splice(6, 1);

      var hold = elements[1]['colSpan'] = 2;
      var hold = elements[3]['colSpan'] = 2;
      var hold = elements[6]['colSpan'] = 1;
    }
  }

  onAppointmentAdding(e) {

    this.spinnerService.show();

    var item: TreatmentAreaAvailabilityForAddModel = { ...e.appointmentData }


    let tempdate = new Date(item.EndTime);

    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    item.EndTime = tempdate;

    tempdate = new Date(item.StartTime);
    tempdate.setMinutes(tempdate.getMinutes() - tempdate.getTimezoneOffset());
    item.StartTime = tempdate;


    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.addTreatmentAreaAvailability(this.facilityUID, this.treatmentAreaUID, item)
        .subscribe(resp => {

          e.appointmentData.UID = resp.UID;
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            this.spinnerService.hide();
            resolve(true);
          }));
    });
  }

  onAppointmentUpdating(e) {

    this.spinnerService.show();

    var item: TreatmentAreaAvailabilityForViewModel = { ...e.oldData, ...e.newData }
    var editItem: TreatmentAreaAvailabilityForEditModel = { ...e.oldData, ...e.newData }

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.editTreatmentAreaAvailability(this.facilityUID, this.treatmentAreaUID, item)
        .subscribe(resp => {
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            this.spinnerService.hide();
            resolve(true);
          }));
    });
  }

  onAppointmentDeleting(e) {
    this.spinnerService.show();

    e.cancel = new Promise((resolve) => {
      this.subs.add(this.availabilityService.removeTreatmentAreaAvailability(this.facilityUID, this.treatmentAreaUID, e.appointmentData.UID)
        .subscribe(resp => {
          this.spinnerService.hide();
          resolve(false);
        },
          error => {
            this.spinnerService.hide();
            resolve(true);
          }));
    });
  }

}
