import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAvailabilityComponent } from './facility-availability.component';

describe('FacilityAvailabilityComponent', () => {
  let component: FacilityAvailabilityComponent;
  let fixture: ComponentFixture<FacilityAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
