import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SubSink } from 'subsink';
import { FacilityForAuthViewModel } from '../../../models/facility/facility-for-auth-view.model';
import { AuthorizeService } from '../../../../../api-authorization/authorize.service';
import { configSettings } from '../../../../app.config';

@Component({
  selector: 'spa-facility-info',
  templateUrl: './facility-info.component.html',
  styleUrls: ['./facility-info.component.scss']
})
export class FacilityInfoComponent implements OnInit, OnDestroy {
  @Input() facility: FacilityForAuthViewModel;
  @Input() canEdit: boolean = false;
  @Input() canSubmit: boolean = false;

  private subs = new SubSink();

  showSaveButton: boolean = false;
  showSubmitButton: boolean = false;
  uploadDisabled: boolean = false;

  imageUrl: string = 'assets/images/agency_logo_placeholder.png';

  images: any[] = [];
  fileUploadApiPath: string;
  headers: any;

  facilityInfoForm: FormGroup;

  constructor(private authService: AuthorizeService) { }

  ngOnInit() {

    this.subs.add(this.authService.getAccessToken()
      .subscribe(token => {
        this.headers = { 'Authorization': 'Bearer ' + token };
      }));

    this.fileUploadApiPath = configSettings.WebAPI_URL + '/Therapist/TherapistProfile/UpdateProfilePhoto';

    this.facilityInfoForm = new FormGroup({
      Name: new FormControl(),
      Address1: new FormControl(),
      Address2: new FormControl(),
      City: new FormControl(),
      StateCode: new FormControl(),
      ZipCode: new FormControl(),
      Description: new FormControl(),
      Latitude: new FormControl(),
      Longitude: new FormControl(),
      HasHotTub: new FormControl(),
      HasSaunaSteamroom: new FormControl(),
      HasValetParking: new FormControl(),
      HasJacuzzi: new FormControl(),
      HasPool: new FormControl(),
      HasQuietEnvironment: new FormControl(),
      URL: new FormControl()
    });

    if (this.facility) {
      this.populateForm(this.facility);
    }    
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onSave() {

  }

  populateForm(facility: FacilityForAuthViewModel) {

    var fac = new FacilityForUI();
    fac.Name = facility.Name;
    fac.Description = facility.Description;
    fac.Address1 = facility.Address1;
    fac.Address2 = facility.Address2;
    fac.City = facility.City;
    fac.StateCode = facility.StateCode;
    fac.ZipCode = facility.ZipCode;
    fac.Latitude = facility.Latitude;
    fac.Longitude = facility.Longitude;
    fac.HasHotTub = facility.HasHotTub;
    fac.HasSaunaSteamroom = facility.HasSaunaSteamroom;
    fac.HasValetParking = facility.HasValetParking;
    fac.HasJacuzzi = facility.HasJacuzzi;
    fac.HasPool = facility.HasPool;
    fac.HasQuietEnvironment = facility.HasQuietEnvironment;
    fac.URL = facility.URL;

    this.facilityInfoForm.setValue(fac);

    if (facility.LogoUrl) {
      this.imageUrl = facility.LogoUrl;
    }

    this.showSaveButton = this.canEdit;
    this.showSubmitButton = this.canSubmit;

  }

  fileUploader_onUploaded(e) {

  }

  fileUploader_onUploadError(e) {

  }
}


export class FacilityForUI {
  public Name: string;
  public Description: string;
  public Address1: string;
  public Address2?: string;
  public City: string;
  public StateCode: string;
  public ZipCode: string;
  public Latitude: number;
  public Longitude: number;
  public HasHotTub: boolean;
  public HasSaunaSteamroom: boolean;
  public HasValetParking: boolean;
  public HasJacuzzi: boolean;
  public HasPool: boolean;
  public HasQuietEnvironment: boolean;
  public URL: string;
}
