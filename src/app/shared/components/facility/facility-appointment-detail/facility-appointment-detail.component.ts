import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { AuthorizeService, IUser } from '../../../../../api-authorization/authorize.service';
import { AppointmentForFacilityDetailModel } from '../../../../shared/models/appointment/appointment-for-facility-detail.model';
import { FacilityAppointmentService } from '../../../../core/services/facility/facility-appointment.service';
import { AppointmentCancelByFacilityRequestModel } from '../../../../shared/models/appointment/appointment-cancel-by-facility-request.model';

@Component({
  selector: 'app-facility-appointment-detail',
  templateUrl: './facility-appointment-detail.component.html',
  styleUrls: ['./facility-appointment-detail.component.scss']
})
export class FacilityAppointmentDetailComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  appointment: AppointmentForFacilityDetailModel;

  facilityUID: string;

  user: IUser;

  errorMessage: string;
  submissionErrors: string[] = [];
  errorsVisible: boolean;

  showCancelPanel: boolean = false;

  showChangeModal: boolean = false;

  cancelRequest: AppointmentCancelByFacilityRequestModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthorizeService,
    private appservice: FacilityAppointmentService
    //private appointmentsService: TherapistAppointmentsService
  ) { }

  ngOnInit() {

    this.facilityUID = this.route.snapshot.paramMap.get('facilityUID');
    console.log('facilityUID=' + this.facilityUID)
    if (!this.facilityUID) {
      this.route.params.subscribe(params => {
        console.log('params=' + params)
        this.facilityUID = params['facilityUID'];
      });
    };

    this.subs.add(this.route.data
      .subscribe((data: { appointment: AppointmentForFacilityDetailModel }) => {
        this.appointment = data.appointment;

      }));

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }


  processErrors(error) {
    this.submissionErrors = [];
    if (error && error.error && error.error.errors) {
      Object.entries(error.error.errors).forEach((key, value) => {
        //console.log('key=' + key);
        //console.log('value=' + value);

        key.forEach(error => {
          this.submissionErrors.push(error.toString());
        })

      });
      this.errorMessage = error.error.title;
      this.errorsVisible = true;
    } else {
      notify('An error occurred.', "error", 3000);
    }
  }

  cancelAppointment() {

    this.subs.add(this.appservice.startCancel(this.facilityUID, this.appointment.UID, new AppointmentCancelByFacilityRequestModel())
      .subscribe(resp => {
        this.cancelRequest = resp;
        this.showCancelPanel = true;
      },
        error => {
          notify('Unable to cancel appointment.', "error", 3000);
        })
    )
  }

  appointmentCancelled(appointment: AppointmentForFacilityDetailModel) {
    this.showCancelPanel = false;
    if (appointment) {
      this.appointment = appointment;
    }
  }

  rescheduleAppointment() {
    this.showChangeModal = true;
  }

}
