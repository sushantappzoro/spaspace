import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAppointmentDetailComponent } from './facility-appointment-detail.component';

describe('FacilityAppointmentDetailComponent', () => {
  let component: FacilityAppointmentDetailComponent;
  let fixture: ComponentFixture<FacilityAppointmentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAppointmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAppointmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
