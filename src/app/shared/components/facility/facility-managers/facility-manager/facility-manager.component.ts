import { Component, OnInit, ViewChild, Input, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { ModalDirective } from 'ng-uikit-pro-standard';
import { FacilityManagerForAdminModel } from '../../../../models/facility/facility-manager-for-admin.model';
import { CustomValidator } from '../../../../classes/custom-validator';

@Component({
  selector: 'spa-facility-manager',
  templateUrl: './facility-manager.component.html',
  styleUrls: ['./facility-manager.component.scss']
})
export class FacilityManagerComponent implements OnInit, OnChanges, OnDestroy {
  @Input() manager: FacilityManagerForAdminModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveManager: EventEmitter<FacilityManagerForAdminModel> = new EventEmitter<FacilityManagerForAdminModel>();

  @ViewChild('managerModal') managerModal: ModalDirective;

  private subs = new SubSink();

  title: string = 'Add Facility Manager';

  managerForm: FormGroup;

  constructor() { }

  ngOnInit() {

    this.managerForm = new FormGroup({
      UID: new FormControl(),
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      Email: new FormControl('', Validators.required),
      PhoneNumber: new FormControl('', [Validators.required, CustomValidator.phoneValidator]),
      Active: new FormControl(),
      IsAdmin: new FormControl(),
      DisplayName: new FormControl(),
      Status: new FormControl(),
    });

    console.log('manager', this.manager);

    if (this.manager) {
      this.populateForm(this.manager);
    }    

  }

  ngOnChanges(changes: SimpleChanges) {

    if (this.managerModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.managerModal.show();
      } else {
        this.managerModal.hide();
      }
    }

    if (changes.manager) {
      var manager: FacilityManagerForAdminModel = <FacilityManagerForAdminModel> changes.manager.currentValue;
      this.populateForm(manager);
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  populateForm(manager: FacilityManagerForAdminModel) {

    this.title = (this.manager?.UID) ? 'Edit Facility Manager' : 'Add Facility Manager';

    if (!manager.IsAdmin) { manager.IsAdmin = false; }
    if (!manager.Active) { manager.Active = false; }

    this.managerForm.setValue(this.manager);

  }

  onClose() {
    this.manager = new FacilityManagerForAdminModel();
    this.managerForm.reset();
    this.showModalChange.emit(false);
  }

  onSave() {
    console.log('onSave');
    if (this.managerForm.invalid) {
      this.managerForm.markAllAsTouched();
      return;
    }

    var mgr = new FacilityManagerForAdminModel();
    mgr = Object.assign({}, this.manager, this.managerForm.value);

    this.saveManager.emit(mgr);

  }

}
