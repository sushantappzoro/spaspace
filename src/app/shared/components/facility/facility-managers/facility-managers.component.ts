import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';
import { FacilityManagerModel } from '../../../models/facility/facility-manager.model';
import { FacilityManagerService } from '../../../../core/services/facility/facility-manager.service';
import { FacilityManagerForAdminModel } from '../../../models/facility/facility-manager-for-admin.model';
import { FacilityManagerForEditModel } from '../../../models/facility/facility-manager-for-edit.model';
import { FacilityManagerForAdminAddModel } from '../../../models/facility/facility-manager-for-admin-add.model';


@Component({
  selector: 'spa-facility-managers',
  templateUrl: './facility-managers.component.html',
  styleUrls: ['./facility-managers.component.scss']
})
export class FacilityManagersComponent implements OnInit, OnDestroy {
  @ViewChild("managersGrid") managersGrid: DxDataGridComponent;

  private subs = new SubSink();

  facilityManagers: FacilityManagerModel[] = [];
  facilityManager: FacilityManagerForAdminModel;
  facilityUID: string;

  showModal: boolean = false;

  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private managerService: FacilityManagerService
  ) {
    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      console.log('params', params);
      this.facilityUID = params['uid'];
    });

    this.subs.add(this.route.data
      .subscribe((data: { facilityManagers: FacilityManagerModel[] }) => {
        this.facilityManagers = data.facilityManagers;
      },
        error => {
          console.log(error);
        }));

  }

  managersGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    }, {
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        icon: "add",
        stylingMode: "outline",
        text: "Add New Facility Manager",
        hint: "Add New Facility Manager",
        onClick: () => {
          this.addNew();
        }
      }
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addNew() {

    this.facilityManager = {
      Active: false,
      DisplayName: '',
      Email: '',
      FirstName: '',
      IsAdmin: false,
      LastName: '',
      PhoneNumber: '',
      Status: '',
      UID: ''
    }

    this.showModal = true;
  }

  editRecord(e) {

    this.subs.add(this.managerService.getFacilityManagerForFacility(this.facilityUID, e.row.data.UID)
      .subscribe(resp => {
        this.facilityManager = resp;
        this.showModal = true;
      },
        error => {
          notify('Unable to edit facility manager.', 'error', 3000);
        }));
  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveManager(manager: FacilityManagerForAdminModel) {    

    if (manager.UID) {

      var eman: FacilityManagerForEditModel = {
        Active: manager.Active,
        Email: manager.Email,
        FirstName: manager.FirstName,
        IsAdmin: manager.IsAdmin,
        LastName: manager.LastName,
        PhoneNumber: manager.PhoneNumber,
        UID: manager.UID
      };

      this.subs.add(this.managerService.edit(this.facilityUID, eman)
        .subscribe(resp => {
          var i = this.facilityManagers.findIndex(item => item.UID === resp.UID);
          this.facilityManagers.splice(i, 1, this.toManagerFromEdit(eman));
          this.showModal = false;
        },
          error => {
            notify('An error occured updating the facility manager.', 'error', 800);
          }));
    } else {

      var aman: FacilityManagerForAdminAddModel = {
        Admin: manager.IsAdmin,
        Email: manager.Email,
        FirstName: manager.FirstName,
        LastName: manager.LastName,
        PhoneNumber: manager.PhoneNumber
      };

      this.subs.add(this.managerService.add(this.facilityUID, aman)
        .subscribe(resp => {
          this.facilityManagers.unshift(this.toManager(resp));
          this.showModal = false;
        },
          error => {
            notify('An error occured adding the facility manager.', 'error', 800);
          }));
    }
  }

  toManager(manager: FacilityManagerForAdminModel): FacilityManagerModel {
    var mgr: FacilityManagerModel = { Active: manager.Active ? 'Active' : 'Not Active', FirstName: manager.FirstName, LastName: manager.LastName, UID: manager.UID, IsAdmin: manager.IsAdmin };
    return mgr;
  }

  toManagerFromEdit(manager: FacilityManagerForEditModel): FacilityManagerModel {
    var mgr: FacilityManagerModel = { Active: manager.Active ? 'Active' : 'Not Active', FirstName: manager.FirstName, LastName: manager.LastName, UID: manager.UID, IsAdmin: manager.IsAdmin };
    return mgr;
  }

  deleteRow(uid: string) {

    this.subs.add(this.managerService.remove(this.facilityUID, uid)
      .subscribe(resp => {
        var i = this.facilityManagers.findIndex(item => item.UID === uid);
        this.facilityManagers.splice(i, 1);
      },
        error => {
          notify('Unable to delete facility manager.', 'error', 800);
        }));

  }
}
