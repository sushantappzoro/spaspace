import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistProfileViewComponent } from './therapist-profile-view.component';

describe('TherapistProfileViewComponent', () => {
  let component: TherapistProfileViewComponent;
  let fixture: ComponentFixture<TherapistProfileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistProfileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistProfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
