import { Component, OnInit } from '@angular/core';
import { FacilityProfileForUiModel } from '../../../../facility/shared/models/facility-profile-for-ui.model';

@Component({
  selector: 'spa-therapist-profile-view',
  templateUrl: './therapist-profile-view.component.html',
  styleUrls: ['./therapist-profile-view.component.scss']
})
export class TherapistProfileViewComponent implements OnInit {

  facility: FacilityProfileForUiModel = new FacilityProfileForUiModel();

  constructor() { }

  ngOnInit() {
  }

}
