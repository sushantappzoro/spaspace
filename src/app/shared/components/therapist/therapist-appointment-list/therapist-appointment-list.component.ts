import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { TherapistAppointmentSummaryForViewModel } from '../../../models/therapist/therapist-appointment-summary-for-view.model';
import { SubSink } from 'subsink';
import { TherapistAppointmentsService } from '../../../../core/services/therapist/therapist-appointments.service';
import notify from 'devextreme/ui/notify';
import { TherapistAppointmentForDashboardModel } from '../../../models/therapist/therapist-appointment-for-dashboard.model';
import { ModalDirective } from 'ng-uikit-pro-standard';

@Component({
  selector: 'spa-therapist-appointment-list',
  templateUrl: './therapist-appointment-list.component.html',
  styleUrls: ['./therapist-appointment-list.component.scss']
})
export class TherapistAppointmentListComponent implements OnInit {
  @Input() therapistUID: string;
  @Input() appointments: TherapistAppointmentForDashboardModel[];
  @Output() appointmentSelected: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('appointmentModal') appointmentModal: ModalDirective;

  private subs = new SubSink();

  appointmentPopupVisible: boolean;
  appointmentUID: string;

  constructor(private therapistAppointmentService: TherapistAppointmentsService) { }

  ngOnInit() {

    //this.loadAppointments();

  }

  loadUpcomingAppointments() {
    this.subs.add(this.therapistAppointmentService.getUpcomingAppointments(this.therapistUID)
      .subscribe(resp => {
        this.appointments = resp;
      },
        error => {
          notify('Unable to load treatment areas.', "error", 3000);
        }));
  }

  itemClicked(e) {    
    this.appointmentUID = e;
    this.appointmentPopupVisible = true;
   
    this.appointmentModal.show();    
  }

  getClientImage() {
    return 'assets/images/placeholder-person.jpg'
  }

  onAppointmentSelected(uid: string) {
    this.appointmentSelected.emit(uid);

  }

  onClosed(e) {
    this.appointmentPopupVisible = false;
  }

}
