import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistServiceEditComponent } from './therapist-service-edit.component';

describe('TherapistServiceEditComponent', () => {
  let component: TherapistServiceEditComponent;
  let fixture: ComponentFixture<TherapistServiceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistServiceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
