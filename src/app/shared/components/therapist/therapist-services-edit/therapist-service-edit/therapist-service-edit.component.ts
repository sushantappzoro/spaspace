import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { AuthorizedServiceForViewEditModel } from '../../../../models/therapist/authorized-service-for-view-edit.model';

@Component({
  selector: 'spa-therapist-service-edit',
  templateUrl: './therapist-service-edit.component.html',
  styleUrls: ['./therapist-service-edit.component.scss']
})
export class TherapistServiceEditComponent implements OnInit, OnChanges {
  @Input() service: AuthorizedServiceForViewEditModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveService: EventEmitter<AuthorizedServiceForViewEditModel> = new EventEmitter<AuthorizedServiceForViewEditModel>();

  @ViewChild('serviceModal') serviceModal: ModalDirective;

  title: string = 'Edit Authorized Service';

  serviceForm: FormGroup;

  constructor() { }

  ngOnInit() {

    this.serviceForm = new FormGroup({
      ServiceCategory: new FormControl(''),
      WillPerform: new FormControl(true)
    });

    if (this.service) {
      this.serviceForm.patchValue(this.service);
      this.title = (this.service.UID) ? 'Edit Service' : 'Add Service';
    }

  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log('changes', changes);
    if (changes.showModal && this.serviceModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.serviceModal.show();
      } else {
        this.serviceModal.hide();
      }
    }

    if (changes.service && this.serviceForm) {
      if (changes.service.currentValue) {
        this.service = { ...changes.service.currentValue }
        this.title = 'Edit Authorized Service';
        this.serviceForm.patchValue(this.service);
      }
    }

  }

  onClose() {
    this.service = null;
    this.showModalChange.emit(false);
  }

  onSave() {

    //if (this.serviceForm.invalid) {
    //  this.serviceForm.markAllAsTouched();
    //  return;
    //}

    var comp = new AuthorizedServiceForViewEditModel();
    comp = Object.assign({}, this.service, this.serviceForm.value);

    this.saveService.emit(comp);

  }

}
