import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { AuthorizedServiceForViewEditModel } from '../../../models/therapist/authorized-service-for-view-edit.model';
import { TherapistAuthorizedServiceService } from '../../../../core/services/therapist/therapist-authorized-services.service';

@Component({
  selector: 'spa-therapist-services-edit',
  templateUrl: './therapist-services-edit.component.html',
  styleUrls: ['./therapist-services-edit.component.scss']
})
export class TherapistServicesEditComponent implements OnInit {
  @Input() canEdit: boolean = false;
  @Input() therapistUID: string;

  @ViewChild("serviceGrid") serviceGrid: DxDataGridComponent;

  private subs = new SubSink();

  service: AuthorizedServiceForViewEditModel;
  services: AuthorizedServiceForViewEditModel[];

  gridDisabled: boolean = false;

  title: string;
  
  errorText: string;

  showModal: boolean = false;

  constructor(
    private serviceService: TherapistAuthorizedServiceService,
  ) {
    this.editRecord = this.editRecord.bind(this);
  }

  ngOnInit() {

    this.gridDisabled = !this.canEdit;

    this.loadService();

    this.serviceService.stateChanged
      .subscribe(state => {
        if (state) {
          this.services = state.therapistAuthorizedServices;
          if (this.serviceGrid && this.serviceGrid.instance) {
            this.serviceGrid.instance.refresh();
          }
        }
      });

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  servicesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    });
  }

  loadService() {
    this.subs.add(this.serviceService.getTherapistAuthorizedServices(this.therapistUID).subscribe(resp => {
      this.services = resp;
    },
      error => {
        this.services = [];
      }));
  }

  editRecord(e) {
    this.service = new AuthorizedServiceForViewEditModel();
    this.service = { ...e.row.data };
    this.showModal = true;
  }

  saveService(service: AuthorizedServiceForViewEditModel) {

    this.subs.add(this.serviceService.edit(this.therapistUID, service)
      .subscribe(resp => {
        var i = this.services.findIndex(item => item.UID === service.UID);
        //this.services.splice(i, 1, resp);
        this.showModal = false;
      },
        error => {
          notify('An error occured updating the service.', 'error', 3000);
        }));
  }
}
