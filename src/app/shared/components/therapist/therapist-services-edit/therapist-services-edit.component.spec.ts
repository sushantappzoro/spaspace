import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistServicesEditComponent } from './therapist-services-edit.component';

describe('TherapistServicesEditComponent', () => {
  let component: TherapistServicesEditComponent;
  let fixture: ComponentFixture<TherapistServicesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistServicesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistServicesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
