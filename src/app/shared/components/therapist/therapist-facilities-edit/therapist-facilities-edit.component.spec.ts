import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistFacilitiesEditComponent } from './therapist-facilities-edit.component';

describe('TherapistFacilitiesEditComponent', () => {
  let component: TherapistFacilitiesEditComponent;
  let fixture: ComponentFixture<TherapistFacilitiesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistFacilitiesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistFacilitiesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
