import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { TherapistAssociatedFacilityModel } from '../../../../models/therapist/therapist-associated-facility.model';

@Component({
  selector: 'spa-therapist-facility-edit',
  templateUrl: './therapist-facility-edit.component.html',
  styleUrls: ['./therapist-facility-edit.component.scss']
})
export class TherapistFacilityEditComponent implements OnInit, OnChanges {
  @Input() facility: TherapistAssociatedFacilityModel;
  @Input() facilities: TherapistAssociatedFacilityModel;
  @Input() showModal: boolean;
  @Input() facilityName: string;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveFacility: EventEmitter<TherapistAssociatedFacilityModel> = new EventEmitter<TherapistAssociatedFacilityModel>();

  @ViewChild('facilityModal') facilityModal: ModalDirective;

  title: string = 'Add Facility';
  isEdit: boolean = false;

  facilityForm: FormGroup;

  facilityDateOptions: any = {
    'closeAfterSelect': true,
    'dateFormat': 'mm/dd/yyyy'
  }

  modalConfig: any = {
    'ignoreBackdropClick': true
  }

  constructor() {
  }

  ngOnInit() {

    this.facilityForm = new FormGroup({
      UID: new FormControl(),
      FacilityUID: new FormControl('', Validators.required),
      IsDefault: new FormControl(),
      Name: new FormControl()
    });

    if (this.facility) {
      if (this.facility.IsDefault == null) {
        this.facility.IsDefault = false;
      }
      this.facilityForm.setValue(this.facility);
    }

    if (this.facility?.UID) {
      this.isEdit = true;
      this.title = 'Edit Facility';
    } else {
      this.isEdit = false;
      this.title = 'Add Facility';
    }
    
   
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.showModal && this.facilityModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.facilityModal.show();
      } else {
        this.facilityModal.hide();
      }
    }

    if (changes.facility && this.facilityForm) {
      if (changes.facility.currentValue && changes.facility.currentValue.UID) {
        this.facility = { ...changes.facility.currentValue }
        if (this.facility.IsDefault == null) {
          this.facility.IsDefault = false;
        }
        this.isEdit = true;
        this.title = 'Edit Facility';
        this.facilityForm.setValue(this.facility);
      } else {
        this.isEdit = false;
        this.title = 'Add Facility';
        this.facilityForm.reset();
      }

    }
  }

  onClose() {
    this.facility = null;
    this.facilityForm.reset();
    this.showModalChange.emit(false);
  }

  onSave() {
    if (this.facilityForm.invalid) {
      this.facilityForm.markAllAsTouched();
      return;
    }

    var comp = new TherapistAssociatedFacilityModel();
    comp = Object.assign({}, this.facility, this.facilityForm.value);

    this.saveFacility.emit(comp);

  }

}
