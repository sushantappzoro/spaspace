import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistFacilityEditComponent } from './therapist-facility-edit.component';

describe('TherapistFacilityEditComponent', () => {
  let component: TherapistFacilityEditComponent;
  let fixture: ComponentFixture<TherapistFacilityEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistFacilityEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistFacilityEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
