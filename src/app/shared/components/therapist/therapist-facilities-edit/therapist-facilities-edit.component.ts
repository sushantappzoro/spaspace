import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';

import { TherapistAssociatedFacilityModel } from '../../../models/therapist/therapist-associated-facility.model';
import { TherapistFacilityForListModel } from '../../../models/therapist/therapist-facility-for-list.model';
import { TherapistFacilitiesService } from '../../../../core/services/therapist/therapist-facilities.service';
import { FacilityService } from '../../../../core/services/facility/facility.service';
import { ProviderStepperService } from '../../../../provider/core/services/provider-stepper.service';
import { FacilityForListModel } from '../../../models/facility/facility-for-list.model';


@Component({
  selector: 'spa-therapist-facilities-edit',
  templateUrl: './therapist-facilities-edit.component.html',
  styleUrls: ['./therapist-facilities-edit.component.scss']
})
export class TherapistFacilitiesEditComponent implements OnInit, OnDestroy {
  @Input() canEdit: boolean = false;
  @Input() therapistUID: string;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  @ViewChild("facilitiesGrid") facilitiesGrid: DxDataGridComponent;

  private subs = new SubSink();

  gridDisabled: boolean = false;

  facility: TherapistAssociatedFacilityModel;
  associatedFacilities: TherapistAssociatedFacilityModel[]
  facilityNameForEdit: string;

  facilitiesList: FacilityForListModel[];
  filteredFacilitiesList: TherapistFacilityForListModel[];

  showModal: boolean = false;
  deleteUID: string;
  showDeleteWarning: boolean = false;


  constructor(
    private facilityService: FacilityService,
    private therapistFacilityService: TherapistFacilitiesService,
    private stepperService: ProviderStepperService

  ) {
    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
  }

  ngOnInit() {

    this.loadFacilitiesList();
    this.loadFacilities(this.therapistUID);

    this.gridDisabled = !this.canEdit;

    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {
        if (step == this.stepNumber) {
          this.stepperService.completeStep(this.stepNumber);
        }
      }));

    this.therapistFacilityService.stateChanged
      .subscribe(state => {
        if (state) {
          this.associatedFacilities = state.therapistFacilities;
          if (this.facilitiesGrid && this.facilitiesGrid.instance) {
            this.facilitiesGrid.instance.refresh();
            this.filterFacilities();
          }          
        }

      });

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadFacilitiesList() {    
    this.subs.add(this.facilityService.getActive()
      .subscribe(facilities => {
        this.facilitiesList = facilities;
        this.filterFacilities();
      },
        error => {
          console.log(error);
        }));
  }

  loadFacilities(userUID: string) {
    this.subs.add(this.therapistFacilityService.getTherapistFacilities(userUID)
      .subscribe(resp => {
        this.associatedFacilities = resp;
        if (this.facilitiesGrid && this.facilitiesGrid.instance) {
          this.facilitiesGrid.instance.refresh();
          this.filterFacilities();
        }    
    },
      error => {
        console.log(error);
      }));
  }

  filterFacilities() {
    this.filteredFacilitiesList = [];
    if (this.facilitiesList && this.associatedFacilities) {
      this.facilitiesList.forEach(fac => {       
        const temp = this.associatedFacilities.find(x => x.FacilityUID == fac.UID);        
        if (!temp) {
          this.filteredFacilitiesList.push(fac);
        }
      });
    }
  }

  facilitiesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    },{
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
        options: {
        visible: this.canEdit,
        icon: "add",
        stylingMode: "outline",
        text: "Add",
        hint: "Add New Facility",
        onClick: () => {
          this.addRecord();
        }
      }
    });
  }

  addRecord() {
    this.filterFacilities();
    this.facility = new TherapistAssociatedFacilityModel();
    this.showModal = true;
  }

  editRecord(e) {
    this.facility = new TherapistAssociatedFacilityModel();
    this.facility = { ...e.row.data };

    this.filteredFacilitiesList = [];
    let fac = this.facilitiesList.find(x => x.UID == e.row.data.FacilityUID);
    if (fac) {
      this.filteredFacilitiesList.push(fac);
    }

    this.showModal = true;
  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveFacility(facility: TherapistAssociatedFacilityModel) {

    if (facility.IsDefault == null) {
      facility.IsDefault = false;
    }

    if (facility.UID) {

      this.subs.add(this.therapistFacilityService.edit(this.therapistUID, facility)
        .subscribe(resp => {
          this.associatedFacilities = resp;
          //var i = this.associatedFacilities.findIndex(item => item.UID === facility.UID);
          //this.associatedFacilities.splice(i, 1, resp);
          this.showModal = false;
        },
          error => {
            notify('An error occured updating the facility.', 'error', 3000);
          }));

    } else {

      this.subs.add(this.therapistFacilityService.add(this.therapistUID, facility)
        .subscribe(resp => {
          //this.associatedFacilities.unshift(resp);
          this.associatedFacilities = resp;
          this.showModal = false;
        },
          error => {
            notify('An error occured adding the facility.', 'error', 3000);
          }));
    }
  }

  deleteRow(uid: string) {

    this.subs.add(this.therapistFacilityService.remove(this.therapistUID, uid)
      .subscribe(resp => {
        //this.loadFacilities(this.therapistUID);
      },
        error => {
          notify('An error occured deleting the facility.', 'error', 3000);
        }));

  }  

}
