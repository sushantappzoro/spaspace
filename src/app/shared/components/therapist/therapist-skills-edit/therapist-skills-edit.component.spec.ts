import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistSkillsEditComponent } from './therapist-skills-edit.component';

describe('TherapistSkillsEditComponent', () => {
  let component: TherapistSkillsEditComponent;
  let fixture: ComponentFixture<TherapistSkillsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistSkillsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistSkillsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
