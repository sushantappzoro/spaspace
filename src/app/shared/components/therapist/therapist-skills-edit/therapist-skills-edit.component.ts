import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { TherapistProficiencyValues, TherapistPressureValues } from '../../../globals';
import { TherapistProfileForEditModel } from '../../../models/therapist/therapist-profile-for-edit.model';
import { TherapistProfileService } from '../../../../core/services/therapist/therapist-profile.service';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { ProviderStepperService } from '../../../../provider/core/services/provider-stepper.service';
import { SpaTherapistExpertiseForViewModel } from '../../../../../../projects/spaspace-lib/src/shared/models';

@Component({
  selector: 'spa-therapist-skills-edit',
  templateUrl: './therapist-skills-edit.component.html',
  styleUrls: ['./therapist-skills-edit.component.scss']
})
export class TherapistSkillsEditComponent implements OnInit, OnDestroy, OnChanges {
  @Input() showSaveButton: boolean;
  @Input() canEdit = false;
  @Input() therapistProfile: TherapistProfileForEditModel;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  private subs = new SubSink();

  value = 5;

  options = TherapistProficiencyValues;
  pressures = TherapistPressureValues;

  personalSkillsForm: FormGroup;

  expertises: SpaTherapistExpertiseForViewModel[];

  constructor(
    private therapistService: TherapistProfileService,
    private stepperService: ProviderStepperService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.subs.add(this.therapistService.stateChanged
      .subscribe(state => {
        console.log('therapistService.stateChanged');
        if (state) {
          this.expertises = state.therapist.Expertises;
        }
      }));

    this.expertises = this.therapistProfile.Expertises;
    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {
        if (step === this.stepNumber) {
          this.performValidation();
        }
      }));

    this.personalSkillsForm = this.formBuilder.group({
      PressureCapability: new FormControl(''),
      items: this.formBuilder.array([])
    });

    this.options.disabled = !this.canEdit;
    this.pressures.disabled = !this.canEdit;

    // this.showProfile();
    this.populateForm();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.therapistProfile) {
      console.log('therapist changed', this.therapistProfile);
    }
  }

  populateForm() {
    // this.therapistProfile.Expertises.forEach(expertise => {
    this.personalSkillsForm.controls['PressureCapability'].patchValue(this.therapistProfile.PressureCapability);
    this.expertises.forEach(expertise => {
      // var need = this.userSession.Needs.find(x => x.ID === reason.ID)
      // reason.IsChecked = (need && need.IsChecked === true) ? true : false;
      this.itemArray.push(this.addFormControl(expertise));
    });
  }

  get itemArray() {
    return this.personalSkillsForm.get('items') as FormArray;
  }

  addFormControl(exp: SpaTherapistExpertiseForViewModel) {
    return this.formBuilder.group({
      UID: exp.UID,
      ExpertiseName: exp.ExpertiseName,
      ExpertiseID: exp.ExpertiseID,
      SkillLevel: exp.SkillLevel
    });
  }

  performValidation() {

    if (this.personalSkillsForm) {
      if (!this.personalSkillsForm.errors && !this.personalSkillsForm.dirty && this.personalSkillsForm.valid) {
        this.stepperService.completeStep(this.stepNumber);
        return;
      }

      if (this.personalSkillsForm.errors || !this.personalSkillsForm.valid) {
        this.stepperService.reportError('Cannot proceed to next step until all required fields are completed.');
        return;
      }

      if (this.personalSkillsForm.dirty) {
        this.onSave();
        return;
      }
    }

  }

  showProfile() {

    // this.onChanges();

    if (this.therapistProfile) {
      this.personalSkillsForm.patchValue(this.therapistProfile);

      if (this.canEdit) {
        // this.showSaveButton = true;
        // this.showSubmitButton = false;
        // this.uploadDisabled = false;
      } else {
        this.personalSkillsForm.controls['PressureCapability'].disable();
        this.personalSkillsForm.controls['SportsMassage'].disable();
        this.personalSkillsForm.controls['DeepTissue'].disable();
        this.personalSkillsForm.controls['Stretching'].disable();
        this.personalSkillsForm.controls['SwedishMassage'].disable();
        this.personalSkillsForm.controls['ThaiMassage'].disable();
        this.personalSkillsForm.controls['Reflexology'].disable();
        this.personalSkillsForm.controls['PreNatalMassage'].disable();
        this.personalSkillsForm.controls['ReikiEnergy'].disable();
        this.personalSkillsForm.controls['Shiatsu'].disable();
        this.personalSkillsForm.controls['LomiLomi'].disable();

      }

      // if (this.personalSkillsForm.valid && !this.personalSkillsForm.errors) {
      //  this.isCompleteChange.emit(true);
      // } else {
      //  this.isCompleteChange.emit(false);
      // }

    }

  }

  // private toProfile(therapist: TherapistProfileForEditModel): TherapistSkillsForUIModel {

  //  var skills = new TherapistSkillsForUIModel();
  //  skills.PressureCapability = therapist.PressureCapability;
  //  skills.SportsMassage = therapist.Expertise.SportsMassage;
  //  skills.DeepTissue = therapist.Expertise.DeepTissue;
  //  skills.Stretching = therapist.Expertise.Stretching;
  //  skills.SwedishMassage = therapist.Expertise.SwedishMassage;
  //  skills.ThaiMassage = therapist.Expertise.ThaiMassage;
  //  skills.Reflexology = therapist.Expertise.Reflexology;
  //  skills.PreNatalMassage = therapist.Expertise.PreNatalMassage;
  //  skills.ReikiEnergy = therapist.Expertise.ReikiEnergy;
  //  skills.Shiatsu = therapist.Expertise.Shiatsu;
  //  skills.LomiLomi = therapist.Expertise.LomiLomi;

  //  return skills;
  // }

  onSave() {

    let therapist = new TherapistProfileForEditModel();

    therapist = Object.assign({}, this.therapistProfile);

    console.log('personalSkillsForm.value: ', this.personalSkillsForm);

    therapist.Expertises = [];
    this.personalSkillsForm.value.items.forEach(item => {
      therapist.Expertises.push(item);
    });

    therapist.PressureCapability = this.personalSkillsForm.controls['PressureCapability'].value;
    console.log(therapist);

    this.therapistService.editTherapist(therapist)
      .subscribe(res => {
        this.therapistProfile = res;
        this.showProfile();
        if (this.runAsStep) {
          this.stepperService.completeStep(this.stepNumber);
        } else {
          notify('Therapist updated successfully.', 'success', 3000);
        }
      },
        error => {
          notify('Unable to save therapist profile.', "error", 3000);
        });

  }
}


// export class TherapistSkillsForUIModel {
//  public PressureCapability: number;
//  public SportsMassage: number;
//  public DeepTissue: number;
//  public Stretching: number;
//  public SwedishMassage: number;
//  public ThaiMassage: number;
//  public Reflexology: number;
//  public PreNatalMassage: number;
//  public ReikiEnergy: number;
//  public Shiatsu: number;
//  public LomiLomi: number;
// }
