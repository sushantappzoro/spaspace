import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TherapistProfileForEditModel } from '../../../models/therapist/therapist-profile-for-edit.model';

@Component({
  selector: 'spa-therapist-requirements-edit',
  templateUrl: './therapist-requirements-edit.component.html',
  styleUrls: ['./therapist-requirements-edit.component.scss']
})
export class TherapistRequirementsEditComponent implements OnInit {
  @Input() canEdit: boolean;
  @Input() therapist: TherapistProfileForEditModel;

  @Output() submitApplication: EventEmitter<any> = new EventEmitter<any>();

  agreedToTerms: boolean = false;
  agreedToIndepententContractor: boolean = false;
  agreedToStandards: boolean = false;

  submitDisabled: boolean = true;
  showSubmitButton: boolean = true;

  theTitle: string;
  theDescription: string;
  popupVisible: boolean = false;

  termssrc: string = '/spa/assets/content/SpaSpaceAppTermsofUse.pdf';

  constructor() { }

  ngOnInit() {

    if (this.therapist) {
      this.agreedToIndepententContractor = this.therapist.IndependentContractorAcknowledged;
      this.agreedToStandards = this.therapist.ServiceStandardsAcknowledged;
      this.agreedToTerms = this.therapist.TermsAndPoliciesAcknowledged;
    }


    this.showSubmitButton = this.canEdit;
  }

  onAgreedToTerms() {
    this.therapist.TermsAndPoliciesAcknowledged = this.agreedToTerms;
    this.therapist.TermsAndPoliciesAcknowledgedDate = this.agreedToTerms ? new Date() : null;
    this.onValueChanged();
  }

  onAgreedToIndepententContractor() {
    this.therapist.IndependentContractorAcknowledged = this.agreedToIndepententContractor;
    this.therapist.IndependentContractorAcknowledgedDate = this.agreedToIndepententContractor ? new Date() : null;
    this.onValueChanged();
  }

  onAgreedToStandards() {
    this.therapist.ServiceStandardsAcknowledged = this.agreedToStandards;
    this.therapist.ServiceStandardsAcknowledgedDate = this.agreedToStandards ? new Date(): null;
    this.onValueChanged();
  }

  onValueChanged() {
    if (this.agreedToIndepententContractor && this.agreedToStandards && this.agreedToTerms) {
      this.submitDisabled = false;
    } else {
      this.submitDisabled = true;
    }
  }

  onSubmit() {
    
    if (this.checkComplete()) {
      this.submitApplication.emit(null);
    } else {
      this.theTitle = 'Agree to Terms';
      this.theDescription = 'Please acknowledge all requirements.';
      this.popupVisible = true;
    }
    
  }

  onTermsViewClick() {
    this.theTitle = 'Terms and Conditions';
    //this.theDescription = 'terms will go here';
    this.popupVisible = true;
  }

  onPrintTermsClick() {
    console.log('onPrintTermsClick')
    window.open(this.termssrc, "_blank");
  }

  checkComplete(): boolean {
    if (this.therapist.TermsAndPoliciesAcknowledged && this.therapist.IndependentContractorAcknowledged && this.therapist.ServiceStandardsAcknowledged) {
      return true;
    }
    return false;
  }

}
