import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistRequirementsEditComponent } from './therapist-requirements-edit.component';

describe('TherapistRequirementsEditComponent', () => {
  let component: TherapistRequirementsEditComponent;
  let fixture: ComponentFixture<TherapistRequirementsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistRequirementsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistRequirementsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
