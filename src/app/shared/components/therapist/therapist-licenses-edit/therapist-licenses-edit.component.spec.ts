import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistLicensesEditComponent } from './therapist-licenses-edit.component';

describe('TherapistLicensesEditComponent', () => {
  let component: TherapistLicensesEditComponent;
  let fixture: ComponentFixture<TherapistLicensesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistLicensesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistLicensesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
