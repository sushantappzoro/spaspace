import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxDataGridComponent } from 'devextreme-angular';

import { TherapistLicenseModel } from '../../../models/therapist/therapist-license.model';
import { configSettings } from '../../../../app.config';
import { TherapistLicensesService } from '../../../../core/services/therapist/therapist-licenses.service';
import { StatesService, StateModel } from '../../../../core/services/states.service';
import { ProviderStepperService } from '../../../../provider/core/services/provider-stepper.service';
import { TherapistAuthorizedServiceService } from '../../../../core/services/therapist/therapist-authorized-services.service';
import { take } from 'rxjs/operators';
import { LicenseTypeModel } from '../../../models/common/license-type.model';
import { LicenseTypeService } from '../../../../core/services/common/license-type.service';
import { TherapistProfileService } from '../../../../core/services/therapist/therapist-profile.service';

@Component({
  selector: 'spa-therapist-licenses-edit',
  templateUrl: './therapist-licenses-edit.component.html',
  styleUrls: ['./therapist-licenses-edit.component.scss']
})
export class TherapistLicensesEditComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() canEdit: boolean = false;
  @Input() therapistUID: string;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  @ViewChild("licensesGrid") licensesGrid: DxDataGridComponent;

  private subs = new SubSink();

  license: TherapistLicenseModel;
  licenses: TherapistLicenseModel[];

  showModal: boolean = false;

  states: StateModel[];
  licenseTypes: LicenseTypeModel[];

  gridDisabled: boolean = false;

  licenseUploadApiPath: string;

  photoUrl: string;
  photoPopupVisible: boolean = false;

  popupVisible: boolean;
  title: string;
  uploadPath: string;

  values: any[] = [];;

  headers: any;

  errorText: string = null;

  licenseSelectedRowIndex: number = -1;

  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private licenseService: TherapistLicensesService,
    private serviceService: TherapistAuthorizedServiceService,
    private licenseTypeService: LicenseTypeService,
    private therapistProfileService: TherapistProfileService,
    private stateService: StatesService,
    private stepperService: ProviderStepperService
  ) {

    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
    this.uploadLicenseClick = this.uploadLicenseClick.bind(this);
    this.showLicenseClick = this.showLicenseClick.bind(this);

  }

  ngOnInit() {

    this.gridDisabled = !this.canEdit;

    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {
        if (step == this.stepNumber) {
          this.performValidation();
        }
      }));

    this.states = this.stateService.getStates();
    //console.log('states', this.states);


    this.licenseUploadApiPath = configSettings.WebAPI_URL + '/auth/therapists/';   

    this.licenseService.stateChanged
      .subscribe(state => {
        if (state) {

          this.licenses = state.therapistLicenses
          this.refreshLicenseGrid();
        }

      });

    this.subs.add(this.licenseTypeService.getAll()
      .subscribe(resp => {
        this.licenseTypes = resp;
      },
        error => {
          console.log(error);
        }
      )
    );
  }

  ngAfterViewInit() {
    this.loadLicenses();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }  

  licensesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    }, {
    //  location: 'center',
    //  template: 'errorText'
    //}, {
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        visible: this.canEdit,
        icon: "add",
        stylingMode: "outline",
        text: "Add",
        hint: "Add New License",
        onClick: () => {
          this.addRecord();
        }
      }
    });
  }
 
  refreshLicenseGrid() {
    //if (this.licensesGrid && this.licensesGrid.instance) {
    //  this.licensesGrid.instance.refresh();
    //}
  }

  loadLicenses() {
    console.log('loadLicenses');
    this.subs.add(this.licenseService.getTherapistLicenses(this.therapistUID, true).subscribe(resp => {
      if (resp) {
        this.licenses = resp;
      }

      if (!this.licenses) {
        this.licenses = [];
      }

      this.refreshLicenseGrid();

      //this.checkForValid();
    },
      error => {
        this.licenses = [];
        console.log('an error occured', error);
      }));
  }

  addRecord() {
    this.license = new TherapistLicenseModel();
    this.showModal = true;
  }

  editRecord(e) {
    this.license = new TherapistLicenseModel();
    this.license = { ...e.row.data };
    this.showModal = true;
  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveLicense(license: TherapistLicenseModel) {

    if (license.UID) {

      this.subs.add(this.licenseService.edit(this.therapistUID, license)
        .subscribe(resp => {
          var i = this.licenses.findIndex(item => item.UID === license.UID);
          //this.licenses.splice(i, 1, resp);
          this.showModal = false;
          this.checkForValid();
          this.reloadAuthorizedServices();
          this.reloadTherapistProfile();
        },
          error => {
            notify('An error occured updating the license.', 'error', 3000);
          }));

    } else {

      this.subs.add(this.licenseService.add(this.therapistUID, license)
        .subscribe(resp => {
          //this.licenses.unshift(resp);
          this.showModal = false;
          this.checkForValid();
          this.reloadAuthorizedServices();
          this.reloadTherapistProfile();
        },
          error => {
            notify('An error occured adding the license.', 'error', 3000);
          }));
    }
  }

  deleteRow(uid: string) {

    this.subs.add(this.licenseService.remove(this.therapistUID, uid)
      .subscribe(resp => {
        this.checkForValid();
        //this.loadLicenses();
        this.reloadAuthorizedServices();
        this.reloadTherapistProfile();
      },
        error => {
          notify('An error occured deleting the license.', 'error', 3000);
        }));

  }

  validateEndDate(e) {
    if (e && e.value && e.data.AsOfDate) {
      var endDate = new Date(e.value);
      if (endDate < new Date()) {
        e.rule.message = "Until Date cannot be prior to today's date"
        return false;
      }

      var startDate = new Date(e.data.AsOfDate);

      if (endDate > startDate) {
        return true;
      }
      return false;
    }
    return true;
  }


  onfileUploaded(resp: string) {
    this.loadLicenses();
  }

  uploadLicenseClick(e) {
    e.event.preventDefault();
    //console.log('uploadLicenseClick', e.row.data.UID);
    this.title = 'Upload License Image';
    this.uploadPath = this.licenseUploadApiPath + this.therapistUID + '/licenses/' + e.row.data.UID;
    this.popupVisible = true;
  }

  onHiding() {
    //this.popupVisible = false;
  }

  isSearchButtonVisible(e) {
    if (e.row.data.FileUrl && e.row.data.FileUrl.length > 0) {
      return true;
    }
    return false;
  }

  isUploadButtonVisible(e) {

    if (this.canEdit == false) {
      return false;
    }

    if (e.row.isNewRow) {
      return false;
    }
    return true;
  }

  showLicenseClick(e) {
    if (e.row.data.FileUrl && e.row.data.FileUrl.endsWith('.pdf')) {
      window.open(e.row.data.FileUrl, '_blank');
    } else {
      this.title = "License on file";
      this.photoUrl = e.row.data.FileUrl;
      this.photoPopupVisible = true;
    }
  }

  checkForValid() {
    console.log('checkForValid');
    if (!this.licenses || this.licenses.length === 0) {
      this.errorText = "At least one license is required.";
    } else {
      this.errorText = null;
    }
  }

  performValidation() {
    if (!this.licenses || this.licenses.length === 0) {
      //console.log('performValidation2');
      this.stepperService.reportError('At least one license is required.');
      return;
    }

    var valid = true;

    this.licenses.forEach(item => {
      //console.log('insurance', item);
      if (!item.FileUrl || item.FileUrl.length == 0) {
        valid = false;
      }
    });

    if (!valid) {
      this.errorText = 'Please upload image of all licenses.';
      this.stepperService.reportError('Cannot proceed to next step until image of license is uploaded.');
      return;
    }

    this.stepperService.completeStep(this.stepNumber);
  }

  reloadAuthorizedServices() {
    this.subs.add(this.serviceService.reloadTherapistAuthorizedServices(this.therapistUID)
      .pipe(take(1))
      .subscribe()  
    )
  }

  reloadTherapistProfile() {
    this.subs.add(this.therapistProfileService.reloadTherapist(this.therapistUID)
      .pipe(take(1))
      .subscribe()
    )
  }

}
