import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistLicenseEditComponent } from './therapist-license-edit.component';

describe('TherapistLicenseEditComponent', () => {
  let component: TherapistLicenseEditComponent;
  let fixture: ComponentFixture<TherapistLicenseEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistLicenseEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistLicenseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
