import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';

import { TherapistLicenseModel } from '../../../../models/therapist/therapist-license.model';
import { StateModel, StatesService } from '../../../../../core/services/states.service';
import { CustomValidator } from '../../../../classes/custom-validator';
import { LicenseTypeModel } from '../../../../models/common/license-type.model';
import { LicenseTypeService } from '../../../../../core/services/common/license-type.service';

@Component({
  selector: 'spa-therapist-license-edit',
  templateUrl: './therapist-license-edit.component.html',
  styleUrls: ['./therapist-license-edit.component.scss']
})
export class TherapistLicenseEditComponent implements OnInit, OnChanges {
  @Input() license: TherapistLicenseModel;
  @Input() showModal: boolean;
  @Input() licenseTypes: LicenseTypeModel[];

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveLicense: EventEmitter<TherapistLicenseModel> = new EventEmitter<TherapistLicenseModel>();

  @ViewChild('licenseModal') licenseModal: ModalDirective;

  title: string = 'Add License';

  licenseForm: FormGroup;

  states: StateModel[];  

  constructor(
    private stateService: StatesService
  ) { }

  ngOnInit() {

    this.licenseForm = new FormGroup({
      UID: new FormControl(),
      StateCode: new FormControl('', Validators.required),
      LicenseTypeID: new FormControl('', Validators.required),
      AsOfDate: new FormControl('', Validators.required),
      UntilDate: new FormControl('', Validators.required)
    },
      { validators: CustomValidator.compareStartAndEndDates('AsOfDate', 'UntilDate') }
    );

    if (this.license) {
      this.licenseForm.patchValue(this.license);
      this.title = (this.license.UID) ? 'Edit License' : 'Add License';
    }       

    this.states = this.stateService.getStates();

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    if (changes.showModal && this.licenseModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.licenseModal.show();
      } else {
        this.licenseModal.hide();
      }
    }

    if (changes.license && this.licenseForm) {
      if (changes.license.currentValue && changes.license.currentValue.UID) {
        this.license = { ...changes.license.currentValue }
        this.title = 'Edit License';
        this.licenseForm.patchValue(this.license);
      } else {
        this.title = 'Add License';
        this.licenseForm.reset();
      }
    }

  }

  onClose() {
    this.license = null;
    this.showModalChange.emit(false);
  }

  onSave() {

    if (this.licenseForm.invalid) {
      this.licenseForm.markAllAsTouched();
      return;
    }

    var comp = new TherapistLicenseModel();
    comp = Object.assign({}, this.license, this.licenseForm.value);

    this.saveLicense.emit(comp);

  }
}
