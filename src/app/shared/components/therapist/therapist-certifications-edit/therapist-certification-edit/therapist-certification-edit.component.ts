import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';

import { TherapistCertificationModel } from '../../../../models/therapist/therapist-certification.model';

@Component({
  selector: 'spa-therapist-certification-edit',
  templateUrl: './therapist-certification-edit.component.html',
  styleUrls: ['./therapist-certification-edit.component.scss']
})
export class TherapistCertificationEditComponent implements OnInit, OnChanges {
  @Input() certification: TherapistCertificationModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveCertification: EventEmitter<TherapistCertificationModel> = new EventEmitter<TherapistCertificationModel>();

  @ViewChild('certificationModal') certificationModal: ModalDirective;

  title: string = 'Add Certification';

  certificationForm: FormGroup;

  certificationDateOptions: any = {
    'closeAfterSelect': true,
    'dateFormat': 'mm/dd/yyyy'
  }

  modalConfig: any = {
    'ignoreBackdropClick': true
  }

  constructor(

  ) { }

  ngOnInit() {

    this.certificationForm = new FormGroup({
      UID: new FormControl(),
      Name: new FormControl('', Validators.required),
      Description: new FormControl('', Validators.required),
      CertificateDate: new FormControl('', Validators.required),
      CertifyingOrganization: new FormControl('', Validators.required),
      FileUrl: new FormControl(),
    });

    if (this.certification) {
      this.certificationForm.setValue(this.certification);
    }

    this.title = (this.certification?.UID) ? 'Edit Certification' : 'Add Certification';

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges', changes);
    if (changes.showModal && this.certificationModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.certificationModal.show();
      } else {
        this.certificationModal.hide();
      }
    }

    if (changes.certification && this.certificationForm) {
      if (changes.certification.currentValue && changes.certification.currentValue.UID) {
        this.certification = { ...changes.certification.currentValue }
        this.title = 'Edit Certification';
        this.certificationForm.setValue(this.certification);
      } else {
        this.title = 'Add Certification';
        this.certificationForm.reset();
      }
      
    }
  }

  onClose() {
    this.certification = null;
    this.certificationForm.reset();
    this.showModalChange.emit(false);
  }

  onSave() {

    if (this.certificationForm.invalid) {
      this.certificationForm.markAllAsTouched();
      return;
    }

    var comp = new TherapistCertificationModel();
    comp = Object.assign({}, this.certification, this.certificationForm.value);

    this.saveCertification.emit(comp);

  }

}
