import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core';
import { SubSink } from 'subsink';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
//import config from 'devextreme/core/config';
//import repaintFloatingActionButton from 'devextreme/ui/speed_dial_action/repaint_floating_action_button';

import { TherapistCertificationModel } from '../../../models/therapist/therapist-certification.model';
import { configSettings } from '../../../../app.config';
import { TherapistCertificatesService } from '../../../../core/services/therapist/therapist-certificates.service';
import { ProviderStepperService } from '../../../../provider/core/services/provider-stepper.service';
import { TherapistProfileService } from '../../../../core/services/therapist/therapist-profile.service';

@Component({
  selector: 'spa-therapist-certifications-edit',
  templateUrl: './therapist-certifications-edit.component.html',
  styleUrls: ['./therapist-certifications-edit.component.scss']
})
export class TherapistCertificationsEditComponent implements OnInit, OnDestroy {
  @Input() canEdit: boolean = false;
  @Input() therapistUID: string;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  @ViewChild("certificationsGrid") certificationsGrid: DxDataGridComponent;

  private subs = new SubSink();

  certification: TherapistCertificationModel;
  certifications: TherapistCertificationModel[];

  showModal: boolean = false;

  gridDisabled: boolean = false;

  certificateUploadApiPath: string;

  photoUrl: string;
  photoPopupVisible: boolean = false;

  popupVisible: boolean;
  title: string;
  uploadPath: string;

  values: any[] = [];;

  headers: any;

  errorText: string = null;

  certificateSelectedRowIndex: number = -1;

  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private certificationService: TherapistCertificatesService,
    //private therapistProfileService: TherapistProfileService,
    private stepperService: ProviderStepperService
  ) {

    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
    this.uploadCertificateClick = this.uploadCertificateClick.bind(this);
    this.showCertificateClick = this.showCertificateClick.bind(this);

  }

  ngOnInit() {

    //config({
    //  floatingActionButtonConfig: {
    //    icon: "rowfield",
    //    shading: true,
    //    position: {
    //      of: "#certificationsGrid",
    //      my: "right bottom",
    //      at: "right bottom",
    //      offset: "-16 -16"
    //    }
    //  }
    //});

    this.gridDisabled = !this.canEdit;

    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {
        if (step == this.stepNumber) {
          this.performValidation();
        }
      }));

    this.certificateUploadApiPath = configSettings.WebAPI_URL + '/auth/therapists/';

    this.loadCertifications();

    this.certificationService.stateChanged
      .subscribe(state => {
        if (state) {
          this.certifications = state.therapistCertifications
          this.refreshCertificationGrid();
        }
       
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  certificationsGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    }, {
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
      options: {
        visible: this.canEdit,
        icon: "add",
        stylingMode: "outline",
        text: "Add",
        hint: "Add New Certification",
        onClick: () => {
          this.addRecord();
        }
      }
    });
  }

  refreshCertificationGrid() {
    if (this.certificationsGrid && this.certificationsGrid.instance) {
      this.certificationsGrid.instance.refresh();
    }  
  }

  loadCertifications() {
    this.subs.add(this.certificationService.getTherapistCertifications(this.therapistUID)
      .subscribe(resp => {
        if (resp) {
          this.certifications = resp;
        }

        if (!this.certifications) {
          this.certifications = [];
        }

        this.refreshCertificationGrid();
        
      },
        error => {
          this.certifications = [];
          //notify('An error occured updating the attendees. Please try again', 'error', 800);            
        }));
  }

  selectedCertificateChanged(e) {
    this.certificateSelectedRowIndex = e.component.getRowIndexByKey(e.selectedRowKeys[0]);
  }
  
  addRecord() {
    this.certification = new TherapistCertificationModel();
    this.showModal = true;
  }

  editRecord(e) {    
    this.certification = new TherapistCertificationModel();
    this.certification = { ...e.row.data };
    this.showModal = true;
  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveCertification(certification: TherapistCertificationModel) {

    if (certification.UID) {

      this.subs.add(this.certificationService.edit(this.therapistUID, certification)
        .subscribe(resp => {
          var i = this.certifications.findIndex(item => item.UID === certification.UID);
          //this.certifications.splice(i, 1, resp);
          //this.therapistProfileService.reloadTherapist(this.therapistUID);
          this.showModal = false;          
        },
          error => {
            notify('An error occured updating the certification.', 'error', 3000);
          }));

    } else {

      this.subs.add(this.certificationService.add(this.therapistUID, certification)
        .subscribe(resp => {
          //this.certifications.unshift(resp);
          //this.therapistProfileService.reloadTherapist(this.therapistUID);
          this.showModal = false;
        },
          error => {
            notify('An error occured adding the certification.', 'error', 3000);  
          }));     
    }
  }

  deleteRow(uid: string) {

    this.subs.add(this.certificationService.remove(this.therapistUID,uid)
      .subscribe(resp => {
        //this.loadCertifications();
        //this.therapistProfileService.reloadTherapist(this.therapistUID);
      },
        error => {
          notify('An error occured deleting the certification.', 'error', 3000);  
        }));

  }

  //validateCertificateDate(e) {
  //  if (e && e.value) {
  //    var endDate = new Date(e.value);
  //    if (endDate > new Date()) {
  //      return false;
  //    }
  //    return true;
  //  }
  //  return true;
  //}  

  //validateEndDate(e) {
  //  if (e && e.value && e.data.AsOfDate) {
  //    var endDate = new Date(e.value);
  //    if (endDate < new Date()) {
  //      e.rule.message = "Until Date cannot be prior to today's date"
  //      return false;
  //    }

  //    var startDate = new Date(e.data.AsOfDate);

  //    if (endDate > startDate) {
  //      return true;
  //    }
  //    return false;
  //  }
  //  return true;
  //}


  uploadCertificateClick(e) {
    e.event.preventDefault();
    //console.log('uploadCertificateClick', e.row.data.UID);
    this.title = 'Upload Certificate Image';
    this.uploadPath = this.certificateUploadApiPath + this.therapistUID + '/certifications/' + e.row.data.UID;
    this.popupVisible = true;
  }

  onfileUploaded(resp: string) {    
    this.certificationService.reloadCertifications(this.therapistUID);
    this.refreshCertificationGrid();
  }
  
  onHiding() {
    this.popupVisible = false;
  }

  showCertificateClick(e) {
    if (e.row.data.FileUrl && e.row.data.FileUrl.endsWith('.pdf')) {
      window.open(e.row.data.FileUrl, '_blank');
    } else {
      this.title = "Certification on file";
      this.photoUrl = e.row.data.FileUrl;
      this.photoPopupVisible = true;
    }   
  }

  isSearchButtonVisible(e) {
    if (e.row.data.FileUrl && e.row.data.FileUrl.length > 0) {
      return true;
    }
    return false;
  }

  isUploadButtonVisible(e) {

    if (this.canEdit == false) {
      return false;
    }

    if (e.row.isNewRow) {
      return false;
    }
    return true;
  }

  checkForValid() {
    console.log('checkForValid');
    //if (!this.licenses || this.licenses.length === 0) {
    //  this.errorText = "At least one license is required.";
    //} else {
    //  this.errorText = null;
    //}

    //return true;
    //var valid = false;

    //this.licenses.forEach(item => {
    //  if (item.ImageUrl && item.ImageUrl.length > 0) {
    //    valid = true;
    //  }
    //});

    //return valid;
    return true;
  }

  performValidation() {
    //if (!this.licenses || this.licenses.length === 0) {
    //  //console.log('performValidation2');
    //  this.stepperService.reportError('Cannot proceed to next step until all requirements are fulfilled.');
    //  return;
    //}
    this.stepperService.completeStep(this.stepNumber);
  }

}
