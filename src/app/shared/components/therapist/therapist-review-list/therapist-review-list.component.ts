import { Component, OnInit, Input } from '@angular/core';
import { ReviewForDashboardModel } from '../../../models/therapist/review-for-dashboard.model';
import { SubSink } from 'subsink';
import { TherapistDashboardService } from 'src/app/core/services/therapist/therapist-dashboard.service';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'spa-therapist-review-list',
  templateUrl: './therapist-review-list.component.html',
  styleUrls: ['./therapist-review-list.component.scss']
})
export class TherapistReviewListComponent implements OnInit {
  @Input() therapistUID: string;
  @Input() reviews: ReviewForDashboardModel[];

  max = 5;

  private subs = new SubSink();

  constructor(private therapistDashboardService: TherapistDashboardService) { }

  ngOnInit() {
    this.loadReviews();
  }

  loadReviews(){
    this.subs.add(this.therapistDashboardService.getRatings(this.therapistUID)
      .subscribe(resp => {
        this.reviews = resp;
      },
        error => {
          notify('Unable to load reviews.', 'error', 3000);
        }));
  }

}


