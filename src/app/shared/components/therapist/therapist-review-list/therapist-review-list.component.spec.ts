import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistReviewListComponent } from './therapist-review-list.component';

describe('TherapistReviewListComponent', () => {
  let component: TherapistReviewListComponent;
  let fixture: ComponentFixture<TherapistReviewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistReviewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistReviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
