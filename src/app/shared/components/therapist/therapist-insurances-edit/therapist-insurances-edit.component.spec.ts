import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistInsurancesEditComponent } from './therapist-insurances-edit.component';

describe('TherapistInsurancesEditComponent', () => {
  let component: TherapistInsurancesEditComponent;
  let fixture: ComponentFixture<TherapistInsurancesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistInsurancesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistInsurancesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
