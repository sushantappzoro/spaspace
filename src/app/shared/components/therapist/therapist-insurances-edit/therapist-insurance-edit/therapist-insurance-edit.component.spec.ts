import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistInsuranceEditComponent } from './therapist-insurance-edit.component';

describe('TherapistInsuranceEditComponent', () => {
  let component: TherapistInsuranceEditComponent;
  let fixture: ComponentFixture<TherapistInsuranceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistInsuranceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistInsuranceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
