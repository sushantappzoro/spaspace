import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { TherapistInsuranceForEditModel } from '../../../../models/therapist/therapist-insurance-for-edit.model';
import { CustomValidator } from '../../../../classes/custom-validator';

@Component({
  selector: 'spa-therapist-insurance-edit',
  templateUrl: './therapist-insurance-edit.component.html',
  styleUrls: ['./therapist-insurance-edit.component.scss']
})
export class TherapistInsuranceEditComponent implements OnInit, OnChanges {
  @Input() insurance: TherapistInsuranceForEditModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() saveInsurance: EventEmitter<TherapistInsuranceForEditModel> = new EventEmitter<TherapistInsuranceForEditModel>();

  @ViewChild('insuranceModal') insuranceModal: ModalDirective;

  title: string = 'Add Insurance';

  insuranceForm: FormGroup;

  insuranceDateOptions: any = {
    'closeAfterSelect': true,
    'dateFormat': 'mm/dd/yyyy'
  }

  modalConfig: any = {
    'ignoreBackdropClick': true
  }

  constructor() {
  }

  ngOnInit() {

    this.insuranceForm = new FormGroup({
      UID: new FormControl(),
      ProviderName: new FormControl('', Validators.required),
      StartDate: new FormControl('', Validators.required),
      ExpirationDate: new FormControl('', Validators.required),
      ConverageAmount: new FormControl('', [ Validators.required, Validators.min(1) ]),
      FileUrl: new FormControl(),
    },
      { validators: CustomValidator.compareStartAndEndDates('StartDate', 'ExpirationDate') }
    );

    if (this.insurance) {
      this.insuranceForm.setValue(this.insurance);
    }

    this.title = (this.insurance?.UID) ? 'Edit Insurance' : 'Add Insurance';

  }

  ngOnChanges(changes: SimpleChanges) {
    
    if (changes.showModal && this.insuranceModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.insuranceModal.show();
      } else {
        this.insuranceModal.hide();
      }
    }

    if (changes.insurance && this.insuranceForm) {
      if (changes.insurance.currentValue && changes.insurance.currentValue.UID) {
        this.insurance = { ...changes.insurance.currentValue }
        this.title = 'Edit Insurance';
        this.insuranceForm.setValue(this.insurance);
      } else {
        this.title = 'Add Insurance';
        this.insuranceForm.reset();
      }

    }
  }

  onClose() {
    this.insurance = null;
    this.insuranceForm.reset();
    this.showModalChange.emit(false);
  }

  onSave() {
    console.log('onSave');
    if (this.insuranceForm.invalid) {
      this.insuranceForm.markAllAsTouched();
      return;
    }

    var comp = new TherapistInsuranceForEditModel();
    comp = Object.assign({}, this.insurance, this.insuranceForm.value);

    this.saveInsurance.emit(comp);

  }

  formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }


  formatCurrency(input, blur = null) {
    console.log('formatCurrency', input);

    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    var original_len = input_val.length;

    // initial caret position 
    var caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {

      // get position of first decimal
      // this prevents multiple decimals from
      // being entered
      var decimal_pos = input_val.indexOf(".");

      // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);

      // add commas to left side of number
      left_side = this.formatNumber(left_side);

      // validate right side
      right_side = this.formatNumber(right_side);

      // On blur make sure 2 numbers after decimal
      if (blur === "blur") {
        right_side += "00";
      }

      // Limit decimal to only 2 digits
      right_side = right_side.substring(0, 2);

      // join number by .
      input_val = "$" + left_side + "." + right_side;

    } else {
      // no decimal entered
      // add commas to number
      // remove all non-digits
      input_val = this.formatNumber(input_val);
      input_val = "$" + input_val;

      // final formatting
      if (blur === "blur") {
        input_val += ".00";
      }
    }

  }

}
