import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SubSink } from 'subsink';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';

import { TherapistInsuranceForViewModel } from '../../../models/therapist/therapist-insurance-for-view.model';
import { TherapistInsuranceService } from '../../../../core/services/therapist/therapist-insurance.service';
import { TherapistInsuranceForAddModel } from '../../../models/therapist/therapist-insurance-for-add.model';
import { TherapistInsuranceForEditModel } from '../../../models/therapist/therapist-insurance-for-edit.model';
import { configSettings } from '../../../../app.config';
import { ProviderStepperService } from '../../../../provider/core/services/provider-stepper.service';

@Component({
  selector: 'spa-therapist-insurances-edit',
  templateUrl: './therapist-insurances-edit.component.html',
  styleUrls: ['./therapist-insurances-edit.component.scss']
})
export class TherapistInsurancesEditComponent implements OnInit {
  @Input() canEdit: boolean = false;
  @Input() therapistUID: string;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  @ViewChild("insuranceGrid") insuranceGrid: DxDataGridComponent;

  private subs = new SubSink();

  insurance: TherapistInsuranceForEditModel;
  insurances: TherapistInsuranceForViewModel[];

  gridDisabled: boolean = false;

  insuranceUploadApiPath: string;

  photoUrl: string;
  photoPopupVisible: boolean = false;

  popupVisible: boolean;
  title: string;
  uploadPath: string;

  errorText: string;

  showModal: boolean = false;
  deleteUID: string;
  showDeleteWarning: boolean = false;

  constructor(
    private insuranceService: TherapistInsuranceService,
    private stepperService: ProviderStepperService
  ) {
    this.editRecord = this.editRecord.bind(this);
    this.deleteRecord = this.deleteRecord.bind(this);
    this.uploadImageClick = this.uploadImageClick.bind(this);
    this.showImageClick = this.showImageClick.bind(this);
  }

  ngOnInit() {

    this.insuranceUploadApiPath = configSettings.WebAPI_URL + '/auth/therapists/';

    this.gridDisabled = !this.canEdit;

    this.loadInsurance();

    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {
        if (step == this.stepNumber) {
          this.performValidation();
        }
      }));


    this.insuranceService.stateChanged
      .subscribe(state => {
        if (state) {
          this.insurances = state.therapistInsurances;
          if (this.insuranceGrid && this.insuranceGrid.instance) {
            this.insuranceGrid.instance.refresh();
          }          
        }

      });    

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  insurancesGrid_onToolbarPreparing(e) {
    var toolbarItems = e.toolbarOptions.items;
    toolbarItems.push({
      location: 'before',
      template: 'headerText'
    }, {
    //  location: 'center',
    //  template: 'errorText'
    //},{
      location: 'after',
      locateInMenu: 'auto',
      //showText: "inMenu",
      widget: "dxButton",
        options: {
          visible: this.canEdit,
        icon: "add",
        stylingMode: "outline",
        text: "Add",
        hint: "Add New Insurance",
        onClick: () => {
          this.addRecord();
        }
      }
    });
  }

  loadInsurance() {
    this.subs.add(this.insuranceService.getTherapistInsurances(this.therapistUID).subscribe(resp => {
      this.insurances = resp;
      this.checkForValid();
    },
      error => {
        this.insurances = [];
      }));
  }

  addRecord() {
    this.insurance = new TherapistInsuranceForEditModel();
    this.showModal = true;
  }

  editRecord(e) {
    this.insurance = new TherapistInsuranceForEditModel();
    this.insurance = { ...e.row.data };
    this.showModal = true;
  }

  deleteRecord(e) {
    this.deleteUID = e.row.data.UID;
    this.showDeleteWarning = true;
  }

  continue(del: boolean) {

    this.showDeleteWarning = false;
    if (del) {
      this.deleteRow(this.deleteUID);
      this.deleteUID = '';
    }
  }

  saveInsurance(insurance: TherapistInsuranceForEditModel) {

    if (insurance.UID) {

      this.subs.add(this.insuranceService.edit(this.therapistUID, insurance)
        .subscribe(resp => {
          var i = this.insurances.findIndex(item => item.UID === insurance.UID);
          //this.insurances.splice(i, 1, resp);
          this.showModal = false;
          this.checkForValid();
        },
          error => {
            notify('An error occured updating the insurance.', 'error', 3000);
          }));

    } else {

      this.subs.add(this.insuranceService.add(this.therapistUID, insurance)
        .subscribe(resp => {
          //this.insurances.unshift(resp);
          this.showModal = false;
          this.checkForValid();
        },
          error => {
            notify('An error occured adding the insurance.', 'error', 3000);
          }));
    }
  }

  deleteRow(uid: string) {

    this.subs.add(this.insuranceService.remove(this.therapistUID, uid)
      .subscribe(resp => {
        this.checkForValid();
        //this.loadInsurance();
      },
        error => {
          notify('An error occured deleting the insurance.', 'error', 3000);
        }));

  }  

  validateEndDate(e) {    
    if (e && e.value && e.data.StartDate) {
      var endDate = new Date(e.value);
      //if (endDate < new Date()) {
      //  e.rule.message = "Expiration Date cannot be prior to today's date"
      //  return false;
      //}
      
      var startDate = new Date(e.data.StartDate);
      
      if (endDate > startDate) {
        return true;
      }
      return false;
    }
    return true;
  }

  uploadImageClick(e) {
    e.event.preventDefault();
    //console.log('uploadCertificateClick', e.row.data.UID);
    this.title = 'Upload Insurance Image';
    this.uploadPath = this.insuranceUploadApiPath + this.therapistUID + '/insurance/' + e.row.data.UID;
    this.popupVisible = true;
  }

  onfileUploaded(resp: string) {
    this.insuranceService.reloadInsurances(this.therapistUID);
    this.checkForValid();
  }

  showImageClick(e) {
    if (e.row.data.FileUrl && e.row.data.FileUrl.endsWith('.pdf')) {
      window.open(e.row.data.FileUrl, '_blank');
    } else {
      this.title = "Insurance on file";
      this.photoUrl = e.row.data.FileUrl;
      this.photoPopupVisible = true;
    }
  }

  isSearchButtonVisible(e) {
    //console.log('isSearchButtonVisible', e);
    if (e.row.data.FileUrl && e.row.data.FileUrl.length > 0) {
      return true;
    }
    return false;
  }

  isUploadButtonVisible(e) {

    if (this.canEdit == false) {
      return false;
    }

    if (e.row.isNewRow) {
      return false;
    }
    return true;
  }

  checkForValid() {

    if (!this.insurances || this.insurances.length === 0) {
      this.errorText = "At least one insurance record is required.";
      return false;
    }

    var valid = false;

    this.insurances.forEach(item => {
      if (item.FileUrl && item.FileUrl.length > 0) {
        valid = true;
      }
    });

    if (!valid) {
      this.errorText = "Cannot proceed to next step until proof of insurance is uploaded.";
    }

    this.errorText = '';
    return valid;

  }

  performValidation() {
    
    //if (!this.insurances || this.insurances.length === 0) {
    //  this.errorText = 'Cannot proceed to next step until at least one insurance record is added.';
    //  this.stepperService.reportError('Cannot proceed to next step until at least one insurance record is added.');
    //  return;
    //}

    //var valid = false;

    //this.insurances.forEach(item => {
    //  //console.log('insurance', item);
    //  if (item.FileUrl && item.FileUrl.length > 0) {
    //    valid = true;
    //  }
    //});

    //if (!valid) {
    //  this.errorText = 'Please upload proof of insurance.';
    //  this.stepperService.reportError('Cannot proceed to next step until proof of insurance is uploaded.');
    //  return;
    //}

    this.stepperService.completeStep(this.stepNumber);

  }

  onHiding() {
    this.popupVisible = false;
  }

}
