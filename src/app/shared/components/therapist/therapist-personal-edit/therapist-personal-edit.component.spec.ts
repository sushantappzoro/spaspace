import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistPersonalEditComponent } from './therapist-personal-edit.component';

describe('TherapistPersonalEditComponent', () => {
  let component: TherapistPersonalEditComponent;
  let fixture: ComponentFixture<TherapistPersonalEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistPersonalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistPersonalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
