import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { DxFileUploaderComponent, DxValidationGroupComponent } from 'devextreme-angular';

import { TherapistProfileForUIModel } from '../../../models/therapist/therapist-profile-for-ui.model';
import { TherapistProfileForEditModel } from '../../../models/therapist/therapist-profile-for-edit.model';
import { AuthorizeService } from '../../../../../api-authorization/authorize.service';
import { WindowService } from '../../../../core/services/window.service';
import { configSettings } from '../../../../app.config';
import { TherapistProfileService } from '../../../../core/services/therapist/therapist-profile.service';
import { ProviderStepperService } from '../../../../provider/core/services/provider-stepper.service';
import { TherapistProfilePhotoModel } from '../../../models/therapist/therapist-profile-photo.model';
import { StatesService, StateModel } from '../../../../core/services/states.service';
import { Observable } from 'rxjs';
import { MaxImageSizeForUpload, InvalidMaxFileSizeMessage } from '../../../globals';
import { CustomValidator } from '../../../classes/custom-validator';

@Component({
  selector: 'spa-therapist-personal-edit',
  templateUrl: './therapist-personal-edit.component.html',
  styleUrls: ['./therapist-personal-edit.component.scss']
})
export class TherapistPersonalEditComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() showSaveButton: boolean
  @Input() canEdit: boolean = false;
  @Input() canSubmit: boolean = false;
  @Input() isComplete: boolean = false;
  @Input() therapistProfile: TherapistProfileForEditModel;
  @Input() stepNumber: number;
  @Input() runAsStep: boolean;

  @Output() isCompleteChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  //@Output() profileSave: EventEmitter<TherapistProfileForEditModel> = new EventEmitter<TherapistProfileForEditModel>();
  //@Output() profileSubmit: EventEmitter<TherapistProfileForEditModel> = new EventEmitter<TherapistProfileForEditModel>();

  @ViewChild("imageUploader") imageUploader: DxFileUploaderComponent;

  private subs = new SubSink();

  photoUrl: string;
  personalInfoForm: FormGroup;
  //phonePattern: any = /^\+\s*1\s*\(\s*[02-9]\d{2}\)\s*\d{3}\s*-\s*\d{4}$/;

  //showSaveButton: boolean = false;
  showSubmitButton: boolean = false;
  uploadDisabled: boolean = false;

  images: any[] = [];
  fileUploadApiPath: string;

  headers: any;

  states: StateModel[];

  constructor(
    private authService: AuthorizeService,
    private therapistService: TherapistProfileService,
    private stepperService: ProviderStepperService,
    private windowService: WindowService,
    private stateService: StatesService
  ) { }

  ngOnInit() {    

    this.states = this.stateService.getStates();

    this.personalInfoForm = new FormGroup({
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      Address1: new FormControl('', Validators.required),
      Address2: new FormControl(''),
      City: new FormControl('', Validators.required),
      StateCode: new FormControl('', Validators.required),
      ZipCode: new FormControl('', [ Validators.required, CustomValidator.zipCodeValidator]),
      EmailAddress: new FormControl('', [ Validators.required, Validators.email ]),
      MobilePhone: new FormControl('', [Validators.required, CustomValidator.phoneValidator]),
      //PressureCapability: new FormControl(),
      YearsExperience: new FormControl('', Validators.required),
      Bio: new FormControl('', Validators.required)
    });    

    // to add the auth header to the image upload
    this.subs.add(this.authService.getAccessToken()
      .subscribe(token => {
        this.headers = { 'Authorization': 'Bearer ' + token };
      }));

    this.fileUploadApiPath = configSettings.WebAPI_URL + '/auth/therapists/' + this.therapistProfile.UID + '/Photo';

    //this._window = this.windowService.nativeWindow;    

    this.subs.add(this.stepperService.getValidationStep()
      .subscribe(step => {        
        if (step == this.stepNumber) {
          this.performValidation();
        }
      }));
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes=', changes);
  }

  ngAfterViewInit() {
    this.showProfile();

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  get f() {
    return this.personalInfoForm.controls;
  }

  performValidation() {
    console.log('personal edit performValidation');
    //this.validateGroup();

    if (!this.personalInfoForm.errors && !this.personalInfoForm.dirty && this.personalInfoForm.valid) {
      this.stepperService.completeStep(this.stepNumber);
      return;
    }

    if (this.personalInfoForm.errors || !this.personalInfoForm.valid) {
      this.personalInfoForm.markAllAsTouched();
      this.stepperService.reportError('Cannot proceed to next step until all required fields are completed.');
      return;
    }

    //if (!this.therapistProfile || !this.therapistProfile.ProfilePhoto || !this.therapistProfile.ProfilePhoto.Url || this.therapistProfile.ProfilePhoto.Url.length < 1) {
    //  this.personalInfoForm.markAllAsTouched();
    //  this.stepperService.reportError('Cannot proceed to next step until photo has been uploaded.');
    //  return;
    //}

    if (this.personalInfoForm.dirty) {
      this.onSave();
      return;
    }

  }

  //validateGroup() {
  //  if (this.validationGroup && this.validationGroup.instance) {
  //    this.validationGroup.instance.validate();
  //  }    
  //}

  //onChanges() {
  //  if (this.personalInfoForm) {
  //    this.personalInfoForm.statusChanges
  //      .subscribe(val => {
  //        console.log('eval form', this.personalInfoForm);
  //        if (this.personalInfoForm.errors || this.personalInfoForm.dirty || !this.personalInfoForm.valid) {
  //          //console.log('emit false');
  //          this.isCompleteChange.emit(false);
  //        } else {
  //          //console.log('emit true');
  //          this.isCompleteChange.emit(true);
  //        }

  //      });
  //  }
  //}

  loadPhoto() {

    //this.subs.add(this.therapistService.getPhoto(this.therapist.UID)
    //  .subscribe((blob: any) => {
    //    console.log('blob', blob);
    //    let objectURL = 'data:image/jpeg;base64,' + blob;
    //    this.image.nativeElement.src = this._window.window.URL.createObjectURL(blob)
    //    //                this.imageUrl = objectURL;
    //  }));
  }

  getMaxFileSize() {
    return MaxImageSizeForUpload;
  }

  getMaxFileSizeMessage() {
    return InvalidMaxFileSizeMessage;
  }

  showProfile() {

    //this.onChanges();

    if (this.therapistProfile) {
      this.personalInfoForm.setValue(this.toProfile(this.therapistProfile));

      //console.log('photourl=' + this.therapistProfile.ProfilePhoto.Url);
      if (this.therapistProfile.ProfilePhoto && this.therapistProfile.ProfilePhoto.Url) {
        this.photoUrl = this.therapistProfile.ProfilePhoto.Url;
      } else {
        this.photoUrl = '/spa/assets/images/placeholder-person.jpg';
      }

      if (this.canEdit) {
        //this.showSaveButton = true;
        this.showSubmitButton = false;
        this.uploadDisabled = false;
      } else {
        this.personalInfoForm.controls['FirstName'].disable();
        this.personalInfoForm.controls['LastName'].disable();
        this.personalInfoForm.controls['EmailAddress'].disable();
        this.personalInfoForm.controls['MobilePhone'].disable();
        this.personalInfoForm.controls['YearsExperience'].disable();
        this.personalInfoForm.controls['Bio'].disable();
        this.personalInfoForm.controls['Address1'].disable();
        this.personalInfoForm.controls['Address2'].disable();
        this.personalInfoForm.controls['City'].disable();
        this.personalInfoForm.controls['StateCode'].disable();
        this.personalInfoForm.controls['ZipCode'].disable();

        this.uploadDisabled = true;
        //this.showSaveButton = false;
        this.showSubmitButton = false;
      }

      this.showSubmitButton = this.canSubmit;

      //this.validateGroup();

      //console.log('the form',this.personalInfoForm);
      //if (this.personalInfoForm.errors || !this.personalInfoForm.valid) {
      //  //console.log('emit false');
      //  this.isCompleteChange.emit(false);
      //} else {
      //  //console.log('emit true');
      //  this.isCompleteChange.emit(true);
      //}

      console.log('this.personalInfoForm.invalid', this.personalInfoForm.invalid);
      console.log('this.personalInfoForm.errors', this.personalInfoForm.errors);

      if (this.personalInfoForm.valid && !this.personalInfoForm.errors) {
        this.isCompleteChange.emit(true);  
      } else {
        this.isCompleteChange.emit(false);        
      }
      
    }
    
  }

  updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
      let result = uri.replace(re, '$1' + key + "=" + value + '$2');
      return result;
    }
    else {
      let result = uri + separator + key + "=" + value;
      return result;
    }
  }

  getTherapistPicture() {
    return this.photoUrl;
  }

  fileUploader_onUploaded(e): void {
    console.log('photouploaded', e);
    this.imageUploader.instance.reset();

    var response: TherapistProfilePhotoModel = JSON.parse(e.request.response);
    
    if (response) {
      console.log('response.Url', response.Url);
      this.photoUrl = response.Url;
      if (!this.therapistProfile.ProfilePhoto) {
        this.therapistProfile.ProfilePhoto = new TherapistProfilePhotoModel();
      }
      this.therapistProfile.ProfilePhoto.Url = response.Url;
    }

    //this.subs.add(this.therapistService.reloadTherapist(this.therapistProfile.UID)
    //  .subscribe(profile => {
    //    this.therapistProfile = profile;
    //    this.showProfile();
    //  },
    //    error => {
    //      notify('Unable to reload therapist profile.', "error", 3000);
    //    }));

    notify('Therapist photo uploaded successfully.', "success", 3000);
  }

  fileUploader_onUploadError(e): void {
    notify('Unable to upload therapist photo.', "error", 3000);
  }

  fileUploader_onValueChanged(e) {
    console.log('fileUploader_onValueChanged', e);

  }

  private toProfile(therapist: TherapistProfileForEditModel): TherapistProfileForUIModel {
    return new TherapistProfileForUIModel(
      therapist.Address1,
      therapist.Address2,
      therapist.Bio,
      therapist.City,    
      therapist.EmailAddress,      
      therapist.FirstName,
      therapist.LastName,
      therapist.MobilePhone,
      therapist.StateCode,
      therapist.YearsExperience,
      therapist.ZipCode
    );
  }

  onSave() {

    if (this.personalInfoForm.invalid) {
      return;
    }

    var therapist = new TherapistProfileForEditModel();
    therapist = Object.assign({}, this.therapistProfile, this.personalInfoForm.value);

    this.therapistService.editTherapist(therapist)
      .subscribe(res => {
        this.therapistProfile = res;
        this.isCompleteChange.emit(true);
        this.personalInfoForm.markAsPristine();
        if (this.runAsStep ) {
          this.stepperService.completeStep(this.stepNumber);
        } else {
          notify('Therapist updated successfully.', "success", 3000);
        }                
      },
        error => {
          if (this.runAsStep) {
            this.stepperService.reportError('Unable to save therapist profile.');
          } else {
            notify('Unable to save therapist profile.', "error", 3000);
          }          
        });

  }

  onSubmit() {

    var therapist = new TherapistProfileForEditModel();
    therapist = Object.assign({}, this.therapistProfile, this.personalInfoForm.value);

    this.therapistService.submitTherapist(therapist)
      .subscribe(res => {
        this.therapistProfile = res;
        this.showProfile();
        notify('Therapist submitted successfully.', "success", 3000);
      },
        error => {
          notify('Unable to submit therapist profile.', "error", 3000);
        });

  }

}
