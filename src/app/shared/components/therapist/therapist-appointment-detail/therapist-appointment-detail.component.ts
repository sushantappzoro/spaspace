import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';
import { TherapistAppointmentsService } from '../../../../core/services/therapist/therapist-appointments.service';
import { AppointmentForTherapistDetailModel } from '../../../models/therapist/appointment-for-therapist-detail.model';
import { SpinnerOverlayService } from 'src/app/core/services/common/spinner-overlay.service';

@Component({
  selector: 'spa-therapist-appointment-detail',
  templateUrl: './therapist-appointment-detail.component.html',
  styleUrls: ['./therapist-appointment-detail.component.scss']
})
export class TherapistAppointmentDetailComponent implements OnInit, OnDestroy, OnChanges {
  @Input() appointmentUID: string;
  @Input() therapistUID: string;
  @Output() appointmentSelected: EventEmitter<string> = new EventEmitter<string>();

  private subs = new SubSink();

  appointment: AppointmentForTherapistDetailModel = new AppointmentForTherapistDetailModel();

  photoUrl = '/spa/assets/images/placeholder-person.jpg';

  showCheckinButton = false;

  submissionErrors: string[] = [];

  constructor(
    private spinnerService: SpinnerOverlayService,
    private appointmentService: TherapistAppointmentsService
  ) { }

  ngOnInit() {
    this.loadAppointment();
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.appointmentUID) {
      this.loadAppointment();
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.spinnerService.hide();
  }

  loadAppointment() {

    if (this.therapistUID && this.therapistUID.length > 0 && this.appointmentUID && this.appointmentUID.length > 0) {
      this.spinnerService.show();
    }

    if (this.therapistUID && this.appointmentUID) {
      this.appointment = new AppointmentForTherapistDetailModel();
      this.subs.add(this.appointmentService.getAppointmentDetail(this.therapistUID, this.appointmentUID)
        .subscribe(resp => {
          this.appointment = resp;
          this.showCheckinButton = this.appointment.Status == 'Upcoming' ? true : false;
          this.spinnerService.hide();
        },
          error => {
            console.log(error);
            this.spinnerService.hide();
          }));
    }
  }

  getCustomerPhoto() {
    if (this.appointment && this.appointment.CustomerPhotoUrl) {
      return this.appointment.CustomerPhotoUrl;
    } else {
      return '/spa/assets/images/placeholder-person.jpg';
    }
  }

  getDuration() {

  }

  onOpen() {
    this.appointmentSelected.emit(this.appointment.UID);
  }

  onCheckIn() {
    this.subs.add(this.appointmentService.checkIn(this.therapistUID, this.appointment.UID)
      .subscribe(appt => {
        this.appointment = appt;
      },
        error => {
          this.processErrors(error);
        }));
  }

  processErrors(error) {
    this.submissionErrors = [];
    if (error && error.error && error.error.errors) {
      Object.entries(error.error.errors).forEach((key, value) => {
        //console.log('key=' + key);
        //console.log('value=' + value);

        key.forEach(error => {
          this.submissionErrors.push(error.toString());
        })

      });
      console.log('error checking in', this.submissionErrors)
      notify('An error occurred.', "error", 3000);
    } else {
      notify('An error occurred.', "error", 3000);
    }
  }

}
