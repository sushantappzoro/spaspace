import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistAppointmentDetailComponent } from './therapist-appointment-detail.component';

describe('TherapistAppointmentDetailComponent', () => {
  let component: TherapistAppointmentDetailComponent;
  let fixture: ComponentFixture<TherapistAppointmentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistAppointmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistAppointmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
