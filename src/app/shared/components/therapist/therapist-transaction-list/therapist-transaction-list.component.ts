import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TherapistAppointmentSummaryForViewModel } from '../../../models/therapist/therapist-appointment-summary-for-view.model';
import { SubSink } from 'subsink';
import { TherapistAppointmentsService } from '../../../../core/services/therapist/therapist-appointments.service';
import notify from 'devextreme/ui/notify';
import { TherapistAppointmentForDashboardModel } from '../../../models/therapist/therapist-appointment-for-dashboard.model';
import { TherapistTransactionsForDashboardModel } from '../../../models/therapist/therapist-transactions-for-dashboard.model';

@Component({
  selector: 'spa-therapist-transaction-list',
  templateUrl: './therapist-transaction-list.component.html',
  styleUrls: ['./therapist-transaction-list.component.scss']
})
export class TherapistTransactionListComponent implements OnInit {
  @Input() therapistUID: string;
  @Input() transactions: TherapistTransactionsForDashboardModel;
  @Output() transactionSelected: EventEmitter<string> = new EventEmitter<string>();

  private subs = new SubSink();

  constructor() { }

  ngOnInit() {

    //this.loadAppointments();

  }

  //loadUpcomingAppointments() {
  //  this.subs.add(this.therapistAppointmentService.getUpcomingAppointments(this.therapistUID)
  //    .subscribe(resp => {
  //      this.appointments = resp;
  //    },
  //      error => {
  //        notify('Unable to load treatment areas.', "error", 3000);
  //      }));
  //}

  itemClicked(e) {
    this.transactionSelected.emit(e);
  }

  getClientImage() {
    return 'assets/images/placeholder-person.jpg'
  }
}
