import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TherapistTransactionListComponent } from './therapist-transaction-list.component';

describe('TherapistTransactionListComponent', () => {
  let component: TherapistTransactionListComponent;
  let fixture: ComponentFixture<TherapistTransactionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapistTransactionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TherapistTransactionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
