import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpaReviewDetailModel } from '../../../../../../projects/spaspace-lib/src/shared/models/common/spa-review-detail.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'spa-review-detail',
  templateUrl: './review-detail.component.html',
  styleUrls: ['./review-detail.component.scss']
})
export class ReviewDetailComponent implements OnInit {
  @Input() review: SpaReviewDetailModel

  @Output() viewAppointment: EventEmitter<string> = new EventEmitter<string>();

  max: number = 5;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  getPhotoURL(): string {
    if (this.review.CustomerPhotoURL && this.review.CustomerPhotoURL.length > 0) {
      return this.review.CustomerPhotoURL;
    }
    return '/spa/assets/images/placeholder-person.jpg';
  }

  onViewAppointment() {
    this.viewAppointment.emit(this.review.AppointmentUID);
  }

}
