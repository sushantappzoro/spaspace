import { Component, OnInit, Input, Output, ViewChild, EventEmitter, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { SubSink } from 'subsink';
import notify from 'devextreme/ui/notify';

import { ApptForCustomerViewModel } from '../../../../customer/shared/models/appt-for-customer-view.model';
import { SpaCustomerReviewForAddModel } from '../../../../../../projects/spaspace-lib/src/shared/models/customer/spa-customer-review-for-add.model';
import { CustomerService } from '../../../../customer/core/services/customer.service';
import { IUser, AuthorizeService } from '../../../../../api-authorization/authorize.service';

@Component({
  selector: 'spa-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent implements OnInit, OnDestroy, OnChanges {
  @Input() type: string;
  @Input() appointment: ApptForCustomerViewModel;
  @Input() showModal: boolean;

  @Output() showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  @ViewChild('reviewModal') reviewModal: ModalDirective;

  private subs = new SubSink();

  title: string = 'Add Review';
  reviewed: string = '';

  reviewForm: FormGroup;

  review: SpaCustomerReviewForAddModel;

  max: number = 5;

  user: IUser;

  constructor(
    private customerService: CustomerService,
    private authService: AuthorizeService
  ) { }

  ngOnInit() {

    this.reviewForm = new FormGroup({
      Stars: new FormControl(),
      ReviewText: new FormControl('', Validators.required)
    });

    this.subs.add(this.authService.getUser()
      .subscribe(user => {
        this.user = user;
      })
    )

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.type) {
      if (changes.type.currentValue == 'Facility') {
        this.title = 'Review Facility';
        this.reviewed = this.appointment.FacilityName;
      } else {
        this.title = 'Review Therapist';
        this.reviewed = this.appointment.TherapistName;
      }
    }

    if (changes.showModal && this.reviewModal) {
      if (changes.showModal?.currentValue && changes.showModal.currentValue == true) {
        this.reviewModal.show();
      } else {
        this.reviewModal.hide();
      }
    }

  }


  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onClose() {
    this.showModalChange.emit(false);
  }

  onSave() {

    var review = new SpaCustomerReviewForAddModel();
    review = Object.assign({}, this.reviewForm.value);
    review.AppointmentUID = this.appointment.UID

    if (this.type == 'Facility') {
      this.customerService.reviewFacility(this.user.sub, review)
        .subscribe(res => {
          //this.reviewForm.markAsPristine();
          //notify('Facility updated successfully.', "success", 3000);
          this.reviewForm.reset();
          this.showModalChange.emit(false);
        },
          error => {
            notify('Unable to save review.', "error", 3000);
          });
    } else {
      this.customerService.reviewTherapist(this.user.sub, review)
        .subscribe(res => {
          //this.reviewForm.markAsPristine();
          this.reviewForm.reset();
          //notify('Facility updated successfully.', "success", 3000);
          this.showModalChange.emit(false);
        },
          error => {
            notify('Unable to save review.', "error", 3000);
          });
    }    

  }

}
