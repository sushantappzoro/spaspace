import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import notify from 'devextreme/ui/notify';
import { SubSink } from 'subsink';

import { SpaTherapistAppointmentStore } from '../../../../../../projects/spaspace-lib/src/core/stores/therapist/spa-therapist-appointment.store';
import { SpaTherapistNotesForEditModel } from '../../../../../../projects/spaspace-lib/src/shared/models/therapist/spa-therapist-notes-for-edit.model';

@Component({
  selector: 'spa-appointment-notes',
  templateUrl: './appointment-notes.component.html',
  styleUrls: ['./appointment-notes.component.scss']
})
export class AppointmentNotesComponent implements OnInit, OnDestroy, OnChanges {
  @Input() therapistUID: string;
  @Input() appointmentUID: string;
  @Input() notes: string;
  @Input() canEdit: boolean = true;

  private subs = new SubSink();

  noteForm: FormGroup;

  constructor(
    private appservice: SpaTherapistAppointmentStore
  ) { }

  ngOnInit() {

    this.noteForm = new FormGroup({
      Notes: new FormControl(),
    });

    this.noteForm.get("Notes").setValue(this.notes);

  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.notes && this.noteForm) {
      this.noteForm.get("Notes").setValue(changes.notes.currentValue);
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onSave() {

    if (this.canEdit) {
      const notes: SpaTherapistNotesForEditModel = { Notes: this.noteForm.get("Notes").value }
      this.subs.add(this.appservice.updateNotes(this.therapistUID, this.appointmentUID, notes)
        .subscribe(resp => {
          notify('Notes updated successfully.', "success", 3000);
        },
          error => {
            notify('Unable to save notes.', "error", 3000);
          })
      )
    }
  }

  onCancel() {
    this.noteForm.get("Notes").setValue(this.notes);
  }
}
