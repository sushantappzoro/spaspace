import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import notify from 'devextreme/ui/notify';
import { SubSink } from 'subsink';
import { FacilityAppointmentService } from '../../../../core/services/facility/facility-appointment.service';
import { AppointmentCancelByFacilityRequestModel } from '../../../models/appointment/appointment-cancel-by-facility-request.model';
import { ActivatedRoute } from '@angular/router';
import { AppointmentForFacilityDetailModel } from '../../../models/appointment/appointment-for-facility-detail.model';



@Component({
  selector: 'spa-appointment-cancel-form',
  templateUrl: './appointment-cancel-form.component.html',
  styleUrls: ['./appointment-cancel-form.component.scss']
})
export class AppointmentCancelFormComponent implements OnInit, OnDestroy, OnChanges {
  @Input() facilityUID: string;
  @Input() appointmentUID: string;
  @Input() cancellationRequest: AppointmentCancelByFacilityRequestModel;

  @Output() appointmentCancelled: EventEmitter<AppointmentForFacilityDetailModel> = new EventEmitter<AppointmentForFacilityDetailModel>();

  private subs = new SubSink();

  cancelForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private appservice: FacilityAppointmentService
  ) { }

  ngOnInit() {


    this.cancelForm = new FormGroup({
      Reason: new FormControl('', Validators.required),
      CancellationFee: new FormControl()
    });

    if (this.cancellationRequest) {
      this.cancelForm.get("CancellationFee").setValue(this.cancellationRequest.CancellationFee);
    }
    

  }

  ngOnChanges(changes: SimpleChanges) {


  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onSave() {

    var request = new AppointmentCancelByFacilityRequestModel();
    request = Object.assign({}, this.cancellationRequest, this.cancelForm.value);

    //const reason: AppointmentCancelByFacilityRequestModel = { Reason: this.cancelForm.get("Reason").value, CancellationFee: this.cancelForm.get("CancellationFee").value }
    this.subs.add(this.appservice.finishCancel(this.facilityUID, this.appointmentUID, request)
      .subscribe(resp => {
        this.appointmentCancelled.emit(resp);
        notify('Appointment cancelled successfully.', "success", 3000);
      },
        error => {
          notify('Unable to cancel appointment.', "error", 3000);
        })
    )

  }

  onCancel() {
    this.cancelForm.reset();
    this.appointmentCancelled.emit(null);
  }
}
