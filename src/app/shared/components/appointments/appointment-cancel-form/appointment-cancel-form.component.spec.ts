import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentCancelFormComponent } from './appointment-cancel-form.component';

describe('AppointmentCancelFormComponent', () => {
  let component: AppointmentCancelFormComponent;
  let fixture: ComponentFixture<AppointmentCancelFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentCancelFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentCancelFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
