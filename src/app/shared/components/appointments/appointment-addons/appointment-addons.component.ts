import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { SubSink } from 'subsink';

import { SpaTherapistAppointmentStore } from '../../../../../../projects/spaspace-lib/src/core/stores/therapist/spa-therapist-appointment.store';
import { SpaTherapistAppointmentAddonsModel } from '../../../../../../projects/spaspace-lib/src/shared/models/therapist/spa-therapist-appointment-addons.model';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'spa-appointment-addons',
  templateUrl: './appointment-addons.component.html',
  styleUrls: ['./appointment-addons.component.scss']
})
export class AppointmentAddonsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() therapistUID: string;
  @Input() appointmentUID: string;
  @Input() appointmentStatus: string;
  @Input() canEdit: boolean = true;

  @Output() addonsUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

  private subs = new SubSink();

  addons: SpaTherapistAppointmentAddonsModel;

  disabled: boolean = false;

  constructor(
    private addonService: SpaTherapistAppointmentStore
  ) { }

  ngOnInit() {


  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log('ngOnChanges', changes);
    if (changes.appointmentUID) {
      this.subs.add(this.addonService.getAddOns(this.therapistUID, this.appointmentUID)
        .subscribe(addons => {
          this.addons = addons;
          this.disabled = this.setDisabled(this.appointmentStatus);
        },
          error => {

          }
        )
      )
    }

  }
  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  setDisabled(status: string) {

    if (this.canEdit == false) {
      return true;
    }

    switch (status) {
      case 'In Process':
      case 'Checking Out':
        return false;
      default:
        return true;
    }

  }

  selectChanged(e) {

    if (this.canEdit) {
      if (e.checked) {

        const added = this.addons.Available.find(x => x.ID = e.source.id);

        if (added) {
          this.subs.add(this.addonService.addAddOn(this.therapistUID, this.appointmentUID, added)
            .subscribe(addons => {
              this.addons = addons;
              this.addonsUpdated.emit(true);
            },
              error => {
                notify('Unable to select addon.', "error", 3000);
              }
            )
          );
        }

      } else {

        this.subs.add(this.addonService.removeAddOn(this.therapistUID, this.appointmentUID, e.source.id)
          .subscribe(addons => {
            this.addons = addons;
            this.addonsUpdated.emit(true);
          },
            error => {
              notify('Unable to remove addon.', "error", 3000);
            }
          )
        );
      }
    }
    
  }

}
