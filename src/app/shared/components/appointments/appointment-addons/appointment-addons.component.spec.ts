import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentAddonsComponent } from './appointment-addons.component';

describe('AppointmentAddonsComponent', () => {
  let component: AppointmentAddonsComponent;
  let fixture: ComponentFixture<AppointmentAddonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentAddonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentAddonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
