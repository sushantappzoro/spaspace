import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AlertComponent } from './alert/alert.component';
import { NgxCurrencyModule } from "ngx-currency";

import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';

import { Ng5SliderModule } from 'ng5-slider';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { DevexModuleModule } from '../devex-module.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from '../widgets/widgets.module';
import { FacilityInfoComponent } from './facility/facility-info/facility-info.component';
import { FacilityDetailComponent } from './facility/facility-detail/facility-detail.component';
import { MainBookBlockComponent } from './main/main-book-block/main-book-block.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { FacilityAmenetiesEditComponent } from './facility/facility-ameneties-edit/facility-ameneties-edit.component';
import { FacilityImagesEditComponent } from './facility/facility-images-edit/facility-images-edit.component';
import { FacilityProfileViewComponent } from './facility/facility-profile-view/facility-profile-view.component';
import { FacilityProfileEditComponent } from './facility/facility-profile-edit/facility-profile-edit.component';
import { FacilityInfoEditComponent } from './facility/facility-info-edit/facility-info-edit.component';
import { FacilitySubmitComponent } from './facility/facility-submit/facility-submit.component';
import { FacilityManagerProfileEditComponent } from './facility/facility-manager-profile-edit/facility-manager-profile-edit.component';
import { MaterialModule } from '../../material-module.module';
import { FacilityTreatmentAreasComponent } from './facility/facility-treatment-areas/facility-treatment-areas.component';
import { FacilityServiceCompensationComponent } from './facility/facility-service-compensations/facility-service-compensation/facility-service-compensation.component';
import { FacilityTreatmentAreaComponent } from './facility/facility-treatment-areas/facility-treatment-area/facility-treatment-area.component';
import { FacilityServiceCompensationsComponent } from './facility/facility-service-compensations/facility-service-compensations.component';
import { FacilityManagersComponent } from './facility/facility-managers/facility-managers.component';
import { FacilityManagerComponent } from './facility/facility-managers/facility-manager/facility-manager.component';
import { FacilityUpgradesCompensationComponent } from './facility/facility-upgrades-compensation/facility-upgrades-compensation.component';
import { FacilityUpgradeCompensationComponent } from './facility/facility-upgrades-compensation/facility-upgrade-compensation/facility-upgrade-compensation.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import {
  TherapistAppointmentDetailComponent,
  TherapistCertificationEditComponent,
  TherapistCertificationsEditComponent,
  TherapistFacilityEditComponent,
  TherapistFacilitiesEditComponent,
  TherapistInsuranceEditComponent,
  TherapistInsurancesEditComponent,
  TherapistLicenseEditComponent,
  TherapistLicensesEditComponent,
  TherapistPersonalEditComponent,
  TherapistProfileViewComponent,
  TherapistRequirementsEditComponent,
  TherapistSkillsEditComponent
} from './therapist';
import { TherapistTransactionListComponent } from './therapist/therapist-transaction-list/therapist-transaction-list.component';
import { TherapistReviewListComponent } from './therapist/therapist-review-list/therapist-review-list.component';
import { SpinnerOverlayComponent } from './common/spinner-overlay/spinner-overlay.component';
import { OVERLAY_PROVIDERS } from '@angular/cdk/overlay';
import { TherapistAppointmentListComponent } from './therapist/therapist-appointment-list/therapist-appointment-list.component';
import { UserProfileComponent } from './user/user-profile/user-profile.component';
import { NotesComponent } from './common/notes/notes.component';
import { PasswordResetComponent } from './user/password-reset/password-reset.component';
import { AddReviewComponent } from './common/add-review/add-review.component';
import { SpaSpaceSharedModule } from '../../../../projects/spaspace-lib/src/public-api';
import { ReviewListComponent } from './common/review-list/review-list.component';
import { ReviewDetailComponent } from './common/review-detail/review-detail.component';
import { GlobalErrorComponent } from './error/global-error/global-error.component';
import { TherapistServicesEditComponent } from './therapist/therapist-services-edit/therapist-services-edit.component';
import { TherapistServiceEditComponent } from './therapist/therapist-services-edit/therapist-service-edit/therapist-service-edit.component';
import { AppointmentNotesComponent } from './appointments/appointment-notes/appointment-notes.component';
import { AppointmentAddonsComponent } from './appointments/appointment-addons/appointment-addons.component';
import { FacilityAppointmentsByRoomViewComponent } from './facility/facility-appointments-by-room-view/facility-appointments-by-room-view.component';
import { FacilityAppointmentsByTherapistViewComponent } from './facility/facility-appointments-by-therapist-view/facility-appointments-by-therapist-view.component';
import { AppointmentCancelFormComponent } from './appointments/appointment-cancel-form/appointment-cancel-form.component';
import { FacilityAvailabilityComponent } from './facility/facility-availability/facility-availability.component';
import { FacilityAppointmentsListComponent } from './facility/facility-appointments-list/facility-appointments-list.component';
import { FacilityAppointmentsViewComponent } from './facility/facility-appointments-view/facility-appointments-view.component';
import { FacilityAppointmentDetailComponent } from './facility/facility-appointment-detail/facility-appointment-detail.component';
import { FacilityAppointmentActionComponent } from './facility/facility-appointment-action/facility-appointment-action.component';
import { FacilityAppointmentChangeComponent } from './facility/facility-appointment-change/facility-appointment-change.component';


@NgModule({
  declarations: [
    AlertComponent,
    TherapistCertificationsEditComponent,
    TherapistCertificationEditComponent,
    TherapistFacilitiesEditComponent,
    TherapistPersonalEditComponent,
    TherapistSkillsEditComponent,
    TherapistInsurancesEditComponent,
    FacilityInfoComponent,
    FacilityDetailComponent,
    TherapistRequirementsEditComponent,
    MainBookBlockComponent,
    FacilityAmenetiesEditComponent,
    FacilityImagesEditComponent,
    FacilityProfileViewComponent,
    TherapistProfileViewComponent,
    TherapistAppointmentDetailComponent,
    TherapistAppointmentListComponent,
    FacilityProfileEditComponent,
    FacilityInfoEditComponent,
    FacilitySubmitComponent,
    FacilityManagerProfileEditComponent,
    FacilityTreatmentAreasComponent,
    FacilityServiceCompensationComponent,
    FacilityTreatmentAreaComponent,
    FacilityServiceCompensationsComponent,
    FacilityManagersComponent,
    FacilityManagerComponent,
    FacilityUpgradesCompensationComponent,
    FacilityUpgradeCompensationComponent,
    TherapistLicenseEditComponent,
    TherapistLicensesEditComponent,
    TherapistInsuranceEditComponent,
    TherapistFacilityEditComponent,
    TherapistTransactionListComponent,
    TherapistReviewListComponent,
    SpinnerOverlayComponent,
    UserProfileComponent,
    NotesComponent,
    PasswordResetComponent,
    AddReviewComponent,
    ReviewListComponent,
    ReviewDetailComponent,
    GlobalErrorComponent,
    TherapistServiceEditComponent,
    TherapistServicesEditComponent,
    AppointmentNotesComponent,
    AppointmentAddonsComponent,
    FacilityAppointmentsByRoomViewComponent,
    FacilityAppointmentsByTherapistViewComponent,
    AppointmentCancelFormComponent,
    FacilityAvailabilityComponent,
    FacilityAppointmentsListComponent,
    FacilityAppointmentsViewComponent,
    FacilityAppointmentDetailComponent,
    FacilityAppointmentActionComponent,
    FacilityAppointmentChangeComponent,

  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro,
    Ng5SliderModule,
    NgbModule,
    NgxGalleryModule,
    WidgetsModule,
    DevexModuleModule,
    MaterialModule,
    NgxCurrencyModule,
    PdfViewerModule,
    SpaSpaceSharedModule
  ],
  exports: [
    AlertComponent,
    TherapistCertificationsEditComponent,
    TherapistLicensesEditComponent,
    TherapistFacilitiesEditComponent,
    TherapistPersonalEditComponent,
    TherapistSkillsEditComponent,
    TherapistInsurancesEditComponent,
    FacilityInfoComponent,
    FacilityDetailComponent,
    TherapistRequirementsEditComponent,
    MainBookBlockComponent,
    FacilityImagesEditComponent,
    FacilityProfileViewComponent,
    TherapistProfileViewComponent,
    TherapistAppointmentDetailComponent,
    TherapistAppointmentListComponent,
    FacilityProfileEditComponent,
    FacilityAmenetiesEditComponent,
    FacilityInfoEditComponent,
    FacilitySubmitComponent,
    FacilityManagerProfileEditComponent,
    FacilityTreatmentAreasComponent,
    FacilityServiceCompensationComponent,
    FacilityTreatmentAreaComponent,
    FacilityServiceCompensationsComponent,
    FacilityUpgradesCompensationComponent,
    FacilityUpgradeCompensationComponent,
    TherapistTransactionListComponent,
    TherapistReviewListComponent,
    UserProfileComponent,
    NotesComponent,
    PasswordResetComponent,
    AddReviewComponent,
    ReviewListComponent,
    ReviewDetailComponent,
    GlobalErrorComponent,
    TherapistServiceEditComponent,
    TherapistServicesEditComponent,
    AppointmentNotesComponent,
    AppointmentAddonsComponent,
    FacilityAppointmentsByRoomViewComponent,
    FacilityAppointmentsByTherapistViewComponent,
    AppointmentCancelFormComponent,
    FacilityAvailabilityComponent,
    FacilityAppointmentsListComponent,
    FacilityAppointmentsViewComponent,
    FacilityAppointmentDetailComponent,
    FacilityAppointmentActionComponent,
    FacilityAppointmentChangeComponent,
  ],
  providers: [
    OVERLAY_PROVIDERS,
    DatePipe
  ]
})
export class ComponentsModule { }
