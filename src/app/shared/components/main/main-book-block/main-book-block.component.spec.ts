import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainBookBlockComponent } from './main-book-block.component';

describe('MainBookBlockComponent', () => {
  let component: MainBookBlockComponent;
  let fixture: ComponentFixture<MainBookBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainBookBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainBookBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
