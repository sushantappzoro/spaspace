import { Component, OnInit, Input } from '@angular/core';import { Router } from '@angular/router';
import { DatePipe } from '@angular/common'
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'spa-main-book-block',
  templateUrl: './main-book-block.component.html',
  styleUrls: ['./main-book-block.component.scss']
})
export class MainBookBlockComponent implements OnInit {
  now: Date = new Date();
  minSelectableDate: Date;
  constructor(
    private router: Router,
    private datePipe: DatePipe
  ) { 
    const currentYear = new Date().getFullYear();
    const currentMonth = new Date().getMonth();
    const currentDay = new Date().getDate();
    this.minSelectableDate = new Date(currentYear, currentMonth, currentDay);
  }


  ngOnInit() {
  }

  bookLocation() {

  }

  bookDate() {

  }

  bookTherapist() {

  }

  dateChange(e) {
    
    const date: Date = e.value;

    const dateString: string = this.datePipe.transform(date, 'yyyy-MM-dd');
    const url = environment.ClientAppSettings.mobileUrl + '/schedule/bydate/' + dateString;
    window.location.href = url;
  }

}
