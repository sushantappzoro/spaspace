import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetsModule } from './widgets/widgets.module';
import { LayoutModule } from './layout/layout.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DirectivesModule } from './directives/directives.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ComponentsModule } from './components/components.module';
import { Ng5SliderModule } from 'ng5-slider';
import { MaterialModule } from '../material-module.module';
import { SpaSpaceSharedModule } from '../../../projects/spaspace-lib/src/public-api';
import { ContentModule } from './content/content.module';

//import { BrowserModule } from '@angular/platform-browser';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [],
    imports: [
      CommonModule,
      ComponentsModule,
      WidgetsModule,
      LayoutModule,
      DirectivesModule,
      SpaSpaceSharedModule,
      ContentModule
    ],
    exports: [
      CommonModule,
      //BrowserModule,
      //BrowserAnimationsModule,
      FontAwesomeModule,
      WidgetsModule,
      LayoutModule,
      DirectivesModule,
      FlexLayoutModule,
      Ng5SliderModule,
      ComponentsModule,
      MaterialModule,
      SpaSpaceSharedModule,
      ContentModule
    ]
})
export class SharedModule { }
