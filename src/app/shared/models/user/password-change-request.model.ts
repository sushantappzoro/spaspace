export interface IPasswordChangeRequest {
    UID: string;
    NewPassword: string;
  }

  export class PasswordChangeRequest implements IPasswordChangeRequest {
    public UID: string;
    public NewPassword: string;
  }
