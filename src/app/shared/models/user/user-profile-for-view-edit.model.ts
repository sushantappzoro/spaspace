export interface IUserProfileForViewEdit {
  UID: string;
  FirstName: string;
  LastName: string;
  Email: string;
  EmailConfirmed: boolean;
  Phone: string;
  PhoneConfirmed: boolean;
  ProfilePhotoURL: string;
  UpdateSuccessful: boolean;
  UpdateMessage: boolean;
}

export class UserProfileForViewEditModel implements IUserProfileForViewEdit {
  public UID: string;
  public FirstName: string;
  public LastName: string;
  public Email: string;
  public EmailConfirmed: boolean;
  public Phone: string;
  public PhoneConfirmed: boolean;
  public ProfilePhotoURL: string;
  public UpdateSuccessful: boolean;
  public UpdateMessage: boolean;
}
