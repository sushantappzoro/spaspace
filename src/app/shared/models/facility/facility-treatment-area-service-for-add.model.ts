export class FacilityTreatmentAreaServiceForAddModel {
  public CategoryID: number;
  public CategoryName: string;
  public ServiceUID: string;
  public ServiceName: string;
  public Accommodates: boolean;
}
