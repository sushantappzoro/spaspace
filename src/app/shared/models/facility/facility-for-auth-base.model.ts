export class FacilityForAuthBaseModel {
  public RegionUID: string;
  public RegionName: string;
  public Active: boolean;
  public Name: string;
  public Description: string;
  public Address1: string;
  public Address2: string;
  public City: string;
  public StateCode: string;
  public ZipCode: string;
  public Latitude: number;
  public Longitude: number;
  public PhoneNumber: string;
  public HasHotTub: boolean;
  public HasSaunaSteamroom: boolean;
  public HasValetParking: boolean;
  public HasJacuzzi: boolean;
  public HasPool: boolean;
  public HasQuietEnvironment: boolean;
  public URL: string;
  public LogoUrl: string;
  public FacilityAdminName: string;
  public FacilityAdminUID: string;
  public Status: string;
}
