export class FacilityProfileForViewModel {
  public UID: string;
  public Name: string;
  public Description: string;
  public Address1: string;
  public Address2: string;
  public City: string;
  public StateCode: string;
  public ZipCode: string;
  public Latitude: number;
  public Longitude: number;
  public PhoneNumber: string;
  public URL: string;
}
