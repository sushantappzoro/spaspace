export class FacilityTreatmentAreaViewModel {
  UID: string;
  FacilityUID: string;
  Name: string;
  Description: string;  
  Active: string;
  Accommodates: number;
}
