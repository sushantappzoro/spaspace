export class FacilityAmenityForViewModel {
 public Name: string ;
 public Icon: string ;
 public Description: string ;
 public IncludedWithTreatment: boolean ;
 public AdditionalCharge: number ;
 public PhotoUrl: string ;
}
