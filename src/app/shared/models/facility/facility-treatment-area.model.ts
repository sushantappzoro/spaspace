import { FacilityTreatmentAreaServiceForAddModel } from './facility-treatment-area-service-for-add.model';
import { FacilityTreatmentAreaServiceForEditModel } from './facility-treatment-area-service-for-edit.model';

export class FacilityTreatmentAreaModel {
  UID: string;
  FacilityUID: string;
  Name: string;
  Description: string;
  Active: boolean;
  Accommodates: number;
  Services: FacilityTreatmentAreaServiceForEditModel[];
}
