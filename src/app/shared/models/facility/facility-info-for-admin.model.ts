export class FacilityInfoForAdminModel {
  public UID: string;
  public Address1: string;
  public Address2: string;
  public City: string;
  public Description: string;
  public ShortDescription: string;
  public Latitude: number;
  public Longitude: number;
  public Name: string;
  public PhoneNumber: string;
  public StateCode: string;
  public ZipCode: string;
  public URL: string;
  public Status: string;
  public TimeZoneID: string;
}
