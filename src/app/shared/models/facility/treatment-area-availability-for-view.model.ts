export class TreatmentAreaAvailabilityForViewModel {
  public UID?: string;
  public TreatmentAreaName?: string;
  public TreatmentAreaUID?: string;
  public StartTime: Date;
  public EndTime: Date;
  public RecurrenceRule?: string;
}
