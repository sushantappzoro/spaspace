export class FacilityForListModel {
  public UID: string;
  public Active: boolean;
  //public AdminName: string;
  public Name: string;
  public Status: string;
  public City: string;
  public StateCode: string;
}
