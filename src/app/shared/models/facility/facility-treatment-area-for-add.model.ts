import { FacilityTreatmentAreaServiceForAddModel } from './facility-treatment-area-service-for-add.model';

export class FacilityTreatmentAreaForAddModel {
  Name: string;
  Description: string;
  Accommodates: number;
  Services: FacilityTreatmentAreaServiceForAddModel[];
}
