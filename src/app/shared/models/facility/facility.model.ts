import { AddressModel } from "../common/address.model";

export class FacilityModel {
    Name: string;
    Code: string;
    Address: AddressModel;
    Manager: string;
    Phone: string;
    EmergencyPhone: string;
    Description: string;
    Marketing: string;
    DateAdded: Date;
    Active: boolean;
    UID: string;
}
