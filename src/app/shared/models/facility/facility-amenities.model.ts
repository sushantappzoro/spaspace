export class FacilityAmenitiesModel {
    public HasHotTub: boolean;
    public HasSaunaSteamroom: boolean;
    public HasValetParking: boolean;
    public HasJacuzzi: boolean;
    public HasPool: boolean;
    public HasQuietEnvironment: boolean;
}
