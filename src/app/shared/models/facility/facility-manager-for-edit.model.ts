export class FacilityManagerForEditModel {
  public UID: string;
  public FirstName: string;
  public LastName: string;
  public Email: string;
  public PhoneNumber: string;
  public Active: boolean;
  public IsAdmin: boolean;
}
