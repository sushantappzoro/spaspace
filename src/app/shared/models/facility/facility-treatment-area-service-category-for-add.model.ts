export class FacilityTreatmentAreaServiceCategoryForAddModel {
    public CategoryID: number;
    public CategoryName: string;
    public Accommodates: boolean;
}
