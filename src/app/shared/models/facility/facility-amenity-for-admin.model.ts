export class FacilityAmenityForAdminModel {
  public UID: string;
  public AmenityID: number;
  public Description: string;
  public IsIncluded: boolean;
  public IncludedWithTreatment: boolean;
  public AmenityName: string;
  public AdditionalCharge: number;
  public PhotoUrl: string;
}
