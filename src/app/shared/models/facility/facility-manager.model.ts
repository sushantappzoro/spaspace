export class FacilityManagerModel {
  public UID: string;
  public Active: string;
  public LastName: string;
  public FirstName: string;
  public IsAdmin: boolean;
}
