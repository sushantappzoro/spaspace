export class TherapistInfoForAdminModel {
  public FacilityUID: string;
  public ParkingInfo: string;
  public UniformInfo: string;
  public CheckInInfo: string;
  public AdditionalInfo: string;
}
