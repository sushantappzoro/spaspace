export class TreatmentAreaAvailabilityForAddModel {
  public TreatmentAreaUID: string;
  public StartTime: Date;
  public EndTime: Date;
  public RecurrenceRule: string;
}
