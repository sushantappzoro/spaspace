export class FacilityManagerForAdminModel {
  public FirstName: string;
  public LastName: string;
  public DisplayName: string;
  public UID: string;
  public Email: string;
  public PhoneNumber: string;
  public Active: boolean;
  public Status: string;
  public IsAdmin: boolean;
}



export enum FacilityManagerStatus {
  new = 'New',
  pending = 'Pending Approval',
  approved = 'Approved',
  rejected = 'Rejected'
}
