export class FacilityPhotoForAdminModel {
  public UID: string;
  public IsMain: boolean;
  public Url: string;
  public Description: string;
}
