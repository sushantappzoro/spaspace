export class FacilityTreatmentAreaServiceCategoryForEditModel {
    public UID?: string;
    public CategoryID: number;
    public CategoryName: string;
    public Accommodates: boolean;
}
