export class FacilityTreatmentAreaServiceForEditModel {
  public UID: string;
  public CategoryID: number;
  public CategoryName: string;
  public ServiceUID: string;
  public ServiceName: string;
  public Accommodates: boolean;
}
