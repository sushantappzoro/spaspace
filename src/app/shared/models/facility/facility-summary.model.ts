export class FacilitySummaryModel {
    public UID: string;
    public Name: string;
    public Description: string;
    public RegionUID: string;
    public RegionName: string;
    public Address1: string;
    public Address2: string;
    public City: string;
    public StateCode: string;
    public ZipCode: string;
    public AverageRating: number;
    public Latitude: number;
    public Longitude: number;
    public Photo: string;
}
