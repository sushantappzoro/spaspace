export class FacilityForAdminListModel {
  public UID: string;
  public Active: string;
  public Name: string;
  public Status: string;
  public City: string;
  public State: string;
}
