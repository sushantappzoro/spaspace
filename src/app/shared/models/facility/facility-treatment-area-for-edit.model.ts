import { FacilityTreatmentAreaServiceForEditModel } from './facility-treatment-area-service-for-edit.model';

export class FacilityTreatmentAreaForEditModel {
  public UID?: string;
  public Name: string;
  public Description: string;
  public Active: boolean;
  public Accommodates: number;
  public FacilityUID: string;
  public Services: FacilityTreatmentAreaServiceForEditModel[];
}
