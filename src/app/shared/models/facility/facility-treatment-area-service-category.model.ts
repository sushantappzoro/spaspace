export class FacilityTreatmentAreaServiceCategoryModel{
    public UID?: string;
    public CategoryID: number;
    public CategoryName: string;
    public Accommodates: boolean;
}