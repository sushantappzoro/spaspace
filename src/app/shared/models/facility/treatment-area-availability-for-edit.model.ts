export class TreatmentAreaAvailabilityForEditModel {
  public UID: string;
  public TreatmentAreaUID: string;
  public StartTime: Date;
  public EndTime: Date;
}
