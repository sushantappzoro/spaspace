import { FacilityForAuthBaseModel } from "./facility-for-auth-base.model";

export class FacilityForAuthViewModel extends FacilityForAuthBaseModel {
  public UID: string;
  public SubmittedForApprovalDate: Date;
  public ApprovedBy: string;
  public ApprovedDate: Date;
  public LastUpdateDate: Date;
  public LastUpdateBy: string;
}
