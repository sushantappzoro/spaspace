import { FacilityForAuthBaseModel } from "./facility-for-auth-base.model";

export class FacilityForAuthEditModel extends FacilityForAuthBaseModel{
  public UID: string;
}
