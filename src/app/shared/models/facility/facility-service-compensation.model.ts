export class FacilityServiceCompensationModel {
  public UID: string;
  public ServiceUID: string;
  public ServiceName: string;
  public BasePrice: number;
  public FaciltyPercentOfBase: number;
  public ServiceChargePercent: number;
  public TherapistPercentOfBase: number;
  public TherapistPercentOfServiceCharge: number;    
  public TherapistFlatRate: number;
  public InheritingFromService: boolean;
}
