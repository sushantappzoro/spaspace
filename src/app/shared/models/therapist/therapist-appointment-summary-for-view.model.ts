export class TherapistAppointmentSummaryForViewModel {
  public UID?: string;
  public StartDateTime?: Date;
  public EndDateTime?: Date;
  public Service?: string;
  public Facilty?: string;
  public FaciltyUID?: string;
  public TreatmentArea?: string;
  public TreatmentAreaUID?: string;
  public Customer?: string;
  public CustomerUID?: string;
}
