export class TherapistAvailabilityForAddModel {
  public FacilityUID: string;
  public StartTime: Date;
  public EndTime: Date;
  public RecurrenceRule: string;

}
