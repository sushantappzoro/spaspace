export class AuthorizedServiceForViewEditModel {
    public UID: string;
    public ServiceCategory: string;
    public WillPerform: boolean;
}