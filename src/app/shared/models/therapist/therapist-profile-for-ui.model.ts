export class TherapistProfileForUIModel {
  //UID: string;
  public Bio: string;
  public EmailAddress: string;
  public MobilePhone: string;
  public FirstName: string;
  public LastName: string;
  public YearsExperience: number;
  public Address1: string;
  public Address2: string;
  public City: string;
  public StateCode: string;
  public ZipCode: string;

  constructor(
    //uid: string,
    address1: string,
    address2: string,    
    bio: string,
    city: string,
    emailAddress: string,    
    firstName: string,
    lastName: string,
    mobilePhone: string,
    stateCode: string,    
    //pressureCapability: number,
    yearsExperience: number,
    zipCode: string
  ) {
    //this.UID = uid;
    this.Address1 = address1;
    this.Address2 = address2;
    this.Bio = bio;
    this.City = city;
    this.EmailAddress = emailAddress;
    this.MobilePhone = mobilePhone;
    this.FirstName = firstName;
    this.LastName = lastName;
    this.StateCode = stateCode;
    this.ZipCode = zipCode;
    //this.PressureCapability = pressureCapability;
    this.YearsExperience = yearsExperience;
  }
}
