export class TherapistAvailabilityForEditModel {
  public UID: string;
  public FacilityUID: string;
  public StartTime: Date;
  public EndTime: Date;
}
