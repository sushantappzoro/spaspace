import { TherapistLicenseModel } from "./therapist-license.model";
import { TherapistCertificationModel } from "./therapist-certification.model";
import { TherapistAssociatedFacilityModel } from "./therapist-associated-facility.model";
import { SpaTherapistExpertiseForViewModel } from '../../../../../projects/spaspace-lib/src/shared/models';

export class TherapistModel {
  UID: string;
  Bio: string;
  EmailAddress: string;
  MobilePhone: string;
  FirstName: string;
  LastName: string;
  PressureCapability: number;
  UserUID: string;
  YearsExperience: number;
  Expertise: SpaTherapistExpertiseForViewModel[];
  Licenses: TherapistLicenseModel[];
  Certifications: TherapistCertificationModel[];
  AssociatedFacilities: TherapistAssociatedFacilityModel[];
  PhotoUrl: string;
  PhotoPublicId: string;
}
