"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TherapistProfileForEditModel = /** @class */ (function () {
    function TherapistProfileForEditModel() {
    }
    return TherapistProfileForEditModel;
}());
exports.TherapistProfileForEditModel = TherapistProfileForEditModel;
var TherapistProfileStatus;
(function (TherapistProfileStatus) {
    TherapistProfileStatus["new"] = "New";
    TherapistProfileStatus["pending"] = "Pending Approval";
    TherapistProfileStatus["approved"] = "Approved";
    TherapistProfileStatus["rejected"] = "Rejected";
})(TherapistProfileStatus = exports.TherapistProfileStatus || (exports.TherapistProfileStatus = {}));
//# sourceMappingURL=therapist-profile-for-edit.model.js.map