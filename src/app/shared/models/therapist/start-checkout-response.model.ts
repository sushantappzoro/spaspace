import { SpaAppointmentCostSummaryForCheckoutModel } from 'projects/spaspace-lib/src/shared/models';

export class StartCheckoutResponseModel {
    public PaymentMethod: string;
    public CostSummary: SpaAppointmentCostSummaryForCheckoutModel;
  }
