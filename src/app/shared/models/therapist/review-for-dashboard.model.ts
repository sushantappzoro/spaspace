export class ReviewForDashboardModel {
  public Customer: string;
  public ReviewText: string;
  public Stars: number;
}
