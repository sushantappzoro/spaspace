import { TherapistCertificationFileModel } from "./therapist-certification-file.model";

export class TherapistCertificationModel {
  public UID: string;
  public Name: string;
  public Description: string;
  public CertificateDate: Date;
  public CertifyingOrganization: string;
  public FileUrl: string;;
}
