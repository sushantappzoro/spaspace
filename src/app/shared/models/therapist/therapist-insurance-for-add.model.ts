export class TherapistInsuranceForAddModel {
  public ProviderName: string;
  public StartDate: Date;
  public ExpirationDate: Date;
  public ConverageAmount: number;
}
