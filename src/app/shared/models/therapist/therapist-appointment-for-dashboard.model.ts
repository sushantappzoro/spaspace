export interface ITherapistAppointmentForDashboard {
  UID: string;
  CustomerUID: string;
  CustomerName: string;
  CustomerPhotoUrl: string;
  FacilityUID: string;
  FacilityName: string;
  ServiceName: string;
  StartDateTime: Date;
  EndDateTime: Date;
}

export class TherapistAppointmentForDashboardModel implements ITherapistAppointmentForDashboard {
  public UID: string;
  public CustomerUID: string;
  public CustomerName: string;
  public CustomerPhotoUrl: string;
  public FacilityUID: string;
  public FacilityName: string;
  public ServiceName: string;
  public StartDateTime: Date;
  public EndDateTime: Date;
}
