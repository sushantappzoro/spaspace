export class TherapistAvailabilityForViewModel {
  public UID?: string;
  public FacilityName: string;
  public FacilityUID: string;
  public StartTime: Date;
  public EndTime: Date;
}
