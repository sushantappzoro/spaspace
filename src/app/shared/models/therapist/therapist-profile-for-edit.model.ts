import { TherapistProfilePhotoModel } from "./therapist-profile-photo.model";
import { SpaTherapistExpertiseForViewModel } from '../../../../../projects/spaspace-lib/src/shared/models';

export class TherapistProfileForEditModel {
  public UID: string;
  public Bio: string;
  public EmailAddress: string;
  public Expertises: SpaTherapistExpertiseForViewModel[];
  public FirstName: string;
  public LastName: string;
  public MobilePhone: string;
  public PressureCapability: number;
  public ProfilePhoto: TherapistProfilePhotoModel;
  public YearsExperience: number;
  public Status: string;
  /*[Required]*/
  public TermsAndPoliciesAcknowledged: boolean;
  public TermsAndPoliciesAcknowledgedDate: Date;
  public IndependentContractorAcknowledged: boolean;
  public IndependentContractorAcknowledgedDate: Date;
  public ServiceStandardsAcknowledged: boolean;
  public ServiceStandardsAcknowledgedDate: Date;
  public Address1: string;
  public Address2: string;
  public City: string;
  public StateCode: string;
  public ZipCode: string;

}

export enum TherapistProfileStatus {
  new ='New',
  pending ='Pending Approval',
  approved ='Approved',
  rejected ='Rejected'
}
