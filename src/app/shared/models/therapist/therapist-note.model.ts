export interface ITherapistNote {
  UID?: string;
  TherapistUID?: string;
  AppointmentUID?: string;
  ClientUID?: string;
  FacilityUID?: string;
  Text?: string;
  DateTime?: Date;
}

export class TherapistNoteModel implements ITherapistNote {
  public Text?: string;
  public UID?: string;
  public TherapistUID?: string;
  public AppointmentUID?: string;
  public ClientUID?: string;
  public FacilityUID?: string;
  public DateTime?: Date;
}
