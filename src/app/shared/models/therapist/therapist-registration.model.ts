export class TherapistRegistrationModel
{
  public FirstName: string;
  public LastName: string;
  public Email: string;
  public MobilePhone: string;
}
