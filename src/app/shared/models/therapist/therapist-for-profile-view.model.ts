export class TherapistForProfileViewModel {
  public UID: string;
  public Name: string;
  public BioExcerpt: string;
  public AverageRating: number;
  public TotalServices: number;
  public PhotoUrl: string;
}
