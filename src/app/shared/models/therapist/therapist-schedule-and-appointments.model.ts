import { TherapistAppointmentSummaryModel } from "./therapist-appointment-summary.model";
import { TherapistAvailabilityForViewModel } from "./therapist-availability-for-view.model";

export class TherapistScheduleAndAppointmentsModel {
 public Schedule: TherapistAppointmentSummaryModel[];
 public Availability: TherapistAvailabilityForViewModel[];
}
