import { TherapistInsuranceForAddModel } from "./therapist-insurance-for-add.model";

export class TherapistInsuranceForEditModel extends TherapistInsuranceForAddModel {
   public UID: string;
}
