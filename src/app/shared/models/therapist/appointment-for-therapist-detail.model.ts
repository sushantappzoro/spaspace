import { AppointmentSummaryBaseModel } from "../common/appointment-detail.model";
import { ServiceNeedModel } from "../services/service-need.model";
import { ServiceFocusAreaModel } from "../services/service-focus-area.model";
import { FacilitySummaryModel } from "../facility/facility-summary.model";
import { ServiceModel } from "../offerings/service.model";
import { SpaFacilityForCardModel } from "../../../../../projects/spaspace-lib/src/shared/models/facility/spa-facility-for-card.model";
import { SpaAppointmentCostSummaryForCheckoutModel } from '../../../../../projects/spaspace-lib/src/shared/models';


export interface IAppointmentForTherapistDetail {
  AdditionalInfo?: string;
  CheckInInfo?: string;
  Customer?: string;
  CustomerPhotoUrl?: string;
  CustomerTotalAppointments?: number;
  CustomerPhone?: number;
  CustomerEmail?: number;
  EndDateTime?: Date;
  Facility?: SpaFacilityForCardModel;
  FocusAreas?: ServiceFocusAreaModel[];
  HowTheyBooked: string;
  Needs?: ServiceNeedModel[];
  Notes?: string;
  ParkingInfo?: string;
  Service?: string;
  StartDateTime?: Date;
  Status?: string;
  TherapistNotes?: string;
  TreatmentArea?: string;
  UID?: string;
  UniformInfo?: string;
  CostSummary?: SpaAppointmentCostSummaryForCheckoutModel;
}

export class AppointmentForTherapistDetailModel {
  public AdditionalInfo?: string;
  public CheckInInfo?: string;
  public Customer?: string;
  public CustomerPhotoUrl?: string;
  public CustomerTotalAppointments?: number;
  public CustomerPhone?: number;
  public CustomerEmail?: number;
  public EndDateTime?: Date;
  public Facility?: SpaFacilityForCardModel;
  public FocusAreas?: ServiceFocusAreaModel[];
  public HowTheyBooked: string;
  public Needs?: ServiceNeedModel[];
  public Notes?: string;
  public ParkingInfo?: string;
  public Service?: string;
  public StartDateTime?: Date;
  public Status?: string;
  public TherapistNotes?: string;
  public TreatmentArea?: string;
  public UID?: string;
  public UniformInfo?: string;
  public CostSummary?: SpaAppointmentCostSummaryForCheckoutModel;
}
