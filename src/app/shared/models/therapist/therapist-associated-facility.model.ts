export class TherapistAssociatedFacilityModel {
  public UID: string;
  public FacilityUID: string; 
  public IsDefault: boolean;
  public Name: string;
}
