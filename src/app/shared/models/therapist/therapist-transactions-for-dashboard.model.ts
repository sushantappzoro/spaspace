export class TherapistTransactionsForDashboardModel {
  public TotalYTD: number;
  public TotalMTD: number;
  public LastPayPeriod: number;
  public CurrentPayPeriod: number;
  public UpcomingAppointments: number;
  public Cancellations: number;
}
