export class TherapistLicenseModel {
  public UID: string;
  public StateCode: string;
  public AsOfDate: Date;
  public UntilDate: Date;
  public FileUrl: string;
  public LicenseTypeID: number;
}
