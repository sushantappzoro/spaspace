import { ServiceModel } from "./service.model";

export class ServiceCategoryModel {
    UID: string;
    ID: number;
    Name: string;
    Services: ServiceModel[];
}
