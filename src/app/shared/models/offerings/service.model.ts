export class ServiceModel {
    UID: string;
    Name: string;
    Description: string;
    Duration: number;
    ServiceCategory: string;
}
