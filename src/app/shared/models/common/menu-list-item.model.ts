export class MenuListItemModel {
  id: number;
  text: string;
    icon: string;
  status?: string[];
  disabled?: boolean;
  link?: string;
  hasSubItems?: boolean;
  subItems?: MenuListSubItemModel[];
}

export class MenuListSubItemModel {
  id: number;
  text: string;
  status?: string[];
  disabled?: boolean;
  link?: string;
}
