export class ApiErrorModel {
  public title: string;
  public status: string;
  public errors: errortype[];
}

export class errortype {
  [key: string]: string
}
