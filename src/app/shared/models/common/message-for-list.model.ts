export interface IMessageForList {
  UID?: string;
  SenderUID?: string;
  SenderName?: string;
  SenderPhotoUrl?: string;
  TimeSent?: string;
  MessageText?: string;
}

export class MessageForListModel implements IMessageForList {
  public UID?: string;
  public SenderUID?: string;
  public SenderName?: string;
  public SenderPhotoUrl?: string;
  public TimeSent?: string;
  public MessageText?: string;
}
