export class AddressModel {
    City: string;
    State: string;
    Street: string;
    Zip: string;
    lng: number;
    lat: number;
}
