export interface INote {
  UID?: string;
  TherapistUID?: string;
  AppointmentUID?: string;
  ClientUID?: string;
  FacilityUID?: string;
  Text?: string;
  DateTime?: Date;
}

export class NoteModel implements INote {
  public Text?: string;
  public UID?: string;
  public TherapistUID?: string;
  public AppointmentUID?: string;
  public ClientUID?: string;
  public FacilityUID?: string;
  public DateTime?: Date;
}
