export interface BreadcrumbModel {
  label: string;
  url: string;
  params: string;
}
