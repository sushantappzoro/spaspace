export class AppointmentChangeRequestModel {
  public NewTreatmentAreaUID: string;
  public NewTherapistUID: string;
  public NewTime: Date;
}
