export class AppointmentCancelByFacilityRequestModel {
  public Reason: string;
  public CancellationFee: number;
}
