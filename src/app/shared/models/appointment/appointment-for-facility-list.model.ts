export class AppointmentForFacilityListModel {
  public UID?: string;
  public CorporateAccount: string;
  public FacilityName?: string;
  public FacilityUid?: string;
  public TreatmentAreaName?: string;
  public TreatmentAreaUid?: string;
  public CustomerName?: string;
  public CustomerUid?: string;
  public CustomerEmail?: string;
  public CustomerPhone?: string;
  public TherapistName?: string;
  public TherapistUid?: string;  
  public ServiceName?: string;
  public ServiceUid?: string;
  public Time: Date;
  public BookedOn: Date;
  public Status: string;
}
