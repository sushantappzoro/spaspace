export class AppointmentForTherapistViewModel {
  public UID: string;
  public CustomerUID: string;
  public CustomerName: string;
  public recurrenceRule: any;
  public ServiceUID: string;
  public ServiceName: string;
  public TreatmentAreaUID: string;
  public TreatementAreaName: string;
  public TherapistUID: string;
  public TherapistName: string;
  public StartDate: Date;
  public EndDate: Date;
}
