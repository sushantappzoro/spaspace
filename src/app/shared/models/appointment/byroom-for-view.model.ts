import { AvailabilityForAppointmentModel } from './availability-for-appointment.model';
import { AppointmentForByroomViewModel } from './appointment-for-byroom-view.model';

export class ByroomForViewModel {
  public TreatmentAreas: AvailabilityForAppointmentModel[];
  public Appointments: AppointmentForByroomViewModel[];
  //public data: AppointmentForByroomViewModel[];
  //public totalCount: number;
  //public groupCount: number;
  //public summary: any;
}
