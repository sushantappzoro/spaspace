export class AvailabilityForAppointmentModel {
  public UID: string;
  public Name: string;
  public AvailabilityStart: Date;
  public AvailabilityEnd: Date;
}
