import { AppointmentForTherapistViewModel } from './appointment-for-therapist-view.model';
import { AvailabilityForAppointmentModel } from './availability-for-appointment.model';

export class TherapistForViewModel {
  public Therapists: AvailabilityForAppointmentModel[];
  public Appointments: AppointmentForTherapistViewModel[];
  //public data: AppointmentForTherapistViewModel[];
  //public totalCount: number;
  //public groupCount: number;
  //public summary: any;
}
