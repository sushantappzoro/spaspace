import { SpaTherapistForCardModel, SpaAppointmentCostSummaryForCheckoutModel } from '../../../../../projects/spaspace-lib/src/shared/models';
import { ServiceNeedModel } from '../services/service-need.model';
import { ServiceFocusAreaModel } from '../services/service-focus-area.model';

export interface IAppointmentForFacilityDetail {
  UID?: string;
  Customer?: string;
  CustomerPhotoUrl?: string;
  CustomerTotalAppointments?: number;
  CustomerPhone?: number;
  CustomerEmail?: number;
  Therapist?: SpaTherapistForCardModel;
  TreatmentArea?: string;
  Service?: string;
  Needs?: ServiceNeedModel[];
  FocusAreas?: ServiceFocusAreaModel[];
  Notes?: string;
  Status?: string;
  HowTheyBooked: string;
  StartDateTime?: Date;
  EndDateTime?: Date;
  CostSummary?: SpaAppointmentCostSummaryForCheckoutModel;
}

export class AppointmentForFacilityDetailModel implements IAppointmentForFacilityDetail {
  public UID?: string;
  public Customer?: string;
  public CustomerPhotoUrl?: string;
  public CustomerTotalAppointments?: number;
  public CustomerPhone?: number;
  public CustomerEmail?: number;
  public Therapist?: SpaTherapistForCardModel;
  public TreatmentArea?: string;
  public Service?: string;
  public Needs?: ServiceNeedModel[];
  public FocusAreas?: ServiceFocusAreaModel[];
  public Notes?: string;
  public Status?: string;
  public HowTheyBooked: string;
  public StartDateTime?: Date;
  public EndDateTime?: Date;
  public CostSummary?: SpaAppointmentCostSummaryForCheckoutModel;
}
