export class AppointmentForByroomViewModel {
  public UID: string;
  public CustomerUID: string;
  public CustomerName: string;
  public ServiceUID: string;
  public ServiceName: string;
  public TherapistUID: string;
  public TherapistName: string;
  public TreatmentAreaUID: string;
  public TreatementAreaName: string;
  public StartTime: Date;
  public EndTime: Date;
  public recurrenceRule: any;
}
