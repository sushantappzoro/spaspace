export interface IServiceNeed {
  Name?: string;
  UID?: string;
}

export class ServiceNeedModel {
  public Name?: string;
  public UID?: string;
}
