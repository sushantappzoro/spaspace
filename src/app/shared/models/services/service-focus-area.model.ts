export interface IServiceFocusArea {
  Name?: string;
  UID?: string;
}

export class ServiceFocusAreaModel {
  public Name?: string;
  public UID?: string;
}
