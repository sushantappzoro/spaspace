import { CustomerProfileModel } from "./customer-profile.model";

export class CustomerModel {
    public UID: string;
    public UserUID: string;
    public FirstName: string;
    public LastName: string;
    public EmailAddress: string;
    public MobilePhone: string;
    public Profile: CustomerProfileModel
}
