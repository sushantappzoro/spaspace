export class CustomerNeedsModel {
    public Relaxation: boolean;
    public Vacation: boolean;
    public Celebration: boolean;
    public Pain: boolean;
    public Tension: boolean;
    public Stress: boolean;
    public WellBeing: boolean;
    public Insomnia: boolean;
    public TMJMigraine: boolean;
  public Pregnant: boolean;
  public Stretching: boolean;
}
