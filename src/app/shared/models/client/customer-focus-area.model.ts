export class CustomerFocusAreaModel {
    public Fullbody: boolean;
    public Feet: boolean;
    public ArmsHands: boolean;
    public UpperBack: boolean;
    public LowerBack: boolean;
    public MusclesOfHeadAndFace: boolean;
    public NeckShoulders: boolean;
    public Legs: boolean;
}
