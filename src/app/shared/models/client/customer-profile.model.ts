import { CustomerNeedsModel } from "./customer-needs.model";
import { CustomerFocusAreaModel } from "./customer-focus-area.model";
import { CustomerTherapistPreferencesModel } from "./customer-therapist-preferences.model";
import { CustomerFacilityPreferencesModel } from "./customer-facility-preferences.model";

export class CustomerProfileModel {
    public UID: string;
    public CustomerUID: string;
    public PreferredTherapistUID: string;
    public PreferredFacilityUID: string;
    public CurrentNeeds: CustomerNeedsModel;
    public CurrentFocusArea: CustomerFocusAreaModel;
    public TherapistPreferences: CustomerTherapistPreferencesModel;
    public FacilityPreferences: CustomerFacilityPreferencesModel;
    public DesiredPressureID: number;
}
