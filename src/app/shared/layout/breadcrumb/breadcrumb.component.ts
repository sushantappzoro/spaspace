import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, BehaviorSubject, of } from 'rxjs';
import { BreadcrumbModel } from '../../models/common/breadcrumb.model';
import { Router, NavigationEnd, ActivatedRoute, ActivationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { BreadcrumbService } from '../../../core/services/common/breadcrumb.service';

@Component({
  selector: 'spa-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  //breadcrumbs$: BehaviorSubject
  breadcrumbs$: Observable<BreadcrumbModel[]> = this.breadcrumbService.GetBreadcrumbs()
    .pipe(map(res => {
      console.log('breadcrumbs', res);
      return res;
    }));

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private breadcrumbService: BreadcrumbService
  ) {  }

  ngOnInit() {

  }

  onClick(route: string, parms: string) {
    this.router.navigate([route]);
  }
}
