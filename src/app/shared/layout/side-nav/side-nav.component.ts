import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SideNavService } from '../../../core/services/common/side-nav.service';
import { Observable } from 'rxjs';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'spa-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  @ViewChild("snav") snav: MatSidenav;

  public userName: Observable<string>;
  public isAuthenticated: Observable<boolean>;

  userRole: string;

  constructor(
    private sideNavService: SideNavService,
    private authorizeService: AuthorizeService,
  ) { }

  ngOnInit() {

    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.firstName));

    this.authorizeService.getRole()
      .subscribe(role => {
        console.log('gotrole', role);
        this.userRole = role;
      });

    this.sideNavService.toggle()
      .subscribe(toggle => {
        this.snav.toggle();
      });

  }

}
