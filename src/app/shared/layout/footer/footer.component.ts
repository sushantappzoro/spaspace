import { Component, OnInit } from '@angular/core';
import { faFacebookSquare, faInstagram, faTwitterSquare, faLinkedin } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'spa-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    faFacebookSquare = faFacebookSquare;
    faInstagram = faInstagram;
    faTwitterSquare = faTwitterSquare;
    faLinkedIn = faLinkedin;

  theTitle: string;
  popupVisible: boolean;
  privacyPopupVisible: boolean;

  termssrc: string = '/spa/assets/content/SpaSpaceAppTermsofUse.pdf';

    constructor() { }

    ngOnInit() {
    }

  onSubscribeClick() {

  }

  onInfoClick() {
    window.location.href = "mailto:hello@spaspace.com?subject=Need%20Info";
  }

  onTermsViewClick() {
    this.theTitle = 'Terms and Conditions';
    //this.theDescription = 'terms will go here';
    this.popupVisible = true;
  }

  onPrintTermsClick() {
    console.log('onPrintTermsClick')
    window.open(this.termssrc, "_blank");
  }

  onPrivacyViewClick() {
    this.privacyPopupVisible = true;
  }

}
