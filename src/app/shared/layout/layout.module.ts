import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ApiAuthorizationModule } from '../../../api-authorization/api-authorization.module';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DevexModuleModule } from '../devex-module.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared.module';
import { MaterialModule } from '../../material-module.module';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { TopBannerComponent } from './top-banner/top-banner.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ContentModule } from '../content/content.module';

@NgModule({
    declarations: [
        NavBarComponent,
        FooterComponent,
        LeftNavComponent,
        BreadcrumbComponent,
        TopBannerComponent,
        SideNavComponent,
    ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FontAwesomeModule,
    MDBBootstrapModulesPro,
    ApiAuthorizationModule,
    RouterModule,
    DevexModuleModule,
    MaterialModule,
    PdfViewerModule,
    ContentModule
    ],
    exports: [
        NavBarComponent,
        FooterComponent,
        LeftNavComponent,
        TopBannerComponent,
        SideNavComponent
    ]
})
export class LayoutModule { }
