import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MenuListItemModel } from '../../models/common/menu-list-item.model';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SidenavComponent } from 'ng-uikit-pro-standard';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'spa-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.scss']
})
export class LeftNavComponent implements OnInit {
  @Input() selectedItems: MenuListItemModel[];
  @Input() pageTitle: string;

  @ViewChild('sidenav', { static: true }) sidenav: SidenavComponent

  dashboardLink: string;
  profileLink: string;

  public isAuthenticated: Observable<boolean>;
  public userName: Observable<string>;
  public userRole: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authorizeService: AuthorizeService
  ) { }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.firstName));
    this.authorizeService.getRole()
      .subscribe(role => {
        this.userRole = role;
        this.setLink(role);
      });

  }

  setLink(role: string) {
    switch (role) {
      case 'admin':
        this.dashboardLink = '/admin/dashboard';
        this.profileLink = '/admin/userprofile';
        break;
      case 'customer':
        this.dashboardLink = '/portal/appointments';
        this.profileLink = '/portal/profile';
        break;
      case 'facilitymanager':
        this.dashboardLink = '/facility/dashboard';
        this.profileLink = '/facility/userprofile';
        break;
      case 'therapist':
        this.dashboardLink = '/therapist/dashboard';
        this.profileLink = '/therapist/userprofile';
        break;
      default:
    }    
  }

  selectItem(item) {

    //this.sidenav.hide();
    this.router.navigate([item], { relativeTo: this.route });
  }

}
