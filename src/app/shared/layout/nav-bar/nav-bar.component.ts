import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavBarService } from '../../../core/services/nav-bar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Input() showToggle: boolean = false;
  @Input() homePage: string = '/';
  @Output() menuButtonClick: EventEmitter<any> = new EventEmitter<any>();

    isExpanded = false;

  constructor(
    private navService: NavBarService,
    private router: Router) { }

    ngOnInit() {
    }

    collapse() {
        this.isExpanded = false;
    }

    toggle() {
        this.isExpanded = !this.isExpanded;
    }

  menuToggle() {
    this.navService.toggleNext();
  }

  goHome() {
    if (this.homePage) {
      this.router.navigate([this.homePage]);
    } else {
      this.router.navigate(['/']);
    }
  }

  sendClick() {
    this.navService.toggleNext();
    this.menuButtonClick.emit(null);
  }

}
