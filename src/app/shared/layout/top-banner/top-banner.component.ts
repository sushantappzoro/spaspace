import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SubSink } from 'subsink';
import { Observable } from 'rxjs';

import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { map } from 'rxjs/operators';
import { SideNavService } from '../../../core/services/common/side-nav.service';

@Component({
  selector: 'spa-top-banner',
  templateUrl: './top-banner.component.html',
  styleUrls: ['./top-banner.component.scss']
})
export class TopBannerComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  userRole: string;

  public userName: Observable<string>;
  public isAuthenticated: Observable<boolean>;

  dashboardLink: string;
  profileLink: string;

  constructor(
    private router: Router,
    private authorizeService: AuthorizeService,
    private sideNavService: SideNavService
  ) { }

  ngOnInit() {

    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.firstName));

    this.authorizeService.getRole()
      .subscribe(role => {
        this.userRole = role;
        this.setLink(role);
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  toggle() {
    this.sideNavService.toggleNext();
  }

  setLink(role: string) {
    switch (role) {
      case 'admin':
        this.dashboardLink = '/admin/dashboard';
        this.profileLink = '/admin/userprofile';
        break;
      case 'customer':
        this.dashboardLink = '/portal/dashboard';
        this.profileLink = '/portal/profile';
        break;
      case 'facilitymanager':
        this.dashboardLink = '/facility/dashboard';
        this.profileLink = '/facility/userprofile';
        break;
      case 'therapist':
        this.dashboardLink = '/therapist/dashboard';
        this.profileLink = '/therapist/userprofile';
        break;
      default:
    }
  }

}
