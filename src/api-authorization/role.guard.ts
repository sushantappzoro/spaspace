import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanLoad, Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthorizeService, IUser } from './authorize.service';
import { take, tap, mergeMap, switchMap } from 'rxjs/operators';
import { ApplicationPaths, QueryParameterNames } from './api-authorization.constants';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate, CanLoad {

  constructor(
    private authService: AuthorizeService,
    private router: Router
  ) { }

  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const expectedRole = _next.data.expectedRole;

    return this.authService.getUser()
      .pipe(take(1),
        mergeMap(user => {
          if (!user) {
            //console.log('QueryParameterNames.ReturnUrl', QueryParameterNames.ReturnUrl);
            this.router.navigate(['/authentication/login'], {
              queryParams: {
                [QueryParameterNames.ReturnUrl]: state.url
              }
            });
            return of(null);
          }
          if (user.role != expectedRole) {
            this.router.navigate(['/main/unauthorized']);
            return of(null);
          }
          return of(true);
        })
      );    
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {

    const expectedRole = route.data.expectedRole;

    return this.authService.getUser()
      .pipe(take(1),
        mergeMap(user => {
          if (!user) {
            //console.log('QueryParameterNames.ReturnUrl', QueryParameterNames.ReturnUrl);
            this.router.navigate(['/authentication/login']);
          }
          if (user.role != expectedRole) {
            this.router.navigate(['/main/unauthorized']);
          }
          return of(true);
        })
      );    

  }

}
