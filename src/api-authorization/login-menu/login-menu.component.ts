import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '../authorize.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-menu',
  templateUrl: './login-menu.component.html',
  styleUrls: ['./login-menu.component.scss']
})
export class LoginMenuComponent implements OnInit {

    faUserCircle = faUserCircle;

  public isAuthenticated: Observable<boolean>;
  public userName: Observable<string>;
  public userRole: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authorizeService: AuthorizeService
  ) { }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.firstName));
    this.authorizeService.getRole()
      .subscribe(role => {
        this.userRole = role;
      });
  }

  profileClick() {
    switch (this.userRole) {
      case 'admin':
        this.router.navigate(['/admin']);
        break;
      case 'customer':
        this.router.navigate(['/portal']);
        break;
      case 'facilitymanager':
        this.router.navigate(['/facility']);
        break;
      case 'therapist':
        this.router.navigate(['/therapist']);
        break;
      default:
    }    
  }
}
