import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginMenuComponent } from './login-menu/login-menu.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RouterModule } from '@angular/router';
import { ApplicationPaths } from './api-authorization.constants';
import { HttpClientModule } from '@angular/common/http';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
      CommonModule,
      HttpClientModule,
      RouterModule.forChild(
        [
          { path: ApplicationPaths.Register, component: LoginComponent },
          { path: ApplicationPaths.Profile, component: LoginComponent },
          { path: ApplicationPaths.Login, component: LoginComponent },
          { path: ApplicationPaths.LoginFailed, component: LoginComponent },
          { path: ApplicationPaths.LoginCallback, component: LoginComponent },
          { path: ApplicationPaths.LogOut, component: LogoutComponent },
          { path: ApplicationPaths.LoggedOut, component: LogoutComponent },
          { path: ApplicationPaths.LogOutCallback, component: LogoutComponent },
          { path: ApplicationPaths.RegisterTherapist, component: LoginComponent },
          { path: ApplicationPaths.RegisterFacilityManager, component: LoginComponent },
        ]
      ),
      MatMenuModule,
      MatIconModule,
      MatButtonModule,
      FontAwesomeModule
  ],
  declarations: [LoginMenuComponent, LoginComponent, LogoutComponent],
  exports: [LoginMenuComponent, LoginComponent, LogoutComponent]
})
export class ApiAuthorizationModule { }
