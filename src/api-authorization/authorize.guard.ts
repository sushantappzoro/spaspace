import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizeService } from './authorize.service';
import { tap } from 'rxjs/operators';
import { ApplicationPaths, QueryParameterNames } from './api-authorization.constants';

@Injectable({
  providedIn: 'root'
})
export class AuthorizeGuard implements CanActivate, CanLoad {
  constructor(private authorize: AuthorizeService, private router: Router) {
  }
  canActivate(
    _next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    //console.log('AuthorizeGuard-canActivate');
      return this.authorize.isAuthenticated()
        .pipe(tap(isAuthenticated => this.handleAuthorization(isAuthenticated, state)));
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    //console.log('AuthorizeGuard-canLoad');
    return this.authorize.isAuthenticated()
      .pipe(tap(isAuthenticated => this.handleLoadAuthorization(isAuthenticated)));
  }

  private handleAuthorization(isAuthenticated: boolean, state: RouterStateSnapshot) {
      if (!isAuthenticated) {
        //console.log('QueryParameterNames.ReturnUrl', QueryParameterNames.ReturnUrl);
        this.router.navigate(ApplicationPaths.LoginPathComponents, {
          queryParams: {
            [QueryParameterNames.ReturnUrl]: state.url
          }
      });
    }
  }

  private handleLoadAuthorization(isAuthenticated: boolean) {
    if (!isAuthenticated) {
      //console.log('QueryParameterNames.ReturnUrl', QueryParameterNames.ReturnUrl);
      this.router.navigate(ApplicationPaths.LoginPathComponents);
    }
  }
}
