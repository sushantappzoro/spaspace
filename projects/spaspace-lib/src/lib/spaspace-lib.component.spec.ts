import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaspaceLibComponent } from './spaspace-lib.component';

describe('SpaspaceLibComponent', () => {
  let component: SpaspaceLibComponent;
  let fixture: ComponentFixture<SpaspaceLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaspaceLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaspaceLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
