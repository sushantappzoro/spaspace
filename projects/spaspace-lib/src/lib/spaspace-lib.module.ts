import { NgModule } from '@angular/core';
import { SpaspaceLibComponent } from './spaspace-lib.component';



@NgModule({
  declarations: [SpaspaceLibComponent],
  imports: [
  ],
  exports: [SpaspaceLibComponent]
})
export class SpaspaceLibModule { }
