import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {
  SpaAppointmentForBookingModel,
  SpaAppointmentForCustomerViewModel,
  SpaUserSessionModel,
  SpaAppointmentCostSummaryForCheckoutModel,
  SpaConfirmAppointmentRequestModel
} from '../../../shared/models';

import { Observable } from 'rxjs';
import { SpaAppointmentsApiService } from '../../api/public/spa-appointments-api.service';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(
    private appointmentApi: SpaAppointmentsApiService
  ) { }

  public startCheckout(userUID: string, session: SpaUserSessionModel, successURL: string, cancelURL: string): Observable<SpaAppointmentForCustomerViewModel> {
    return this.appointmentApi.book(this.buildBookRequest(userUID, session, successURL, cancelURL));
  }

  public getPaymentMethodDescription(userUID: string, appointmentUID: string): Observable<any> {
    return this.appointmentApi.getPaymentMethodDescription(userUID, appointmentUID);
  }

  public confirmAppointment(userUID: string, appointmentUID: string, request: SpaConfirmAppointmentRequestModel): Observable<any> {
    return this.appointmentApi.confirmAppointment(userUID, appointmentUID, request);
  }

  public getCostSummary(userUID: string, appointmentUID: string): Observable<SpaAppointmentCostSummaryForCheckoutModel> {
    return this.appointmentApi.getCostSummary(userUID, appointmentUID);
  }

  buildBookRequest(userUID: string, session: SpaUserSessionModel, successURL: string, cancelURL: string): SpaAppointmentForBookingModel {

    var appointment: SpaAppointmentForBookingModel = new SpaAppointmentForBookingModel();

    appointment.SuccessURL = successURL;
    appointment.CancelURL = cancelURL;
    appointment.CustomerUID = userUID;
    appointment.ServiceUID = session.SelectedService.ServiceUID;
    appointment.StartDateTime = session.SelectedAppointment.SelectedDateTime;
    appointment.TherapistUID = session.SelectedAppointment.TherapistUID;
    appointment.TreatmentAreaUID = session.SelectedAppointment.TreatmentAreaUID;
    appointment.NotesToTherapist = session.NotesToTherapist;

    try {
      appointment.FocusAreas = [];
      if (session.FocusAreas && session.FocusAreas.length > 0) {
        session.FocusAreas.forEach(area => appointment.FocusAreas.push(+area.ID))
      }
    } catch {

    }

    try {
      appointment.Needs = [];
      if (session.Needs && session.Needs.length > 0) {
        session.Needs.forEach(need => appointment.Needs.push(+need.ID))
      }
    } catch {

    }

    appointment.PressureLevel = session.PressurePreference;

    appointment.AddOnIDs
    try {
      appointment.AddOnIDs = [];
      if (session.Addons && session.Addons.length > 0) {
        session.Addons.forEach(addon => appointment.AddOnIDs.push(addon.ID))
      }
    } catch {

    }

    return appointment;

  }

}
