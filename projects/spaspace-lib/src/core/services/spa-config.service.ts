import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpaConfigService {

  private apiServer: string;
  private mobileURL: string;

  get ApiServer() {
    return this.apiServer;
  }

  set ApiServer(apiServer: string) {
    this.apiServer = apiServer;
  }

  get MobileURL() {
    
    return this.mobileURL;
  }

  set MobileURL(mobileURL: string) {
    
    this.mobileURL = mobileURL;
  }

  //get MobileURL() {

  //  return this.mobileURL;
  //}

  constructor() {

  }
}
