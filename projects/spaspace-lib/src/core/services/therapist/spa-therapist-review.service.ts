import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SpaReviewsModel } from '../../../shared/models/common/spa-reviews.model';
import { SpaTherapistReviewApiService } from '../../api/therapist/spa-therapist-review-api.service';

@Injectable({
  providedIn: 'root'
})
export class SpaTherapistReviewService {

  constructor(
    private api: SpaTherapistReviewApiService
) { }

  public getReviews(therapistUID: string): Observable<SpaReviewsModel> {
    return this.api.getReviews(therapistUID);
  }

}
