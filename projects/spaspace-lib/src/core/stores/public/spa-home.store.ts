import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SpaHomeApiService } from '../../api/public/spa-home-api.service';
import { SpaFacilityForCardModel } from '../../../shared/models';
import { SpaMassageTypeForCardModel } from '../../../shared/models';
import { SpaTherapistForCardModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaHomeStore {

   constructor(private api: SpaHomeApiService) { }

  public getFacilitiesNearMe(): Observable<SpaFacilityForCardModel[]> {
    return this.api.getFacilitiesNearMe();
  }

  public getTherapistsNearMe(): Observable<SpaTherapistForCardModel[]> {
    return this.api.getTherapistsNearMe()
  }

  public getMassageTypes(): Observable<SpaMassageTypeForCardModel[]> {
    return this.api.getMassageTypes();
  }

}
