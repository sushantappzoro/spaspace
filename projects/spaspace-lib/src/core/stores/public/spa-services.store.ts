import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { SpaServiceCategoryForPublicViewModel, SpaServiceAddonForPublicViewModel } from '../../../shared/models';
import { SpaServicesApiService } from '../../api/public/spa-services-api.service';


@Injectable({
  providedIn: 'root'
})
export class SpaServicesStore extends ObservableStore<ServicesState> {

  constructor(private servicesApi: SpaServicesApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  get ServiceCategories(): Observable<SpaServiceCategoryForPublicViewModel[]> {
    const state = this.getState();
    if (state && state.serviceCategories) {
      return of(state.serviceCategories);
    } else {
      return this.fetchServiceCategories();
    }
  }

  getServiceAddons(categoryID: number): Observable<SpaServiceAddonForPublicViewModel[]> {
    const state = this.getState();
    if (state && state.serviceAddons) {
      return of(state.serviceAddons);
    } else {
      return this.fetchServiceAddons(categoryID);
    }
  }

  private fetchServiceCategories() {
    return this.servicesApi.getCategories()
      .pipe(
        map(serviceCategories => {
          this.setState({ serviceCategories }, ServicesStoreActions.GetServiceCategories);
          return serviceCategories;
        }),
        catchError(this.handleError)
      )
  }

  private fetchServiceAddons(categoryID: number) {
    return this.servicesApi.getServiceAddons(categoryID)
      .pipe(
        map(addons => {
          //this.setState({ serviceCategories }, ServicesStoreActions.GetServiceCategories);
          return addons;
        }),
        catchError(this.handleError)
      )
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }

}

export interface ServicesState {
  serviceCategories: SpaServiceCategoryForPublicViewModel[];
  serviceAddons: SpaServiceAddonForPublicViewModel[];
}

export enum ServicesStoreActions {
  GetServiceCategories = 'get_service_categories',
  SetServiceCategories = 'set_service_categories',
  GetServiceAddons = 'get_service_addons',
  SetServiceAddons = 'set_service_addons',
}
