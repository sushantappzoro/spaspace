import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SpaTherapistPublicProfileApiService } from '../../../api/public/therapist/spa-therapist-public-profile-api.service';
import { SpaTherapistProfileForPublicModel } from '../../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaTherapistPublicProfileStore {

  constructor(private api: SpaTherapistPublicProfileApiService) { }

  public getTherapistProfile(therapistUID: string): Observable<SpaTherapistProfileForPublicModel> {
    return this.api.getTherapistProfile(therapistUID);
  }

}
