import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { SpaAppointmentsApiService } from '../../api/public/spa-appointments-api.service';
import { SpaSearchAppointmentsByDateRequestModel } from '../../../shared/models';
import { SpaSearchAppointmentsByDateResponseModel } from '../../../shared/models';
import { SpaAppointmentForBookingModel } from '../../../shared/models';
import { take, map } from 'rxjs/operators';
import { SpaAppointmentForCustomerViewModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaAppointmentsStore {

  private appointments$: BehaviorSubject<SpaSearchAppointmentsByDateResponseModel[]> = new BehaviorSubject([]);

  public getAppointments(): Observable<SpaSearchAppointmentsByDateResponseModel[]> {
    return this.appointments$;
  }

  constructor(private api: SpaAppointmentsApiService) { }

  public searchForAppointmentsByDate(request: SpaSearchAppointmentsByDateRequestModel): Observable<SpaSearchAppointmentsByDateResponseModel[]> {
    this.api.searchForAppointmentsByDate(request)
      .pipe(take(1))
      .subscribe(resp => {
        this.appointments$.next(resp);
      })
    return this.appointments$;
  }

  public book(request: SpaAppointmentForBookingModel): Observable<SpaAppointmentForCustomerViewModel> {
    return this.api.book(request);
  }

  public updateSearchbyDate(request: SpaSearchAppointmentsByDateRequestModel) {
    this.api.searchForAppointmentsByDate(request)
      .pipe(take(1))
      .subscribe(resp => {
        this.appointments$.next(resp);
      });
  }


  //loadApptData(): Observable<SpaSearchAppointmentsByDateResponseModel[]> {
  //  //console.log('facility search request');
  //  return this.sessionService.getAppointmentSearchInfo()
  //    .pipe(
  //      take(1),
  //      switchMap(req => {
  //        //console.log('facility search request', req);
  //        return this.apptService.searchForAppointmentsByDate(req).pipe(
  //          map(resp => resp),
  //          catchError(error => {
  //            //console.log('there was an error', error);
  //            return of(null);
  //          })
  //        )
  //      })
  //    );
  //}
}
