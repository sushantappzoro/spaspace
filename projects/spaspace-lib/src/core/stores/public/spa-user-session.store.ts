import { Injectable } from '@angular/core';
import { Observable, from, of, BehaviorSubject } from 'rxjs';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep';

import {
  SpaLocationInfoModel,
  ISpaLocationInfo,
  SpaUserSessionModel,
  SpaSearchForFacilitiesRequestModel,
  SpaSearchAppointmentsByDateRequestModel,
  SpaSelectedAppointmentModel,
  SpaSelectedServiceModel,

  SpaServiceAddonForPublicViewModel,

  SpaAppointmentForCustomerViewModel
} from '../../../shared/models';
import { User } from 'oidc-client';

@Injectable({
  providedIn: 'root',
})

export class SpaUserSessionStore extends ObservableStore<UserSessionState> {

  logSets: boolean = true;

  constructor() {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  get checkoutSessionID(): string {
    return this.getSession().CheckoutSessionID;
  }

  set checkoutSessionID(checkoutSessionID: string) {
    if (this.logSets) { console.log('set checkoutSessionID', checkoutSessionID); }

    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.CheckoutSessionID = checkoutSessionID;
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get paymentMethodDescription(): string {
    return this.getSession().PaymentMethodDescription;
  }

  set paymentMethodDescription(paymentMethodDescription: string) {
    if (this.logSets) { console.log('set paymentMethodDescription', paymentMethodDescription); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.PaymentMethodDescription = paymentMethodDescription;
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get appointmentUID(): string {
    return this.getSession().AppointmentUID;
  }

  set appointmentUID(appointmentUID: string) {
    if (this.logSets) { console.log('set appointmentUID', appointmentUID); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.AppointmentUID = appointmentUID;
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  resetUserSession() {
    if (this.logSets) { console.log('set resetUserSession'); }
    const userSession = new SpaUserSessionModel();
    this.clearSessionState();

    if (this.getState() && this.getState().userSession) {
      this.setState({ userSession }, UserSessionStoreActions.SetUserSession, false);
    }



    //if (this.getState() && this.getState().userSession) {
    //  const session = this.getState().userSession;
    //  var userSession: SpaUserSessionModel = cloneDeep(session);
    //  userSession.SelectedAppointment = null;
    //  userSession.Addons = null;
    //  userSession.SearchCriteria = null;
    //  userSession.SelectedFacility = null;
    //  userSession.SearchLocation = null;
    //  userSession.CurrentLocation = null;

    //  this.setState({ userSession }, UserSessionStoreActions.SetUserSession)
    //  this.saveSessionState(userSession);
    //}
  }

  getUserSession(): Observable<SpaUserSessionModel> {
    return of(this.getSession());
    //if (this.getState() && this.getState().userSession) {
    //  const userSession = this.getState().userSession;
    //  return of(userSession);
    //} else {
    //  return of(this.initializeSession());
    //}
  }

  getFacilityRequestInfo(): SpaSearchForFacilitiesRequestModel {
    if (this.getState() && this.getState().userSession) {
      const userSession = this.getState().userSession;

      var request: SpaSearchForFacilitiesRequestModel = {
        Latitude: userSession.SearchLocation.Latitude,
        City: userSession.SearchLocation.City,
        Longitude: userSession.SearchLocation.Longitude,
        StateCode: userSession.SearchLocation.State,
        County: userSession.SearchLocation.County,
        ZipCode: userSession.SearchLocation.ZipCode,
        AmenityIDs: [],
        DesiredDate: userSession.SearchDate,
        ServiceUID: userSession.SelectedService.ServiceUID
      };

      if (userSession.FacilityAmenities) {
        userSession.FacilityAmenities.forEach(item => {
          request.AmenityIDs.push(item.ID);
        })
      }

      return request;
    } else {
      return new SpaSearchForFacilitiesRequestModel();
    }
  }

  getAppointmentSearchInfo(): SpaSearchAppointmentsByDateRequestModel {
    if (this.getState() && this.getState().userSession) {
      const userSession = this.getState().userSession;

      var request = new SpaSearchAppointmentsByDateRequestModel();
      request.Date = userSession.SearchDate ? userSession.SearchDate : new Date();
      if (userSession.SelectedFacility) {
        request.FacilityUID = userSession.SelectedFacility.UID;
      }
      if (userSession.SelectedService) {
        request.ServiceUID = userSession.SelectedService.ServiceUID;
      }

      request.FocusAreas = [];
      if (userSession.FocusAreas) {
        userSession.FocusAreas.forEach(item => {
          request.FocusAreas.push(+item.ID);
        })
      }

      request.Needs = [];
      if (userSession.Needs) {
        userSession.Needs.forEach(item => {
          request.Needs.push(+item.ID);
        })
      }

      request.PreferredGender = userSession.GenderPreference;

      return request;
    } else {
      return new SpaSearchAppointmentsByDateRequestModel();
    }
  }

  setUserSession(session: SpaUserSessionModel) {
    if (this.logSets) { console.log('set UserSession', session); }
    var userSession: SpaUserSessionModel = cloneDeep(session);
    this.setState({ userSession }, UserSessionStoreActions.SetUserSession)
    this.saveSessionState(userSession);
  }


  get hasCurrentLocation(): boolean {
    return this.getSession().HasCurrentLocation;
    //if (this.getState() && this.getState().userSession) {
    //  const userSession = this.getState().userSession;
    //  return userSession.HasCurrentLocation;
    //} else {
    //  return false;
    //}
  }

  get currentLocation(): SpaLocationInfoModel {
    return this.getSession().CurrentLocation;
    //if (this.getState() && this.getState().userSession) {
    //  const userSession = this.getState().userSession;
    //  return userSession.CurrentLocation;
    //} else {
    //  return null;
    //}
  }

  set currentLocation(currentLocation: ISpaLocationInfo) {
    if (this.logSets) { console.log('set currentLocation', currentLocation); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.CurrentLocation = { ...currentLocation };
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get appointmentForCustomerView(): SpaAppointmentForCustomerViewModel {
    return this.getSession().AppointmentForCustomerView;
    //if (this.getState() && this.getState().userSession) {
    //  const userSession = this.getState().userSession;
    //  return userSession.CurrentLocation;
    //} else {
    //  return null;
    //}
  }

  set appointmentForCustomerView(appointment: SpaAppointmentForCustomerViewModel) {
    if (this.logSets) { console.log('set appointment', appointment); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.AppointmentForCustomerView = { ...appointment };
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get hasSearchLocation(): boolean {
    return this.getSession().HasSearchLocation;
    //if (this.getState() && this.getState().userSession) {
    //  const userSession = this.getState().userSession;
    //  return userSession.HasSearchLocation;
    //} else {
    //  return false;
    //}
  }

  get searchLocation(): SpaLocationInfoModel {
    return this.getSession().SearchLocation;
    //if (this.getState() && this.getState().userSession) {
    //  const userSession = this.getState().userSession;
    //  return userSession.SearchLocation;
    //} else {
    //  return null;
    //}
  }

  set searchLocation(searchLocation: ISpaLocationInfo) {
    if (this.logSets) { console.log('set searchLocation', searchLocation); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);

    userSession.SelectedFacility = null;

    userSession.SearchLocation = { ...searchLocation };
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get selectedAppointment(): SpaSelectedAppointmentModel {
    return this.getSession().SelectedAppointment;
  }

  set selectedAppointment(selectedAppointment: SpaSelectedAppointmentModel) {
    if (this.logSets) { console.log('set selectedAppointment', selectedAppointment); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.SelectedAppointment = { ...selectedAppointment };
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get selectedService(): SpaSelectedServiceModel {
    return this.getSession().SelectedService;
  }

  set selectedService(selectedService: SpaSelectedServiceModel) {
    if (this.logSets) { console.log('set selectedService', selectedService); }

    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);

    if (session.SelectedService && session.SelectedService.ServiceCategoryID != selectedService.ServiceCategoryID) {
      userSession.FocusAreas = [];
      userSession.Needs = [];
    }

    userSession.SelectedService = { ...selectedService };

    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get addons(): SpaServiceAddonForPublicViewModel[] {
    return this.getSession().Addons;
  }

  set addons(addons: SpaServiceAddonForPublicViewModel[]) {
    if (this.logSets) { console.log('set addons', addons); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.Addons = addons;
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  get LoginReturnURL(): string {
    return this.getSession().LoginReturnURL;
  }

  set LoginReturnURL(loginReturnURL: string) {
    if (this.logSets) { console.log('set LoginReturnURL', loginReturnURL); }
    const session = this.getSession();
    var userSession: SpaUserSessionModel = cloneDeep(session);
    userSession.LoginReturnURL = loginReturnURL;
    this.setState({ userSession: userSession }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(userSession);
  }

  private initializeSession(): SpaUserSessionModel {
    var session: SpaUserSessionModel = new SpaUserSessionModel();
    session.CurrentLocation = new SpaLocationInfoModel();
    session.SearchLocation = new SpaLocationInfoModel();
    this.setState({ userSession: session }, UserSessionStoreActions.SetUserSession);
    this.saveSessionState(session);
    return session;
  }

  private getSession(): SpaUserSessionModel {
    if (this.getState() && this.getState().userSession) {
      const session = this.getState().userSession;
      //console.log('return user session', session);
      return session;
    } else {
      const userSession = this.getSessionState();
      if (userSession) {
        this.setState({ userSession }, UserSessionStoreActions.SetUserSession)
        //console.log('return hold state', userSession);
        return userSession;
      } else {
        //console.log('return initialized');
        return this.initializeSession();
      }
    }
  }

  private clearSessionState() {
    sessionStorage.removeItem('spaSession');
    sessionStorage.removeItem('searchDate');
  }

  private saveSessionState(session: SpaUserSessionModel) {
    sessionStorage.setItem('spaSession', JSON.stringify(session));
    if (session && session.SearchDate) {
      sessionStorage.setItem('searchDate', session.SearchDate.toDateString());
    }
  }

  private getSessionState(): SpaUserSessionModel {
    var session = sessionStorage.getItem('spaSession');
    if (session) {

      var sessionObject: SpaUserSessionModel = JSON.parse(session);
      sessionObject.SearchDate = new Date(sessionStorage.getItem('searchDate'));
      return sessionObject;
    } else
      return null;
  }

}

export interface UserSessionState {
  userSession: SpaUserSessionModel
}

export enum UserSessionStoreActions {
  GetUserSession = 'get_user_session',
  SetUserSession = 'set_user_session',
}
