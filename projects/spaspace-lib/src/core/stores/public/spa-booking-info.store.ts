import { Injectable } from '@angular/core';
import { ObservableStore } from '@codewithdan/observable-store';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { SpaBookingInfoApiService } from '../../api/public/spa-booking-info-api.service';
import {
  SpaSearchForFacilitiesRequestModel,
  SpaFacilityAmenityForBookingModel,
  SpaFacilityForCardModel
} from '../../../shared/models';
import { SpaServiceNeedForBookingModel } from '../../../shared/models/book/spa-service-need-for-booking.model';
import { SpaServiceFocusAreaForBookingModel } from '../../../shared/models/book/spa-service-focus-area-for-booking.model';

@Injectable({
  providedIn: 'root'
})
export class SpaBookingInfoStore extends ObservableStore<BookingInfoState> {

  constructor(private api: SpaBookingInfoApiService) {
    super({ logStateChanges: false, trackStateHistory: false });
  }

  public getServiceNeeds(categoryID: number): Observable<SpaServiceNeedForBookingModel[]> {

    const state = this.getState();

    if (state && state.categoryID && state.categoryID != categoryID) {
      this.clearState();
    }

    if (state && state.categoryID == categoryID && state.serviceNeeds) {
      return of(state.serviceNeeds);
    } else {
      return this.fetchServiceNeeds(categoryID);
    }
  }

  public getServiceFocusAreas(categoryID: number): Observable<SpaServiceFocusAreaForBookingModel[]> {

    const state = this.getState();

    if (state && state.categoryID && state.categoryID != categoryID) {
      this.clearState();
    }

    if (state && state.categoryID == categoryID && state.serviceFocusAreas) {
      return of(state.serviceFocusAreas);
    } else {
      return this.fetchServiceFocusAreas(categoryID);
    }
  }

  public getFacilityAmenities(): Observable<SpaFacilityAmenityForBookingModel[]> {
    return this.api.getFacilityAmenities();
  }

  public SearchForFacilities(request: SpaSearchForFacilitiesRequestModel): Observable<SpaFacilityForCardModel[]> {
    return this.api.SearchForFacilities(request);
  }

  private clearState() {
    const state = this.getState();

    if (state) {
      if (state.serviceNeeds) {
        this.setState({ serviceNeeds: null }, ServicesStoreActions.SetServiceNeeds, false);
      }
      if (state.serviceFocusAreas) {
        this.setState({ serviceFocusAreas: null }, ServicesStoreActions.SetServiceFocusAreas, false);
      }
      if (state.categoryID) {
        this.setState({ categoryID: null }, ServicesStoreActions.SetServiceFocusAreas, false);
      }
    }
  }

  private fetchServiceNeeds(categoryID: number) {
    return this.api.getServiceNeeds(categoryID)
      .pipe(
        map(needs => {
          this.setState({ serviceNeeds: needs }, ServicesStoreActions.SetServiceNeeds);
          this.setState({ categoryID: categoryID }, ServicesStoreActions.SetCategoryID);
          return needs;
        }),
        catchError(this.handleError)
      )
  }

  private fetchServiceFocusAreas(categoryID: number) {
    return this.api.getServiceFocusAreas(categoryID)
      .pipe(
        map(focusAreas => {
          this.setState({ serviceFocusAreas: focusAreas }, ServicesStoreActions.SetServiceFocusAreas);
          this.setState({ categoryID: categoryID }, ServicesStoreActions.SetCategoryID);
          return focusAreas;
        }),
        catchError(this.handleError)
      )
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }

}

export interface BookingInfoState {
  categoryID: number;
  serviceNeeds: SpaServiceNeedForBookingModel[];
  serviceFocusAreas: SpaServiceFocusAreaForBookingModel[];
}

export enum ServicesStoreActions {
  GetServiceNeeds = 'get_service_needs',
  SetServiceNeeds = 'set_service_needs',
  SetCategoryID = 'set_category_id',
  GetServiceFocusAreas = 'get_service_focusareas',
  SetServiceFocusAreas = 'set_service_focusareas',
}

