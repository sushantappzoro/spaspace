import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { SpaFacilityProfileApiService } from '../../../api/public/facility/spa-facility-profile-api.service';
import { SpaFacilityForPublicProfileViewModel } from '../../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaFacilityProfileStore {

  constructor(private api: SpaFacilityProfileApiService) { }

  public getFacilityProfile(UID: string): Observable<SpaFacilityForPublicProfileViewModel> {
    return this.api.getFacilityProfile(UID);
  }

}
