import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';


import { SpaTherapistAppointmentApiService } from '../../api/therapist/spa-therapist-appointment-api.service';
import { SpaTherapistAppointmentAddonsModel } from '../../../shared/models';
import { SpaTherapistAvailableAppointmentAddonModel } from '../../../shared/models';
import { SpaTherapistNotesForEditModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaTherapistAppointmentStore {

  constructor(private api: SpaTherapistAppointmentApiService) { }

  public getAddOns(therapistUID: string, appointmentUID: string): Observable<SpaTherapistAppointmentAddonsModel> {
    return this.api.getAddOns(therapistUID, appointmentUID);
  }

  public addAddOn(therapistUID: string, appointmentUID: string, addon: SpaTherapistAvailableAppointmentAddonModel): Observable<SpaTherapistAppointmentAddonsModel> {
    return this.api.addAddOn(therapistUID, appointmentUID, addon);
  }

  public removeAddOn(therapistUID: string, appointmentUID: string, addonUID: string): Observable<SpaTherapistAppointmentAddonsModel> {
    return this.api.removeAddOn(therapistUID, appointmentUID, addonUID);
  }

  public updateNotes(therapistUID: string, appointmentUID: string, note: SpaTherapistNotesForEditModel): Observable<any> {
    return this.api.updateNotes(therapistUID, appointmentUID, note);
  }

}
