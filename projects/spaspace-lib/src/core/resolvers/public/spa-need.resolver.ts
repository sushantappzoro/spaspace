import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SpaBookingInfoStore } from '../../stores/public/spa-booking-info.store';
import { MassageNeedForBookingModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaNeedResolver implements Resolve<MassageNeedForBookingModel[]> {

  constructor(
    private service: SpaBookingInfoStore
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<MassageNeedForBookingModel[]> {
    return this.service.getMassageNeeds().pipe(
      catchError(error => {
        return of(null);
      })
    );
  }
}
