import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SpaFacilityProfileStore } from '../../stores';
import { SpaFacilityForPublicProfileViewModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaPublicFacilityProfileResolver implements Resolve<SpaFacilityForPublicProfileViewModel[]> {

  constructor(private service: SpaFacilityProfileStore) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaFacilityForPublicProfileViewModel[]> {
    return this.service.getFacilityProfile(route.params['uid']).pipe(
      catchError(error => {
        return of(null);
      })
    );
  }


}
