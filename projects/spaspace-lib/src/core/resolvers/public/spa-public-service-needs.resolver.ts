import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { SpaBookingInfoStore, SpaUserSessionStore } from '../../stores';
import { SpaServiceNeedForBookingModel } from '../../../shared/models';


@Injectable({
  providedIn: 'root'
})
export class SpaPublicServiceNeedsResolver implements Resolve<SpaServiceNeedForBookingModel[]> {

  constructor(
    private service: SpaBookingInfoStore,
    private sessionService: SpaUserSessionStore
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaServiceNeedForBookingModel[]> {

    return this.service.getServiceNeeds(this.sessionService.selectedService.ServiceCategoryID).pipe(
      catchError(error => {
        return of(null);
      })
    );
  }
}
