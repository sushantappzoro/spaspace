import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { MassageFocusAreaForBookingModel } from '../../../public-api';
import { SpaBookingInfoStore } from '../../stores/public/spa-booking-info.store';

@Injectable({
  providedIn: 'root'
})
export class SpaFocusAreasResolver implements Resolve<MassageFocusAreaForBookingModel[]> {

  constructor(private service: SpaBookingInfoStore) { }

  resolve(route: ActivatedRouteSnapshot): Observable<MassageFocusAreaForBookingModel[]> {
    return this.service.getMassageFocusAreas().pipe(
      catchError(error => {
        console.log('type of error=' + typeof (error), error);
        return of(null);
      })
    );
  }
}
