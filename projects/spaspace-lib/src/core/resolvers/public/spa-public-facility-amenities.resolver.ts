import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SpaFacilityAmenityForBookingModel } from '../../../shared/models';
import { SpaBookingInfoStore } from '../../stores';

@Injectable({
  providedIn: 'root'
})
export class SpaPublicFacilityAmenitiesResolver implements Resolve<SpaFacilityAmenityForBookingModel[]> {

  constructor(private service: SpaBookingInfoStore) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaFacilityAmenityForBookingModel[]> {
    return this.service.getFacilityAmenities().pipe(
      catchError(error => {
        return of(null);
      })
    );
  }

}
