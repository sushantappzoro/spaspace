import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';

import { SpaFacilityForCardModel, SpaSearchForFacilitiesRequestModel } from '../../../shared/models';
import { SpaUserSessionStore } from '../../stores';
import { SpaBookingInfoStore } from '../../stores';

@Injectable({
  providedIn: 'root'
})
export class SpaPublicFacilitySearchResolver implements Resolve<SpaFacilityForCardModel[]> {

  constructor(
    private sessionService: SpaUserSessionStore,
    private bookingService: SpaBookingInfoStore,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpaFacilityForCardModel[]> | Observable<never> {

    var req = this.sessionService.getFacilityRequestInfo();

    return this.bookingService.SearchForFacilities(req).pipe(
      map(resp => resp),
      catchError(error => {

        this.router.navigate(['./error']);
        return of([]);
      })
    );
  }

}
