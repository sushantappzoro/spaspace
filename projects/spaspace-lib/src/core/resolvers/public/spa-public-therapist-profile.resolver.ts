import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { SpaTherapistProfileForPublicModel } from '../../../shared/models';
import { SpaTherapistPublicProfileStore } from '../../stores';

@Injectable({
  providedIn: 'root'
})
export class SpaPublicTherapistProfileResolver implements Resolve<SpaTherapistProfileForPublicModel> {

  constructor(private service: SpaTherapistPublicProfileStore) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaTherapistProfileForPublicModel> {
   
    return this.service.getTherapistProfile(route.params['therapistUID']).pipe(      
      catchError(error => {
        console.log('resolve1');
        return of(null);
      })
    );

  }


}
