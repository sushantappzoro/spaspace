import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SpaServiceFocusAreaForBookingModel } from '../../../shared/models';
import { SpaBookingInfoStore, SpaUserSessionStore } from '../../stores';

@Injectable({
  providedIn: 'root'
})
export class SpaPublicServiceFocusAreasResolver implements Resolve<SpaServiceFocusAreaForBookingModel[]> {

  constructor(
    private service: SpaBookingInfoStore,
    private sessionService: SpaUserSessionStore
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaServiceFocusAreaForBookingModel[]> {

    return this.service.getServiceFocusAreas(this.sessionService.selectedService.ServiceCategoryID).pipe(
      catchError(error => {
        return of(null);
      })
    );
  }
}
