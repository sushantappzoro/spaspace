import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SpaServicesStore } from '../../stores';
import { SpaServiceCategoryForPublicViewModel } from '../../../shared/models';


@Injectable({
  providedIn: 'root'
})
export class SpaPublicServicesResolver implements Resolve<SpaServiceCategoryForPublicViewModel[]> {

  constructor(
    private service: SpaServicesStore
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaServiceCategoryForPublicViewModel[]> {
    return this.service.ServiceCategories.pipe(
      catchError(error => {
        return of(null);
      })
    );
  }

}
