import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take, switchMap, catchError, concatMap, map } from 'rxjs/operators';
import { SpaSearchAppointmentsByDateResponseModel } from '../../../shared/models';
import { SpaUserSessionStore } from '../../stores';
import { SpaAppointmentsStore } from '../../stores';

@Injectable({
  providedIn: 'root'
})
export class SpaPublicAppointmentSearchService implements Resolve<SpaSearchAppointmentsByDateResponseModel[]> {

  constructor(
    private sessionService: SpaUserSessionStore,
    private apptService: SpaAppointmentsStore,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpaSearchAppointmentsByDateResponseModel[]> | Observable<never> {

    var searchInfo = this.sessionService.getAppointmentSearchInfo();

    return this.apptService.searchForAppointmentsByDate(searchInfo).pipe(
      map(resp => resp),
      catchError(error => {

        this.router.navigate(['./error']);
        return of([]);
      })
    );
  }

}
