import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, take, switchMap, map } from 'rxjs/operators';
import { SpaReviewsModel } from '../../../shared/models/common/spa-reviews.model';
import { SpaTherapistReviewService } from '../../services/therapist/spa-therapist-review.service';
import { AuthorizeService } from '../../../../../../src/api-authorization/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class SpaTherapistReviewResolver implements Resolve<SpaReviewsModel> {

  constructor(
    private reviewService: SpaTherapistReviewService,
    private authService: AuthorizeService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaReviewsModel> {

    var uid = route.params['uid'];
    if (uid) {

      return this.reviewService.getReviews(uid).pipe(
        catchError(error => {
          return of(null);
        })
      );
    } else {

      return this.authService.getUser()
        .pipe(
          take(1),
          switchMap(user => {
            return this.reviewService.getReviews(user.sub).pipe(
              map(resp => resp),
              catchError(error => {
                console.log(error);
                return of(null);
              })
            )
          })
        );

    }



  }

}
