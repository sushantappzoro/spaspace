import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SpaUserSessionModel } from '../../shared/models';
import { SpaUserSessionStore } from '../stores';

@Injectable({
  providedIn: 'root'
})
export class SpaUserSessionResolver implements Resolve<SpaUserSessionModel> {

  constructor(
    private store: SpaUserSessionStore
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<SpaUserSessionModel> {
    return this.store.getUserSession().pipe(
      catchError(error => {
        return of(null);
      })
    );
  }

}
