import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';
import { SpaConfigService } from '../services/spa-config.service';

@Injectable({
  providedIn: 'root'
})
export class SpaPortalGuard implements CanActivate {


  constructor(private configService: SpaConfigService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log('bydate', route);
    const url = state.url;

    if (url.indexOf("byDate") > 0) {
      //window.location.href = this.configService.MobileURL + '/schedule/byDate' + route.queryParams;
      return false;
    }

    if (url.indexOf("byLocation") > 0) {
      window.location.href = this.configService.MobileURL + '/schedule/byLocation';
      return false;
    }

    if (url.indexOf("byTherapist") > 0) {
      window.location.href = this.configService.MobileURL + '/schedule/byTherapist';
      return false;
    }
   
    return true;

  }

}
