import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SpaFacilityForCardModel } from '../../../shared/models/facility/spa-facility-for-card.model';
import { SpaConfigService } from '../../services/spa-config.service';
import { SpaMassageTypeForCardModel } from '../../../shared/models/home/massage-types-for-card.model';
import { SpaTherapistForCardModel } from '../../../shared/models/therapist/spa-therapist-for-card.model';

@Injectable({
  providedIn: 'root'
})
export class SpaHomeApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public getFacilitiesNearMe(): Observable<SpaFacilityForCardModel[]> {
    return this.http.get<SpaFacilityForCardModel[]>(this.configService.ApiServer + '/public/Home/LocationsNearMe');
  }

  public getTherapistsNearMe(): Observable<SpaTherapistForCardModel[]> {
    return this.http.get<SpaTherapistForCardModel[]>(this.configService.ApiServer + '/public/Home/TherapistsNearMe');
  }

  public getMassageTypes(): Observable<SpaMassageTypeForCardModel[]> {
    return this.http.get<SpaMassageTypeForCardModel[]>(this.configService.ApiServer + '/public/Home/MassageTypes');
  }

}
