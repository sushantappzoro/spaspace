import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SpaConfigService } from '../../../services/spa-config.service';
import { SpaTherapistProfileForPublicModel } from '../../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaTherapistPublicProfileApiService {

  constructor(
    private http: HttpClient,
    private configService: SpaConfigService
  ) { }

  public getTherapistProfile(therapistUID: string): Observable<SpaTherapistProfileForPublicModel> {
    return this.http.get<SpaTherapistProfileForPublicModel>(this.configService.ApiServer + '/public/therapists/' + therapistUID + '/Profile/');
  }


}
