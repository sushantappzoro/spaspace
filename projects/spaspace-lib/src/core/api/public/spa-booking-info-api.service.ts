import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SpaConfigService } from '../../services/spa-config.service';
import { SpaFacilityForCardModel } from '../../../shared/models/facility/spa-facility-for-card.model';
import { SpaSearchForFacilitiesRequestModel, SpaFacilityAmenityForBookingModel } from '../../../shared/models';
import { SpaServiceNeedForBookingModel } from '../../../shared/models/book/spa-service-need-for-booking.model';
import { SpaServiceFocusAreaForBookingModel } from '../../../shared/models/book/spa-service-focus-area-for-booking.model';

@Injectable({
  providedIn: 'root'
})
export class SpaBookingInfoApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public getServiceNeeds(categoryID: number): Observable<SpaServiceNeedForBookingModel[]> {
    return this.http.get<SpaServiceNeedForBookingModel[]>(this.configService.ApiServer + '/public/BookingInfo/ServiceNeeds/' + categoryID);
  }

  public getServiceFocusAreas(categoryID: number): Observable<SpaServiceFocusAreaForBookingModel[]> {
    return this.http.get<SpaServiceFocusAreaForBookingModel[]>(this.configService.ApiServer + '/public/BookingInfo/ServiceFocusAreas/' + categoryID);
  }

  public getFacilityAmenities(): Observable<SpaFacilityAmenityForBookingModel[]> {
    return this.http.get<SpaFacilityAmenityForBookingModel[]>(this.configService.ApiServer + '/public/BookingInfo/FacilityAmenities');
  }

  public SearchForFacilities(request: SpaSearchForFacilitiesRequestModel): Observable<SpaFacilityForCardModel[]> {
    return this.http.post<SpaFacilityForCardModel[]>(this.configService.ApiServer + '/public/BookingInfo/Facilities', request);
  }
}
