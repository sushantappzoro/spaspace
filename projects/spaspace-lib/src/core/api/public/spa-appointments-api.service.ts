import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SpaConfigService } from '../../services/spa-config.service';
import { SpaSearchAppointmentsByDateRequestModel } from '../../../shared/models/appointments/spa-search-appointments-by-date-request.model';
import { SpaSearchAppointmentsByDateResponseModel } from '../../../shared/models/appointments/spa-search-appointments-by-date-response.model';
import { SpaAppointmentForBookingModel } from '../../../shared/models/appointments/spa-appointment-for-booking.model';
import { SpaAppointmentForCustomerViewModel, SpaAppointmentCostSummaryForCheckoutModel, SpaConfirmAppointmentRequestModel } from '../../../shared/models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpaAppointmentsApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public searchForAppointmentsByDate(request: SpaSearchAppointmentsByDateRequestModel): Observable<SpaSearchAppointmentsByDateResponseModel[]> {
    return this.http.post<SpaSearchAppointmentsByDateResponseModel[]>(this.configService.ApiServer + '/public/Appointments/SearchByDate', request);
  }

  public book(request: SpaAppointmentForBookingModel): Observable<SpaAppointmentForCustomerViewModel> {
    return this.http.post<any>(this.configService.ApiServer + '/auth/Appointments', request);
  }

  public getPaymentMethodDescription(userUID: string, appointmentUID: string): Observable<any> {
    return this.http.get<any>(this.configService.ApiServer + '/auth/customers/' + userUID + '/appointments/' + appointmentUID + '/PaymentMethodDesc');
  }

  public confirmAppointment(userUID: string, appointmentUID: string, request: SpaConfirmAppointmentRequestModel): Observable<any> {
    return this.http.post<any>(this.configService.ApiServer + '/auth/customers/' + userUID + '/appointments/' + appointmentUID + '/Confirm', request);
  }

  public getCostSummary(userUID: string, appointmentUID: string): Observable<SpaAppointmentCostSummaryForCheckoutModel> {
    return this.http.get<SpaAppointmentCostSummaryForCheckoutModel>(this.configService.ApiServer + '/auth/customers/' + userUID + '/appointments/' + appointmentUID + '/CostSummary');
  }

}
