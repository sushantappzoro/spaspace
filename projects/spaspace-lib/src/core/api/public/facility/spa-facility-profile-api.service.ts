import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SpaConfigService } from '../../../services/spa-config.service';
import { SpaFacilityForPublicProfileViewModel } from '../../../../shared/models/facility/spa-facility-for-public-profile-view.model';

@Injectable({
  providedIn: 'root'
})
export class SpaFacilityProfileApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public getFacilityProfile(UID: string): Observable<SpaFacilityForPublicProfileViewModel> {
    return this.http.get<SpaFacilityForPublicProfileViewModel>(this.configService.ApiServer + '/public/Facilities/' + UID + '/Profile');
  }

}
