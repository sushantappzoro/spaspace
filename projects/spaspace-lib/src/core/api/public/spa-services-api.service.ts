import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

//import { environment } from '@env/environment';

//import {
//  SpaServiceCategoryForPublicViewModel,
//  SpaServiceForPublicViewModel,
//  SpaServiceAddonForPublicViewModel,
//} from '../../m';
import { SpaConfigService } from '../../services/spa-config.service';
import { SpaServiceCategoryForPublicViewModel, SpaServiceForPublicViewModel, SpaServiceAddonForPublicViewModel } from '../../../shared/models';

@Injectable({
  providedIn: 'root'
})
export class SpaServicesApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public getCategories(): Observable<SpaServiceCategoryForPublicViewModel[]> {
    return this.http.get<SpaServiceCategoryForPublicViewModel[]>(this.configService.ApiServer + '/public/ServiceCategories');
  }

  public getServices(categoryID: number): Observable<SpaServiceForPublicViewModel[]> {
    return this.http.get<SpaServiceForPublicViewModel[]>(this.configService.ApiServer + '/public/ServiceCategories/' + categoryID + '/services');
  }

  public getServiceAddons(categoryID: number): Observable<SpaServiceAddonForPublicViewModel[]> {
    return this.http.get<SpaServiceAddonForPublicViewModel[]>(this.configService.ApiServer + '/public/ServiceCategories/' + categoryID + '/serviceaddons');
  }

}
