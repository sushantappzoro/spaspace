import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';

import { SpaConfigService } from '../../services/spa-config.service';
import { SpaTherapistAppointmentAddonsModel } from '../../../shared/models';
import { SpaTherapistAvailableAppointmentAddonModel } from '../../../shared/models';
import { SpaTherapistNotesForEditModel } from '../../../shared/models';


@Injectable({
  providedIn: 'root'
})
export class SpaTherapistAppointmentApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public getAddOns(therapistUID: string, appointmentUID: string): Observable<SpaTherapistAppointmentAddonsModel> {
    return this.http.get<SpaTherapistAppointmentAddonsModel>(this.configService.ApiServer + '/auth/therapists/' + therapistUID + '/Appointments/' + appointmentUID + '/AddOns' );
  }

  public addAddOn(therapistUID: string, appointmentUID: string, addon: SpaTherapistAvailableAppointmentAddonModel): Observable<SpaTherapistAppointmentAddonsModel> {
    return this.http.post<SpaTherapistAppointmentAddonsModel>(this.configService.ApiServer + '/auth/therapists/' + therapistUID + '/Appointments/' + appointmentUID + '/AddOns', addon);
  }

  public removeAddOn(therapistUID: string, appointmentUID: string, addonUID: string): Observable<SpaTherapistAppointmentAddonsModel> {
    return this.http.delete<SpaTherapistAppointmentAddonsModel>(this.configService.ApiServer + '/auth/therapists/' + therapistUID + '/Appointments/' + appointmentUID + '/AddOns/' + addonUID);
  }

  public updateNotes(therapistUID: string, appointmentUID: string, note: SpaTherapistNotesForEditModel): Observable<any> {
    return this.http.put<any>(this.configService.ApiServer + '/auth/therapists/' + therapistUID + '/Appointments/' + appointmentUID + '/Notes', note);
  }

}
