import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { SpaReviewsModel } from '../../../shared/models/common/spa-reviews.model';
import { SpaConfigService } from '../../services/spa-config.service';

@Injectable({
  providedIn: 'root'
})
export class SpaTherapistReviewApiService {

  constructor(private http: HttpClient, private configService: SpaConfigService) { }

  public getReviews(therapistUID: string): Observable<SpaReviewsModel> {
    return this.http.get<SpaReviewsModel>(this.configService.ApiServer + '/auth/therapists/' + therapistUID + '/Reviews');
  }

}
