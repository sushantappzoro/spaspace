/*
 * Public API Surface of spaspace-lib
 */

export * from './core/stores';
export * from './core/resolvers';
export * from './core/guards';
export * from './core/services';

export { SpaConfigService } from './core/services/spa-config.service';

export * from './shared/models';
export * from './shared/spaspace-shared.module';

export * from './shared/directives/spaspace-directives.module';

export * from './spaspace-lib.module';
