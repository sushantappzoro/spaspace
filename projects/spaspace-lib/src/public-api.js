"use strict";
/*
 * Public API Surface of spaspace-lib
 */
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var spa_user_session_store_1 = require("./core/stores/public/spa-user-session.store");
exports.SpaUserSessionStore = spa_user_session_store_1.SpaUserSessionStore;
var spa_services_store_1 = require("./core/stores/public/spa-services.store");
exports.SpaServicesStore = spa_services_store_1.SpaServicesStore;
var spa_home_store_1 = require("./core/stores/public/spa-home.store");
exports.SpaHomeStore = spa_home_store_1.SpaHomeStore;
__export(require("./core/resolvers"));
var spa_config_service_1 = require("./core/services/spa-config.service");
exports.SpaConfigService = spa_config_service_1.SpaConfigService;
__export(require("./shared/models"));
__export(require("./shared/spaspace-shared.module"));
//export * from './shared/directives/spaspace-directives.module';
__export(require("./spaspace-lib.module"));
//# sourceMappingURL=public-api.js.map