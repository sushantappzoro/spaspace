import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class SpaspaceLibModule {
  static forRoot() {
    return {
      ngModule: SpaspaceLibModule,
      providers: [

      ]
    }
  }
}
