import { ISpaTherapistExistingAppointmentAddon, SpaTherapistExistingAppointmentAddonModel } from './spa-therapist-existing-appointment-addon.model';
import { ISpaTherapistAvailableAppointmentAddon, SpaTherapistAvailableAppointmentAddonModel } from './spa-therapist-available-appointment-addon.model';


export interface ISpaTherapistAppointmentAddons {
  Existing: ISpaTherapistExistingAppointmentAddon[];
  Available: ISpaTherapistAvailableAppointmentAddon[];
}

export class SpaTherapistAppointmentAddonsModel implements ISpaTherapistAppointmentAddons {
  public Existing: SpaTherapistExistingAppointmentAddonModel[];
  public Available: SpaTherapistAvailableAppointmentAddonModel[];
}
