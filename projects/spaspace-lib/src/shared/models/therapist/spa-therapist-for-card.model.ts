export interface ISpaTherapistForCard {
  UID: string
  Name: string;
  City: string;
  StateCode: string;
  AverageRating: number;
  TotalRatings: number;
  TotalAppointments: number;
  MainPhotoUrl: string;
  Email: string;
  Phone: string;

  Expertises?: string[];
  Gender?: string;
  NextAvailableAppointment: Date;
}

export class SpaTherapistForCardModel implements ISpaTherapistForCard  {
  public UID: string
  public Name: string;
  public City: string;
  public StateCode: string;
  public AverageRating: number;
  public TotalRatings: number;
  public TotalAppointments: number;
  public MainPhotoUrl: string;
  public Email: string;
  public Phone: string;

  public Expertises?: string[];
  public Gender?: string;
  public NextAvailableAppointment: Date;
}
