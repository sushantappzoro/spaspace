export interface ISpaTherapistExistingAppointmentAddon {
  UID: string;
  AddOnUID: string;
  Name: string;
  Description: string;
  Price: number;
}

export class SpaTherapistExistingAppointmentAddonModel implements ISpaTherapistExistingAppointmentAddon {
  public UID: string;
  public AddOnUID: string;
  public Name: string;
  public Description: string;
  public Price: number;
}
