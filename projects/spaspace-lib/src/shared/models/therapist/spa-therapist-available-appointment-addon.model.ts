export interface ISpaTherapistAvailableAppointmentAddon {
  ID: number;
  Name: string;
  Description: string;
  Price: number;
}

export class SpaTherapistAvailableAppointmentAddonModel implements ISpaTherapistAvailableAppointmentAddon {
  public ID: number;
  public Name: string;
  public Description: string;
  public Price: number;
}
