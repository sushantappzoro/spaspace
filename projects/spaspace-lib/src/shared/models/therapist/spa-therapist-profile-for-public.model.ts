import { SpaReviewSummaryModel, ISpaReviewSummary} from '../common/spa-review-summary.model';
import { ISpaTherapistForCard, SpaTherapistForCardModel } from './spa-therapist-for-card.model';
import { SpaFacilityForCardModel } from '../facility/spa-facility-for-card.model';

export interface ISpaTherapistProfileForPublic extends ISpaTherapistForCard {
  Description: string;
  Specialties: string[];
  PressureCapability: number;
  YearsExperience: number;
  Reviews: ISpaReviewSummary[];
  AssociatedFacilities: SpaFacilityForCardModel[];
  Location: string;
}

export class SpaTherapistProfileForPublicModel extends SpaTherapistForCardModel implements ISpaTherapistProfileForPublic {
  public Description: string;
  public Specialties: string[];
  public PressureCapability: number;
  public YearsExperience: number;
  public Reviews: SpaReviewSummaryModel[];
  public AssociatedFacilities: SpaFacilityForCardModel[];
  public Location: string;
}
