"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var spa_therapist_for_card_model_1 = require("./spa-therapist-for-card.model");
var SpaTherapistProfileForPublicModel = /** @class */ (function (_super) {
    __extends(SpaTherapistProfileForPublicModel, _super);
    function SpaTherapistProfileForPublicModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SpaTherapistProfileForPublicModel;
}(spa_therapist_for_card_model_1.SpaTherapistForCardModel));
exports.SpaTherapistProfileForPublicModel = SpaTherapistProfileForPublicModel;
//# sourceMappingURL=spa-therapist-profile-for-public.model.js.map