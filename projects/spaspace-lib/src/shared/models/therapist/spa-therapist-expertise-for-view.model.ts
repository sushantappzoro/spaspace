import { SpaExpertiseModel } from '../services/spa-expertise.model';

export class SpaTherapistExpertiseForViewModel extends SpaExpertiseModel {
  public SkillLevel: number;
}
