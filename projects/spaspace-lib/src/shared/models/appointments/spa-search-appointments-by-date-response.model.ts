export interface ISpaSearchAppointmentsByDateResponse {
  FacilityUID: string;
  FacilityName: string;
  TherapistUID: string;
  TherapistName: string;
  TherapistPhotoUrl: string;
  AverageRating: number;
  TotalRatings: number;
  TotalAppointments: number;
  TreatmentAreaUID: string;
  AvailableTimes: ISpaAvailableTimes[];
}

export class SpaSearchAppointmentsByDateResponseModel implements ISpaSearchAppointmentsByDateResponse {
  public FacilityUID: string;
  public FacilityName: string;
  public TherapistUID: string;
  public TherapistName: string;
  public TherapistPhotoUrl: string;
  public AverageRating: number;
  public TotalRatings: number;
  public TotalAppointments: number;  
  public TreatmentAreaUID: string;
  public AvailableTimes: SpaAvailableTimesModel[];
}

export interface ISpaAvailableTimes {
  TreatmentAreaUID: string;
  Time: Date;
}

export class SpaAvailableTimesModel implements ISpaAvailableTimes {
  public TreatmentAreaUID: string;
  public Time: Date;
}
