export interface ISpaSearchAppointmentsByDateRequest {
  Date: Date;
  FacilityUID: string;
  ServiceUID: string;
  FocusAreas: number[];
  Needs: number[];
  PreferredGender: string;
}

export class SpaSearchAppointmentsByDateRequestModel implements ISpaSearchAppointmentsByDateRequest {
  public Date: Date;
  public FacilityUID: string;
  public ServiceUID: string;
  public FocusAreas: number[];
  public Needs: number[];
  public PreferredGender: string;
}
