export interface ISpaAppointmentForBooking {
  CustomerUID: string;
  TherapistUID: string;
  TreatmentAreaUID: string;
  ServiceUID: string;
  NotesToTherapist: string;
  StartDateTime: Date;
  FocusAreas: number[];
  Needs: number[];
  PressureLevel: number;
  AddOnIDs: string[];
  HowTheyBooked: string;
  SuccessURL: string;
  CancelURL: string;
}

export class SpaAppointmentForBookingModel {
  public CustomerUID: string;
  public TherapistUID: string;
  public TreatmentAreaUID: string;
  public ServiceUID: string;
  public NotesToTherapist: string;
  public StartDateTime: Date;
  public FocusAreas: number[];
  public Needs: number[];
  public PressureLevel: number;
  public AddOnIDs: string[];
  public HowTheyBooked: string;
  public SuccessURL: string;
  public CancelURL: string;
}
