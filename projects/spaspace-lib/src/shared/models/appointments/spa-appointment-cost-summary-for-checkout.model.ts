export class SpaAppointmentCostSummaryForCheckoutModel {
  public Service: SpaServiceForCostSummaryModel;
  public ServiceCharge: SpaServiceChargeForCostSummaryModel;
  public FacilityCharge: SpaFacilityChargeForCostSummaryModel;
  public Gratuity: number;
  public Total: number;
}

export class SpaServiceForCostSummaryModel {
  public Description: string;
  public Amount: number;
  public AddOns: SpaAddOnForCostSummaryModel[];
}

export class SpaAddOnForCostSummaryModel {
  public Description: string;
  public Amount: number;
}

export class SpaServiceChargeForCostSummaryModel {
  public Description: string;
  public Amount: number;
}

export class SpaFacilityChargeForCostSummaryModel {
  public Description: string;
  public Amount: number;
}
