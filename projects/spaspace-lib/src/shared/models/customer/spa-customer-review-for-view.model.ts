export class SpaCustomerReviewForViewModel {
  public UID: string;
  public ReviewText: string;
  public Stars: number;
}
