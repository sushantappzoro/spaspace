import { SpaFacilityAmenityForBookingModel, SpaServiceFocusAreaForBookingModel, SpaServiceNeedForBookingModel } from '..';

export class SpaCustomerPreferencesModel {
  public Needs: SpaServiceNeedForBookingModel[];
  public FocusAreas: SpaServiceFocusAreaForBookingModel[];
  public PreferredAmenities: SpaFacilityAmenityForBookingModel[];
  public PressureLevel: number;
  public TherapistGender: string;
}
