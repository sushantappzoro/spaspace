export class SpaCustomerReviewForAddModel {
  public AppointmentUID: string;
  public ReviewText: string;
  public Stars: number;
}
