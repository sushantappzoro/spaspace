export interface ISpaMassageTypeForCard {
  ID: number;
  Name: string;
  Description: string;
  IconURL: string;
}

export class SpaMassageTypeForCardModel implements ISpaMassageTypeForCard {
  public ID: number;
  public Name: string;
  public Description: string;
  public IconURL: string;
}
