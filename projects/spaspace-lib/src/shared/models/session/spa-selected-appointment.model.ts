export interface ISpaSelectedAppointment {
  FacilityUID: string;
  FacilityName: string;
  TherapistUID: string;
  TherapistName: string;
  SelectedDateTime: Date;
  TreatmentAreaUID: string;
}

export class SpaSelectedAppointmentModel implements ISpaSelectedAppointment {
  public FacilityUID: string;
  public FacilityName: string;
  public TherapistUID: string;
  public TherapistName: string;
  public SelectedDateTime: Date;
  public TreatmentAreaUID: string;
}
