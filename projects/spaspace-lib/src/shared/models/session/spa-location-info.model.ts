export interface ISpaLocationInfo {
  City?: string;
  State?: string;
  Latitude?: number;
  Longitude?: number;
  County?: string;
  ZipCode?: string;
}

export class SpaLocationInfoModel implements ISpaLocationInfo {
  public City?: string;
  public State?: string;
  public Latitude?: number;
  public Longitude?: number;
  public County?: string;
  public ZipCode?: string;
}
