export class ISpaSelectedFacility {
  Name: string;
  UID: string;
  City: string;
  StateCode: string;
}

export class SpaSelectedFacilityModel implements ISpaSelectedFacility {
  public Name: string;
  public UID: string;
  public City: string;
  public StateCode: string;
}
