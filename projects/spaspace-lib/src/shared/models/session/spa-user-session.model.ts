import { ISpaLocationInfo, SpaLocationInfoModel } from './spa-location-info.model';
import { ISpaSearchCriteria, SpaSearchCriteriaModel } from './spa-search-criteria.model';
import { IMassageNeedForBooking, MassageNeedForBookingModel } from '../book/massage-need-for-booking.model';
import { IMassageFocusAreaForBooking, MassageFocusAreaForBookingModel } from '../book/massage-focus-area-for-booking.model';
import { ISpaSelectedService, SpaSelectedServiceModel } from './spa-selected-service.model';
import { ISpaFacilityAmenityForBooking, SpaFacilityAmenityForBookingModel } from '../book/spa-facility-amenity-for-booking.model';
import { ISpaSelectedFacility, SpaSelectedFacilityModel } from './spa-selected-facility.model';
import { SpaSelectedAppointmentModel } from './spa-selected-appointment.model';
import { SpaServiceAddonForPublicViewModel } from '../services/spa-service-addon-for-public-view.model';
import { SpaAppointmentForCustomerViewModel } from '../appointments/spa-appointment-for-customer-view.model';

export interface ISpaUserSession {
  BlocksCurrentLocation: boolean;
  CurrentLocation?: ISpaLocationInfo;
  CustomerUID?: string;
  FacilityAmenities?: ISpaFacilityAmenityForBooking[];
  FocusAreas?: IMassageFocusAreaForBooking[];
  GenderPreference?: string;
  HasCurrentLocation: boolean;
  HasSearchLocation: boolean;
  LoginReturnURL: string;
  Needs?: IMassageNeedForBooking[];
  PressurePreference?: number;
  SearchCriteria?: ISpaSearchCriteria;
  SearchDate?: Date;
  SearchLocation?: ISpaLocationInfo;
  SelectedService?: ISpaSelectedService;
  SelectedServiceCategory?: string;
  SelectedFacility: ISpaSelectedFacility;
  NotesToTherapist: string;
  CheckoutSessionID: string;
  PaymentMethodDescription: string;
  AppointmentUID: string;
  AppointmentForCustomerView: SpaAppointmentForCustomerViewModel;
}

export class SpaUserSessionModel implements ISpaUserSession  {
  public BlocksCurrentLocation: boolean;
  public CurrentLocation?: SpaLocationInfoModel;
  public CustomerUID?: string;
  public FacilityAmenities?: SpaFacilityAmenityForBookingModel[];
  public FocusAreas?: MassageFocusAreaForBookingModel[];
  public GenderPreference?: string;
  public HasCurrentLocation: boolean;
  public HasSearchLocation: boolean;
  public LoginReturnURL: string;
  public Needs?: MassageNeedForBookingModel[];
  public Addons: SpaServiceAddonForPublicViewModel[];
  public PressurePreference?: number;
  public SearchCriteria?: SpaSearchCriteriaModel;
  public SearchDate?: Date;
  public SearchLocation?: SpaLocationInfoModel;
  public SelectedAppointment: SpaSelectedAppointmentModel;
  public SelectedService?: SpaSelectedServiceModel;
  public SelectedFacility: SpaSelectedFacilityModel;
  public NotesToTherapist: string;
  public CheckoutSessionID: string;
  public PaymentMethodDescription: string;
  public AppointmentUID: string;
  public AppointmentForCustomerView: SpaAppointmentForCustomerViewModel;
}
