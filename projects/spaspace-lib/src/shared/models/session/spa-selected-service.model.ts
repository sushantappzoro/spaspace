export interface ISpaSelectedService {
  ServiceCategory: string;
  ServiceCategoryID: number;
  Service: string;
  ServiceUID: string;
  ServicePrice: number;
  ServiceDisplayPrice: string;
}

export class SpaSelectedServiceModel implements ISpaSelectedService{
  public ServiceCategory: string;
  public ServiceCategoryID: number;
  public Service: string;
  public ServiceUID: string;
  public ServicePrice: number;
  public ServiceDisplayPrice: string;
}
