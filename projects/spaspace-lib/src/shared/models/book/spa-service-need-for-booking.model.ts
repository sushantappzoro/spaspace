export interface ISpaServiceNeedForBooking {
  ID: string;
  Name: string;
  IsChecked: boolean;
}

export class SpaServiceNeedForBookingModel implements ISpaServiceNeedForBooking {
  public ID: string;
  public Name: string;
  public IsChecked: boolean;
}
