export interface IMassageFocusAreaForBooking {
  ID: string;
  Name: string;
  IsChecked: boolean;
}

export class MassageFocusAreaForBookingModel implements IMassageFocusAreaForBooking {
  public ID: string;
  public Name: string;
  public IsChecked: boolean;
}
