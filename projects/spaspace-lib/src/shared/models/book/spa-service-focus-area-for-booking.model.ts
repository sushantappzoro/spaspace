
export interface ISpaServiceFocusAreaForBooking {
  ID: string;
  Name: string;
  IsChecked: boolean;
}

export class SpaServiceFocusAreaForBookingModel implements ISpaServiceFocusAreaForBooking {
  public ID: string;
  public Name: string;
  public IsChecked: boolean;
}
