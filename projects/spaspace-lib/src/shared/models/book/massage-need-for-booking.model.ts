export interface IMassageNeedForBooking {
  ID: string;
  Name: string;
  IsChecked: boolean;
}

export class MassageNeedForBookingModel implements IMassageNeedForBooking {
  public ID: string;
  public Name: string;
  public IsChecked: boolean;
}
