export interface ISpaSearchForFacilitiesRequest {
  Latitude: number;
  Longitude: number;
  City: string;
  StateCode: string;
  County: string;
  ZipCode: string;
  AmenityIDs: number[];
  ServiceUID: string;
  DesiredDate: Date;
}

export class SpaSearchForFacilitiesRequestModel implements ISpaSearchForFacilitiesRequest {
  public Latitude: number;
  public Longitude: number;
  public City: string;
  public StateCode: string;
  public County: string;
  public ZipCode: string;
  public AmenityIDs: number[];
  public ServiceUID: string;
  public DesiredDate: Date;
}
