export interface ISpaFacilityAmenityForBooking {
  ID: number;
  Name: string;
  Icon: string;
  IsChecked: boolean;
}


export class SpaFacilityAmenityForBookingModel implements ISpaFacilityAmenityForBooking {
  public ID: number;
  public Name: string;
  public Icon: string;
  public IsChecked: boolean;
}
