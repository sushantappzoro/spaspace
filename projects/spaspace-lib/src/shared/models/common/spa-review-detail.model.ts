export interface ISpaReviewDetail {
  UID: string;
  CustomerName: string;
  CustomerPhotoURL: string;
  AppointmentUID: string;
  AppointmentDate: Date;
  Rating: number;    
  ReviewDate: Date;
  ReviewText: string;
}

export class SpaReviewDetailModel implements ISpaReviewDetail {
  public UID: string;
  public CustomerName: string;
  public CustomerPhotoURL: string;
  public AppointmentUID: string;
  public AppointmentDate: Date;
  public Rating: number;
  public ReviewDate: Date;
  public ReviewText: string;
}
