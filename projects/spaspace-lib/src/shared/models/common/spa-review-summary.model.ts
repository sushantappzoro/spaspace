export interface ISpaReviewSummary {
  Rating: number;
  Customer: string;
  ReviewDate: Date;
  ReviewText: string;
}

export class SpaReviewSummaryModel implements ISpaReviewSummary {
  public Rating: number;
  public Customer: string;
  public ReviewDate: Date;
  public ReviewText: string;
}
