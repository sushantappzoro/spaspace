import { ISpaReviewDetail } from './spa-review-detail.model';

export interface ISpaReviews {
  TotalReviews: number;
  AverageRating?: number;
  Reviews?: ISpaReviewDetail[];
}

export class SpaReviewsModel implements ISpaReviews {
  public TotalReviews: number;
  public AverageRating?: number;
  public Reviews?: ISpaReviewDetail[];
}
