export class SpaErrorModel {
  public detail?: string;
  public status?: number;
  public title?: string;
  public traceId?: string;
  public type?: string;
}
