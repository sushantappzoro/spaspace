import { SpaServiceForPublicViewModel } from "./spa-service-for-public-view.model";
import { SpaServiceAddonForPublicViewModel } from './spa-service-addon-for-public-view.model';

export class SpaServiceCategoryForPublicViewModel {
  public ID: number;
  public Name: string;
  public Services: SpaServiceForPublicViewModel[];
  public IsChecked: boolean;
  public AddOns: SpaServiceAddonForPublicViewModel[];
}
