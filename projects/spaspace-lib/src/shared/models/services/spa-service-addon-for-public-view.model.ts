export class SpaServiceAddonForPublicViewModel {
  public ID: string;
  public Name: string;
  public Price: number;
  public Category: string;
  public Description: string;
  public IsChecked: boolean;
}
