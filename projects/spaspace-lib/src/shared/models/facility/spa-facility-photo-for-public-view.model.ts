export interface ISpaFacilityPhotoForPublicView {
  URL: string;
  Description: string;
  IsMain: boolean;
}

export class SpaFacilityPhotoForPublicViewModel implements ISpaFacilityPhotoForPublicView {
  public URL: string;
  public Description: string;
  public IsMain: boolean;
}
