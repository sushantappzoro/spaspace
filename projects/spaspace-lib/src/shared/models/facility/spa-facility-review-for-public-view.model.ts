export interface ISpaFacilityReviewForPublicView {
  Customer: string;
  ReviewDate: Date;
  Rating: number;
  ReviewText: string;
}

export class SpaFacilityReviewForPublicViewModel implements ISpaFacilityReviewForPublicView {
  public Customer: string;
  public ReviewDate: Date;
  public Rating: number;
  public ReviewText: string;
}
