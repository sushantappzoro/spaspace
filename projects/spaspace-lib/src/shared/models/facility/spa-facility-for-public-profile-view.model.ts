import { ISpaFacilityPhotoForPublicView, SpaFacilityPhotoForPublicViewModel } from './spa-facility-photo-for-public-view.model';
import { ISpaFacilityAmenityForCard, SpaFacilityAmenityForCardModel } from './spa-facility-amenity-for-card.model';
import { ISpaFacilityReviewForPublicView, SpaFacilityReviewForPublicViewModel } from './spa-facility-review-for-public-view.model';

export interface ISpaFacilityForPublicProfileView {
  UID: string;
  Name: string;
  City: string;
  StateCode: string;
  AverageRating: number;
  TotalRatings: number;
  TotaAppointments: number;
  Description: string;
  Photos: ISpaFacilityPhotoForPublicView[];
  Amenities: ISpaFacilityAmenityForCard[];
  Reviews: ISpaFacilityReviewForPublicView[];
}

export class SpaFacilityForPublicProfileViewModel implements ISpaFacilityForPublicProfileView{
  public UID: string;
  public Name: string;
  public City: string;
  public StateCode: string;
  public AverageRating: number;
  public TotalRatings: number;
  public TotaAppointments: number;
  public Description: string;
  public Photos: SpaFacilityPhotoForPublicViewModel[];
  public Amenities: SpaFacilityAmenityForCardModel[];
  public Reviews: SpaFacilityReviewForPublicViewModel[];
}
