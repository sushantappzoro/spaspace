import { SpaFacilityAmenityForCardModel, ISpaFacilityAmenityForCard } from './spa-facility-amenity-for-card.model';

export interface ISpaFacilityForCard {
  UID: string;
  Name: string;
  City: string;
  StateCode: string;
  ShortDescription: string;
  AverageRating: string;
  TotalRatings?: number;
  TotalAppointments?: number;
  MainPhotoUrl: string;
  Amenities: ISpaFacilityAmenityForCard[];
  AvailableAppointments?: number;
}

export class SpaFacilityForCardModel implements ISpaFacilityForCard {
  public UID: string;
  public Name: string;
  public City: string;
  public StateCode: string;
  public ShortDescription: string;
  public AverageRating: string;
  public TotalRatings?: number;
  public TotalAppointments?: number;
  public MainPhotoUrl: string;
  public Amenities: SpaFacilityAmenityForCardModel[];
  public AvailableAppointments?: number;
}
