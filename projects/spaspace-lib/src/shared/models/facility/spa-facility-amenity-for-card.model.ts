export interface ISpaFacilityAmenityForCard {
  Name: string;
  Icon: string;
}

export class SpaFacilityAmenityForCardModel implements ISpaFacilityAmenityForCard {
  public Name: string;
  public Icon: string;
}
