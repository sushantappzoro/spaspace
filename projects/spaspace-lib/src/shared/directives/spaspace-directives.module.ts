import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaLocationLookupDirective } from './spa-location-lookup.directive';
import { SpaPressurePreferenceDirective } from './spa-pressure-preference.directive';



@NgModule({
  declarations: [
    SpaLocationLookupDirective,
    SpaPressurePreferenceDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SpaLocationLookupDirective
  ]
})
export class SpaSpaceDirectivesModule { }
