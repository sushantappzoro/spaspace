/// <reference types="@types/googlemaps" />
import { Directive, ElementRef, EventEmitter, OnInit, Output } from '@angular/core';


@Directive({
  selector: '[spaLocationLookup]'
})
export class SpaLocationLookupDirective implements OnInit {
  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  private element: HTMLInputElement;

  constructor(el: ElementRef) {
    console.log('spaLocationLookup', el);
    this.element = el.nativeElement;
    el.nativeElement.innerText="Text is changed by changeText Directive. ";
  }

  ngOnInit() {
    const autocomplete = new google.maps.places.Autocomplete(this.element, {
      types: ['(regions)'],
      componentRestrictions: {country: 'us'}
    });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      console.log('autocomplete', autocomplete);
      const place = autocomplete.getPlace();
      this.onSelect.emit(place);
    });
  }
}
