import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaMassageTypesComponent } from './home/massage-types/spa-massage-types.component';
import { SpaMassageTypeCardComponent } from './home/massage-types/spa-massage-type-card/spa-massage-type-card.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SpaFacilitySearchResultsCardComponent } from './book/spa-facility-search-results-card/spa-facility-search-results-card.component';
import { SpaFacilityRatingsBlockComponent } from './facility/spa-facility-ratings-block/spa-facility-ratings-block.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SpaFacilityProfileCardComponent } from './facility/spa-facility-profile-card/spa-facility-profile-card.component';
import { SpaspaceSharedMaterialModuleModule } from '../spaspace-shared-material-module.module';
import { SpaAppointmentSearchResultsCardComponent } from './book/spa-appointment-search-results-card/spa-appointment-search-results-card.component';
//import { SpaTherapistProfileCardComponent } from './therapist/spa-therapist-profile-card/spa-therapist-profile-card.component';
//import { SpaAppointmentPreferencesComponent } from './appointment/spa-appointment-preferences/spa-appointment-preferences.component';
import { SpaTherapistProfileDetailComponent } from './therapist/spa-therapist-profile-detail/spa-therapist-profile-detail.component';
import { SpaTherapistProfileSummaryCardComponent } from './therapist/spa-therapist-profile-summary-card/spa-therapist-profile-summary-card.component';
import { SpaFacilityCardComponent } from './facility/spa-facility-card/spa-facility-card.component';
import { SpaTherapistProfileVerticalCardComponent } from './therapist/spa-therapist-profile-vertical-card/spa-therapist-profile-vertical-card.component';
import { SpaRatingsQuickViewComponent } from './common/spa-ratings-quick-view/spa-ratings-quick-view.component';
import { SpaAppointmentCostSummaryComponent } from './appointment/spa-appointment-cost-summary/spa-appointment-cost-summary.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  declarations: [
    SpaMassageTypesComponent,
    SpaMassageTypeCardComponent,
    SpaFacilitySearchResultsCardComponent,
    SpaFacilityRatingsBlockComponent,
    SpaFacilityProfileCardComponent,
    SpaAppointmentSearchResultsCardComponent,
    SpaTherapistProfileDetailComponent,
    SpaTherapistProfileSummaryCardComponent,
    SpaFacilityCardComponent,
    SpaTherapistProfileVerticalCardComponent,
    SpaRatingsQuickViewComponent,
    SpaAppointmentCostSummaryComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MDBBootstrapModulesPro,
    NgbModule,
    SwiperModule,
    SpaspaceSharedMaterialModuleModule,
    NgxCurrencyModule
  ],
  exports: [
    SpaMassageTypesComponent,
    SpaMassageTypeCardComponent,
    SpaFacilitySearchResultsCardComponent,
    SpaFacilityRatingsBlockComponent,
    SpaFacilityProfileCardComponent,
    SpaAppointmentSearchResultsCardComponent,
    SpaTherapistProfileDetailComponent,
    SpaTherapistProfileSummaryCardComponent,
    SpaFacilityCardComponent,
    SpaTherapistProfileVerticalCardComponent,
    SpaRatingsQuickViewComponent,
    SpaAppointmentCostSummaryComponent
  ]
})
export class SpaSpaceComponentsModule { }
