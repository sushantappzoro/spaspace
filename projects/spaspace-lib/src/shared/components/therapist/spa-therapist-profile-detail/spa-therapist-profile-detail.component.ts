import { Component, OnInit, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { SubSink } from 'subsink';

import { SpaTherapistProfileForPublicModel } from '../../../models';
import { SpaTherapistPublicProfileStore } from '../../../../core/stores/public/therapist/spa-therapist-public-profile.store';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'spa-therapist-profile-detail',
  templateUrl: './spa-therapist-profile-detail.component.html',
  styleUrls: ['./spa-therapist-profile-detail.component.scss']
})
export class SpaTherapistProfileDetailComponent implements OnInit, OnChanges, OnDestroy {
  @Input() therapistUID: string;

  private subs = new SubSink();

  therapistProfile: SpaTherapistProfileForPublicModel;

  constructor(
    private route: ActivatedRoute,
    private profileService: SpaTherapistPublicProfileStore
  ) { }

  ngOnInit() {

    this.therapistProfile = this.route.snapshot.data['therapistProfile'];
    console.log('therapistProfile', this.therapistProfile);

  }

  ngOnChanges(changes: SimpleChanges) {
    //if (changes && changes.therapistUID) {      
    //  this.subs.add(this.profileService.getTherapistProfile(this.therapistUID)
    //    .subscribe(profile => {
    //      this.therapistProfile = profile;
    //    })
    //  )
    //}
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  facilityClick() {

  }

  onBookNext() {

  }


}
