import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaTherapistProfileDetailComponent } from './spa-therapist-profile-detail.component';

describe('SpaTherapistProfileDetailComponent', () => {
  let component: SpaTherapistProfileDetailComponent;
  let fixture: ComponentFixture<SpaTherapistProfileDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaTherapistProfileDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaTherapistProfileDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
