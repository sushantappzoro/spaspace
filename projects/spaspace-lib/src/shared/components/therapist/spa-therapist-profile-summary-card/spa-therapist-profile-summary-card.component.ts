import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpaTherapistForCardModel } from '../../../models';

@Component({
  selector: 'spa-therapist-profile-summary-card',
  templateUrl: './spa-therapist-profile-summary-card.component.html',
  styleUrls: ['./spa-therapist-profile-summary-card.component.scss']
})
export class SpaTherapistProfileSummaryCardComponent implements OnInit {
  @Input() therapist: SpaTherapistForCardModel;
  @Output() click: EventEmitter<any> = new EventEmitter<boolean>();

  currentRate = 4;
  max = 5;

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit() {

  }

  getTherapistImage() {
    if (this.therapist && this.therapist.MainPhotoUrl && this.therapist.MainPhotoUrl.length > 0) {
      return this.therapist.MainPhotoUrl;
    } else {
      return '/spa/assets/images/placeholder-person.jpg';
    }
  }

  onClick() {
    this.click.emit(this.therapist.UID);
  }

}
