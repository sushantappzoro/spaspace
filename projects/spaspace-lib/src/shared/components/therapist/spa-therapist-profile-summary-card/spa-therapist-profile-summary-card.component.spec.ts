import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaTherapistProfileSummaryCardComponent } from './spa-therapist-profile-summary-card.component';

describe('SpaTherapistProfileSummaryCardComponent', () => {
  let component: SpaTherapistProfileSummaryCardComponent;
  let fixture: ComponentFixture<SpaTherapistProfileSummaryCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaTherapistProfileSummaryCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaTherapistProfileSummaryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
