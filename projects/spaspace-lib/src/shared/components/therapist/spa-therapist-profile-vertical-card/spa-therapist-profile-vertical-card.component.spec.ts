import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaTherapistProfileVerticalCardComponent } from './spa-therapist-profile-vertical-card.component';

describe('SpaTherapistProfileVerticalCardComponent', () => {
  let component: SpaTherapistProfileVerticalCardComponent;
  let fixture: ComponentFixture<SpaTherapistProfileVerticalCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaTherapistProfileVerticalCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaTherapistProfileVerticalCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
