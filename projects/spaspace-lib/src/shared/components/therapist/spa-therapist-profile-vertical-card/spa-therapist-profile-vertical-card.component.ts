import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpaTherapistProfileForPublicModel } from '../../../models';

@Component({
  selector: 'spa-therapist-profile-vertical-card',
  templateUrl: './spa-therapist-profile-vertical-card.component.html',
  styleUrls: ['./spa-therapist-profile-vertical-card.component.scss']
})
export class SpaTherapistProfileVerticalCardComponent implements OnInit {
  @Input() therapist: SpaTherapistProfileForPublicModel;
  @Output() click: EventEmitter<any> = new EventEmitter<boolean>();

  currentRate = 4;
  max = 5;

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    console.log('SpaTherapistProfileVerticalCardComponent', this.therapist);
  }

  getTherapistImage() {
    if (this.therapist && this.therapist.MainPhotoUrl && this.therapist.MainPhotoUrl.length > 0) {
      return this.therapist.MainPhotoUrl;
    } else {
      return '/spa/assets/images/placeholder-person.jpg';
    }
  }

  onClick() {
    this.click.emit(this.therapist.UID);
  }

}
