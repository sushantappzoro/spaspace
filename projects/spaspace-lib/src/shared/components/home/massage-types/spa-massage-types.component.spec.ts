import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaMassageTypesComponent } from './spa-massage-types.component';

describe('SpaMassageTypesComponent', () => {
  let component: SpaMassageTypesComponent;
  let fixture: ComponentFixture<SpaMassageTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaMassageTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaMassageTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
