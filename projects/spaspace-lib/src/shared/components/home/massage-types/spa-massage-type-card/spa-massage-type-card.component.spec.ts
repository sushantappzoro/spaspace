import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaMassageTypeCardComponent } from './spa-massage-type-card.component';

describe('SpaMassageTypeCardComponent', () => {
  let component: SpaMassageTypeCardComponent;
  let fixture: ComponentFixture<SpaMassageTypeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaMassageTypeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaMassageTypeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
