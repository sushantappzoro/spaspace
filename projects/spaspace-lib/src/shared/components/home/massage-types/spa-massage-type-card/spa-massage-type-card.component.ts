import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SpaMassageTypeForCardModel } from '../../../../models';

@Component({
  selector: 'spa-massage-type-card',
  templateUrl: './spa-massage-type-card.component.html',
  styleUrls: ['./spa-massage-type-card.component.scss']
})
export class SpaMassageTypeCardComponent implements OnInit {
  @Input() massageTypeForCard: SpaMassageTypeForCardModel;

  @Output() click: EventEmitter<any> = new EventEmitter<string>();
  @Output() loaded: EventEmitter<any> = new EventEmitter<boolean>();

  currentRate = 4;
  max = 5;

  constructor(
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.loaded.emit(true);
  }

  onClick() {
    this.click.emit(this.massageTypeForCard.ID);
  }

  getMassageImage() {
    return this.massageTypeForCard.IconURL;
  }

}
