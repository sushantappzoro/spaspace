import { Component, OnInit, Input } from '@angular/core';
import { SpaMassageTypeForCardModel } from '../../../models';

@Component({
  selector: 'spa-massage-types',
  templateUrl: './spa-massage-types.component.html',
  styleUrls: ['./spa-massage-types.component.scss']
})
export class SpaMassageTypesComponent implements OnInit {
  @Input() massageTypes: SpaMassageTypeForCardModel[];

  constructor() {
    console.log('SpaMassageTypesComponent - constructor');
  }

  ngOnInit() {
  }

  click() {

  }
}
