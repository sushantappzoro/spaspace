import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpaFacilityForCardModel } from '../../../models';

@Component({
  selector: 'spa-facility-search-results-card',
  templateUrl: './spa-facility-search-results-card.component.html',
  styleUrls: ['./spa-facility-search-results-card.component.scss']
})
export class SpaFacilitySearchResultsCardComponent implements OnInit {
  @Input() facility: SpaFacilityForCardModel;
  @Output() selected: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  getFacilityImage() {
    if (this.facility && this.facility.MainPhotoUrl && this.facility.MainPhotoUrl.length > 0) {
      return this.facility.MainPhotoUrl;
    } else {
      return '/spa/assets/images/image_placeholder.png'
    }
  }

  click(e) {
    if (this.hasAppointments()) {
      this.selected.emit({'event': e, 'facility': this.facility.UID});
    } else {
      this.selected.emit({'event': e, 'facility': 'noappts'});
    }

  }

  hasAppointmentsText() {
    if (this.hasAppointments()) {
      return this.facility.AvailableAppointments.toString() + ' appointments available'
    } else {
      return 'No available appointments'
    }
  }

  hasAppointments() {
    if (this.facility && this.facility.AvailableAppointments && this.facility.AvailableAppointments > 0) {
      return true
    } else {
      return false
    }
  }
}
