import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaFacilitySearchResultsCardComponent } from './spa-facility-search-results-card.component';

describe('SpaFacilitySearchResultsCardComponent', () => {
  let component: SpaFacilitySearchResultsCardComponent;
  let fixture: ComponentFixture<SpaFacilitySearchResultsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaFacilitySearchResultsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaFacilitySearchResultsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
