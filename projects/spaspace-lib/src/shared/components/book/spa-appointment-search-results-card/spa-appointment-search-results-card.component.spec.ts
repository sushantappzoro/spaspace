import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaAppointmentSearchResultsCardComponent } from './spa-appointment-search-results-card.component';

describe('SpaAppointmentSearchResultsCardComponent', () => {
  let component: SpaAppointmentSearchResultsCardComponent;
  let fixture: ComponentFixture<SpaAppointmentSearchResultsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaAppointmentSearchResultsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaAppointmentSearchResultsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
