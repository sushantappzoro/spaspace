import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpaSearchAppointmentsByDateResponseModel } from '../../../models';

@Component({
  selector: 'spa-appointment-search-results-card',
  templateUrl: './spa-appointment-search-results-card.component.html',
  styleUrls: ['./spa-appointment-search-results-card.component.scss']
})
export class SpaAppointmentSearchResultsCardComponent implements OnInit {
  @Input() appointment: SpaSearchAppointmentsByDateResponseModel;
  @Output() selected: EventEmitter<SpaSearchAppointmentsByDateResponseModel> = new EventEmitter<SpaSearchAppointmentsByDateResponseModel>();
  @Output() viewTherapist: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  getImage() {
    if (this.appointment && this.appointment.TherapistPhotoUrl && this.appointment.TherapistPhotoUrl.length > 0) {
      return this.appointment.TherapistPhotoUrl;
    } else {
      return '/spa/assets/images/image_placeholder.png'
    }
  }

  timeClicked(time) {
    var selected: SpaSearchAppointmentsByDateResponseModel = { ... this.appointment };
    selected.AvailableTimes = [];
    selected.AvailableTimes.push(time);
    this.selected.emit(selected);
  }

  clickTherapist() {
    //console.log('clicktherapist');
    //this.viewTherapist.emit(this.appointment.TherapistUID);
  }
}
