import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaFacilityCardComponent } from './spa-facility-card.component';

describe('SpaFacilityCardComponent', () => {
  let component: SpaFacilityCardComponent;
  let fixture: ComponentFixture<SpaFacilityCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaFacilityCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaFacilityCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
