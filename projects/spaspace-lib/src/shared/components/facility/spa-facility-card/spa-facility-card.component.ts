import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { SpaFacilityForCardModel } from '../../../models/facility/spa-facility-for-card.model';


@Component({
  selector: 'spa-facility-card',
  templateUrl: './spa-facility-card.component.html',
  styleUrls: ['./spa-facility-card.component.scss']
})
export class SpaFacilityCardComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() facilityForCard: SpaFacilityForCardModel;
  @Input() showFooter: boolean = true;


  @Output() selected: EventEmitter<any> = new EventEmitter<string>();
  @Output() loaded: EventEmitter<any> = new EventEmitter<boolean>();

  currentRate = 4;
  max = 5;

  constructor(
  ) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {

  }

  ngAfterViewInit() {
    this.loaded.emit(true);
  }

  onClick() {
    this.selected.emit(this.facilityForCard.UID);
  }

  getFacilityImage() {
    if (this.facilityForCard && this.facilityForCard.MainPhotoUrl && this.facilityForCard.MainPhotoUrl.length > 0) {
      return this.facilityForCard.MainPhotoUrl;
    } else {
      return '/spa/assets/images/image_placeholder.png';
    }
  }

}
