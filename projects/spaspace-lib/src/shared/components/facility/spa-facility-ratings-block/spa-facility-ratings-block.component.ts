import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'spa-facility-ratings-block',
  templateUrl: './spa-facility-ratings-block.component.html',
  styleUrls: ['./spa-facility-ratings-block.component.scss']
})
export class SpaFacilityRatingsBlockComponent implements OnInit {
  @Input() averageRating: number;
  @Input() totalRatings: number;
  @Input() totalAppointments: number;

  max = 5;

  constructor() { }

  ngOnInit() {
  }

}
