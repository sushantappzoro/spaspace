import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaFacilityRatingsBlockComponent } from './spa-facility-ratings-block.component';

describe('SpaFacilityRatingsBlockComponent', () => {
  let component: SpaFacilityRatingsBlockComponent;
  let fixture: ComponentFixture<SpaFacilityRatingsBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaFacilityRatingsBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaFacilityRatingsBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
