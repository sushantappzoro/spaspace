import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaFacilityProfileCardComponent } from './spa-facility-profile-card.component';

describe('SpaFacilityProfileCardComponent', () => {
  let component: SpaFacilityProfileCardComponent;
  let fixture: ComponentFixture<SpaFacilityProfileCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaFacilityProfileCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaFacilityProfileCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
