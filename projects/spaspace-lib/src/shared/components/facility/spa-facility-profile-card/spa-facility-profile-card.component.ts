import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SpaFacilityForPublicProfileViewModel } from '../../../models';

@Component({
  selector: 'spa-spa-facility-profile-card',
  templateUrl: './spa-facility-profile-card.component.html',
  styleUrls: ['./spa-facility-profile-card.component.scss']
})
export class SpaFacilityProfileCardComponent implements OnInit {  

  facilityProfile: SpaFacilityForPublicProfileViewModel

  constructor() { }

  ngOnInit() {
  }

}
