import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { SubSink } from 'subsink';

import { SpaAppointmentCostSummaryForCheckoutModel } from '../../../models';

@Component({
  selector: 'spa-appointment-cost-summary',
  templateUrl: './spa-appointment-cost-summary.component.html',
  styleUrls: ['./spa-appointment-cost-summary.component.scss']
})
export class SpaAppointmentCostSummaryComponent implements OnInit, OnDestroy {
  @Input() costSummary: SpaAppointmentCostSummaryForCheckoutModel;
  @Input() appointmentDateTime: Date;

  private subs = new SubSink();



  constructor(
   
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }



}
