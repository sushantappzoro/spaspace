import { Component, OnInit, Input } from '@angular/core';
import { SpaReviewSummaryModel } from '../../../models';

@Component({
  selector: 'spa-ratings-quick-view',
  templateUrl: './spa-ratings-quick-view.component.html',
  styleUrls: ['./spa-ratings-quick-view.component.scss']
})
export class SpaRatingsQuickViewComponent implements OnInit {
  @Input() reviews: SpaReviewSummaryModel[];

  max = 5;

  constructor() { }

  ngOnInit() {
    console.log('reviews=', this.reviews)
  }

}
