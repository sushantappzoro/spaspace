import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaRatingsQuickViewComponent } from './spa-ratings-quick-view.component';

describe('SpaRatingsQuickViewComponent', () => {
  let component: SpaRatingsQuickViewComponent;
  let fixture: ComponentFixture<SpaRatingsQuickViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaRatingsQuickViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaRatingsQuickViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
