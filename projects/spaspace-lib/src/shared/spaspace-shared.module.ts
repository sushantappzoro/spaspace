import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaSpaceComponentsModule } from './components/spaspace-components.module';
import { SpaSpaceDirectivesModule } from './directives/spaspace-directives.module';
import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MDBBootstrapModulesPro,
    SpaSpaceComponentsModule,
    SpaSpaceDirectivesModule
  ],
  exports: [
    SpaSpaceComponentsModule,
    SpaSpaceDirectivesModule
  ]
})
export class SpaSpaceSharedModule { }
